<?php
/*include ("<?php echo base_url();?>includes/classes/class.db");
include ("<?php echo base_url();?>includes/functions/general");*/

//-------------------------------------------------------------------------------------------
								/*Objects*/
//-------------------------------------------------------------------------------------------					
	/*$db = new db();*/
//-------------------------------------------------------------------------------------------
								/*General Coding Area*/
	/*
	if (isset($_GET['id']) && isset($_GET['mode']) && isset($_GET['record']) && $_GET['id'] != "" && $_GET['mode'] == "delete" && $_GET['record'] == "rec") {
		$sqlDelete = "DELETE FROM recurring_tasks WHERE task_id = '".$_GET['id']."'";
		$db->query($sqlDelete);
		redirect("?");
	}
		
	$sql = "SELECT task_id, subject, property, frequency, next_date, duration
			FROM recurring_tasks
			ORDER BY task_id DESC";
	
	$query= $db->ExeQuersys($sql);
	
	while ($row = mysql_fetch_array($query))
	{
		$valTaskID = $row['task_id'];
		$valSubject = $row['subject'];
		$valProperty = $row['property'];
		$valFrequency = $row['frequency'];
		$valNextDate = formatDate($row['next_date']);
		$valDuration = $row['duration'];
		
		$trRecurring .= '<tr>
				  <td>'.$valSubject.'</td>
				  <td>'.$valProperty.'</td>
				  <td>'.$valFrequency.'</td>
				  <td>'.$valNextDate.'</td>
				  <td>'.$valDuration.'</td>
				  <td align="center">
				  <a href="javascript:void();" title="Click here to edit" onclick="window.open(\'update_task?id='.$valTaskID.'\',\'mywindow\',\'menubar=1,resizable=1, scrollbars=1, width=750,height=650\');" >
				  <img src="<?php echo base_url();?>images/icon_edit.png" width="16" height="16" alt="Edit" border="0" /></a> &nbsp;&nbsp;|&nbsp;&nbsp;
				  <a href="?id='.$valTaskID.'&mode=delete&record=rec" title="Click here to delete" onclick="return confirm(\'Do you want to delete this record?\');">
				  <img src="<?php echo base_url();?>images/icon_delete.png" width="16" height="16" alt="Delete" border="0" />
				  </a>
				  </td>
				</tr>';
	}
	
*/?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Recurring Tasks</title>
<link href="<?php echo base_url();?>css/styles.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url();?>js/SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<link href="<?php echo base_url();?>css/SpryAssets/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="top_menu">
  <img src="<?php echo base_url();?>images/logo.png" width="126" height="67" alt="Logo" style="float:left; padding-right:10px;" />
  <h1>Real Estate Shark</h1>
</div>
<div id="wrapper_header">
  <ul id="MenuBar1" class="MenuBarHorizontal">
    <li><a class="MenuBarItemSubmenu" href="#">Rentals</a>
      <ul>
        <li><a href="<?php echo base_url();?>rentals/index">Properties</a></li>
        <li><a href="<?php echo base_url();?>rentals/leases">Leases</a></li>
        <li><a href="<?php echo base_url();?>rentals/tenants">Tenants</a></li>
        <li><a href="<?php echo base_url();?>rentals/listings">Listings</a></li>
        <li><a href="<?php echo base_url();?>rentals/rental_applications">Rental applications</a></li>
        <li><a href="<?php echo base_url();?>rentals/rental_owners">Rental owners</a></li>
        <li><a href="<?php echo base_url();?>rentals/outstanding_balances">Outstanding balances</a></li>
      </ul>
    </li>
    <li><a href="#" class="MenuBarItemSubmenu">Associations</a>
      <ul>
        <li><a href="<?php echo base_url();?>associations/index">Associations</a></li>
        <li><a href="<?php echo base_url();?>associations/ownership_account">Ownership accounts</a></li>
        <li><a href="<?php echo base_url();?>associations/association_owners">Association owners</a></li>
        <li><a href="<?php echo base_url();?>associations/outstanding_balances">Outstanding balances</a></li>
      </ul>
    </li>
    <li><a class="MenuBarItemSubmenu" href="#">Accounting</a>
      <ul>
        <li><a href="<?php echo base_url();?>accounts/index">accounts</a></li>
        <li><a href="<?php echo base_url();?>accounts/general_ledger">General ledger</a></li>
        <li><a href="<?php echo base_url();?>accounts/banking">Banking</a></li>
        <li class="divider"><a href="<?php echo base_url();?>accounts/vendors">Vendors</a></li>
        <li><a href="<?php echo base_url();?>accounts/work_orders">Work orders</a></li>
        <li><a href="<?php echo base_url();?>accounts/bills">Bills</a></li>
        <li><a href="<?php echo base_url();?>accounts/recurring_transactions">Recurring transactions</a></li>
        <li class="divider"><a href="<?php echo base_url();?>accounts/budget">Budgets</a></li>
        <li><a href="<?php echo base_url();?>accounts/chart_of_accounts">Chart of accounts</a></li>
        <li class="divider"><a href="<?php echo base_url();?>accounts/filing_1099.html">1099-MISC tax filings</a></li>
      </ul>
    </li>
    <li><a href="#" class="MenuBarItemSubmenu">Tasks</a>
      <ul>
        <li><a href="<?php echo base_url();?>tasks/index">My tasks</a></li>
        <li><a href="<?php echo base_url();?>tasks/unassigned_tasks">Unassigned tasks</a></li>
        <li><a href="<?php echo base_url();?>tasks/all_tasks">All tasks</a></li>
        <li class="divider"><a href="<?php echo base_url();?>tasks/recurring_tasks">Recurring tasks</a></li>
      </ul>
    </li>
    <li><a href="#" class="MenuBarItemSubmenu">Communications</a>
      <ul>
        <li><a href='#'> Public site</a></li>
        <li class="divider"><a href="#">Resident site users</a></li>
        <li><a href="#">Resident site contact directory</a></li>
        <li><a href="#">Resident site announcements</a></li>
        <li><a href="#">Association discussions</a></li>
        <li class="divider"><a href="#">Mailings</a></li>
        <li><a href="#">Mailing templates</a></li>
        <li><a href="#">Email templates</a></li>
      </ul>
    </li>
  </ul>
  <input name="txt" type="text" class="searchBox" id="txt" value="Search..." />
</div>
<div id="wrapper">
  <h1> Recurring Tasks</h1>
  <div class="button" style="float:right"><a href="#" onclick="window.open('add_recurring_task','mywindow','menubar=1,resizable=1, scrollbars=1, width=750,height=700');">Add Recurring Task</a></div>
  
  <p><br /><br />

  </p>
  <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td colspan="9" bgcolor="#EFEFEF"><label>
        <select name="ddlProperty2" id="ddlProperty2">
          <option value="lkjl">All Properties</option>
        </select>
        </label>
          <label style="padding-left:20px;"></label></td>
    </tr>
    <tr>
      <td bgcolor="#EFEFEF"><strong>Subject</strong></td>
      <td bgcolor="#EFEFEF"><strong>Property Type</strong></td>
      <td bgcolor="#EFEFEF"><strong>Frequency</strong></td>
      <td bgcolor="#EFEFEF"><strong>Next Date</strong></td>
      <td bgcolor="#EFEFEF"><strong>Duration</strong></td>
      <td bgcolor="#EFEFEF" align="center"><strong>Actions</strong></td>
    </tr>
<<<<<<< HEAD
=======
      <?php foreach ($show_data as $tdata){?>
      <tr>
          <td bgcolor="#FFF"><strong><?php echo $tdata['subject'];?></strong></td>
          <td bgcolor="#FFF"><strong><?php echo $tdata['property'];?>p</strong></td>
          <td bgcolor="#FFF"><strong><?php echo $tdata['frequency'];?></strong></td>
          <td bgcolor="#FFF"><strong><?php echo $tdata['next_date'];?></strong></td>
          <td bgcolor='#FFF'><strong><?php echo $tdata['duration'];?></strong></td>
          <!--<td bgcolor="#FFF"><strong><?php /*echo $$tdata[''];*/?>Actions</strong></td>
-->      </tr>
      <?php  }?>
>>>>>>> b347afd0eb920a12639eee3bd135cc529043d426
<!--    --><?php /*echo $trRecurring; */?>
  </table>
  <p>&nbsp;</p>
  <p>
    <label for="txt"></label></p>
  <p>&nbsp;</p>
</div>
<div id="footer" align="right">&copy; 2012 Real Estate Shark • All rights reserved.</div>
<script type="text/javascript">
var MenuBar1 = new Spry.Widget.MenuBar("MenuBar1", {imgDown:"SpryAssets/SpryMenuBarDownHover.gif", imgRight:"SpryAssets/SpryMenuBarRightHover.gif"});
</script>
</body>
</html>
