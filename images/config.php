<?php

/**
    @File Name:         config.php
    @Created By:        Noman Naseem, <amigosoft77@gmail.com>
    @Created On:        21-Oct-2012
    @Version:           1.0.0
    @Description:       File contains definition of all global variables.    
*/
    
    // Defining directory separator.
    define('DS', DIRECTORY_SEPARATOR);

    // Server paths configuration
    define('WEBROOT', '/print');
    define('ROOT_DIR', dirname(dirname(__FILE__)));
    define('TEMPLATES_DIR', ROOT_DIR . DS. 'templates'. DS);
    define('OUTPUT_DIR', ROOT_DIR . DS. 'output' . DS);
    define('OUTPUT','output');
    

    // Database configruation.
    $db_config['host']         = 'localhost';
    $db_config['db_name']      = 'devmcom_realestate';
    $db_config['user_name']    = 'devmcom_demodb';
    $db_config['password']     = 'bismillah';
    
    
    