<?php
/*include ("../includes/classes/class.db.php");
include ("../includes/functions/general.php");*/
//-------------------------------------------------------------------------------------------
								/*Objects*/
//-------------------------------------------------------------------------------------------					
	/*$db = new db();*/
	//isLoggedUser();
//-------------------------------------------------------------------------------------------
								/*General Coding Area*/
	/*$isAdded = false;
	if ($_POST){
		$sqlInsert = "INSERT INTO record_bill ( 
			dated,
			bill_due_date,
			vendor,
			ref_number,
			memo
		) VALUES (
			'".clearFormData("txtDate")."',
			'".clearFormData("txtDueDate")."',
			'".clearFormData("ddlVendor")."',
			'".clearFormData("txtRefNumber")."',
			'".clearFormData("txtMemo")."'
		)";
		$db->query($sqlInsert);
		$isAdded = true;
	}
*/?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Record Bill</title>
<link href="<?php echo base_url();?>css/styles_popup.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url();?>js/calendarDateInput.js" type="text/javascript"></script>
</head>

<body>
<form action="<?php echo base_url();?>accounts/record_bills" method="post" enctype="multipart/form-data">
<div id="wrapper_header"><a href="#" onclick="return confirm('Do you want to cancel?');"><img src="<?php echo base_url();?>images/btn_cancel.png" alt="Cancel" width="109" height="34" border="0" style="float:right" /></a>
  <input type="image" name="submit2" id="submit2" src="<?php echo base_url();?>images/btn_save.png" />
</div>
<p>&nbsp;</p>
<?php /*if ($isAdded) { echo '<div class="msg_success">Bill Record is added successfully.</div>'; } */?>
<table width="675" border="0" align="center" cellpadding="2" cellspacing="3">
  <tr>
    <td colspan="3"><h1>Record Bill</h1></td>
    </tr>
  <tr>
    <td colspan="3"><h2>Step 1 of 2: Enter bill information</h2></td>
    </tr>
  <tr>
    <td width="188">Date</td>
    <td width="156" colspan="2"><script>DateInput('txtDate', true, 'YYYY-MM-DD')</script></td>
  </tr>
  <tr>
    <td>Bill Due Date</td>
    <td colspan="2"><script>DateInput('txtDueDate', true, 'YYYY-MM-DD')</script></td>
  </tr>
  <tr>
    <td valign="top">Vendor</td>
    <td colspan="2"><select name="ddlVendor" id="ddlVendor">
      <option value="Vendor">Select Vendor</option>
    </select>
[<a href="add_vendor"> Add Vendor</a> ]</td>
  </tr>
  <tr>
    <td valign="top">Reference Number</td>
    <td colspan="2"><input name="txtRefNumber" type="text" class="textbox" id="txtRefNumber" size="10" /></td>
  </tr>
  <tr>
    <td valign="top">Memo</td>
    <td colspan="2"><input name="txtMemo" type="text" class="textbox" id="txtMemo" size="70" /></td>
  </tr>
  <tr>
    <td colspan="3"><h2>Step 2 of 2: Allocate bill to specific properties</h2></td>
    </tr>
  <tr>
    <td colspan="3" valign="top"><table width="100%" border="0" bgcolor="#FFFFFF">
      <tr>
        <td width="24%" height="26" bgcolor="#FFDFEF">Property </td>
        <td width="37%" bgcolor="#FFDFEF">Account</td>
        <td width="29%" bgcolor="#FFDFEF">Description</td>
        <td width="10%" bgcolor="#FFDFEF">Amount</td>
      </tr>
      <tr>
        <td height="39"><select name="ddlCountries3" id="ddlCountries3">
          <option>Select Property</option>
                </select></td>
        <td><select name="ddlCountries4" id="ddlCountries4">
          <option>Select Account</option>
                </select></td>
        <td><input name="txtPropertyName22" type="text" class="textbox" id="txtPropertyName37" /></td>
        <td><input name="txtPropertyName23" type="text" class="textbox" id="txtPropertyName38" size="7" /></td>
      </tr>
      <tr>
        <td colspan="5"><strong>Total:</strong></td>
      </tr>
    </table></td>
    </tr>
  <tr>
    <td colspan="3" valign="top"><h2>Attachment (1 file upto 1000kb):</h2></td>
    </tr>
  <tr>
    <td>Upload Attachment</td>
    <td colspan="2"><input type="file" name="attach" id="attach" /></td>
  </tr>
  <tr>
    <td colspan="3" align="center"><br />
      <input name="submit" type="submit" class="submit_button" id="submit" value="Save" /></td>
    </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
</form>
</body>
</html>
