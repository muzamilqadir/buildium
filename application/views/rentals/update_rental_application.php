<?php
include ("../includes/classes/class.db.php");
include ("../includes/functions/general.php");

//-------------------------------------------------------------------------------------------
								/*Objects*/
//-------------------------------------------------------------------------------------------					
	$db = new db();
	//isLoggedUser();
//-------------------------------------------------------------------------------------------
								/*General Coding Area*/
	
	$rid = "0";
	if (isset($_GET['id']) && $_GET['id'] != "" ) {
		$rid = $_GET['id'];
	}							
	//---------------------------------------------------------------------------------------
	
	$isUpdated = false;
	if ($_POST){
		$sqlUpdate = "UPDATE rental_applications SET 
		details_property = '".clearFormData("ddlPropertyType")."',
		details_number_of_units = '".clearFormData("ddlNumberOfUnits")."',
		details_first_name = '".clearFormData("txtFirstName")."',
		details_last_name = '".clearFormData("txtLastName")."',
		details_notes = '".clearFormData("txtNotes")."',
		application_property = '".clearFormData("ddlAppPropertyType")."',
		application_number_of_units = '".clearFormData("ddlAppNumberOfUnits")."',
		application_status = '".clearFormData("ddlAppStatus")."',
		general_first_name = '".clearFormData("txtGeneralFirstName")."',
		general_last_name = '".clearFormData("txtGeneralLastName")."',
		general_email = '".clearFormData("txtGeneralEmail")."',
		general_ssn = '".clearFormData("txtGeneralSSN")."',
		general_phone = '".clearFormData("txtGeneralPhone")."',
		general_date_of_birth = '".clearFormData("txtGeneralBirthDate")."',
		res_address = '".clearFormData("txtResAddress")."',
		res_city = '".clearFormData("txtResCity")."',
		res_state = '".clearFormData("ddlResState")."',
		res_postal_code = '".clearFormData("txtResPostalCode")."',
		res_landlord = '".clearFormData("txtResLandlord")."',
		res_monthly_rent = '".clearFormData("txtResMonhtlyRent")."',
		res_date_from = '".clearFormData("txtResDateFrom")."',
		res_date_to = '".clearFormData("txtResDateTo")."',
		res_leaving_reason = '".clearFormData("txtResReason")."',
		res_landlord_phone = '".clearFormData("txtResLandlordPhone")."',
		employer_name = '".clearFormData("txtEmployerName")."',
		employer_city = '".clearFormData("txtEmployerCity")."',
		employer_phone = '".clearFormData("txtEmployerPhone")."',
		employer_date_from = '".clearFormData("txtEmployerDateFrom")."',
		employer_date_to = '".clearFormData("txtEmployerDateTo")."',
		employer_gross_pay = '".clearFormData("txtEmployerGross")."',
		employer_occupation = '".clearFormData("txtEmployerOccupation")."',
		ref_name_1 = '".clearFormData("txtRefName_1")."',
		ref_name_1_phone = '".clearFormData("txtRefPhone_1")."',
		ref_name_2 = '".clearFormData("txtRefName_2")."',
		ref_name_2_phone = '".clearFormData("txtRefPhone_2")."',
		ref_gross_pay = '".clearFormData("txtRefGross")."',
		ref_occupation = '".clearFormData("txtRefOccupation")."',
		other_comments = '".clearFormData("txtOtherComments")."'
		WHERE 
		app_id = '".$rid."'";	
		$db->query($sqlUpdate);
		
		$isUpdated = true;
	}
	
	$sqlSelect = "SELECT * FROM rental_applications WHERE app_id = '".$rid."'";
	
	$query= $db->ExeQuersys($sqlSelect);
		
	while ($row = mysql_fetch_array($query)) {
		$details_property = $row['details_property'];
		$details_number_of_units = $row['details_number_of_units'];
		$details_first_name = $row['details_first_name'];
		$details_last_name = $row['details_last_name'];
		$details_notes = $row['details_notes'];
		$application_property = $row['application_property'];
		$application_number_of_units = $row['application_number_of_units'];
		$application_status = $row['application_status'];
		$general_first_name = $row['general_first_name'];
		$general_last_name = $row['general_last_name'];
		$general_email = $row['general_email'];
		$general_ssn = $row['general_ssn'];
		$general_phone = $row['general_phone'];
		$general_date_of_birth = $row['general_date_of_birth'];
		$res_address = $row['res_address'];
		$res_city = $row['res_city'];
		$res_state = $row['res_state'];
		$res_postal_code = $row['res_postal_code'];
		$res_landlord = $row['res_landlord'];
		$res_monthly_rent = $row['res_monthly_rent'];
		$res_date_from = $row['res_date_from'];
		$res_date_to = $row['res_date_to'];
		$res_leaving_reason = $row['res_leaving_reason'];
		$res_landlord_phone = $row['res_landlord_phone'];
		$employer_name = $row['employer_name'];
		$employer_city = $row['employer_city'];
		$employer_phone = $row['employer_phone'];
		$employer_date_from = $row['employer_date_from'];
		$employer_date_to = $row['employer_date_to'];
		$employer_gross_pay = $row['employer_gross_pay'];
		$employer_occupation = $row['employer_occupation'];
		$ref_name_1 = $row['ref_name_1'];
		$ref_name_1_phone = $row['ref_name_1_phone'];
		$ref_name_2 = $row['ref_name_2'];
		$ref_name_2_phone = $row['ref_name_2_phone'];
		$ref_gross_pay = $row['ref_gross_pay'];
		$ref_occupation = $row['ref_occupation'];
		$other_comments = $row['other_comments'];
	}
	
	
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Rental Application</title>
<link href="../styles_popup.css" rel="stylesheet" type="text/css" />
<script src="../calendarDateInput.js" type="text/javascript"></script>
</head>

<body>
<form action="" method="post">
<div id="wrapper_header"><a href="#" onclick="return confirm('Do you want to cancel?');"><img src="../images/btn_cancel.png" alt="Cancel" width="109" height="34" border="0" style="float:right" /></a>
  <input type="image" name="submit2" id="submit2" src="../images/btn_save.png" />
</div>
<p>&nbsp;</p>
<?php if ($isUpdated) { echo '<div class="msg_success">Rental Application is updated successfully.</div>'; } ?>
<table width="738" border="0" align="center" cellpadding="2" cellspacing="3">
  <tr>
    <td colspan="2"><h1>Rental Application</h1></td>
    </tr>
  <tr>
    <td colspan="2"><h2>Details:</h2></td>
    </tr>
  <tr>
    <td>Property</td>
    <td><select name="ddlPropertyType" id="ddlPropertyType">
      <option value="Residential Rental" <?php if ($details_property == "Residential Rental") { echo 'selected="selected"';} ?> >Residential Rental</option>
      <option value="Commercial Rental" <?php if ($details_property == "Commercial Rental") { echo 'selected="selected"';} ?> >Commercial Rental</option>
    </select></td>
  </tr>
  <tr>
    <td>Unit</td>
    <td><select name="ddlNumberOfUnits" id="ddlNumberOfUnits">
      <option value="1" <?php if ($details_number_of_units == "1") { echo 'selected="selected"';} ?> >1</option>
      <option value="2" <?php if ($details_number_of_units == "2") { echo 'selected="selected"';} ?> >2</option>
      <option value="3" <?php if ($details_number_of_units == "3") { echo 'selected="selected"';} ?> >3</option>
      <option value="4" <?php if ($details_number_of_units == "4") { echo 'selected="selected"';} ?> >4</option>
      <option value="5" <?php if ($details_number_of_units == "5") { echo 'selected="selected"';} ?> >5</option>
      <option value="6" <?php if ($details_number_of_units == "6") { echo 'selected="selected"';} ?> >6</option>
      <option value="7" <?php if ($details_number_of_units == "7") { echo 'selected="selected"';} ?> >7</option>
      <option value="8" <?php if ($details_number_of_units == "8") { echo 'selected="selected"';} ?> >8</option>
      <option value="9" <?php if ($details_number_of_units == "9") { echo 'selected="selected"';} ?> >9</option>
      <option value="10" <?php if ($details_number_of_units == "10") { echo 'selected="selected"';} ?> >10</option>
      <option value="11" <?php if ($details_number_of_units == "11") { echo 'selected="selected"';} ?> >11</option>
      <option value="12" <?php if ($details_number_of_units == "12") { echo 'selected="selected"';} ?> >12</option>
      <option value="13" <?php if ($details_number_of_units == "13") { echo 'selected="selected"';} ?> >13</option>
      <option value="14" <?php if ($details_number_of_units == "14") { echo 'selected="selected"';} ?> >14</option>
      <option value="15" <?php if ($details_number_of_units == "15") { echo 'selected="selected"';} ?> >15</option>
        </select></td>
  </tr>
  <tr>
    <td width="188">First name</td>
    <td width="370"><input name="txtFirstName" type="text" class="textbox" id="txtFirstName" value="<?php echo $details_first_name; ?>" /></td>
  </tr>
  <tr>
    <td>Last name</td>
    <td><input name="txtLastName" type="text" class="textbox" id="txtLastName" value="<?php echo $details_last_name; ?>" /></td>
  </tr>
  <tr>
    <td colspan="2"><h2>Notes:</h2></td>
    </tr>
  <tr>
    <td colspan="2"><textarea name="txtNotes" cols="95" rows="5" class="textarea" id="txtNotes"><?php echo $details_notes; ?></textarea></td>
    </tr>
  <tr>
    <td colspan="2"><h2>Application Information</h2></td>
    </tr>
  <tr>
    <td>Property</td>
    <td><select name="ddlAppPropertyType" id="ddlAppPropertyType">
      <option value="Residential Rental" <?php if ($application_property == "Residential Rental") { echo 'selected="selected"';} ?>>Residential Rental</option>
      <option value="Commercial Rental" <?php if ($application_property == "Commercial Rental") { echo 'selected="selected"';} ?>>Commercial Rental</option>
    </select></td>
  </tr>
  <tr>
    <td>Unit</td>
    <td>
    <select name="ddlAppNumberOfUnits" id="ddlAppNumberOfUnits">
      <option value="1" <?php if ($application_number_of_units == "1") { echo 'selected="selected"';} ?> >1</option>
      <option value="2" <?php if ($application_number_of_units == "2") { echo 'selected="selected"';} ?> >2</option>
      <option value="3" <?php if ($application_number_of_units == "3") { echo 'selected="selected"';} ?> >3</option>
      <option value="4" <?php if ($application_number_of_units == "4") { echo 'selected="selected"';} ?> >4</option>
      <option value="5" <?php if ($application_number_of_units == "5") { echo 'selected="selected"';} ?> >5</option>
      <option value="6" <?php if ($application_number_of_units == "6") { echo 'selected="selected"';} ?> >6</option>
      <option value="7" <?php if ($application_number_of_units == "7") { echo 'selected="selected"';} ?> >7</option>
      <option value="8" <?php if ($application_number_of_units == "8") { echo 'selected="selected"';} ?> >8</option>
      <option value="9" <?php if ($application_number_of_units == "9") { echo 'selected="selected"';} ?> >9</option>
      <option value="10" <?php if ($application_number_of_units == "10") { echo 'selected="selected"';} ?> >10</option>
      <option value="11" <?php if ($application_number_of_units == "11") { echo 'selected="selected"';} ?> >11</option>
      <option value="12" <?php if ($application_number_of_units == "12") { echo 'selected="selected"';} ?> >12</option>
      <option value="13" <?php if ($application_number_of_units == "13") { echo 'selected="selected"';} ?> >13</option>
      <option value="14" <?php if ($application_number_of_units == "14") { echo 'selected="selected"';} ?> >14</option>
      <option value="15" <?php if ($application_number_of_units == "15") { echo 'selected="selected"';} ?> >15</option>
    </select>
    </td>
  </tr>
  <tr>
    <td>Status</td>
    <td><select name="ddlAppStatus" id="ddlAppStatus">
      <option value="Undecided" <?php if ($application_status == "Undecided") { echo 'selected="selected"';} ?> >Undecided</option>
      <option value="Defered" <?php if ($application_status == "Defered") { echo 'selected="selected"';} ?> >Defered</option>
      <option value="Approved" <?php if ($application_status == "Approved") { echo 'selected="selected"';} ?> >Approved</option>
      <option value="Cancelled" <?php if ($application_status == "Cancelled") { echo 'selected="selected"';} ?> >Cancelled</option>
      <option value="Rejected" <?php if ($application_status == "Rejected") { echo 'selected="selected"';} ?> >Rejected</option>
        </select></td>
  </tr>
  <tr>
    <td colspan="2"><h2>General Information</h2></td>
    </tr>
  <tr>
    <td colspan="2"><table width="100%" border="0" cellspacing="2" cellpadding="0">
      <tr>
        <td>First Name</td>
        <td><input name="txtGeneralFirstName" type="text" class="textbox" id="txtGeneralFirstName" value="<?php echo $general_first_name; ?>" /></td>
        <td>Last Name</td>
        <td><input name="txtGeneralLastName" type="text" class="textbox" id="txtGeneralLastName" value="<?php echo $general_last_name; ?>" /></td>
      </tr>
      <tr>
        <td>Email</td>
        <td><input name="txtGeneralEmail" type="text" class="textbox" id="txtGeneralEmail" value="<?php echo $general_email; ?>" /></td>
        <td>SSN</td>
        <td><input name="txtGeneralSSN" type="text" class="textbox" id="txtGeneralSSN" value="<?php echo $general_ssn; ?>" /></td>
      </tr>
      <tr>
        <td>Phone Number</td>
        <td><input name="txtGeneralPhone" type="text" class="textbox" id="txtGeneralPhone" value="<?php echo $general_phone; ?>" /></td>
        <td>Birth Date</td>
        <td><script>DateInput('txtGeneralBirthDate', true, 'YYYY-MM-DD', '<?php echo $general_date_of_birth; ?>')</script>&nbsp;</td>
      </tr>
    </table></td>
    </tr>
  <tr>
    <td colspan="2"><h2>Residence and rental history</h2></td>
    </tr>
  <tr>
    <td colspan="2"><table width="100%" border="0" cellspacing="2" cellpadding="0">
      <tr>
        <td width="19%">Address</td>
        <td width="33%"><input name="txtResAddress" type="text" class="textbox" id="txtResAddress" value="<?php echo $res_address; ?>" /></td>
        <td width="14%">City/Locality</td>
        <td width="34%"><input name="txtResCity" type="text" class="textbox" id="txtResCity" value="<?php echo $res_city; ?>" /></td>
      </tr>
      <tr>
        <td>State/Province</td>
        <td><select name="ddlResState" id="ddlResState">
          <option value="AE" <?php if ($res_state == "AE") { echo 'selected="selected"';} ?> >AE</option>
          <option value="AK" <?php if ($res_state == "AK") { echo 'selected="selected"';} ?> >AK</option>
          <option value="AL" <?php if ($res_state == "AL") { echo 'selected="selected"';} ?> >AL</option>
          <option value="AP" <?php if ($res_state == "AP") { echo 'selected="selected"';} ?> >AP</option>
          <option value="AR" <?php if ($res_state == "AR") { echo 'selected="selected"';} ?> >AR</option>
          <option value="AZ" <?php if ($res_state == "AZ") { echo 'selected="selected"';} ?> >AZ</option>
          <option value="CA" <?php if ($res_state == "CA") { echo 'selected="selected"';} ?> >CA</option>
          <option value="CO" <?php if ($res_state == "CO") { echo 'selected="selected"';} ?> >CO</option>
          <option value="CT" <?php if ($res_state == "CT") { echo 'selected="selected"';} ?> >CT</option>
          <option value="DC" <?php if ($res_state == "DC") { echo 'selected="selected"';} ?> >DC</option>
          <option value="DE" <?php if ($res_state == "DE") { echo 'selected="selected"';} ?> >DE</option>
          <option value="FL" <?php if ($res_state == "FL") { echo 'selected="selected"';} ?> >FL</option>
          <option value="GA" <?php if ($res_state == "GA") { echo 'selected="selected"';} ?> >GA</option>
          <option value="HI" <?php if ($res_state == "HI") { echo 'selected="selected"';} ?> >HI</option>
          <option value="IA" <?php if ($res_state == "IA") { echo 'selected="selected"';} ?> >IA</option>
          <option value="ID" <?php if ($res_state == "ID") { echo 'selected="selected"';} ?> >ID</option>
          <option value="IL" <?php if ($res_state == "IL") { echo 'selected="selected"';} ?> >IL</option>
          <option value="IN" <?php if ($res_state == "IN") { echo 'selected="selected"';} ?> >IN</option>
          <option value="KS" <?php if ($res_state == "KS") { echo 'selected="selected"';} ?> >KS</option>
          <option value="KY" <?php if ($res_state == "KY") { echo 'selected="selected"';} ?> >KY</option>
          <option value="LA" <?php if ($res_state == "LA") { echo 'selected="selected"';} ?> >LA</option>
          <option value="MA" <?php if ($res_state == "MA") { echo 'selected="selected"';} ?> >MA</option>
          <option value="MD" <?php if ($res_state == "MD") { echo 'selected="selected"';} ?> >MD</option>
          <option value="ME" <?php if ($res_state == "ME") { echo 'selected="selected"';} ?> >ME</option>
          <option value="MI" <?php if ($res_state == "MI") { echo 'selected="selected"';} ?> >MI</option>
          <option value="MN" <?php if ($res_state == "MN") { echo 'selected="selected"';} ?> >MN</option>
          <option value="MO" <?php if ($res_state == "MO") { echo 'selected="selected"';} ?> >MO</option>
          <option value="MS" <?php if ($res_state == "MS") { echo 'selected="selected"';} ?> >MS</option>
          <option value="MT" <?php if ($res_state == "MT") { echo 'selected="selected"';} ?> >MT</option>
          <option value="NC" <?php if ($res_state == "NC") { echo 'selected="selected"';} ?> >NC</option>
          <option value="ND" <?php if ($res_state == "ND") { echo 'selected="selected"';} ?> >ND</option>
          <option value="NE" <?php if ($res_state == "NE") { echo 'selected="selected"';} ?> >NE</option>
          <option value="NH" <?php if ($res_state == "NH") { echo 'selected="selected"';} ?> >NH</option>
          <option value="NJ" <?php if ($res_state == "NJ") { echo 'selected="selected"';} ?> >NJ</option>
          <option value="NM" <?php if ($res_state == "NM") { echo 'selected="selected"';} ?> >NM</option>
          <option value="NV" <?php if ($res_state == "NV") { echo 'selected="selected"';} ?> >NV</option>
          <option value="NY" <?php if ($res_state == "NY") { echo 'selected="selected"';} ?> >NY</option>
          <option value="OH" <?php if ($res_state == "OH") { echo 'selected="selected"';} ?> >OH</option>
          <option value="OK" <?php if ($res_state == "OK") { echo 'selected="selected"';} ?> >OK</option>
          <option value="OR" <?php if ($res_state == "OR") { echo 'selected="selected"';} ?> >OR</option>
          <option value="PA" <?php if ($res_state == "PA") { echo 'selected="selected"';} ?> >PA</option>
          <option value="RI" <?php if ($res_state == "RI") { echo 'selected="selected"';} ?> >RI</option>
          <option value="SC" <?php if ($res_state == "SC") { echo 'selected="selected"';} ?> >SC</option>
          <option value="SD" <?php if ($res_state == "SD") { echo 'selected="selected"';} ?> >SD</option>
          <option value="TN" <?php if ($res_state == "TN") { echo 'selected="selected"';} ?> >TN</option>
          <option value="TX" <?php if ($res_state == "TX") { echo 'selected="selected"';} ?> >TX</option>
          <option value="UT" <?php if ($res_state == "UT") { echo 'selected="selected"';} ?> >UT</option>
          <option value="VA" <?php if ($res_state == "VA") { echo 'selected="selected"';} ?> >VA</option>
          <option value="VT" <?php if ($res_state == "VT") { echo 'selected="selected"';} ?> >VT</option>
          <option value="WA" <?php if ($res_state == "WA") { echo 'selected="selected"';} ?> >WA</option>
          <option value="WI" <?php if ($res_state == "WI") { echo 'selected="selected"';} ?> >WI</option>
          <option value="WV" <?php if ($res_state == "WV") { echo 'selected="selected"';} ?> >WV</option>
          <option value="WY" <?php if ($res_state == "WY") { echo 'selected="selected"';} ?> >WY</option>
        </select></td>
        <td>Zip/Postal code</td>
        <td><input name="txtResPostalCode" type="text" class="textbox" id="txtResPostalCode" value="<?php echo $res_postal_code; ?>" /></td>
      </tr>
      <tr>
        <td>Landlord/Manager Name</td>
        <td><input name="txtResLandlord" type="text" class="textbox" id="txtResLandlord" value="<?php echo $res_landlord; ?>" /></td>
        <td>Monthly Rent</td>
        <td><input name="txtResMonhtlyRent" type="text" class="textbox" id="txtResMonhtlyRent" value="<?php echo $res_monthly_rent; ?>" /></td>
      </tr>
      <tr>
        <td>Dates of Residency (From/To)</td>
        <td>From: <script>DateInput('txtResDateFrom', true, 'YYYY-MM-DD', '<?php echo $res_date_from; ?>')</script> 
      To: <script>DateInput('txtResDateTo', true, 'YYYY-MM-DD', '<?php echo $res_date_to; ?>')</script>
      &nbsp;</td>
        <td>Reason for Leaving</td>
        <td><input name="txtResReason" type="text" class="textbox" id="txtResReason" value="<?php echo $res_leaving_reason; ?>" /></td>
      </tr>
      <tr>
        <td>Landlord/Manager Phone</td>
        <td><input name="txtResLandlordPhone" type="text" class="textbox" id="txtResLandlordPhone" value="<?php echo $res_landlord_phone; ?>" /></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
    </table></td>
    </tr>
  <tr>
    <td colspan="2"><h2>Employment and income history</h2></td>
    </tr>
  <tr>
    <td colspan="2"><table width="100%" border="0" cellspacing="2" cellpadding="0">
      <tr>
        <td width="19%">Employer Name</td>
        <td width="33%"><input name="txtEmployerName" type="text" class="textbox" id="txtEmployerName" value="<?php echo $employer_name; ?>" /></td>
        <td width="14%">City</td>
        <td width="34%"><input name="txtEmployerCity" type="text" class="textbox" id="txtEmployerCity" value="<?php echo $employer_city; ?>" /></td>
      </tr>
      <tr>
        <td>Employer Phone</td>
        <td><input name="txtEmployerPhone" type="text" class="textbox" id="txtEmployerPhone" value="<?php echo $employer_phone; ?>" /></td>
        <td>Dates of Employment (From/To)</td>
        <td>From: <script>DateInput('txtEmployerDateFrom', true, 'YYYY-MM-DD', '<?php echo $employer_date_from; ?>')</script> 
      To: <script>DateInput('txtEmployerDateTo', true, 'YYYY-MM-DD', '<?php echo $employer_date_to; ?>')</script>
      &nbsp;</td>
      </tr>
      <tr>
        <td>Monthly Gross Pay</td>
        <td><input name="txtEmployerGross" type="text" class="textbox" id="txtEmployerGross" value="<?php echo $employer_gross_pay; ?>" /></td>
        <td>Occupation</td>
        <td><input name="txtEmployerOccupation" type="text" class="textbox" id="txtEmployerOccupation" value="<?php echo $employer_occupation; ?>" /></td>
      </tr>

    </table></td>
    </tr>
  <tr>
    <td colspan="2"><h2>References</h2></td>
    </tr>
  <tr>
    <td colspan="2"><table width="100%" border="0" cellspacing="2" cellpadding="0">
      <tr>
        <td width="19%">Name 1</td>
        <td width="33%"><input name="txtRefName_1" type="text" class="textbox" id="txtRefName_1" value="<?php echo $ref_name_1; ?>" /></td>
        <td width="14%">Name 2</td>
        <td width="34%"><input name="txtRefName_2" type="text" class="textbox" id="txtRefName_2" value="<?php echo $ref_name_2; ?>" /></td>
      </tr>
      <tr>
        <td>Phone 1</td>
        <td><input name="txtRefPhone_1" type="text" class="textbox" id="txtRefPhone_1" value="<?php echo $ref_name_1_phone; ?>" /></td>
        <td>Phone 2</td>
        <td><input name="txtRefPhone_2" type="text" class="textbox" id="txtRefPhone_2" value="<?php echo $ref_name_2_phone; ?>" /></td>
      </tr>
      <tr>
        <td>Monthly Gross Pay</td>
        <td><input name="txtRefGross" type="text" class="textbox" id="txtRefGross" value="<?php echo $ref_gross_pay; ?>" /></td>
        <td>Occupation</td>
        <td><input name="txtRefOccupation" type="text" class="textbox" id="txtRefOccupation" value="<?php echo $ref_occupation; ?>" /></td>
      </tr>
    </table></td>
    </tr>
  <tr>
    <td colspan="2"><h2>Other Comments:</h2></td>
    </tr>
  <tr>
    <td colspan="2" align="center"><textarea name="txtOtherComments" cols="95" rows="5" class="textarea" id="txtOtherComments"><?php echo $other_comments; ?></textarea></td>
    </tr>
  <tr>
    <td colspan="2" align="center"><input name="submit" type="submit" class="submit_button" id="submit" value="Save" /></td>
    </tr>
</table>
<p><br />
</p>
</form>
</body>
</html>
