<?php
/*include ("../includes/classes/class.db.php");
include ("../includes/functions/general.php");*/
//-------------------------------------------------------------------------------------------
								/*Objects*/
//-------------------------------------------------------------------------------------------					
	/*$db = new db();*/
	//isLoggedUser();
//-------------------------------------------------------------------------------------------
								/*General Coding Area*/
	/*$isAdded = false;
	if ($_POST){
		$sqlInsert = "INSERT INTO account (account_name, account_type, sub_account, comments) 
		VALUES (
		'".clearFormData("txtName")."',
		'".clearFormData("ddlType")."',
		'".clearFormData("ddlSubAccount")."',
		'".clearFormData("txtComments")."'
		)";

		$db->query($sqlInsert);
		$isAdded = true;
	}
*/?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Add Account</title>
<link href="<?php echo base_url();?>css/styles_popup.css" rel="stylesheet" type="text/css" />
</head>

<body>

<form action="<?php echo base_url();?>accounts/save_account" method="post">
<div id="wrapper_header"><a href="#" onclick="return confirm('Do you want to cancel?');"><img src="<?php echo base_url();?>/images/btn_cancel.png" alt="Cancel" width="109" height="34" border="0" style="float:right" /></a>
  <input type="image" name="submit2" id="submit2" src="<?php echo base_url();?>images/btn_save.png" />
</div>
<p>&nbsp;</p>
<?php /*if ($isAdded) { echo '<div class="msg_success">Account is added successfully.</div>'; } */?>
<table width="551" border="0" align="center" cellpadding="2" cellspacing="3">
  <tr>
    <td colspan="3"><h1>Add Account</h1></td>
    </tr>
  <tr>
    <td colspan="3"><h2>Account Information</h2></td>
    </tr>
  <tr>
    <td width="210">Name</td>
    <td width="448" colspan="2"><input name="txtName" type="text" class="textbox" id="txtName" size="35" /></td>
  </tr>
  <tr>
    <td>Type</td>
    <td colspan="2"><select name="ddlType" id="ddlType">
      <option value="10">Current Asset</option>
	<option value="11">Fixed Asset</option>
	<option value="20">Current Liability</option>
	<option value="21">Long Term Liability</option>
	<option value="30">Equity</option>
	<option value="40">Income</option>
	<option value="41">Non-operating Income</option>
	<option value="50">Expenses</option>
	<option value="51">Non-operating Expenses</option>
            </select></td>
  </tr>
  <tr>
    <td valign="top">Subaccount of <em>(optional)</em></td>
    <td colspan="2"><select name="ddlSubAccount" id="ddlSubAccount">
      <option value="1">None</option>
    </select></td>
  </tr>
  <tr>
    <td valign="top">Comments</td>
    <td colspan="2"><input name="txtComments" type="text" class="textbox" id="txtComments" size="35" /></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2"><input name="submit" type="submit" class="submit_button" id="submit" value="Save" /></td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
</form>
</body>
</html>
