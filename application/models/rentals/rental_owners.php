<?php
class Rental_Owners extends CI_Model{
    function owners_info(){
    $query = "SELECT * FROM rental_owner";
    return $this->db->query($query)->result_array();
}
function add_rental_owner($ownerData){
    $query = "INSERT INTO rental_owner(first_name,last_name,company,primary_email,alternate_email,
			taxpayer_id,1099_eligible,home_phone,work_phone,mobile_phone,fax,date_of_birth,country,
			address_1,address_2,address_3,city,state,address_zipcode,comments,properties)VALUES('$ownerData[txtFirstName]',
			'$ownerData[txtLastName]',
        '$ownerData[txtCompany]','$ownerData[txtPrimaryEmail]','$ownerData[txtAlternateEmail]',
        '$ownerData[txtTaxID]','$ownerData[chkEligible]','$ownerData[txtHomePhone]','$ownerData[txtWorkPhone]',
        '$ownerData[txtMobilePhone]','$ownerData[txtFax]','$ownerData[txtBirthDate]','$ownerData[ddlCountries]','$ownerData[txtAddress_1]',
        '$ownerData[txtAddress_2]','$ownerData[txtAddress_3]','$ownerData[txtCity]','$ownerData[ddlState]','$ownerData[txtZipCode]','$ownerData[txtComments]','$ownerData[chkProperties]')";

    if ($this->db->query($query)>0){
        return "insert successfully";
    }
    return "error occurred";
}
}
