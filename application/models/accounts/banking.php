<?php
class Banking extends CI_Model{

    function account_info(){
    $query="SELECT bank_account_id, name, type, account_number, bank_information,
    check_format FROM bank_account";
    return $this->db->query($query)->result_array();
}
    function add_bank_account($saveBankData) {
        $query = "INSERT INTO bank_account(name,type,account_number,routing_number,comments,
			enable_check_printing,check_stock_paper,check_format,signature_heading,
			fractional_number,bank_information) VALUES ('$saveBankData[txtName]',
			'$saveBankData[ddlType]','$saveBankData[txtAccountNumber]',
            '$saveBankData[txtRoutingNumber]','$saveBankData[txtComment]',
            '$saveBankData[chkLocalPrinting]',
            '$saveBankData[rdCheckStock]','$saveBankData[ddlCheckFormat]',
            '$saveBankData[txtSignatureHeading]', '$saveBankData[txtFractionalNumber]',
            '$saveBankData[txtBankInformation]')";

        if($this->db->query($query) > 0){
            return "successfully Inserted";
        }
        return "Error occurred";
    }

}