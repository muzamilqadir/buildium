<?php
include ("../includes/classes/class.db.php");
include ("../includes/functions/general.php");
//-------------------------------------------------------------------------------------------
								/*Objects*/
//-------------------------------------------------------------------------------------------					
	$db = new db();
	//isLoggedUser();
//-------------------------------------------------------------------------------------------
								/*General Coding Area*/
	$aid = "0";
	if (isset($_GET['id']) && $_GET['id'] != "" ) {
		$aid = $_GET['id'];
	}							
	//---------------------------------------------------------------------------------------
	
	$isUpdated = false;
	
	if ($_POST){
		$sqlUpdate = "UPDATE account  = '".clearFormData("txtName")."',
		account_type = '".clearFormData("ddlType")."', 
		sub_account = '".clearFormData("ddlSubAccount")."', 
		comments = '".clearFormData("txtComments")."'
		WHERE account_id = '".$aid."'";

		$db->query($sqlUpdate);
		$isUpdated = true;
	}
	
	$sqlSelect = "SELECT * FROM account WHERE account_id = '".$aid."'";
	
	$query= $db->ExeQuersys($sqlSelect);
		
	while ($row = mysql_fetch_array($query)) {
		$account_name = $row['account_name'];
		$account_type = $row['account_type'];
		$sub_account = $row['sub_account'];
		$comments = $row['comments'];
	}
	
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Update Account</title>
<link href="../styles_popup.css" rel="stylesheet" type="text/css" />
</head>

<body>
<form action="" method="post">
<div id="wrapper_header"><a href="#" onclick="return confirm('Do you want to cancel?');"><img src="../images/btn_cancel.png" alt="Cancel" width="109" height="34" border="0" style="float:right" /></a>
  <input type="image" name="submit2" id="submit2" src="../images/btn_save.png" />
</div>
<p>&nbsp;</p>
<?php if ($isUpdated) { echo '<div class="msg_success">Account is updated successfully.</div>'; } ?>
<table width="551" border="0" align="center" cellpadding="2" cellspacing="3">
  <tr>
    <td colspan="3"><h1>Update Account</h1></td>
    </tr>
  <tr>
    <td colspan="3"><h2>Account Information</h2></td>
    </tr>
  <tr>
    <td width="210">Name</td>
    <td width="448" colspan="2"><input name="txtName" type="text" class="textbox" id="txtName" size="35" value="<?php echo $account_name; ?>" /></td>
  </tr>
  <tr>
    <td>Type</td>
    <td colspan="2"><select name="ddlType" id="ddlType">
    <option value="10" <?php if($account_type == "10") { echo 'selected="selected"'; } ?> >Current Asset</option>
	<option value="11" <?php if($account_type == "11") { echo 'selected="selected"'; } ?> >Fixed Asset</option>
	<option value="20" <?php if($account_type == "20") { echo 'selected="selected"'; } ?> >Current Liability</option>
	<option value="21" <?php if($account_type == "21") { echo 'selected="selected"'; } ?> >Long Term Liability</option>
	<option value="30" <?php if($account_type == "30") { echo 'selected="selected"'; } ?> >Equity</option>
	<option value="40" <?php if($account_type == "40") { echo 'selected="selected"'; } ?> >Income</option>
	<option value="41" <?php if($account_type == "41") { echo 'selected="selected"'; } ?> >Non-operating Income</option>
	<option value="50" <?php if($account_type == "50") { echo 'selected="selected"'; } ?> >Expenses</option>
	<option value="51" <?php if($account_type == "51") { echo 'selected="selected"'; } ?> >Non-operating Expenses</option>
    </select></td>
  </tr>
  <tr>
    <td valign="top">Subaccount of <em>(optional)</em></td>
    <td colspan="2"><select name="ddlSubAccount" id="ddlSubAccount">
      <option value="1" <?php if ($sub_account == "1") { echo 'selected="selected"'; } ?> >None</option>
    </select></td>
  </tr>
  <tr>
    <td valign="top">Comments</td>
    <td colspan="2"><input name="txtComments" type="text" class="textbox" id="txtComments" size="35" value="<?php echo $comments; ?>" /></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2"><input name="submit" type="submit" class="submit_button" id="submit" value="Update" /></td>
  </tr>d
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
</form>
</body>
</html>
