<?php
/*include ("../includes/classes/class.db.php");
include ("../includes/functions/general.php");*/
//-------------------------------------------------------------------------------------------
								/*Objects*/
//-------------------------------------------------------------------------------------------					
	/*$db = new db();*/
	//isLoggedUser();
//-------------------------------------------------------------------------------------------
								/*General Coding Area*/
	/*$isAdded = false;

	if ($_POST){
		$sqlInsert = "INSERT INTO recurring_tasks (subject, description, property, unit, assigned_to, priority, category, due_date, resident, 
		email, home_phone, work_phone, mobile_phone, frequency, next_date, duration) 
		VALUES (
		'".clearFormData("txtSubject")."',
		'".clearFormData("txtDescription")."',
		'".clearFormData("ddlProperty")."',
		'".clearFormData("ddlNumberOfUnits")."',
		'".clearFormData("ddlAssignedTo")."',
		'".clearFormData("ddlPriority")."',
		'".clearFormData("ddlCategory")."',
		'".clearFormData("txtDueDate")."',
		'".clearFormData("ddlResident")."',
		'".clearFormData("txtEmail")."',
		'".clearFormData("txtHome")."',
		'".clearFormData("txtWork")."',
		'".clearFormData("txtMobile")."',
		'".clearFormData("ddlFrequency")."',
		'".clearFormData("txtNextDate")."',
		'".clearFormData("rdDuration")."')";
		
		$db->query($sqlInsert);
		$isAdded = true;
	}
*/?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Add Recurring Task</title>
<link href="<?php echo base_url();?>css/styles_popup.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url();?>js/calendarDateInput.js" type="text/javascript"></script>
</head>

<body>
<form action="<?php echo base_url();?>tasks/saveRecurringTasks" method="post" enctype="multipart/form-data">
<div id="wrapper_header"><a href="#" onclick="return confirm('Do you want to cancel?');"><img src="<?php echo base_url();?>images/btn_cancel.png" alt="Cancel" width="109" height="34" border="0" style="float:right" /></a>
  <input type="image" name="submit2" id="submit2" src="<?php echo base_url();?>images/btn_save.png" />
</div>
<p>&nbsp;</p>
<?php /*if ($isAdded) { echo '<div class="msg_success">Task is added successfully.</div>'; } */?>
<table width="664" border="0" align="center" cellpadding="2" cellspacing="3">
  <tr>
    <td colspan="2"><h1>Add Recurring Task</h1></td>
    </tr>
  <tr>
    <td colspan="2"><h2>Details:</h2></td>
    </tr>
  <tr>
    <td width="98">Subject</td>
    <td width="549"><input name="txtSubject" type="text" class="textbox" id="txtSubject" size="80" /></td>
  </tr>
  <tr>
    <td valign="top">Description</td>
    <td><textarea name="txtDescription" cols="77" rows="5" class="textarea" id="txtDescription"></textarea></td>
  </tr>
  <tr>
    <td colspan="2"><table width="100%" border="0" cellspacing="2" cellpadding="0">
      <tr>
        <td width="16%" valign="top">Property</td>
        <td width="40%" valign="top"><select name="ddlProperty" id="ddlProperty">
          <option value="Residential Rental">Residential Rental</option>
          <option value="Commercial Rental">Commercial Rental</option>
        </select></td>
        <td width="14%" valign="top">Resident:</td>
        <td width="30%" valign="top"><select name="ddlResident" id="ddlResident">
          <option>-- Select --</option>
          <option value="Resident">Resident</option>
                </select>
          <br />
          <input type="checkbox" name="chgk" id="chgk" />
          Share with resident</td>
      </tr>
      <tr>
        <td valign="top">Unit</td>
        <td valign="top"><select name="ddlNumberOfUnits" id="ddlNumberOfUnits">
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
          <option value="6">6</option>
          <option value="7">7</option>
          <option value="8">8</option>
          <option value="9">9</option>
          <option value="10">10</option>
          <option value="11">11</option>
          <option value="12">12</option>
          <option value="13">13</option>
          <option value="14">14</option>
          <option value="15">15</option>
        </select></td>
        <td valign="top">Email</td>
        <td valign="top"><input name="txtEmail" type="text" class="textbox" id="txtPropertyName4" /></td>
      </tr>
      <tr>
        <td valign="top">Attachment</td>
        <td valign="top"><input name="fileField" type="file" class="textbox" id="fileField" /></td>
        <td valign="top">Home</td>
        <td valign="top"><input name="txtHome" type="text" class="textbox" id="txtHome" /></td>
      </tr>
      <tr>
        <td valign="top">Assigned To</td>
        <td valign="top"><select name="ddlAssignedTo" id="ddlAssignedTo">
          <option value="Assigned To">Assigned To</option>
        </select></td>
        <td valign="top">Work</td>
        <td valign="top"><input name="txtWork" type="text" class="textbox" id="txtPropertyName8" /></td>
      </tr>
      <tr>
        <td valign="top">Priority</td>
        <td valign="top"><select name="ddlPriority" id="ddlPriority">
          <option value="Low">Low</option>
          <option value="Normal">Normal</option>
          <option value="High">High</option>
                </select></td>
        <td valign="top">Mobile</td>
        <td valign="top"><input name="txtMobile" type="text" class="textbox" id="txtPropertyName17" /></td>
      </tr>
      <tr>
        <td valign="top">Category</td>
        <td valign="top"><select name="ddlCategory" id="ddlCategory">
          <option value="Uncategorized">Uncategorized</option>
          <option value="Complaint">Complaint</option>
          <option value="Feedback/Suggestion">Feedback/Suggestion</option>
          <option value="General Inquiry">General Inquiry</option>
          <option value="Maintence Request">Maintence Request</option>
          <option value="Other">Other</option>
                </select></td>
        <td valign="top">&nbsp;</td>
        <td valign="top">&nbsp;</td>
      </tr>
      <tr>
        <td valign="top">Due Date</td>
        <td valign="top"><script>DateInput('txtDueDate', true, 'YYYY-MM-DD')</script>&nbsp;</td>
        <td valign="top">&nbsp;</td>
        <td valign="top">&nbsp;</td>
      </tr>
    </table></td>
    </tr>
  <tr>
    <td colspan="2"><h2>Recurrence information</h2></td>
  </tr>
  <tr>
    <td>Frequency:</td>
    <td><select name="ddlFrequency" id="ddlFrequency">
    <option selected="selected" value="">Select a frequency</option>
		<option value="Daily">Daily</option>
		<option value="Weekly">Weekly</option>
		<option value="Every two weeks">Every two weeks</option>
		<option value="Monthly">Monthly</option>
		<option value="Every two months">Every two months</option>
		<option value="Quarterly">Quarterly</option>
		<option value="Every six months">Every six months</option>
		<option value="Yearly">Yearly</option>
		<option value="One time">One time</option>
    </select>    </td>
  </tr>
  <tr>
    <td>Next date 	Choose<br /></td>
    <td><script>DateInput('txtNextDate', true, 'YYYY-MM-DD')</script>&nbsp;</td>
  </tr>
  <tr>
    <td valign="top">Duration <br /></td>
    <td><input name="rdDuration" type="radio" id="rd" value="No end date" checked="checked" />
      No end date<br />
      <input type="radio" name="rdDuration" id="rd2" value="End after occurrences" />
      End after 
  <input name="txtPropertyName3" type="text" class="textbox" id="txtPropertyName3" size="10" />
  occurrences<br />
  <input type="radio" name="rdDuration" id="rd3" value="End by 10" />
  End by 10</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" align="center"><input name="submit" type="submit" class="submit_button" id="submit" value="Save" /></td>
    </tr>
</table>
</form>
</body>
</html>
