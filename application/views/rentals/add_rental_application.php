<?php
/*include ("../includes/classes/class.db.php");
include ("../includes/functions/general.php");*/

//-------------------------------------------------------------------------------------------
								/*Objects*/
//-------------------------------------------------------------------------------------------					
	/*$db = new db();*/
	//isLoggedUser();
//-------------------------------------------------------------------------------------------
								/*General Coding Area*/
	/*$isAdded = false;
	if ($_POST){
		$sqlInsert = "INSERT INTO rental_applications ( 
			details_property,
			details_number_of_units,
			details_first_name,
			details_last_name,
			details_notes,
			application_property,
			application_number_of_units,
			application_status,
			general_first_name,
			general_last_name,
			general_email,
			general_ssn,
			general_phone,
			general_date_of_birth,
			res_address,
			res_city,
			res_state,
			res_postal_code,
			res_landlord,
			res_monthly_rent,
			res_date_from,
			res_date_to,
			res_leaving_reason,
			res_landlord_phone,
			employer_name,
			employer_city,
			employer_phone,
			employer_date_from,
			employer_date_to,
			employer_gross_pay,
			employer_occupation,
			ref_name_1,
			ref_name_1_phone,
			ref_name_2,
			ref_name_2_phone,
			ref_gross_pay,
			ref_occupation,
			other_comments	
		) VALUES (
			'".clearFormData("ddlPropertyType")."',
			'".clearFormData("ddlNumberOfUnits")."',
			'".clearFormData("txtFirstName")."',
			'".clearFormData("txtLastName")."',
			'".clearFormData("txtNotes")."',
			'".clearFormData("ddlAppPropertyType")."',
			'".clearFormData("ddlAppNumberOfUnits")."',
			'".clearFormData("ddlAppStatus")."',
			'".clearFormData("txtGeneralFirstName")."',
			'".clearFormData("txtGeneralLastName")."',
			'".clearFormData("txtGeneralEmail")."',
			'".clearFormData("txtGeneralSSN")."',
			'".clearFormData("txtGeneralPhone")."',
			'".clearFormData("txtGeneralBirthDate")."',
			'".clearFormData("txtResAddress")."',
			'".clearFormData("txtResCity")."',
			'".clearFormData("ddlResState")."',
			'".clearFormData("txtResPostalCode")."',
			'".clearFormData("txtResLandlord")."',
			'".clearFormData("txtResMonhtlyRent")."',
			'".clearFormData("txtResDateFrom")."',
			'".clearFormData("txtResDateTo")."',
			'".clearFormData("txtResReason")."',
			'".clearFormData("txtResLandlordPhone")."',
			'".clearFormData("txtEmployerName")."',
			'".clearFormData("txtEmployerCity")."',
			'".clearFormData("txtEmployerPhone")."',
			'".clearFormData("txtEmployerDateFrom")."',
			'".clearFormData("txtEmployerDateTo")."',
			'".clearFormData("txtEmployerGross")."',
			'".clearFormData("txtEmployerOccupation")."',
			'".clearFormData("txtRefName_1")."',
			'".clearFormData("txtRefPhone_1")."',
			'".clearFormData("txtRefName_2")."',
			'".clearFormData("txtRefPhone_2")."',
			'".clearFormData("txtRefGross")."',
			'".clearFormData("txtRefOccupation")."',
			'".clearFormData("txtOtherComments")."'
		)";	
		$db->query($sqlInsert);
		$isAdded = true;
	}
	
*/?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Rental Application</title>
<link href="<?php echo base_url();?>css/styles_popup.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url();?>js/calendarDateInput.js" type="text/javascript"></script>
</head>

<body>
<form action="<?php echo base_url();?>rentals/create_application" method="post">
<div id="wrapper_header"><a href="#" onclick="return confirm('Do you want to cancel?');"><img src="<?php echo base_url();?>/images/btn_cancel.png" alt="Cancel" width="109" height="34" border="0" style="float:right" /></a>
  <input type="image" name="submit2" id="submit2" src="<?php echo base_url();?>/images/btn_save.png" />
</div>
<p>&nbsp;</p>
<?php /*if ($isAdded) { echo '<div class="msg_success">Rental Application is added successfully.</div>'; } */?>
<table width="738" border="0" align="center" cellpadding="2" cellspacing="3">
  <tr>
    <td colspan="2"><h1>Rental Application</h1></td>
    </tr>
  <tr>
    <td colspan="2"><h2>Details:</h2></td>
    </tr>
  <tr>
    <td>Property</td>
    <td><select name="ddlPropertyType" id="ddlPropertyType">
      <option value="Residential Rental">Residential Rental</option>
      <option value="Commercial Rental">Commercial Rental</option>
    </select></td>
  </tr>
  <tr>
    <td>Unit</td>
    <td><select name="ddlNumberOfUnits" id="ddlNumberOfUnits">
      <option value="1">1</option>
      <option value="2">2</option>
      <option value="3">3</option>
      <option value="4">4</option>
      <option value="5">5</option>
      <option value="6">6</option>
      <option value="7">7</option>
      <option value="8">8</option>
      <option value="9">9</option>
      <option value="10">10</option>
      <option value="11">11</option>
      <option value="12">12</option>
      <option value="13">13</option>
      <option value="14">14</option>
      <option value="15">15</option>
        </select></td>
  </tr>
  <tr>
    <td width="188">First name</td>
    <td width="370"><input name="txtFirstName" type="text" class="textbox" id="txtFirstName" /></td>
  </tr>
  <tr>
    <td>Last name</td>
    <td><input name="txtLastName" type="text" class="textbox" id="txtPropertyName35" /></td>
  </tr>
  <tr>
    <td colspan="2"><h2>Notes:</h2></td>
    </tr>
  <tr>
    <td colspan="2"><textarea name="txtNotes" cols="95" rows="5" class="textarea" id="txtNotes"></textarea></td>
    </tr>
  <tr>
    <td colspan="2"><h2>Application Information</h2></td>
    </tr>
  <tr>
    <td>Property</td>
    <td><select name="ddlAppPropertyType" id="ddlAppPropertyType">
      <option value="Residential Rental">Residential Rental</option>
      <option value="Commercial Rental">Commercial Rental</option>
    </select></td>
  </tr>
  <tr>
    <td>Unit</td>
    <td>
    <select name="ddlAppNumberOfUnits" id="ddlAppNumberOfUnits">
      <option value="1">1</option>
      <option value="2">2</option>
      <option value="3">3</option>
      <option value="4">4</option>
      <option value="5">5</option>
      <option value="6">6</option>
      <option value="7">7</option>
      <option value="8">8</option>
      <option value="9">9</option>
      <option value="10">10</option>
      <option value="11">11</option>
      <option value="12">12</option>
      <option value="13">13</option>
      <option value="14">14</option>
      <option value="15">15</option>
    </select>
    </td>
  </tr>
  <tr>
    <td>Status</td>
    <td><select name="ddlAppStatus" id="ddlAppStatus">
      <option value="Undecided">Undecided</option>
      <option value="Defered">Defered</option>
      <option value="Approved">Approved</option>
      <option value="Cancelled">Cancelled</option>
      <option value="Rejected">Rejected</option>
        </select></td>
  </tr>
  <tr>
    <td colspan="2"><h2>General Information</h2></td>
    </tr>
  <tr>
    <td colspan="2"><table width="100%" border="0" cellspacing="2" cellpadding="0">
      <tr>
        <td>First Name</td>
        <td><input name="txtGeneralFirstName" type="text" class="textbox" id="txtGeneralFirstName" /></td>
        <td>Last Name</td>
        <td><input name="txtGeneralLastName" type="text" class="textbox" id="txtGeneralLastName" /></td>
      </tr>
      <tr>
        <td>Email</td>
        <td><input name="txtGeneralEmail" type="text" class="textbox" id="txtGeneralEmail" /></td>
        <td>SSN</td>
        <td><input name="txtGeneralSSN" type="text" class="textbox" id="txtGeneralSSN" /></td>
      </tr>
      <tr>
        <td>Phone Number</td>
        <td><input name="txtGeneralPhone" type="text" class="textbox" id="txtGeneralPhone" /></td>
        <td>Birth Date</td>
        <td><script>DateInput('txtGeneralBirthDate', true, 'YYYY-MM-DD')</script>&nbsp;</td>
      </tr>
    </table></td>
    </tr>
  <tr>
    <td colspan="2"><h2>Residence and rental history</h2></td>
    </tr>
  <tr>
    <td colspan="2"><table width="100%" border="0" cellspacing="2" cellpadding="0">
      <tr>
        <td width="19%">Address</td>
        <td width="33%"><input name="txtResAddress" type="text" class="textbox" id="txtResAddress" /></td>
        <td width="14%">City/Locality</td>
        <td width="34%"><input name="txtResCity" type="text" class="textbox" id="txtPropertyName16" /></td>
      </tr>
      <tr>
        <td>State/Province</td>
        <td><select name="ddlResState" id="ddlResState">
          <option value="AE">AE</option>
          <option value="AK">AK</option>
          <option value="AL">AL</option>
          <option value="AP">AP</option>
          <option value="AR">AR</option>
          <option value="AZ">AZ</option>
          <option value="CA">CA</option>
          <option value="CO">CO</option>
          <option value="CT">CT</option>
          <option value="DC">DC</option>
          <option value="DE">DE</option>
          <option value="FL">FL</option>
          <option value="GA">GA</option>
          <option value="HI">HI</option>
          <option value="IA">IA</option>
          <option value="ID">ID</option>
          <option value="IL">IL</option>
          <option value="IN">IN</option>
          <option value="KS">KS</option>
          <option value="KY">KY</option>
          <option value="LA">LA</option>
          <option value="MA">MA</option>
          <option value="MD">MD</option>
          <option value="ME">ME</option>
          <option value="MI">MI</option>
          <option value="MN">MN</option>
          <option value="MO">MO</option>
          <option value="MS">MS</option>
          <option value="MT">MT</option>
          <option value="NC">NC</option>
          <option value="ND">ND</option>
          <option value="NE">NE</option>
          <option value="NH">NH</option>
          <option value="NJ">NJ</option>
          <option value="NM">NM</option>
          <option value="NV">NV</option>
          <option value="NY">NY</option>
          <option value="OH">OH</option>
          <option value="OK">OK</option>
          <option value="OR">OR</option>
          <option value="PA">PA</option>
          <option value="RI">RI</option>
          <option value="SC">SC</option>
          <option value="SD">SD</option>
          <option value="TN">TN</option>
          <option value="TX">TX</option>
          <option value="UT">UT</option>
          <option value="VA">VA</option>
          <option value="VT">VT</option>
          <option value="WA">WA</option>
          <option value="WI">WI</option>
          <option value="WV">WV</option>
          <option value="WY">WY</option>
        </select></td>
        <td>Zip/Postal code</td>
        <td><input name="txtResPostalCode" type="text" class="textbox" id="txtPropertyName18" /></td>
      </tr>
      <tr>
        <td>Landlord/Manager Name</td>
        <td><input name="txtResLandlord" type="text" class="textbox" id="txtPropertyName19" /></td>
        <td>Monthly Rent</td>
        <td><input name="txtResMonhtlyRent" type="text" class="textbox" id="txtPropertyName20" /></td>
      </tr>
      <tr>
        <td>Dates of Residency (From/To)</td>
        <td>From: <script>DateInput('txtResDateFrom', true, 'YYYY-MM-DD')</script> 
      To: <script>DateInput('txtResDateTo', true, 'YYYY-MM-DD')</script>
      &nbsp;</td>
        <td>Reason for Leaving</td>
        <td><input name="txtResReason" type="text" class="textbox" id="txtPropertyName22" /></td>
      </tr>
      <tr>
        <td>Landlord/Manager Phone</td>
        <td><input name="txtResLandlordPhone" type="text" class="textbox" id="txtPropertyName21" /></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
    </table></td>
    </tr>
  <tr>
    <td colspan="2"><h2>Employment and income history</h2></td>
    </tr>
  <tr>
    <td colspan="2"><table width="100%" border="0" cellspacing="2" cellpadding="0">
      <tr>
        <td width="19%">Employer Name</td>
        <td width="33%"><input name="txtEmployerName" type="text" class="textbox" id="txtPropertyName23" /></td>
        <td width="14%">City</td>
        <td width="34%"><input name="txtEmployerCity" type="text" class="textbox" id="txtPropertyName24" /></td>
      </tr>
      <tr>
        <td>Employer Phone</td>
        <td><input name="txtEmployerPhone" type="text" class="textbox" id="txtPropertyName25" /></td>
        <td>Dates of Employment (From/To)</td>
        <td>From: <script>DateInput('txtEmployerDateFrom', true, 'YYYY-MM-DD')</script> 
      To: <script>DateInput('txtEmployerDateTo', true, 'YYYY-MM-DD')</script>
      &nbsp;</td>
      </tr>
      <tr>
        <td>Monthly Gross Pay</td>
        <td><input name="txtEmployerGross" type="text" class="textbox" id="txtPropertyName27" /></td>
        <td>Occupation</td>
        <td><input name="txtEmployerOccupation" type="text" class="textbox" id="txtPropertyName28" /></td>
      </tr>

    </table></td>
    </tr>
  <tr>
    <td colspan="2"><h2>References</h2></td>
    </tr>
  <tr>
    <td colspan="2"><table width="100%" border="0" cellspacing="2" cellpadding="0">
      <tr>
        <td width="19%">Name 1</td>
        <td width="33%"><input name="txtRefName_1" type="text" class="textbox" id="txtPropertyName29" /></td>
        <td width="14%">Name 2</td>
        <td width="34%"><input name="txtRefName_2" type="text" class="textbox" id="txtPropertyName30" /></td>
      </tr>
      <tr>
        <td>Phone 1</td>
        <td><input name="txtRefPhone_1" type="text" class="textbox" id="txtPropertyName31" /></td>
        <td>Phone 2</td>
        <td><input name="txtRefPhone_2" type="text" class="textbox" id="txtPropertyName32" /></td>
      </tr>
      <tr>
        <td>Monthly Gross Pay</td>
        <td><input name="txtRefGross" type="text" class="textbox" id="txtPropertyName33" /></td>
        <td>Occupation</td>
        <td><input name="txtRefOccupation" type="text" class="textbox" id="txtPropertyName34" /></td>
      </tr>
    </table></td>
    </tr>
  <tr>
    <td colspan="2"><h2>Other Comments:</h2></td>
    </tr>
  <tr>
    <td colspan="2" align="center"><textarea name="txtOtherComments" cols="95" rows="5" class="textarea" id="txtOtherComments"></textarea></td>
    </tr>
  <tr>
    <td colspan="2" align="center"><input name="submit" type="submit" class="submit_button" id="submit" value="Save" /></td>
    </tr>
</table>
<p><br />
</p>
</form>
</body>
</html>
