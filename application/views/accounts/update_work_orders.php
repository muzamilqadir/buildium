<?php
include ("../includes/classes/class.db.php");
include ("../includes/functions/general.php");
//-------------------------------------------------------------------------------------------
								/*Objects*/
//-------------------------------------------------------------------------------------------					
	$db = new db();
	//isLoggedUser();
//-------------------------------------------------------------------------------------------
								/*General Coding Area*/
	$wid = "0";
	if (isset($_GET['id']) && $_GET['id'] != "" ) {
		$wid = $_GET['id'];
	}							
	//---------------------------------------------------------------------------------------
	
	$isUpdated = false;
	
	if ($_POST){
		$sqlUpdate = "UPDATE work_orders SET 
		subject = '".clearFormData("txtSubject")."',
		description = '".clearFormData("txtDescription")."',
		property = '".clearFormData("ddlProperty")."',
		units = '".clearFormData("ddlUnit")."',
		assigned_to = '".clearFormData("ddlAssignedTo")."',
		priority = '".clearFormData("ddlPriority")."',
		category = '".clearFormData("ddlCategory")."',
		due_date = '".clearFormData("txtDueDate")."',
		request_type = '".clearFormData("rdRequestType")."',
		contact_name = '".clearFormData("txtContactName")."',
		email = '".clearFormData("txtEmail")."',
		home = '".clearFormData("txtHome")."',
		work = '".clearFormData("txtWork")."',
		mobile = '".clearFormData("txtMobile")."',
		vendor = '".clearFormData("ddlVendor")."',
		invoice_number = '".clearFormData("ddlInvoiceNumber")."',
		rechargable_to = '".clearFormData("txtRechargableTo")."',
		entry_allowed = '".clearFormData("ddlEntryAllowed")."',
		entry_notes = '".clearFormData("txtEntryNotes")."',
		work_perform_vendor = '".clearFormData("txtVendorWork")."',
		vendor_notes = '".clearFormData("txtVendorNotes")."'
		WHERE 
		work_order_id = '".$wid."'";

		$db->query($sqlUpdate);
		
		$isUpdated = true;
	}
	
	$sqlSelect = "SELECT * FROM work_orders WHERE work_order_id = '".$wid."'";
	
	$query= $db->ExeQuersys($sqlSelect);
		
	while ($row = mysql_fetch_array($query)) {
		$subject = $row['subject'];
		$description = $row['description'];
		$property = $row['property'];
		$units = $row['units'];
		$assigned_to = $row['assigned_to'];
		$priority = $row['priority'];
		$category = $row['category'];
		$due_date = $row['due_date'];
		$request_type = $row['request_type'];
		$contact_name = $row['contact_name'];
		$email = $row['email'];
		$home = $row['home'];
		$work = $row['work'];
		$mobile = $row['mobile'];
		$vendor = $row['vendor'];
		$invoice_number = $row['invoice_number'];
		$rechargable_to = $row['rechargable_to'];
		$entry_allowed = $row['entry_allowed'];
		$entry_notes = $row['entry_notes'];
		$work_perform_vendor = $row['work_perform_vendor'];
		$vendor_notes = $row['vendor_notes'];
	}
		
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Update Work Orders</title>
<link href="../styles_popup.css" rel="stylesheet" type="text/css" />
<script src="../calendarDateInput.js" type="text/javascript"></script>
<script src="../SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<link href="../SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
</head>

<body>
<form action="" method="post" enctype="multipart/form-data">
<div id="wrapper_header"><a href="#" onclick="return confirm('Do you want to cancel?');"><img src="../images/btn_cancel.png" alt="Cancel" width="109" height="34" border="0" style="float:right" /></a>
  <input type="image" name="submit2" id="submit2" src="../images/btn_save.png" />
</div>
<p>&nbsp;</p>
<?php if ($isUpdated) { echo '<div class="msg_success">Work Order is udpated successfully.</div>'; } ?>
<table width="664" border="0" align="center" cellpadding="2" cellspacing="3">
  <tr>
    <td colspan="2"><h1>Update Work Orders</h1></td>
    </tr>
  <tr>
    <td colspan="2"><h2>Details:</h2></td>
    </tr>
  <tr>
    <td width="98">Subject</td>
    <td width="549"><span id="sprytextfield1">
      <input name="txtSubject" type="text" class="textbox" id="txtSubject" size="80" value="<?php echo $subject; ?>" />
      <span class="textfieldRequiredMsg">*</span></span></td>
  </tr>
  <tr>
    <td valign="top">Description</td>
    <td><textarea name="txtDescription" cols="77" rows="5" class="textarea" id="txtDescription"><?php echo $description; ?></textarea></td>
  </tr>
  <tr>
    <td colspan="2"><table width="100%" border="0" cellspacing="2" cellpadding="0">
      <tr>
        <td width="16%" valign="top">Property</td>
        <td width="40%" valign="top"><select name="ddlProperty" id="ddlProperty">
          <option value="Residential Rental" <?php if ($property == "Residential Rental") { echo 'selected="selected"'; } ?> >Residential Rental</option>
          <option value="Commercial Rental" <?php if ($property == "Commercial Rental") { echo 'selected="selected"'; } ?> >Commercial Rental</option>
        </select></td>
        <td width="14%" valign="top">Request Type</td>
        <td width="30%" valign="top"><input name="rdRequestType" type="radio" id="rd" value="To do" checked="checked" />
          To do</td>
      </tr>
      <tr>
        <td valign="top">Unit</td>
        <td valign="top"><select name="ddlUnit" id="ddlUnit">
          <option value="1" <?php if ($units == "1") { echo 'selected="selected"'; } ?> >1</option>
          <option value="2" <?php if ($units == "2") { echo 'selected="selected"'; } ?> >2</option>
          <option value="3" <?php if ($units == "3") { echo 'selected="selected"'; } ?> >3</option>
          <option value="4" <?php if ($units == "4") { echo 'selected="selected"'; } ?> >4</option>
          <option value="5" <?php if ($units == "5") { echo 'selected="selected"'; } ?> >5</option>
          <option value="6" <?php if ($units == "6") { echo 'selected="selected"'; } ?> >6</option>
          <option value="7" <?php if ($units == "7") { echo 'selected="selected"'; } ?> >7</option>
          <option value="8" <?php if ($units == "8") { echo 'selected="selected"'; } ?> >8</option>
          <option value="9" <?php if ($units == "9") { echo 'selected="selected"'; } ?> >9</option>
          <option value="10" <?php if ($units == "10") { echo 'selected="selected"'; } ?> >10</option>
          <option value="11" <?php if ($units == "11") { echo 'selected="selected"'; } ?> >11</option>
          <option value="12" <?php if ($units == "12") { echo 'selected="selected"'; } ?> >12</option>
          <option value="13" <?php if ($units == "13") { echo 'selected="selected"'; } ?> >13</option>
          <option value="14" <?php if ($units == "14") { echo 'selected="selected"'; } ?> >14</option>
          <option value="15" <?php if ($units == "15") { echo 'selected="selected"'; } ?> >15</option> 
        </select></td>
        <td valign="top">&nbsp;</td>
        <td valign="top"><input type="radio" name="rdRequestType" id="rd2" value="Prospect request" <?php if($request_type == "Prospect request") { echo 'checked="checked"';} ?> />
Prospect request</td>
      </tr>
      <tr>
        <td valign="top">Attachment</td>
        <td valign="top"><input name="attach" type="file" class="textbox" id="attach" /></td>
        <td valign="top">&nbsp;</td>
        <td valign="top"><input type="radio" name="rdRequestType" id="rd3" value="Resident request" <?php if($request_type == "Resident request") { echo 'checked="checked"';} ?> />
Resident request</td>
      </tr>
      <tr>
        <td valign="top">Assigned To</td>
        <td valign="top">
        <select name="ddlAssignedTo" id="ddlAssignedTo">
          <option value="1" <?php if ($assigned_to == "1") { echo 'selected="selected"'; } ?> >Nameone</option>
          <option value="2" <?php if ($assigned_to == "2") { echo 'selected="selected"'; } ?> >Nametwo</option>
        </select></td>
        <td valign="top">&nbsp;</td>
        <td valign="top"><input type="radio" name="rdRequestType" id="rd4" value="Rental owner request" <?php if($request_type == "Rental owner request") { echo 'checked="checked"';} ?> />
Rental owner request</td>
      </tr>
      <tr>
        <td valign="top">Priority</td>
        <td valign="top"><select name="ddlPriority" id="ddlPriority">
          <option value="Low" <?php if ($priority == "Low") { echo 'selected="selected"'; } ?> >Low</option>
          <option value="Normal" <?php if ($priority == "Normal") { echo 'selected="selected"'; } ?> >Normal</option>
          <option value="High" <?php if ($priority == "High") { echo 'selected="selected"'; } ?> >High</option>
                </select></td>
        <td valign="top">Contact Name</td>
        <td valign="top"><input name="txtContactName" type="text" class="textbox" id="txtPropertyName3" value="<?php echo $contact_name; ?>" /></td>
      </tr>
      <tr>
        <td valign="top">Category</td>
        <td valign="top"><select name="ddlCategory" id="ddlCategory">
          <option value="Uncategorized" <?php if($category == "Uncategorized") { echo 'selected="selected"';} ?> >Uncategorized</option>
          <option value="Complaint" <?php if($category == "Complaint") { echo 'selected="selected"';} ?> >Complaint</option>
          <option value="Feedback/Suggestion" <?php if($category == "Feedback/Suggestion") { echo 'selected="selected"';} ?> >Feedback/Suggestion</option>
          <option value="General Inquiry" <?php if($category == "General Inquiry") { echo 'selected="selected"';} ?> >General Inquiry</option>
          <option value="Maintence Request" <?php if($category == "Maintence Request") { echo 'selected="selected"';} ?> >Maintence Request</option>
          <option>Other</option>
                </select></td>
        <td valign="top">Email</td>
        <td valign="top"><input name="txtEmail" type="text" class="textbox" id="txtPropertyName4" value="<?php echo $email; ?>" /></td>
      </tr>
      <tr>
        <td valign="top">Due Date</td>
        <td valign="top"><script>DateInput('txtDueDate', true, 'YYYY-MM-DD', '<?php echo $due_date; ?>')</script></td>
        <td valign="top">Home</td>
        <td valign="top"><input name="txtHome" type="text" class="textbox" id="txtHome" value="<?php echo $home; ?>" /></td>
      </tr>
      <tr>
        <td valign="top">&nbsp;</td>
        <td valign="top">&nbsp;</td>
        <td valign="top">Work</td>
        <td valign="top"><input name="txtWork" type="text" class="textbox" id="txtPropertyName8" value="<?php echo $work; ?>" /></td>
      </tr>
      <tr>
        <td valign="top">&nbsp;</td>
        <td valign="top">&nbsp;</td>
        <td valign="top">Mobile</td>
        <td valign="top"><input name="txtMobile" type="text" class="textbox" id="txtPropertyName17" value="<?php echo $mobile; ?>" /></td>
      </tr>
    </table></td>
    </tr>
  <tr>
    <td colspan="2"><h2>Work Order Details:</h2></td>
    </tr>
  <tr>
    <td colspan="2"><table width="100%" border="0" cellspacing="2" cellpadding="0">
      <tr>
        <td width="16%" valign="top">Vendor</td>
        <td colspan="3" valign="top"><select name="ddlVendor" id="ddlVendor">
    <option value="1" <?php if ($vendor == "1") { echo 'selected="selected"'; } ?> >Select Vendor</option>
                        </select> 
        [ <a href="add_vendors.php">Add Vendor</a> ]</td>
        </tr>
      <tr>
        <td valign="top">Invoice Number</td>
        <td width="37%" valign="top"><select name="ddlInvoiceNumber" id="ddlPropertyType9">
            <option value="1" <?php if ($invoice_number == "1") { echo 'selected="selected"'; } ?> >-- Select --</option>
        </select></td>
        <td width="15%" valign="top">Entry Allowed?</td>
        <td width="32%" valign="top"><select name="ddlEntryAllowed" id="ddlPropertyType12">
          <option value="Unknown" <?php if ($entry_allowed == "Unknown") { echo 'selected="selected"'; } ?> >Unknown</option>
          <option value="Yes" <?php if ($entry_allowed == "Yes") { echo 'selected="selected"'; } ?> >Yes</option>
          <option value="No" <?php if ($entry_allowed == "No") { echo 'selected="selected"'; } ?> >No</option>
                </select></td>
      </tr>
      <tr>
        <td valign="top">Rechargable to</td>
        <td valign="top"><input name="txtRechargableTo" type="text" class="textbox" id="txtPropertyName36" value="<?php echo $rechargable_to; ?>" /></td>
        <td valign="top">Entry Notes</td>
        <td valign="top"><textarea name="txtEntryNotes" cols="20" rows="3" class="textarea" id="txtEntryNotes"><?php echo $entry_notes; ?></textarea></td>
      </tr>
      <tr>
        <td valign="top">Work to be performed by vendor</td>
        <td colspan="3" valign="top"><textarea name="txtVendorWork" cols="77" rows="3" class="textarea" id="txtVendorWork"><?php echo $work_perform_vendor; ?></textarea></td>
        </tr>
      <tr>
        <td valign="top">Vendor notes</td>
        <td colspan="3" valign="top"><textarea name="txtVendorNotes" cols="77" rows="3" class="textarea" id="txtVendorNotes"><?php echo $vendor_notes; ?></textarea></td>
        </tr>

    </table></td>
    </tr>
  <tr>
    <td colspan="2"><h2>Parts and Labor</h2></td>
    </tr>
  <tr>
    <td colspan="2"><table width="100%" border="0" bgcolor="#FFFFFF">
      <tr>
        <td height="26" bgcolor="#FFDFEF">Qty 	</td>
        <td bgcolor="#FFDFEF">Account 	</td>
        <td bgcolor="#FFDFEF">Memo 	</td>
        <td bgcolor="#FFDFEF">Price 	</td>
        <td bgcolor="#FFDFEF">Total</td>
      </tr>
      <tr>
        <td height="39"><input name="txtPropertyName3" type="text" class="textbox" id="txtPropertyName7" size="3" /></td>
        <td><select name="ddlPropertyType10" id="ddlPropertyType10">
          <option>Select</option>
                </select></td>
        <td><input name="txtPropertyName22" type="text" class="textbox" id="txtPropertyName37" /></td>
        <td><input name="txtPropertyName23" type="text" class="textbox" id="txtPropertyName38" /></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td colspan="5"><strong>Total:</strong></td>
        </tr>
    </table></td>
    </tr>
  <tr>
    <td colspan="2" align="center"><input name="submit" type="submit" class="submit_button" id="submit" value="Update" /></td>
    </tr>
</table>
</form>
<script type="text/javascript">
<!--
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1");
//-->
</script>
</body>
</html>
