<?php
class Ownership_Account extends CI_Model{
    function owner_info(){
        $query = "SELECT * FROM assoc_ownership";
        return $this->db->query($query)->result_array();

    }
    function add_ownership_account($saveAccountData){
        $query = "INSERT INTO assoc_ownership(association_name,number_of_units,
        move_in_date,association_fee,  frequency,owner_info,first_name,
        last_name,login_email,alternate_email,home,
        work,mobile,fax,country,address_1,address_2,address_3,city,
        state,postal_code,alt_country,alt_address_1,alt_address_2,
        alt_address_3,alt_city,alt_state,alt_postal_code,
        mailing_pref,additional_comments,additional_birth_date,
        emergency_name,emergency_phone,
        is_board_member,is_occupancy) VALUES('$saveAccountData[txtAssociationName]',
                '$saveAccountData[ddlNumberOfUnits]','$saveAccountData[txtMoveInDate]',
                '$saveAccountData[txtAssociationFee]','$saveAccountData[ddlFrequency]','$saveAccountData[rdOwnerInfo]',
                '$saveAccountData[txtFirstName]','$saveAccountData[txtLastName]','$saveAccountData[txtLoginEmail]',
			    '$saveAccountData[txtAlternateEmail]','$saveAccountData[txtHome]',
                '$saveAccountData[txtWork]','$saveAccountData[txtMobile]',
                '$saveAccountData[txtFax]','$saveAccountData[ddlCountries]','$saveAccountData[txtAddress_1]',
                '$saveAccountData[txtAddress_2]','$saveAccountData[txtAddress_3]','$saveAccountData[txtCity]',
			    '$saveAccountData[ddlState]',
			    '$saveAccountData[txtPostalCode]',
                '$saveAccountData[ddlAltCountries]','$saveAccountData[txtAltAddress_1]',
                '$saveAccountData[txtAltAddress_2]','$saveAccountData[txtAltAddress_3]','$saveAccountData[txtAltCity]',
                '$saveAccountData[ddlAltState]','$saveAccountData[txtAltZipcode]','$saveAccountData[rdMailingRef]',
			    '$saveAccountData[txtComments]','$saveAccountData[txtBirthDate]',
                '$saveAccountData[txtEmergencyName]','$saveAccountData[txtEmergencyPhone]',
                '$saveAccountData[chkBoardMember]','$saveAccountData[chkOccupancy]')";

        if($this->db->query($query) > 0){
            return "successfully Inserted";
        }
        return "Error occurred";

    }
}
