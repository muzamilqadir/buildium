<?php
class Leases extends CI_Model{
    function leases_info(){
        $query = "SELECT * FROM tenant_leases";
        return $this->db->query($query)->result_array();

    }
    function add_leases($leasesData){
        $query  = "INSERT INTO tenant_leases(property_name,type,number_of_units,date_to,
              date_from,rent_due_day,frequency,rent,prepaid_rent,sec_dep,
              tenant_entry,first_name,   last_name,primary_email,alternate_email,home,work,mobile,fax,
              country,address_1,address_2,address_3,city,state,postal_code,  alt_country,alt_address_1,alt_address_2,
              alt_address_3,alt_city,alt_postal_code,mailing_pref,comments,date_of_birth,emergency_contact_name,emergency_contact_phone)  VALUES('$leasesData[propertyName]',
             '$leasesData[type]', '$leasesData[unitNumber]','$leasesData[date_from]',
              '$leasesData[date_to]','$leasesData[rentDue]',
			'$leasesData[frequency]','$leasesData[txtRent]','$leasesData[txtPrepaidRent]',
			'$leasesData[txtSecurityDeposit]','$leasesData[rdTenant]','$leasesData[txtFirstName]',
			'$leasesData[txtLastName]','$leasesData[txtPrimaryEmail]','$leasesData[txtAlternateEmail]',
            '$leasesData[txtHome]','$leasesData[txtWork]','$leasesData[txtMobile]','$leasesData[txtFax]',
            '$leasesData[countries]','$leasesData[txtAddress_1]','$leasesData[txtAddress_2]','$leasesData[txtAddress_3]',
           '$leasesData[txtCity]','$leasesData[state]','$leasesData[txtPostalCode]','$leasesData[ddlAltCountry]',
           '$leasesData[txtAltAddress_1]','$leasesData[txtAltAddress_2]','$leasesData[txtAltAddress_3]',
           '$leasesData[txtAltCity]','$leasesData[txtAltZipcode]','$leasesData[rdMailing]','$leasesData[txtComments]','$leasesData[birth_date]',
            '$leasesData[txtEmergencyName]','$leasesData[txtEmergencyPhone]')";


        if($this->db->query($query) > 0){
            return "successfully Inserted";
        }
        return "Error occurred";
    }

}
