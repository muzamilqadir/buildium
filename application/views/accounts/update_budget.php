<?php
include ("../includes/classes/class.db.php");
include ("../includes/functions/general.php");
//-------------------------------------------------------------------------------------------
								/*Objects*/
//-------------------------------------------------------------------------------------------					
	$db = new db();
	//isLoggedUser();
//-------------------------------------------------------------------------------------------
								/*General Coding Area*/
	$bid = "0";
	if (isset($_GET['id']) && $_GET['id'] != "" ) {
		$bid = $_GET['id'];
	}							
	//---------------------------------------------------------------------------------------
	
	$isUpdated = false;
	
	if ($_POST){
		$sqlUpdate = "UPDATE budget SET
		property = '".clearFormData("ddlProperty")."', 
		budget_name = '".clearFormData("txtBudgetName")."', 
		fiscal_year_start = '".clearFormData("ddlFiscalYearStart")."', 
		fiscal_year = '".clearFormData("ddlFiscalYear")."', 
		copy_amount_from = '".clearFormData("rdCopyAmount")."'
		WHERE budget_id = '".$bid."'";
		
		$db->query($sqlUpdate);
		$isUpdated = true;
	}
	
	$sqlSelect = "SELECT * FROM budget WHERE budget_id = '".$bid."'";
	
	$query= $db->ExeQuersys($sqlSelect);
		
	while ($row = mysql_fetch_array($query)) {
		$property = $row['property'];
		$budget_name = $row['budget_name'];
		$fiscal_year_start = $row['fiscal_year_start'];
		$fiscal_year = $row['fiscal_year'];
		$copy_amount_from = $row['copy_amount_from'];
	}
	
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Update Budget</title>
<link href="../styles_popup.css" rel="stylesheet" type="text/css" />
</head>

<body>
<form action="" method="post">
<div id="wrapper_header"><a href="#" onclick="return confirm('Do you want to cancel?');"><img src="../images/btn_cancel.png" alt="Cancel" width="109" height="34" border="0" style="float:right" /></a>
  <input type="image" name="submit2" id="submit2" src="../images/btn_save.png" />
</div>
<p>&nbsp;</p>
<?php if ($isUpdated) { echo '<div class="msg_success">Budget is updated successfully.</div>'; } ?>
<table width="632" border="0" align="center" cellpadding="2" cellspacing="3">
  <tr>
    <td colspan="3"><h1>Add Budget</h1></td>
    </tr>
  <tr>
    <td colspan="3"><h2>Budget Information</h2></td>
    </tr>
  <tr>
    <td width="184">Property </td>
    <td width="474" colspan="2"><select name="ddlProperty" id="ddlProperty">
      <option value="Residential Area" <?php if ($property == "Residential Area") { echo 'selected="selected"'; } ?> >Residential Area</option>
      <option value="Commercial Area" <?php if ($property == "Commercial Area") { echo 'selected="selected"'; } ?> >Commercial Area</option>
    </select></td>
  </tr>
  <tr>
    <td>Budget Name</td>
    <td colspan="2"><input name="txtBudgetName" type="text" class="textbox" id="txtBudgetName" size="35" value="<?php echo $budget_name; ?>" /></td>
  </tr>
  <tr>
    <td>Fiscal year start </td>
    <td colspan="2">
    <select name="ddlFiscalYearStart" id="ddlFiscalYearStart">
    <option value="January" <?php if ($fiscal_year_start == "January") { echo 'selected="selected"'; } ?> >January</option>
	<option value="February" <?php if ($fiscal_year_start == "February") { echo 'selected="selected"'; } ?> >February</option>
	<option value="March" <?php if ($fiscal_year_start == "March") { echo 'selected="selected"'; } ?> >March</option>
	<option value="April" <?php if ($fiscal_year_start == "April") { echo 'selected="selected"'; } ?> >April</option>
	<option value="May" <?php if ($fiscal_year_start == "May") { echo 'selected="selected"'; } ?> >May</option>
	<option value="June" <?php if ($fiscal_year_start == "June") { echo 'selected="selected"'; } ?> >June</option>
	<option value="July" <?php if ($fiscal_year_start == "July") { echo 'selected="selected"'; } ?> >July</option>
	<option value="August" <?php if ($fiscal_year_start == "August") { echo 'selected="selected"'; } ?> >August</option>
	<option value="September" <?php if ($fiscal_year_start == "September") { echo 'selected="selected"'; } ?> >September</option>
	<option value="October" <?php if ($fiscal_year_start == "October") { echo 'selected="selected"'; } ?> >October</option>
	<option value="November" <?php if ($fiscal_year_start == "November") { echo 'selected="selected"'; } ?> >November</option>
	<option value="December" <?php if ($fiscal_year_start == "December") { echo 'selected="selected"'; } ?> >December</option>
    </select>    </td>
  </tr>
  <tr>
    <td valign="top"><p>Fiscal year </p>
      </td>
    <td colspan="2"><select name="ddlFiscalYear" id="ddlFiscalYear">
    <option value="FY2009 (Jan 2009 - Dec 2009)" <?php if ($fiscal_year == "FY2009 (Jan 2009 - Dec 2009)") { echo 'selected="selected"'; } ?> >FY2009 (Jan 2009 - Dec 2009)</option>
	<option value="FY2010 (Jan 2010 - Dec 2010)" <?php if ($fiscal_year == "FY2010 (Jan 2010 - Dec 2010)") { echo 'selected="selected"'; } ?> >FY2010 (Jan 2010 - Dec 2010)</option>
	<option value="FY2011 (Jan 2011 - Dec 2011)" <?php if ($fiscal_year == "FY2011 (Jan 2011 - Dec 2011)") { echo 'selected="selected"'; } ?> >FY2011 (Jan 2011 - Dec 2011)</option>
	<option value="FY2012 (Jan 2012 - Dec 2012)" <?php if ($fiscal_year == "FY2012 (Jan 2012 - Dec 2012)") { echo 'selected="selected"'; } ?> >FY2012 (Jan 2012 - Dec 2012)</option>
	<option value="FY2013 (Jan 2013 - Dec 2013)" <?php if ($fiscal_year == "FY2013 (Jan 2013 - Dec 2013)") { echo 'selected="selected"'; } ?> >FY2013 (Jan 2013 - Dec 2013)</option>
	<option value="FY2014 (Jan 2014 - Dec 2014)" <?php if ($fiscal_year == "FY2014 (Jan 2014 - Dec 2014)") { echo 'selected="selected"'; } ?> >FY2014 (Jan 2014 - Dec 2014)</option>
	<option value="FY2015 (Jan 2015 - Dec 2015)" <?php if ($fiscal_year == "FY2015 (Jan 2015 - Dec 2015)") { echo 'selected="selected"'; } ?> >FY2015 (Jan 2015 - Dec 2015)</option>
	<option value="FY2016 (Jan 2016 - Dec 2016)" <?php if ($fiscal_year == "FY2016 (Jan 2016 - Dec 2016)") { echo 'selected="selected"'; } ?> >FY2016 (Jan 2016 - Dec 2016)</option>
	<option value="FY2017 (Jan 2017 - Dec 2017)" <?php if ($fiscal_year == "FY2017 (Jan 2017 - Dec 2017)") { echo 'selected="selected"'; } ?> >FY2017 (Jan 2017 - Dec 2017)</option>
    </select></td>
  </tr>
  <tr>
    <td valign="top"><p>      Copy amounts from</p>      </td>
    <td colspan="2"><input name="rdCopyAmount" type="radio" id="rd3" value="None; default budget amounts to zero" <?php if ($copy_amount_from == "None; default budget amounts to zero") { echo 'checked="checked"'; } ?> />
      None; default budget amounts to zero<br />
      <input type="radio" name="rdCopyAmount" id="rd4" value="Previous fiscal years actual amounts" <?php if ($copy_amount_from == "Previous fiscal years actual amounts") { echo 'checked="checked"'; } ?> />
      Previous fiscal year's actual amounts<br />
      <input type="radio" name="rdCopyAmount" id="rd5" value="Another budget" <?php if ($copy_amount_from == "Another budget") { echo 'checked="checked"'; } ?> />
      Another budget<br /></td>
  </tr>
  <tr>
    <td valign="top">&nbsp;</td>
    <td colspan="2">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" valign="top"><table width="100%" id="headerTable">
      <thead>
        <tr>
          <th align="left" bgcolor="#FFE8F3" scope="col"><strong>Totals</strong></th>
          <th bgcolor="#FFE8F3" scope="col"><strong>Jan</strong></th>
          <th bgcolor="#FFE8F3" scope="col"><strong>Feb</strong></th>
          <th bgcolor="#FFE8F3" scope="col"><strong>Mar</strong></th>
          <th bgcolor="#FFE8F3" scope="col"><strong>Apr</strong></th>
          <th bgcolor="#FFE8F3" scope="col"><strong>May</strong></th>
          <th bgcolor="#FFE8F3" scope="col"><strong>Jun</strong></th>
          <th bgcolor="#FFE8F3" scope="col"><strong>Jul</strong></th>
          <th bgcolor="#FFE8F3" scope="col"><strong>Aug</strong></th>
          <th bgcolor="#FFE8F3" scope="col"><strong>Sep</strong></th>
          <th bgcolor="#FFE8F3" scope="col"><strong>Oct</strong></th>
          <th bgcolor="#FFE8F3" scope="col"><strong>Nov</strong></th>
          <th bgcolor="#FFE8F3" scope="col"><strong>Dec</strong></th>
          <th bgcolor="#FFE8F3" scope="col"><strong>FY2012</strong></th>
        </tr>
      </thead>
      <tbody id="_ctl7_usrBudget_headerTbody">
        <tr id="rowOperating_Income">
          <th align="left" bgcolor="#FFFFFF" scope="row">Income</th>
          <td bgcolor="#FFFFFF" id="Income_monthRow_1">0.00</td>
          <td bgcolor="#FFFFFF" id="Income_monthRow_2">0.00</td>
          <td bgcolor="#FFFFFF" id="Income_monthRow_3">0.00</td>
          <td bgcolor="#FFFFFF" id="Income_monthRow_4">0.00</td>
          <td bgcolor="#FFFFFF" id="Income_monthRow_5">0.00</td>
          <td bgcolor="#FFFFFF" id="Income_monthRow_6">0.00</td>
          <td bgcolor="#FFFFFF" id="Income_monthRow_7">0.00</td>
          <td bgcolor="#FFFFFF" id="Income_monthRow_8">0.00</td>
          <td bgcolor="#FFFFFF" id="Income_monthRow_9">0.00</td>
          <td bgcolor="#FFFFFF" id="Income_monthRow_10">0.00</td>
          <td bgcolor="#FFFFFF" id="Income_monthRow_11">0.00</td>
          <td bgcolor="#FFFFFF" id="Income_monthRow_12">0.00</td>
          <td bgcolor="#FFFFFF" id="Income_headerYearlyTotal">$0.00</td>
        </tr>
        <tr id="rowOperating_Expense">
          <th align="left" bgcolor="#FFFFFF" scope="row">Expense</th>
          <td bgcolor="#FFFFFF" id="Expense_monthRow_1">0.00</td>
          <td bgcolor="#FFFFFF" id="Expense_monthRow_2">0.00</td>
          <td bgcolor="#FFFFFF" id="Expense_monthRow_3">0.00</td>
          <td bgcolor="#FFFFFF" id="Expense_monthRow_4">0.00</td>
          <td bgcolor="#FFFFFF" id="Expense_monthRow_5">0.00</td>
          <td bgcolor="#FFFFFF" id="Expense_monthRow_6">0.00</td>
          <td bgcolor="#FFFFFF" id="Expense_monthRow_7">0.00</td>
          <td bgcolor="#FFFFFF" id="Expense_monthRow_8">0.00</td>
          <td bgcolor="#FFFFFF" id="Expense_monthRow_9">0.00</td>
          <td bgcolor="#FFFFFF" id="Expense_monthRow_10">0.00</td>
          <td bgcolor="#FFFFFF" id="Expense_monthRow_11">0.00</td>
          <td bgcolor="#FFFFFF" id="Expense_monthRow_12">0.00</td>
          <td bgcolor="#FFFFFF" id="Expense_headerYearlyTotal">$0.00</td>
        </tr>
        <tr>
          <th align="left" bgcolor="#FFFFFF" scope="row">Net Income</th>
          <td bgcolor="#FFFFFF" id="netTotalmonthRow_1">0.00</td>
          <td bgcolor="#FFFFFF" id="netTotalmonthRow_2">0.00</td>
          <td bgcolor="#FFFFFF" id="netTotalmonthRow_3">0.00</td>
          <td bgcolor="#FFFFFF" id="netTotalmonthRow_4">0.00</td>
          <td bgcolor="#FFFFFF" id="netTotalmonthRow_5">0.00</td>
          <td bgcolor="#FFFFFF" id="netTotalmonthRow_6">0.00</td>
          <td bgcolor="#FFFFFF" id="netTotalmonthRow_7">0.00</td>
          <td bgcolor="#FFFFFF" id="netTotalmonthRow_8">0.00</td>
          <td bgcolor="#FFFFFF" id="netTotalmonthRow_9">0.00</td>
          <td bgcolor="#FFFFFF" id="netTotalmonthRow_10">0.00</td>
          <td bgcolor="#FFFFFF" id="netTotalmonthRow_11">0.00</td>
          <td bgcolor="#FFFFFF" id="netTotalmonthRow_12">0.00</td>
          <td bgcolor="#FFFFFF" id="netTotalYearly">$0.00</td>
        </tr>
      </tbody>
    </table>    </td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2"><input name="submit" type="submit" class="submit_button" id="submit" value="Update" /></td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
</form>
</body>
</html>
