<?php
include ("../includes/classes/class.db.php");
include ("../includes/functions/general.php");

//-------------------------------------------------------------------------------------------
								/*Objects*/
//-------------------------------------------------------------------------------------------					
	$db = new db();
//-------------------------------------------------------------------------------------------
								/*General Coding Area*/
	
	if (isset($_GET['id']) && isset($_GET['mode']) && $_GET['id'] != "" && $_GET['mode'] == "delete") {
		$sqlDelete = "DELETE FROM payments WHERE payment_id = '".$_GET['id']."'";
		$db->query($sqlDelete);
		redirect("?");
	}
	
	$sql = "SELECT
			payments.payment_id,
			payments.amount,
			payments.payment_date,
			payments.tenant_id,
			tenant_leases.property_name,
			tenant_leases.tenant_entry,
			tenant_leases.first_name,
			tenant_leases.last_name
			FROM
			payments
			Inner Join tenant_leases ON payments.tenant_id = tenant_leases.tenant_id
			ORDER BY
			payments.payment_id DESC";
	
	$query= $db->ExeQuersys($sql);
	
	while ($row = mysql_fetch_array($query))
	{
		$valPaymentID = $row['payment_id'];
		$valAmount = $row['amount'];
		$valPaymentDate = $row['payment_date'];
		$valPropertyName = $row['property_name'];
		$valTenantEntry = $row['tenant_entry'];
		$valFullName = $row['first_name'] . ' ' . $row['last_name'];
		$valTenantID = $row['tenant_id'];
		
		$tr .= '<tr>
				  <td>'.$valAmount.'</td>
				  <td>'.formatDate($valPaymentDate).'</td>
				  <td>'.$valPropertyName.'</td>
				  <td>'.$valTenantEntry.'</td>
				  <td>'.$valFullName.'</td>
				  <td align="center">
				  <a href="javascript:void();" title="Click here to edit" onclick="window.open(\'update_manual_payment.php?tid='.$valTenantID.'\',\'mywindow\',\'menubar=1,resizable=1, scrollbars=1, width=750,height=650\');" >
				  <img src="../images/icon_edit.png" width="16" height="16" alt="Edit" border="0" /></a> &nbsp;&nbsp;|&nbsp;&nbsp;
				  <a href="?id='.$valTenantID.'&mode=delete" title="Click here to delete" onclick="return confirm(\'Do you want to delete this record?\');">
				  <img src="../images/icon_delete.png" width="16" height="16" alt="Delete" border="0" />
				  </a>
				  </td>
				</tr>';
	}
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Manual Payment</title>
<link href="../styles.css" rel="stylesheet" type="text/css" />
<script src="../SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<link href="../SpryAssets/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="top_menu">
  <img src="../images/logo.png" width="126" height="67" alt="Logo" style="float:left; padding-right:10px;" />
  <h1>Real Estate Shark</h1>
</div>
<div id="wrapper_header">
  <ul id="MenuBar1" class="MenuBarHorizontal">
    <li><a class="MenuBarItemSubmenu" href="#">Rentals</a>
      <ul>
        <li><a href="../rentals/index.php">Properties</a></li>
        <li><a href="../rentals/leases.php">Leases</a></li>
        <li><a href="../rentals/tenants.php">Tenants</a></li>
        <li><a href="../rentals/listings.php">Listings</a></li>
        <li><a href="../rentals/rental_applications.php">Rental applications</a></li>
        <li><a href="../rentals/rental_owners.php">Rental owners</a></li>
        <li><a href="../rentals/outstanding_balances.php">Outstanding balances</a></li>
      </ul>
    </li>
    <li><a href="#" class="MenuBarItemSubmenu">Associations</a>
      <ul>
        <li><a href="../associations/index.php">Associations</a></li>
        <li><a href="../associations/ownership_account.php">Ownership accounts</a></li>
        <li><a href="../associations/association_owners.php">Association owners</a></li>
        <li><a href="../associations/outstanding_balances.php">Outstanding balances</a></li>
      </ul>
    </li>
    <li><a class="MenuBarItemSubmenu" href="#">Accounting</a>
      <ul>
        <li><a href="index.php">Financials</a></li>
        <li><a href="general_ledger.php">General ledger</a></li>
        <li><a href="banking.php">Banking</a></li>
        <li class="divider"><a href="vendors.php">Vendors</a></li>
        <li><a href="work_orders.php">Work orders</a></li>
        <li><a href="bills.php">Bills</a></li>
        <li><a href="recurring_transactions.php">Recurring transactions</a></li>
        <li class="divider"><a href="budget.php">Budgets</a></li>
        <li><a href="chart_of_accounts.php">Chart of accounts</a></li>
        <li class="divider"><a href="filing_1099.html">1099-MISC tax filings</a></li>
      </ul>
    </li>
    <li><a href="#" class="MenuBarItemSubmenu">Tasks</a>
      <ul>
        <li><a href="../tasks/mytasks.php">My tasks</a></li>
        <li><a href="../tasks/unassigned_tasks.html">Unassigned tasks</a></li>
        <li><a href="../tasks/all_tasks.html">All tasks</a></li>
        <li class="divider"><a href="../tasks/recurring_tasks.html">Recurring tasks</a></li>
      </ul>
    </li>
    <li><a href="#" class="MenuBarItemSubmenu">Communications</a>
      <ul>
        <li><a href='#'> Public site</a></li>
        <li class="divider"><a href="#">Resident site users</a></li>
        <li><a href="#">Resident site contact directory</a></li>
        <li><a href="#">Resident site announcements</a></li>
        <li><a href="#">Association discussions</a></li>
        <li class="divider"><a href="#">Mailings</a></li>
        <li><a href="#">Mailing templates</a></li>
        <li><a href="#">Email templates</a></li>
      </ul>
    </li>
  </ul>
  <input name="txt" type="text" class="searchBox" id="txt" value="Search..." />
</div>
<div id="wrapper">
  <h1>Manual Payment</h1>
  <div class="button" style="float:right"><a href="#" onclick="window.open('add_manual_payment.php','mywindow','menubar=1,resizable=1, scrollbars=1, width=750,height=600');">Add A Payment</a></div>
  <br />
<br />
</p>                  
  <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td bgcolor="#EFEFEF"><strong>Amount</strong></td>
      <td bgcolor="#EFEFEF"><strong>Payment Date</strong></td>
      <td bgcolor="#EFEFEF"><strong>Property Name</strong></td>
      <td bgcolor="#EFEFEF"><strong>Tenant Entry</strong></td>
      <td bgcolor="#EFEFEF"><strong>Tenant Name</strong></td>
      <td bgcolor="#EFEFEF" align="center"><strong>Actions</strong></td>
    </tr>
    <?php echo $tr; ?>
  </table>
  <p>&nbsp;</p>
  <p>
    <label for="txt"></label></p>
  <p>&nbsp;</p>
</div>
<div id="footer" align="right">&copy; 2012 Real Estate Shark • All rights reserved.</div>
<script type="text/javascript">
var MenuBar1 = new Spry.Widget.MenuBar("MenuBar1", {imgDown:"SpryAssets/SpryMenuBarDownHover.gif", imgRight:"SpryAssets/SpryMenuBarRightHover.gif"});
</script>
</body>
</html>