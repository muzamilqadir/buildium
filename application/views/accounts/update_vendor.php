<?php
include ("../includes/classes/class.db.php");
include ("../includes/functions/general.php");

//-------------------------------------------------------------------------------------------
								/*Objects*/
//-------------------------------------------------------------------------------------------					
	$db = new db();
	//isLoggedUser();
//-------------------------------------------------------------------------------------------
								/*General Coding Area*/
	$vid = "0";
	if (isset($_GET['id']) && $_GET['id'] != "" ) {
		$vid = $_GET['id'];
	}							
	//---------------------------------------------------------------------------------------
	
	$isUpdated = false;
	
	if ($_POST){
		$sqlUpdate = "UPDATE bank_vendor SET 
			first_name = '".clearFormData("txtFirstName")."',
			last_name = '".clearFormData("txtLastName")."',
			company = '".clearFormData("txtCompany")."',
			is_company = '".clearFormData("chkIsCompany")."',
			category = '".clearFormData("ddlCategory")."',
			expense_account = '".clearFormData("ddlExpenseAccount")."',
			primary_email = '".clearFormData("txtPrimaryEmail")."',
			alternate_email = '".clearFormData("txtAlternateEmail")."',
			website = '".clearFormData("txtWebsite")."',
			home = '".clearFormData("txtHomePhone")."',
			work = '".clearFormData("txtWorkPhone")."',
			mobile = '".clearFormData("txtMobilePhone")."',
			fax = '".clearFormData("txtFax")."',
			account_number = '".clearFormData("txtAccountNumber")."',
			taxpayer_id = '".clearFormData("txtTaxPayerID")."',
			1099_eligible = '".clearFormData("chkEligible1099")."',
			country = '".clearFormData("ddlCountries")."',
			address_1 = '".clearFormData("txtAddress_1")."',
			address_2 = '".clearFormData("txtAddress_2")."',
			address_3 = '".clearFormData("txtAddress_3")."',
			city = '".clearFormData("txtCity")."',
			state = '".clearFormData("ddlState")."',
			zipcode = '".clearFormData("txtZipCode")."',
			comments = '".clearFormData("txtComments")."'
			WHERE 
			vendor_id = '".$vid."'";

		$db->query($sqlUpdate);
		
		$isUpdated = true;
	}
	
	$sqlSelect = "SELECT * FROM bank_vendor WHERE vendor_id = '".$vid."'";
	
	$query= $db->ExeQuersys($sqlSelect);
		
	while ($row = mysql_fetch_array($query)) {
		$first_name = $row['first_name'];
		$last_name = $row['last_name'];
		$company = $row['company'];
		$is_company = $row['is_company'];
		$category = $row['category'];
		$expense_account = $row['expense_account'];
		$primary_email = $row['primary_email'];
		$alternate_email = $row['alternate_email'];
		$website = $row['website'];
		$home = $row['home'];
		$work = $row['work'];
		$mobile = $row['mobile'];
		$fax = $row['fax'];
		$account_number = $row['account_number'];
		$taxpayer_id = $row['taxpayer_id'];
		$eligible_1099 = $row['1099_eligible'];
		$country = $row['country'];
		$address_1 = $row['address_1'];
		$address_2 = $row['address_2'];
		$address_3 = $row['address_3'];
		$city = $row['city'];
		$state = $row['state'];
		$zipcode = $row['zipcode'];
		$comments = $row['comments'];
	}
		
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Update Vendor</title>
<link href="../styles_popup.css" rel="stylesheet" type="text/css" />
</head>

<body>
<form action="" method="post">
<div id="wrapper_header"><a href="#" onclick="return confirm('Do you want to cancel?');"><img src="../images/btn_cancel.png" alt="Cancel" width="109" height="34" border="0" style="float:right" /></a>
  <input type="image" name="submit2" id="submit2" src="../images/btn_save.png" />
</div>
<p>&nbsp;</p>
<?php if ($isUpdated) { echo '<div class="msg_success">Vendor is updated successfully.</div>'; } ?>
<table width="575" border="0" align="center" cellpadding="2" cellspacing="3">
  <tr>
    <td colspan="2"><h1>Update Vendor</h1></td>
    </tr>
  <tr>
    <td colspan="2"><h2>Add Vendor Information</h2></td>
    </tr>
  <tr>
    <td width="188">First Name </td>
    <td width="370"><input name="txtFirstName" type="text" class="textbox" id="txtFirstName" value="<?php echo $first_name; ?>" /></td>
  </tr>
  <tr>
    <td>Last Name</td>
    <td><input name="txtLastName" type="text" class="textbox" id="txtLastName" value="<?php echo $last_name; ?>" /></td>
  </tr>
  <tr>
    <td valign="top">Company</td>
    <td><input name="txtCompany" type="text" class="textbox" id="txtCompany" value="<?php echo $company; ?>" />
      <br />
      <label>
      <input name="chkIsCompany" type="checkbox" id="chkIsCompany" value="1" <?php if ($is_company == "1") {echo 'checked="checked"';} ?> />
      Company</label></td>
  </tr>
  <tr>
    <td>Category</td>
    <td><select name="ddlCategory" id="ddlCategory">
    <option value="1701" <?php if ($category == "1701") {echo 'selected="selected"';} ?> >Uncategorized</option>
	<option value="1702" <?php if ($category == "1702") {echo 'selected="selected"';} ?> >Contractors - Dry Wall</option>
	<option value="1703" <?php if ($category == "1703") {echo 'selected="selected"';} ?> >Contractors - Electrical</option>
	<option value="1704" <?php if ($category == "1704") {echo 'selected="selected"';} ?> >Contractors - Flooring</option>
	<option value="1705" <?php if ($category == "1705") {echo 'selected="selected"';} ?> >Contractors - General</option>
	<option value="1706" <?php if ($category == "1706") {echo 'selected="selected"';} ?> >Contractors - HVAC</option>
	<option value="1707" <?php if ($category == "1707") {echo 'selected="selected"';} ?> >Contractors - Landscaping</option>
	<option value="1708" <?php if ($category == "1709") {echo 'selected="selected"';} ?> >Contractors - Masonry</option>
	<option value="1709" <?php if ($category == "1700") {echo 'selected="selected"';} ?> >Contractors - Painting</option>
	<option value="1710" <?php if ($category == "1710") {echo 'selected="selected"';} ?> >Contractors - Paving</option>
	<option value="1726" <?php if ($category == "1726") {echo 'selected="selected"';} ?> >Contractors - Pest</option>
	<option value="1711" <?php if ($category == "1711") {echo 'selected="selected"';} ?> >Contractors - Plumbing</option>
	<option value="1712" <?php if ($category == "1712") {echo 'selected="selected"';} ?> >Contractors - Roofing</option>
	<option value="1721" <?php if ($category == "1721") {echo 'selected="selected"';} ?> >Contractors - Trash</option>
	<option value="1713" <?php if ($category == "1713") {echo 'selected="selected"';} ?> >Contractors - Windows</option>
	<option value="1727" <?php if ($category == "1727") {echo 'selected="selected"';} ?> >Lenders - Bank</option>
	<option value="1714" <?php if ($category == "1714") {echo 'selected="selected"';} ?> >Management Company</option>
	<option value="1730" <?php if ($category == "1730") {echo 'selected="selected"';} ?> >Professionals - Accounting</option>
	<option value="1729" <?php if ($category == "1729") {echo 'selected="selected"';} ?> >Professionals - Legal</option>
	<option value="1732" <?php if ($category == "1732") {echo 'selected="selected"';} ?> >Professionals - Realtor</option>
	<option value="1715" <?php if ($category == "1715") {echo 'selected="selected"';} ?> >Suppliers - Electrical</option>
	<option value="1716" <?php if ($category == "1716") {echo 'selected="selected"';} ?> >Suppliers - General</option>
	<option value="1728" <?php if ($category == "1728") {echo 'selected="selected"';} ?> >Suppliers - Office</option>
	<option value="1717" <?php if ($category == "1717") {echo 'selected="selected"';} ?> >Suppliers - Plumbing</option>
	<option value="1731" <?php if ($category == "1731") {echo 'selected="selected"';} ?> >Suppliers - Software &amp; Services </option>
	<option value="1724" <?php if ($category == "1724") {echo 'selected="selected"';} ?> >Tax - Federal</option>
	<option value="1723" <?php if ($category == "1723") {echo 'selected="selected"';} ?> >Tax - State</option>
	<option value="1722" <?php if ($category == "1722") {echo 'selected="selected"';} ?> >Tax - Town</option>
	<option value="1719" <?php if ($category == "1719") {echo 'selected="selected"';} ?> >Utilities - Gas &amp; Electric</option>
	<option value="1725" <?php if ($category == "1725") {echo 'selected="selected"';} ?> >Utilities - Insurance</option>
	<option value="1718" <?php if ($category == "1718") {echo 'selected="selected"';} ?> >Utilities - Internet &amp; Phone</option>
	<option value="1720" <?php if ($category == "1720") {echo 'selected="selected"';} ?> >Utilities - Water and Sewer</option>
    </select></td>
  </tr>
  <tr>
    <td>Expense account</td>
    <td><select name="ddlExpenseAccount" id="ddlExpenseAccount">
    <option value="0" <?php if ($expense_account == "0") {echo 'selected="selected"';} ?> >No default expense account</option>
	<option value="114711" <?php if ($expense_account == "114711") {echo 'selected="selected"';} ?> >Advertising</option>
	<option value="114712" <?php if ($expense_account == "114712") {echo 'selected="selected"';} ?> >Association Fees</option>
	<option value="114713" <?php if ($expense_account == "114713") {echo 'selected="selected"';} ?> >Auto and Travel</option>
	<option value="14" <?php if ($expense_account == "14") {echo 'selected="selected"';} ?> >Bank Fees</option>
	<option value="114714" <?php if ($expense_account == "114714") {echo 'selected="selected"';} ?> >Cleaning and Maintenance</option>
	<option value="114715" <?php if ($expense_account == "114715") {echo 'selected="selected"';} ?> >Commissions</option>
	<option value="114716" <?php if ($expense_account == "114716") {echo 'selected="selected"';} ?> >Depreciation Expense</option>
	<option value="114717" <?php if ($expense_account == "114717") {echo 'selected="selected"';} ?> >Insurance</option>
	<option value="114718" <?php if ($expense_account == "114718") {echo 'selected="selected"';} ?> >Landscaping</option>
	<option value="114719" <?php if ($expense_account == "114719") {echo 'selected="selected"';} ?> >Legal and Professional Fees</option>
	<option value="114720" <?php if ($expense_account == "114720") {echo 'selected="selected"';} ?> >Licenses and Permits</option>
	<option value="114721" <?php if ($expense_account == "114721") {echo 'selected="selected"';} ?> >Management Fees</option>
	<option value="114722" <?php if ($expense_account == "114722") {echo 'selected="selected"';} ?> >Mortgage Interest</option>
	<option value="114723" <?php if ($expense_account == "114723") {echo 'selected="selected"';} ?> >Other Expenses</option>
	<option value="114724" <?php if ($expense_account == "114724") {echo 'selected="selected"';} ?> >Other Interest Expense</option>
	<option value="114725" <?php if ($expense_account == "114725") {echo 'selected="selected"';} ?> >Postage and Delivery</option>
	<option value="114726" <?php if ($expense_account == "114726") {echo 'selected="selected"';} ?> >Repairs</option>
	<option value="114727" <?php if ($expense_account == "114727") {echo 'selected="selected"';} ?> >Supplies</option>
	<option value="114728" <?php if ($expense_account == "114728") {echo 'selected="selected"';} ?> >Taxes</option>
	<option value="114729" <?php if ($expense_account == "114729") {echo 'selected="selected"';} ?> >Utilities</option>
    </select></td>
  </tr>
  <tr>
    <td>Primary email</td>
    <td><input name="txtPrimaryEmail" type="text" class="textbox" id="txtPrimaryEmail" size="35" value="<?php echo $primary_email; ?>" /></td>
  </tr>
  <tr>
    <td>Alternate email</td>
    <td><input name="txtAlternateEmail" type="text" class="textbox" id="txtAlternateEmail" size="35" value="<?php echo $alternate_email; ?>" /></td>
  </tr>
  <tr>
    <td> Website</td>
    <td><input name="txtWebsite" type="text" class="textbox" id="txtWebsite" size="35" value="<?php echo $website; ?>" /></td>
  </tr>
  <tr>
    <td>Home phone</td>
    <td><input name="txtHomePhone" type="text" class="textbox" id="txtHomePhone" size="35" value="<?php echo $home; ?>" /></td>
  </tr>
  <tr>
    <td>Work phone</td>
    <td><input name="txtWorkPhone" type="text" class="textbox" id="txtWorkPhone" size="35" value="<?php echo $work; ?>" /></td>
  </tr>
  <tr>
    <td>Mobile phone </td>
    <td><input name="txtMobilePhone" type="text" class="textbox" id="txtMobilePhone" size="35" value="<?php echo $mobile; ?>" /></td>
  </tr>
  <tr>
    <td>Fax </td>
    <td><input name="txtFax" type="text" class="textbox" id="txtFax" size="35" value="<?php echo $fax; ?>" /></td>
  </tr>
  <tr>
    <td>Account number</td>
    <td><input name="txtAccountNumber" type="text" class="textbox" id="txtAccountNumber" size="35" value="<?php echo $account_number; ?>" /></td>
  </tr>
  <tr>
    <td valign="top">Taxpayer ID (TIN)</td>
    <td><input name="txtTaxPayerID" type="text" class="textbox" id="txtTaxPayerID" size="35" value="<?php echo $taxpayer_id; ?>" />
      <br />
      <label>
      <input name="chkEligible1099" type="checkbox" id="chkEligible1099" value="1" <?php if ($eligible_1099 == "1") {echo 'checked="checked"';} ?> />
      Eligible for 1099</label></td>
  </tr>
  <tr>
    <td><h2>Address Information</h2></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>Country</td>
    <td><select name="ddlCountries" id="ddlCountries">
   		<option value="1" <?php if ($country == "1") { echo 'selected="selected"';} ?> >Afghanistan</option>
        <option value="2" <?php if ($country == "2") { echo 'selected="selected"';} ?> >Akrotiri</option>
        <option value="3" <?php if ($country == "3") { echo 'selected="selected"';} ?> >Albania</option>
        <option value="4" <?php if ($country == "4") { echo 'selected="selected"';} ?> >Algeria</option>
        <option value="5" <?php if ($country == "5") { echo 'selected="selected"';} ?> >American Samoa</option>
        <option value="6" <?php if ($country == "6") { echo 'selected="selected"';} ?> >Andorra</option>
        <option value="7" <?php if ($country == "7") { echo 'selected="selected"';} ?> >Angola</option>
        <option value="8" <?php if ($country == "8") { echo 'selected="selected"';} ?> >Anguilla</option>
        <option value="9" <?php if ($country == "9") { echo 'selected="selected"';} ?> >Antarctica</option>
        <option value="10" <?php if ($country == "10") { echo 'selected="selected"';} ?> >Antigua and Barbuda</option>
        <option value="11" <?php if ($country == "11") { echo 'selected="selected"';} ?> >Argentina</option>
        <option value="12" <?php if ($country == "12") { echo 'selected="selected"';} ?> >Armenia</option>
        <option value="13" <?php if ($country == "13") { echo 'selected="selected"';} ?> >Aruba</option>
        <option value="14" <?php if ($country == "14") { echo 'selected="selected"';} ?> >Ashmore and Cartier Islands</option>
        <option value="15" <?php if ($country == "15") { echo 'selected="selected"';} ?> >Australia</option>
        <option value="16" <?php if ($country == "16") { echo 'selected="selected"';} ?> >Austria</option>
        <option value="17" <?php if ($country == "17") { echo 'selected="selected"';} ?> >Azerbaijan</option>
        <option value="18" <?php if ($country == "18") { echo 'selected="selected"';} ?> >Bahamas, The</option>
        <option value="19" <?php if ($country == "19") { echo 'selected="selected"';} ?> >Bahrain</option>
        <option value="20" <?php if ($country == "20") { echo 'selected="selected"';} ?> >Bangladesh</option>
        <option value="21" <?php if ($country == "21") { echo 'selected="selected"';} ?> >Barbados</option>
        <option value="22" <?php if ($country == "22") { echo 'selected="selected"';} ?> >Bassas da India</option>
        <option value="23" <?php if ($country == "23") { echo 'selected="selected"';} ?> >Belarus</option>
        <option value="24" <?php if ($country == "24") { echo 'selected="selected"';} ?> >Belgium</option>
        <option value="25" <?php if ($country == "25") { echo 'selected="selected"';} ?> >Belize</option>
        <option value="26" <?php if ($country == "26") { echo 'selected="selected"';} ?> >Benin</option>
        <option value="27" <?php if ($country == "27") { echo 'selected="selected"';} ?> >Bermuda</option>
        <option value="28" <?php if ($country == "28") { echo 'selected="selected"';} ?> >Bhutan</option>
        <option value="29" <?php if ($country == "29") { echo 'selected="selected"';} ?> >Bolivia</option>
        <option value="30" <?php if ($country == "30") { echo 'selected="selected"';} ?> >Bosnia and Herzegovina</option>
        <option value="31" <?php if ($country == "31") { echo 'selected="selected"';} ?> >Botswana</option>
        <option value="32" <?php if ($country == "32") { echo 'selected="selected"';} ?> >Bouvet Island</option>
        <option value="33" <?php if ($country == "33") { echo 'selected="selected"';} ?> >Brazil</option>
        <option value="34" <?php if ($country == "34") { echo 'selected="selected"';} ?> >British Indian Ocean Territory</option>
        <option value="35" <?php if ($country == "35") { echo 'selected="selected"';} ?> >British Virgin Islands</option>
        <option value="36" <?php if ($country == "36") { echo 'selected="selected"';} ?> >Brunei</option>
        <option value="37" <?php if ($country == "37") { echo 'selected="selected"';} ?> >Bulgaria</option>
        <option value="38" <?php if ($country == "38") { echo 'selected="selected"';} ?> >Burkina Faso</option>
        <option value="39" <?php if ($country == "39") { echo 'selected="selected"';} ?> >Burma</option>
        <option value="40" <?php if ($country == "40") { echo 'selected="selected"';} ?> >Burundi</option>
        <option value="41" <?php if ($country == "41") { echo 'selected="selected"';} ?> >Cambodia</option>
        <option value="42" <?php if ($country == "42") { echo 'selected="selected"';} ?> >Cameroon</option>
        <option value="43" <?php if ($country == "43") { echo 'selected="selected"';} ?> >Canada</option>
        <option value="44" <?php if ($country == "44") { echo 'selected="selected"';} ?> >Cape Verde</option>
        <option value="45" <?php if ($country == "45") { echo 'selected="selected"';} ?> >Cayman Islands</option>
        <option value="46" <?php if ($country == "46") { echo 'selected="selected"';} ?> >Central African Republic</option>
        <option value="47" <?php if ($country == "47") { echo 'selected="selected"';} ?> >Chad</option>
        <option value="48" <?php if ($country == "48") { echo 'selected="selected"';} ?> >Chile</option>
        <option value="49" <?php if ($country == "49") { echo 'selected="selected"';} ?> >China</option>
        <option value="50" <?php if ($country == "50") { echo 'selected="selected"';} ?> >Christmas Island</option>
        <option value="51" <?php if ($country == "51") { echo 'selected="selected"';} ?> >Clipperton Island</option>
        <option value="52" <?php if ($country == "52") { echo 'selected="selected"';} ?> >Cocos (Keeling) Islands</option>
        <option value="53" <?php if ($country == "53") { echo 'selected="selected"';} ?> >Colombia</option>
        <option value="54" <?php if ($country == "54") { echo 'selected="selected"';} ?> >Comoros</option>
        <option value="55" <?php if ($country == "55") { echo 'selected="selected"';} ?> >Congo, Democratic Republic of the</option>
        <option value="56" <?php if ($country == "56") { echo 'selected="selected"';} ?> >Congo, Republic of the</option>
        <option value="57" <?php if ($country == "57") { echo 'selected="selected"';} ?> >Cook Islands</option>
        <option value="58" <?php if ($country == "58") { echo 'selected="selected"';} ?> >Coral Sea Islands</option>
        <option value="59" <?php if ($country == "59") { echo 'selected="selected"';} ?> >Costa Rica</option>
        <option value="60" <?php if ($country == "60") { echo 'selected="selected"';} ?> >Cote d&#39;Ivoire</option>
        <option value="61" <?php if ($country == "61") { echo 'selected="selected"';} ?> >Croatia</option>
        <option value="62" <?php if ($country == "62") { echo 'selected="selected"';} ?> >Cuba</option>
        <option value="63" <?php if ($country == "63") { echo 'selected="selected"';} ?> >Cyprus</option>
        <option value="64" <?php if ($country == "64") { echo 'selected="selected"';} ?> >Czech Republic</option>
        <option value="66" <?php if ($country == "66") { echo 'selected="selected"';} ?> >Dhekelia</option>
        <option value="67" <?php if ($country == "67") { echo 'selected="selected"';} ?> >Djibouti</option>
        <option value="68" <?php if ($country == "68") { echo 'selected="selected"';} ?> >Dominica</option>
        <option value="69" <?php if ($country == "69") { echo 'selected="selected"';} ?> >Dominican Republic</option>
        <option value="70" <?php if ($country == "70") { echo 'selected="selected"';} ?> >Ecuador</option>
        <option value="71" <?php if ($country == "71") { echo 'selected="selected"';} ?> >Egypt</option>
        <option value="72" <?php if ($country == "72") { echo 'selected="selected"';} ?> >El Salvador</option>
        <option value="73" <?php if ($country == "73") { echo 'selected="selected"';} ?> >Equatorial Guinea</option>
        <option value="74" <?php if ($country == "74") { echo 'selected="selected"';} ?> >Eritrea</option>
        <option value="75" <?php if ($country == "75") { echo 'selected="selected"';} ?> >Estonia</option>
        <option value="76" <?php if ($country == "76") { echo 'selected="selected"';} ?> >Ethiopia</option>
        <option value="77" <?php if ($country == "77") { echo 'selected="selected"';} ?> >Europa Island</option>
        <option value="78" <?php if ($country == "78") { echo 'selected="selected"';} ?> >Falkland Islands (Islas Malvinas)</option>
        <option value="79" <?php if ($country == "79") { echo 'selected="selected"';} ?> >Faroe Islands</option>
        <option value="80" <?php if ($country == "80") { echo 'selected="selected"';} ?> >Fiji</option>
        <option value="81" <?php if ($country == "81") { echo 'selected="selected"';} ?> >Finland</option>
        <option value="82" <?php if ($country == "82") { echo 'selected="selected"';} ?> >France</option>
        <option value="83" <?php if ($country == "83") { echo 'selected="selected"';} ?> >French Guiana</option>
        <option value="84" <?php if ($country == "84") { echo 'selected="selected"';} ?> >French Polynesia</option>
        <option value="85" <?php if ($country == "85") { echo 'selected="selected"';} ?> >French Southern and Antarctic Lands</option>
        <option value="86" <?php if ($country == "86") { echo 'selected="selected"';} ?> >Gabon</option>
        <option value="87" <?php if ($country == "87") { echo 'selected="selected"';} ?> >Gambia, The</option>
        <option value="88" <?php if ($country == "88") { echo 'selected="selected"';} ?> >Gaza Strip</option>
        <option value="89" <?php if ($country == "89") { echo 'selected="selected"';} ?> >Georgia</option>
        <option value="90" <?php if ($country == "90") { echo 'selected="selected"';} ?> >Germany</option>
        <option value="91" <?php if ($country == "91") { echo 'selected="selected"';} ?> >Ghana</option>
        <option value="92" <?php if ($country == "92") { echo 'selected="selected"';} ?> >Gibraltar</option>
        <option value="93" <?php if ($country == "93") { echo 'selected="selected"';} ?> >Glorioso Islands</option>
        <option value="94" <?php if ($country == "94") { echo 'selected="selected"';} ?> >Greece</option>
        <option value="95" <?php if ($country == "95") { echo 'selected="selected"';} ?> >Greenland</option>
        <option value="96" <?php if ($country == "96") { echo 'selected="selected"';} ?> >Grenada</option>
        <option value="97" <?php if ($country == "97") { echo 'selected="selected"';} ?> >Guadeloupe</option>
        <option value="98" <?php if ($country == "98") { echo 'selected="selected"';} ?> >Guam</option>
        <option value="99" <?php if ($country == "99") { echo 'selected="selected"';} ?> >Guatemala</option>
        <option value="100" <?php if ($country == "100") { echo 'selected="selected"';} ?> >Guernsey</option>
        <option value="101" <?php if ($country == "101") { echo 'selected="selected"';} ?> >Guinea</option>
        <option value="102" <?php if ($country == "102") { echo 'selected="selected"';} ?> >Guinea-Bissau</option>
        <option value="103" <?php if ($country == "103") { echo 'selected="selected"';} ?> >Guyana</option>
        <option value="104" <?php if ($country == "104") { echo 'selected="selected"';} ?> >Haiti</option>
        <option value="105" <?php if ($country == "105") { echo 'selected="selected"';} ?> >Heard Island and McDonald Islands</option>
        <option value="106" <?php if ($country == "106") { echo 'selected="selected"';} ?> >Holy See (Vatican City)</option>
        <option value="107" <?php if ($country == "107") { echo 'selected="selected"';} ?> >Honduras</option>
        <option value="108" <?php if ($country == "108") { echo 'selected="selected"';} ?> >Hong Kong</option>
        <option value="109" <?php if ($country == "109") { echo 'selected="selected"';} ?> >Hungary</option>
        <option value="110" <?php if ($country == "110") { echo 'selected="selected"';} ?> >Iceland</option>
        <option value="111" <?php if ($country == "111") { echo 'selected="selected"';} ?> >India</option>
        <option value="112" <?php if ($country == "112") { echo 'selected="selected"';} ?> >Indonesia</option>
        <option value="113" <?php if ($country == "113") { echo 'selected="selected"';} ?> >Iran</option>
        <option value="114" <?php if ($country == "114") { echo 'selected="selected"';} ?> >Iraq</option>
        <option value="115" <?php if ($country == "115") { echo 'selected="selected"';} ?> >Ireland</option>
        <option value="116" <?php if ($country == "116") { echo 'selected="selected"';} ?> >Isle of Man</option>
        <option value="117" <?php if ($country == "117") { echo 'selected="selected"';} ?> >Israel</option>
        <option value="118" <?php if ($country == "118") { echo 'selected="selected"';} ?> >Italy</option>
        <option value="119" <?php if ($country == "119") { echo 'selected="selected"';} ?> >Jamaica</option>
        <option value="120" <?php if ($country == "120") { echo 'selected="selected"';} ?> >Jan Mayen</option>
        <option value="121" <?php if ($country == "121") { echo 'selected="selected"';} ?> >Japan</option>
        <option value="122" <?php if ($country == "122") { echo 'selected="selected"';} ?> >Jersey</option>
        <option value="123" <?php if ($country == "123") { echo 'selected="selected"';} ?> >Jordan</option>
        <option value="124" <?php if ($country == "124") { echo 'selected="selected"';} ?> >Juan de Nova Island</option>
        <option value="125" <?php if ($country == "125") { echo 'selected="selected"';} ?> >Kazakhstan</option>
        <option value="126" <?php if ($country == "126") { echo 'selected="selected"';} ?> >Kenya</option>
        <option value="127" <?php if ($country == "127") { echo 'selected="selected"';} ?> >Kiribati</option>
        <option value="128" <?php if ($country == "128") { echo 'selected="selected"';} ?> >Korea, North</option>
        <option value="129" <?php if ($country == "129") { echo 'selected="selected"';} ?> >Korea, South</option>
        <option value="130" <?php if ($country == "130") { echo 'selected="selected"';} ?> >Kuwait</option>
        <option value="131" <?php if ($country == "131") { echo 'selected="selected"';} ?> >Kyrgyzstan</option>
        <option value="132" <?php if ($country == "132") { echo 'selected="selected"';} ?> >Laos</option>
        <option value="133" <?php if ($country == "133") { echo 'selected="selected"';} ?> >Latvia</option>
        <option value="134" <?php if ($country == "134") { echo 'selected="selected"';} ?> >Lebanon</option>
        <option value="135" <?php if ($country == "135") { echo 'selected="selected"';} ?> >Lesotho</option>
        <option value="136" <?php if ($country == "136") { echo 'selected="selected"';} ?> >Liberia</option>
        <option value="137" <?php if ($country == "137") { echo 'selected="selected"';} ?> >Libya</option>
        <option value="138" <?php if ($country == "138") { echo 'selected="selected"';} ?> >Liechtenstein</option>
        <option value="139" <?php if ($country == "139") { echo 'selected="selected"';} ?> >Lithuania</option>
        <option value="140" <?php if ($country == "140") { echo 'selected="selected"';} ?> >Luxembourg</option>
        <option value="141" <?php if ($country == "141") { echo 'selected="selected"';} ?> >Macau</option>
        <option value="142" <?php if ($country == "142") { echo 'selected="selected"';} ?> >Macedonia</option>
        <option value="143" <?php if ($country == "143") { echo 'selected="selected"';} ?> >Madagascar</option>
        <option value="144" <?php if ($country == "144") { echo 'selected="selected"';} ?> >Malawi</option>
        <option value="145" <?php if ($country == "145") { echo 'selected="selected"';} ?> >Malaysia</option>
        <option value="146" <?php if ($country == "146") { echo 'selected="selected"';} ?> >Maldives</option>
        <option value="147" <?php if ($country == "147") { echo 'selected="selected"';} ?> >Mali</option>
        <option value="148" <?php if ($country == "148") { echo 'selected="selected"';} ?> >Malta</option>
        <option value="149" <?php if ($country == "149") { echo 'selected="selected"';} ?> >Marshall Islands</option>
        <option value="150" <?php if ($country == "150") { echo 'selected="selected"';} ?> >Martinique</option>
        <option value="151" <?php if ($country == "151") { echo 'selected="selected"';} ?> >Mauritania</option>
        <option value="152" <?php if ($country == "152") { echo 'selected="selected"';} ?> >Mauritius</option>
        <option value="153" <?php if ($country == "153") { echo 'selected="selected"';} ?> >Mayotte</option>
        <option value="154" <?php if ($country == "154") { echo 'selected="selected"';} ?> >Mexico</option>
        <option value="155" <?php if ($country == "155") { echo 'selected="selected"';} ?> >Micronesia, Federated States of</option>
        <option value="156" <?php if ($country == "156") { echo 'selected="selected"';} ?> >Moldova</option>
        <option value="157" <?php if ($country == "157") { echo 'selected="selected"';} ?> >Monaco</option>
        <option value="158" <?php if ($country == "158") { echo 'selected="selected"';} ?> >Mongolia</option>
        <option value="159" <?php if ($country == "159") { echo 'selected="selected"';} ?> >Montserrat</option>
        <option value="160" <?php if ($country == "160") { echo 'selected="selected"';} ?> >Morocco</option>
        <option value="161" <?php if ($country == "161") { echo 'selected="selected"';} ?> >Mozambique</option>
        <option value="162" <?php if ($country == "162") { echo 'selected="selected"';} ?> >Namibia</option>
        <option value="163" <?php if ($country == "163") { echo 'selected="selected"';} ?> >Nauru</option>
        <option value="164" <?php if ($country == "164") { echo 'selected="selected"';} ?> >Navassa Island</option>
        <option value="165" <?php if ($country == "165") { echo 'selected="selected"';} ?> >Nepal</option>
        <option value="166" <?php if ($country == "166") { echo 'selected="selected"';} ?> >Netherlands</option>
        <option value="167" <?php if ($country == "167") { echo 'selected="selected"';} ?> >Netherlands Antilles</option>
        <option value="168" <?php if ($country == "168") { echo 'selected="selected"';} ?> >New Caledonia</option>
        <option value="169" <?php if ($country == "169") { echo 'selected="selected"';} ?> >New Zealand</option>
        <option value="170" <?php if ($country == "170") { echo 'selected="selected"';} ?> >Nicaragua</option>
        <option value="171" <?php if ($country == "171") { echo 'selected="selected"';} ?> >Niger</option>
        <option value="172" <?php if ($country == "172") { echo 'selected="selected"';} ?> >Nigeria</option>
        <option value="173" <?php if ($country == "173") { echo 'selected="selected"';} ?> >Niue</option>
        <option value="174" <?php if ($country == "174") { echo 'selected="selected"';} ?> >Norfolk Island</option>
        <option value="175" <?php if ($country == "175") { echo 'selected="selected"';} ?> >Northern Mariana Islands</option>
        <option value="176" <?php if ($country == "176") { echo 'selected="selected"';} ?> >Norway</option>
        <option value="177" <?php if ($country == "177") { echo 'selected="selected"';} ?> >Oman</option>
        <option value="178" <?php if ($country == "178") { echo 'selected="selected"';} ?> >Pakistan</option>
        <option value="179" <?php if ($country == "179") { echo 'selected="selected"';} ?> >Palau</option>
        <option value="180" <?php if ($country == "180") { echo 'selected="selected"';} ?> >Panama</option>
        <option value="181" <?php if ($country == "181") { echo 'selected="selected"';} ?> >Papua New Guinea</option>
        <option value="182" <?php if ($country == "182") { echo 'selected="selected"';} ?> >Paracel Islands</option>
        <option value="183" <?php if ($country == "183") { echo 'selected="selected"';} ?> >Paraguay</option>
        <option value="184" <?php if ($country == "184") { echo 'selected="selected"';} ?> >Peru</option>
        <option value="185" <?php if ($country == "185") { echo 'selected="selected"';} ?> >Philippines</option>
        <option value="186" <?php if ($country == "186") { echo 'selected="selected"';} ?> >Pitcairn Islands</option>
        <option value="187" <?php if ($country == "187") { echo 'selected="selected"';} ?> >Poland</option>
        <option value="188" <?php if ($country == "188") { echo 'selected="selected"';} ?> >Portugal</option>
        <option value="189" <?php if ($country == "189") { echo 'selected="selected"';} ?> >Puerto Rico</option>
        <option value="190" <?php if ($country == "190") { echo 'selected="selected"';} ?> >Qatar</option>
        <option value="191" <?php if ($country == "191") { echo 'selected="selected"';} ?> >Reunion</option>
        <option value="192" <?php if ($country == "192") { echo 'selected="selected"';} ?> >Romania</option>
        <option value="193" <?php if ($country == "193") { echo 'selected="selected"';} ?> >Russia</option>
        <option value="194" <?php if ($country == "194") { echo 'selected="selected"';} ?> >Rwanda</option>
        <option value="195" <?php if ($country == "195") { echo 'selected="selected"';} ?> >Saint Helena</option>
        <option value="196" <?php if ($country == "196") { echo 'selected="selected"';} ?> >Saint Kitts and Nevis</option>
        <option value="197" <?php if ($country == "197") { echo 'selected="selected"';} ?> >Saint Lucia</option>
        <option value="198" <?php if ($country == "198") { echo 'selected="selected"';} ?> >Saint Pierre and Miquelon</option>
        <option value="199" <?php if ($country == "199") { echo 'selected="selected"';} ?> >Saint Vincent and the Grenadines</option>
        <option value="200" <?php if ($country == "200") { echo 'selected="selected"';} ?> >Samoa</option>
        <option value="201" <?php if ($country == "201") { echo 'selected="selected"';} ?> >San Marino</option>
        <option value="202" <?php if ($country == "202") { echo 'selected="selected"';} ?> >Sao Tome and Principe</option>
        <option value="203" <?php if ($country == "203") { echo 'selected="selected"';} ?> >Saudi Arabia</option>
        <option value="204" <?php if ($country == "204") { echo 'selected="selected"';} ?> >Senegal</option>
        <option value="205" <?php if ($country == "205") { echo 'selected="selected"';} ?> >Serbia and Montenegro</option>
        <option value="206" <?php if ($country == "206") { echo 'selected="selected"';} ?> >Seychelles</option>
        <option value="207" <?php if ($country == "207") { echo 'selected="selected"';} ?> >Sierra Leone</option>
        <option value="208" <?php if ($country == "208") { echo 'selected="selected"';} ?> >Singapore</option>
        <option value="209" <?php if ($country == "209") { echo 'selected="selected"';} ?> >Slovakia</option>
        <option value="210" <?php if ($country == "210") { echo 'selected="selected"';} ?> >Slovenia</option>
        <option value="211" <?php if ($country == "211") { echo 'selected="selected"';} ?> >Solomon Islands</option>
        <option value="212" <?php if ($country == "212") { echo 'selected="selected"';} ?> >Somalia</option>
        <option value="213" <?php if ($country == "213") { echo 'selected="selected"';} ?> >South Africa</option>
        <option value="214" <?php if ($country == "214") { echo 'selected="selected"';} ?> >South Georgia and the South Sandwich Islands</option>
        <option value="215" <?php if ($country == "215") { echo 'selected="selected"';} ?> >Spain</option>
        <option value="216" <?php if ($country == "216") { echo 'selected="selected"';} ?> >Spratly Islands</option>
        <option value="217" <?php if ($country == "217") { echo 'selected="selected"';} ?> >Sri Lanka</option>
        <option value="218" <?php if ($country == "218") { echo 'selected="selected"';} ?> >Sudan</option>
        <option value="219" <?php if ($country == "219") { echo 'selected="selected"';} ?> >Suriname</option>
        <option value="220" <?php if ($country == "220") { echo 'selected="selected"';} ?> >Svalbard</option>
        <option value="221" <?php if ($country == "221") { echo 'selected="selected"';} ?> >Swaziland</option>
        <option value="222" <?php if ($country == "222") { echo 'selected="selected"';} ?> >Sweden</option>
        <option value="223" <?php if ($country == "223") { echo 'selected="selected"';} ?> >Switzerland</option>
        <option value="224" <?php if ($country == "224") { echo 'selected="selected"';} ?> >Syria</option>
        <option value="225" <?php if ($country == "225") { echo 'selected="selected"';} ?> >Taiwan</option>
        <option value="226" <?php if ($country == "226") { echo 'selected="selected"';} ?> >Tajikistan</option>
        <option value="227" <?php if ($country == "227") { echo 'selected="selected"';} ?> >Tanzania</option>
        <option value="228" <?php if ($country == "228") { echo 'selected="selected"';} ?> >Thailand</option>
        <option value="229" <?php if ($country == "229") { echo 'selected="selected"';} ?> >Timor-Leste</option>
        <option value="230" <?php if ($country == "230") { echo 'selected="selected"';} ?> >Togo</option>
        <option value="231" <?php if ($country == "231") { echo 'selected="selected"';} ?> >Tokelau</option>
        <option value="232" <?php if ($country == "232") { echo 'selected="selected"';} ?> >Tonga</option>
        <option value="233" <?php if ($country == "233") { echo 'selected="selected"';} ?> >Trinidad and Tobago</option>
        <option value="234" <?php if ($country == "234") { echo 'selected="selected"';} ?> >Tromelin Island</option>
        <option value="235" <?php if ($country == "235") { echo 'selected="selected"';} ?> >Tunisia</option>
        <option value="236" <?php if ($country == "236") { echo 'selected="selected"';} ?> >Turkey</option>
        <option value="237" <?php if ($country == "237") { echo 'selected="selected"';} ?> >Turkmenistan</option>
        <option value="238" <?php if ($country == "238") { echo 'selected="selected"';} ?> >Turks and Caicos Islands</option>
        <option value="239" <?php if ($country == "238") { echo 'selected="selected"';} ?> >Tuvalu</option>
        <option value="240" <?php if ($country == "240") { echo 'selected="selected"';} ?> >Uganda</option>
        <option value="241" <?php if ($country == "241") { echo 'selected="selected"';} ?> >Ukraine</option>
        <option value="242" <?php if ($country == "242") { echo 'selected="selected"';} ?> >United Arab Emirates</option>
        <option value="243" <?php if ($country == "243") { echo 'selected="selected"';} ?> >United Kingdom</option>
        <option value="245" <?php if ($country == "245") { echo 'selected="selected"';} ?> >Uruguay</option>
        <option value="246" <?php if ($country == "246") { echo 'selected="selected"';} ?> >Uzbekistan</option>
        <option value="247" <?php if ($country == "247") { echo 'selected="selected"';} ?> >Vanuatu</option>
        <option value="248" <?php if ($country == "248") { echo 'selected="selected"';} ?> >Venezuela</option>
        <option value="249" <?php if ($country == "249") { echo 'selected="selected"';} ?> >Vietnam</option>
        <option value="250" <?php if ($country == "250") { echo 'selected="selected"';} ?> >Virgin Islands</option>
        <option value="251" <?php if ($country == "251") { echo 'selected="selected"';} ?> >Wake Island</option>
        <option value="252" <?php if ($country == "252") { echo 'selected="selected"';} ?> >Wallis and Futuna</option>
        <option value="253" <?php if ($country == "253") { echo 'selected="selected"';} ?> >West Bank</option>
        <option value="254" <?php if ($country == "254") { echo 'selected="selected"';} ?> >Western Sahara</option>
        <option value="255" <?php if ($country == "255") { echo 'selected="selected"';} ?> >Yemen</option>
        <option value="256" <?php if ($country == "256") { echo 'selected="selected"';} ?> >Zambia</option>
        <option value="257" <?php if ($country == "257") { echo 'selected="selected"';} ?> >Zimbabwe</option>
    </select>    </td>
  </tr>
  <tr>
    <td>Address 1</td>
    <td><input name="txtAddress_1" type="text" class="textbox" id="txtAddress_1" size="35" value="<?php echo $address_1; ?>" /></td>
  </tr>
  <tr>
    <td>Address 2 </td>
    <td><input name="txtAddress_2" type="text" class="textbox" id="txtAddress_2" size="35" value="<?php echo $address_2; ?>" /></td>
  </tr>
  <tr>
    <td>Address 3</td>
    <td><input name="txtAddress_3" type="text" class="textbox" id="txtAddress_3" size="35" value="<?php echo $address_3; ?>" /></td>
  </tr>
  <tr>
    <td>City</td>
    <td><input name="txtCity" type="text" class="textbox" id="txtCity" size="35" value="<?php echo $city; ?>" /></td>
  </tr>
  <tr>
    <td>State</td>
    <td><select name="ddlState" id="ddlState">
      	<option value="AE" <?php if ($state == "AE") { echo 'selected="selected"';} ?> >AE</option>
        <option value="AK" <?php if ($state == "AK") { echo 'selected="selected"';} ?> >AK</option>
        <option value="AL" <?php if ($state == "AL") { echo 'selected="selected"';} ?> >AL</option>
        <option value="AP" <?php if ($state == "AP") { echo 'selected="selected"';} ?> >AP</option>
        <option value="AR" <?php if ($state == "AR") { echo 'selected="selected"';} ?> >AR</option>
        <option value="AZ" <?php if ($state == "AZ") { echo 'selected="selected"';} ?> >AZ</option>
        <option value="CA" <?php if ($state == "CA") { echo 'selected="selected"';} ?> >CA</option>
        <option value="CO" <?php if ($state == "CO") { echo 'selected="selected"';} ?> >CO</option>
        <option value="CT" <?php if ($state == "CT") { echo 'selected="selected"';} ?> >CT</option>
        <option value="DC" <?php if ($state == "DC") { echo 'selected="selected"';} ?> >DC</option>
        <option value="DE" <?php if ($state == "DE") { echo 'selected="selected"';} ?> >DE</option>
        <option value="FL" <?php if ($state == "FL") { echo 'selected="selected"';} ?> >FL</option>
        <option value="GA" <?php if ($state == "GA") { echo 'selected="selected"';} ?> >GA</option>
        <option value="HI" <?php if ($state == "HI") { echo 'selected="selected"';} ?> >HI</option>
        <option value="IA" <?php if ($state == "IA") { echo 'selected="selected"';} ?> >IA</option>
        <option value="ID" <?php if ($state == "ID") { echo 'selected="selected"';} ?> >ID</option>
        <option value="IL" <?php if ($state == "IL") { echo 'selected="selected"';} ?> >IL</option>
        <option value="IN" <?php if ($state == "IN") { echo 'selected="selected"';} ?> >IN</option>
        <option value="KS" <?php if ($state == "KS") { echo 'selected="selected"';} ?> >KS</option>
        <option value="KY" <?php if ($state == "KY") { echo 'selected="selected"';} ?> >KY</option>
        <option value="LA" <?php if ($state == "LA") { echo 'selected="selected"';} ?> >LA</option>
        <option value="MA" <?php if ($state == "MA") { echo 'selected="selected"';} ?> >MA</option>
        <option value="MD" <?php if ($state == "MD") { echo 'selected="selected"';} ?> >MD</option>
        <option value="ME" <?php if ($state == "ME") { echo 'selected="selected"';} ?> >ME</option>
        <option value="MI" <?php if ($state == "MI") { echo 'selected="selected"';} ?> >MI</option>
        <option value="MN" <?php if ($state == "MN") { echo 'selected="selected"';} ?> >MN</option>
        <option value="MO" <?php if ($state == "MO") { echo 'selected="selected"';} ?> >MO</option>
        <option value="MS" <?php if ($state == "MS") { echo 'selected="selected"';} ?> >MS</option>
        <option value="MT" <?php if ($state == "MT") { echo 'selected="selected"';} ?> >MT</option>
        <option value="NC" <?php if ($state == "NC") { echo 'selected="selected"';} ?> >NC</option>
        <option value="ND" <?php if ($state == "ND") { echo 'selected="selected"';} ?> >ND</option>
        <option value="NE" <?php if ($state == "NE") { echo 'selected="selected"';} ?> >NE</option>
        <option value="NH" <?php if ($state == "NH") { echo 'selected="selected"';} ?> >NH</option>
        <option value="NJ" <?php if ($state == "NJ") { echo 'selected="selected"';} ?> >NJ</option>
        <option value="NM" <?php if ($state == "NM") { echo 'selected="selected"';} ?> >NM</option>
        <option value="NV" <?php if ($state == "NV") { echo 'selected="selected"';} ?> >NV</option>
        <option value="NY" <?php if ($state == "NY") { echo 'selected="selected"';} ?> >NY</option>
        <option value="OH" <?php if ($state == "OH") { echo 'selected="selected"';} ?> >OH</option>
        <option value="OK" <?php if ($state == "OK") { echo 'selected="selected"';} ?> >OK</option>
        <option value="OR" <?php if ($state == "OR") { echo 'selected="selected"';} ?> >OR</option>
        <option value="PA" <?php if ($state == "PA") { echo 'selected="selected"';} ?> >PA</option>
        <option value="RI" <?php if ($state == "RI") { echo 'selected="selected"';} ?> >RI</option>
        <option value="SC" <?php if ($state == "SC") { echo 'selected="selected"';} ?> >SC</option>
        <option value="SD" <?php if ($state == "SD") { echo 'selected="selected"';} ?> >SD</option>
        <option value="TN" <?php if ($state == "TN") { echo 'selected="selected"';} ?> >TN</option>
        <option value="TX" <?php if ($state == "TX") { echo 'selected="selected"';} ?> >TX</option>
        <option value="UT" <?php if ($state == "UT") { echo 'selected="selected"';} ?> >UT</option>
        <option value="VA" <?php if ($state == "VA") { echo 'selected="selected"';} ?> >VA</option>
        <option value="VT" <?php if ($state == "VT") { echo 'selected="selected"';} ?> >VT</option>
        <option value="WA" <?php if ($state == "WA") { echo 'selected="selected"';} ?> >WA</option>
        <option value="WI" <?php if ($state == "WI") { echo 'selected="selected"';} ?> >WI</option>
        <option value="WV" <?php if ($state == "WV") { echo 'selected="selected"';} ?> >WV</option>
        <option value="WY" <?php if ($state == "WY") { echo 'selected="selected"';} ?> >WY</option>
    </select></td>
  </tr>
  <tr>
    <td>Zip code</td>
    <td><input name="txtZipCode" type="text" class="textbox" id="txtZipCode" size="35" value="<?php echo $zipcode; ?>" /></td>
  </tr>
  <tr>
    <td valign="top">Comments</td>
    <td><textarea name="txtComments" cols="45" rows="5" class="textarea" id="txtComments"><?php echo $comments; ?></textarea></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><input name="submit" type="submit" class="submit_button" id="submit" value="Update" /></td>
  </tr>
</table>
</form>
</body>
</html>
