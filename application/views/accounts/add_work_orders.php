<?php
/*include ("../includes/classes/class.db.php");
include ("../includes/functions/general.php");*/
//-------------------------------------------------------------------------------------------
								/*Objects*/
//-------------------------------------------------------------------------------------------					
	/*$db = new db();*/
	//isLoggedUser();
//-------------------------------------------------------------------------------------------
								/*General Coding Area*/
	/*$isAdded = false;
	if ($_POST){
		$sqlInsert = "INSERT INTO work_orders ( 
			subject,
			description,
			property,
			units,
			assigned_to,
			priority,
			category,
			due_date,
			request_type,
			contact_name,
			email,
			home,
			work,
			mobile,
			vendor,
			invoice_number,
			rechargable_to,
			entry_allowed,
			entry_notes,
			work_perform_vendor,
			vendor_notes
		) VALUES (
			'".clearFormData("txtSubject")."',
			'".clearFormData("txtDescription")."',
			'".clearFormData("ddlProperty")."',
			'".clearFormData("ddlUnit")."',
			'".clearFormData("ddlAssignedTo")."',
			'".clearFormData("ddlPriority")."',
			'".clearFormData("ddlCategory")."',
			'".clearFormData("txtDueDate")."',
			'".clearFormData("rdRequestType")."',
			'".clearFormData("txtContactName")."',
			'".clearFormData("txtEmail")."',
			'".clearFormData("txtHome")."',
			'".clearFormData("txtWork")."',
			'".clearFormData("txtMobile")."',
			'".clearFormData("ddlVendor")."',
			'".clearFormData("ddlInvoiceNumber")."',
			'".clearFormData("txtRechargableTo")."',
			'".clearFormData("ddlEntryAllowed")."',
			'".clearFormData("txtEntryNotes")."',
			'".clearFormData("txtVendorWork")."',
			'".clearFormData("txtVendorNotes")."'
		)";

		$db->query($sqlInsert);
		$isAdded = true;
	}
*/?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Add Work Orders</title>
<link href="<?php echo base_url();?>css/styles_popup.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url();?>js/calendarDateInput.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>js/SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<link href="<?php echo base_url();?>css/SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
</head>

<body>
<form action="<?php echo base_url();?>accounts/save_work_order" method="post" enctype="multipart/form-data">
<div id="wrapper_header"><a href="#" onclick="return confirm('Do you want to cancel?');"><img src="<?php echo base_url();?>images/btn_cancel.png" alt="Cancel" width="109" height="34" border="0" style="float:right" /></a>
  <input type="image" name="submit2" id="submit2" src="<?php echo base_url();?>images/btn_save.png" />
</div>
<p>&nbsp;</p>
<?php /*if ($isAdded) { echo '<div class="msg_success">Work Order is added successfully.</div>'; } */?>
<table width="664" border="0" align="center" cellpadding="2" cellspacing="3">
  <tr>
    <td colspan="2"><h1>Add Work Orders</h1></td>
    </tr>
  <tr>
    <td colspan="2"><h2>Details:</h2></td>
    </tr>
  <tr>
    <td width="98">Subject</td>
    <td width="549"><span id="sprytextfield1">
      <input name="txtSubject" type="text" class="textbox" id="txtSubject" size="80" />
      <span class="textfieldRequiredMsg">*</span></span></td>
  </tr>
  <tr>
    <td valign="top">Description</td>
    <td><textarea name="txtDescription" cols="77" rows="5" class="textarea" id="txtDescription"></textarea></td>
  </tr>
  <tr>
    <td colspan="2"><table width="100%" border="0" cellspacing="2" cellpadding="0">
      <tr>
        <td width="16%" valign="top">Property</td>
        <td width="40%" valign="top"><select name="ddlProperty" id="ddlProperty">
          <option value="Residential Rental">Residential Rental</option>
          <option value="Commercial Rental">Commercial Rental</option>
        </select></td>
        <td width="14%" valign="top">Request Type</td>
        <td width="30%" valign="top"><input name="rdRequestType" type="radio" id="rd" value="To do" checked="checked" />
          To do</td>
      </tr>
      <tr>
        <td valign="top">Unit</td>
        <td valign="top"><select name="ddlUnit" id="ddlUnit">
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="2">3</option>
          <option value="2">4</option>
          <option value="2">5</option>
          <option value="2">6</option>
          <option value="2">7</option>
          <option value="2">8</option>
          <option value="2">9</option>
          <option value="2">10</option>
          <option value="2">11</option>
          <option value="2">12</option>
          <option value="2">13</option>
          <option value="2">14</option>
          <option value="2">15</option> 
        </select></td>
        <td valign="top">&nbsp;</td>
        <td valign="top"><input type="radio" name="rdRequestType" id="rd2" value="Prospect request" />
Prospect request</td>
      </tr>
      <tr>
        <td valign="top">Attachment</td>
        <td valign="top"><input name="attach" type="file" class="textbox" id="attach" /></td>
        <td valign="top">&nbsp;</td>
        <td valign="top"><input type="radio" name="rdRequestType" id="rd3" value="Resident request" />
Resident request</td>
      </tr>
      <tr>
        <td valign="top">Assigned To</td>
        <td valign="top">
        <select name="ddlAssignedTo" id="ddlAssignedTo">
          <option value="1">Nameone</option>
          <option value="2">Nametwo</option>
        </select></td>
        <td valign="top">&nbsp;</td>
        <td valign="top"><input type="radio" name="rdRequestType" id="rd4" value="Rental owner request" />
Rental owner request</td>
      </tr>
      <tr>
        <td valign="top">Priority</td>
        <td valign="top"><select name="ddlPriority" id="ddlPriority">
          <option value="Low">Low</option>
          <option value="Normal">Normal</option>
          <option value="High">High</option>
                </select></td>
        <td valign="top">Contact Name</td>
        <td valign="top"><input name="txtContactName" type="text" class="textbox" id="txtPropertyName3" /></td>
      </tr>
      <tr>
        <td valign="top">Category</td>
        <td valign="top"><select name="ddlCategory" id="ddlCategory">
          <option value="Uncategorized">Uncategorized</option>
          <option value="Complaint">Complaint</option>
          <option value="Feedback/Suggestion">Feedback/Suggestion</option>
          <option value="General Inquiry">General Inquiry</option>
          <option value="Maintence Request">Maintence Request</option>
          <option>Other</option>
                </select></td>
        <td valign="top">Email</td>
        <td valign="top"><input name="txtEmail" type="text" class="textbox" id="txtPropertyName4" /></td>
      </tr>
      <tr>
        <td valign="top">Due Date</td>
        <td valign="top"><script>DateInput('txtDueDate', true, 'YYYY-MM-DD')</script></td>
        <td valign="top">Home</td>
        <td valign="top"><input name="txtHome" type="text" class="textbox" id="txtHome" /></td>
      </tr>
      <tr>
        <td valign="top">&nbsp;</td>
        <td valign="top">&nbsp;</td>
        <td valign="top">Work</td>
        <td valign="top"><input name="txtWork" type="text" class="textbox" id="txtPropertyName8" /></td>
      </tr>
      <tr>
        <td valign="top">&nbsp;</td>
        <td valign="top">&nbsp;</td>
        <td valign="top">Mobile</td>
        <td valign="top"><input name="txtMobile" type="text" class="textbox" id="txtPropertyName17" /></td>
      </tr>
    </table></td>
    </tr>
  <tr>
    <td colspan="2"><h2>Work Order Details:</h2></td>
    </tr>
  <tr>
    <td colspan="2"><table width="100%" border="0" cellspacing="2" cellpadding="0">
      <tr>
        <td width="16%" valign="top">Vendor</td>
        <td colspan="3" valign="top"><select name="ddlVendor" id="ddlVendor">
    <option value="1">Select Vendor</option>
                        </select> 
        [ <a href="add_vendor">Add Vendor</a> ]</td>
        </tr>
      <tr>
        <td valign="top">Invoice Number</td>
        <td width="37%" valign="top"><select name="ddlInvoiceNumber" id="ddlPropertyType9">
            <option value="1">-- Select --</option>
        </select></td>
        <td width="15%" valign="top">Entry Allowed?</td>
        <td width="32%" valign="top"><select name="ddlEntryAllowed" id="ddlPropertyType12">
          <option value="Unknown">Unknown</option>
          <option value="Yes">Yes</option>
          <option value="No">No</option>
                </select></td>
      </tr>
      <tr>
        <td valign="top">Rechargable to</td>
        <td valign="top"><input name="txtRechargableTo" type="text" class="textbox" id="txtPropertyName36" /></td>
        <td valign="top">Entry Notes</td>
        <td valign="top"><textarea name="txtEntryNotes" cols="20" rows="3" class="textarea" id="txtEntryNotes"></textarea></td>
      </tr>
      <tr>
        <td valign="top">Work to be performed by vendor</td>
        <td colspan="3" valign="top"><textarea name="txtVendorWork" cols="77" rows="3" class="textarea" id="txtVendorWork"></textarea></td>
        </tr>
      <tr>
        <td valign="top">Vendor notes</td>
        <td colspan="3" valign="top"><textarea name="txtVendorNotes" cols="77" rows="3" class="textarea" id="txtVendorNotes"></textarea></td>
        </tr>

    </table></td>
    </tr>
  <tr>
    <td colspan="2"><h2>Parts and Labor</h2></td>
    </tr>
  <tr>
    <td colspan="2"><table width="100%" border="0" bgcolor="#FFFFFF">
      <tr>
        <td height="26" bgcolor="#FFDFEF">Qty 	</td>
        <td bgcolor="#FFDFEF">Account 	</td>
        <td bgcolor="#FFDFEF">Memo 	</td>
        <td bgcolor="#FFDFEF">Price 	</td>
        <td bgcolor="#FFDFEF">Total</td>
      </tr>
      <tr>
        <td height="39"><input name="txtPropertyName3" type="text" class="textbox" id="txtPropertyName7" size="3" /></td>
        <td><select name="ddlPropertyType10" id="ddlPropertyType10">
          <option>Select</option>
                </select></td>
        <td><input name="txtPropertyName22" type="text" class="textbox" id="txtPropertyName37" /></td>
        <td><input name="txtPropertyName23" type="text" class="textbox" id="txtPropertyName38" /></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td colspan="5"><strong>Total:</strong></td>
        </tr>
    </table></td>
    </tr>
  <tr>
    <td colspan="2" align="center"><input name="submit" type="submit" class="submit_button" id="submit" value="Save" /></td>
    </tr>
</table>
</form>
<script type="text/javascript">
<!--
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1");
//-->
</script>
</body>
</html>
