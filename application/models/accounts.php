<?php
class Banking extends CI_Model{

    function account_info(){
        $query="SELECT bank_account_id, name, type, account_number, bank_information, check_format FROM bank_account";
        return $this->db->query($query)->result_array();
    }

}