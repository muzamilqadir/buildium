<?php``
/*include ("../includes/classes/class.db.php");
include ("../includes/functions/general.php");*/

//-------------------------------------------------------------------------------------------
								/*Objects*/
//-------------------------------------------------------------------------------------------					
/*	$db = new db();*/
//-------------------------------------------------------------------------------------------
								/*General Coding Area*/
	/*
	if (isset($_GET['id']) && isset($_GET['mode']) && isset($_GET['record']) && $_GET['id'] != "" && $_GET['mode'] == "delete" && $_GET['record'] == "bill" ) {
		$sqlDelete = "DELETE FROM record_bill WHERE bill_id = '".$_GET['id']."'";
		$db->query($sqlDelete);
		redirect("?");
	}
	
	if (isset($_GET['id']) && isset($_GET['mode']) && isset($_GET['record']) && $_GET['id'] != "" && $_GET['mode'] == "delete" && $_GET['record'] == "credit" ) {
		$sqlDelete = "DELETE FROM credit_bill WHERE bill_id = '".$_GET['id']."'";
		$db->query($sqlDelete);
		redirect("?");
	}
	
	$sql = "SELECT recurring_bill_id, vendor, memo, frequency, next_date, next_due_date, duration, txtOccurrences, posting_day
			FROM recurring_bill
			ORDER BY recurring_bill_id DESC";
	
	$query= $db->ExeQuersys($sql);
	
	while ($row = mysql_fetch_array($query))
	{
		$valBillID = $row['recurring_bill_id'];
		$valFrequency = $row['frequency'];
		$valNextDate = formatDate($row['next_date']);
		$valDuration = $row['duration'];
		$valOccurances = $row['txtOccurrences'];
		$valPostingDay = $row['posting_day'];
		
		$trBill .= '<tr>
				  <td>'.$valFrequency.'</td>
				  <td>'.$valNextDate.'</td>
				  <td>'.$valDuration.'</td>
				  <td>'.$valOccurances.'</td>
				  <td>'.$valPostingDay.'</td>
				  <td align="center">
				  <a href="javascript:void();" title="Click here to edit" onclick="window.open(\'update_recurring_bill.php?id='.$valBillID.'\',\'mywindow\',\'menubar=1,resizable=1, scrollbars=1, width=750,height=650\');" >
				  <img src="../images/icon_edit.png" width="16" height="16" alt="Edit" border="0" /></a> &nbsp;&nbsp;|&nbsp;&nbsp;
				  <a href="?id='.$valBillID.'&mode=delete&record=bill" title="Click here to delete" onclick="return confirm(\'Do you want to delete this record?\');">
				  <img src="../images/icon_delete.png" width="16" height="16" alt="Delete" border="0" />
				  </a>
				  </td>
				</tr>';
	}
	
	
	$sql = "SELECT recurring_credit_id, vendor, payee, memo, frequency, next_date, duration, occurrances, posting_day
			FROM recurring_credit
			ORDER BY recurring_credit_id DESC";
	
	$query= $db->ExeQuersys($sql);
	
	while ($row = mysql_fetch_array($query))
	{
		$valBillID = $row['recurring_credit_id'];
		$valFrequency = $row['frequency'];
		$valDated = formatDate($row['next_date']);
		$valDuration = $row['duration'];
		$valOccurances = $row['occurrances'];
		$valPostingDay = $row['posting_day'];
		
		$trCredit .= '<tr>
				  <td>'.$valFrequency.'</td>
				  <td>'.$valDated.'</td>
				  <td>'.$valDuration.'</td>
				  <td>'.$valOccurances.'</td>
				  <td>'.$valPostingDay.'</td>
				  <td align="center">
				  <a href="javascript:void();" title="Click here to edit" onclick="window.open(\'update_recurring_credit.php?id='.$valBillID.'\',\'mywindow\',\'menubar=1,resizable=1, scrollbars=1, width=750,height=650\');" >
				  <img src="../images/icon_edit.png" width="16" height="16" alt="Edit" border="0" /></a> &nbsp;&nbsp;|&nbsp;&nbsp;
				  <a href="?id='.$valBillID.'&mode=delete&record=credit" title="Click here to delete" onclick="return confirm(\'Do you want to delete this record?\');">
				  <img src="../images/icon_delete.png" width="16" height="16" alt="Delete" border="0" />
				  </a>
				  </td>
				</tr>';
	}
	
*/?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Recurring Transactions</title>
<link href="<?php echo base_url();?>css/styles.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url();?>js/SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<link href="<?php echo base_url();?>css/SpryAssets/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="top_menu">
  <img src="<?php echo base_url();?>images/logo.png" width="126" height="67" alt="Logo" style="float:left; padding-right:10px;" />
  <h1>Real Estate Shark</h1>
</div>
<div id="wrapper_header">
  <ul id="MenuBar1" class="MenuBarHorizontal">
    <li><a class="MenuBarItemSubmenu" href="#">Rentals</a>
      <ul>
        <li><a href="<?php echo base_url();?>rentals/index">Properties</a></li>
        <li><a href="<?php echo base_url();?>rentals/leases">Leases</a></li>
        <li><a href="<?php echo base_url();?>rentals/tenants">Tenants</a></li>
        <li><a href="<?php echo base_url();?>rentals/listings">Listings</a></li>
        <li><a href="<?php echo base_url();?>rentals/rental_applications">Rental applications</a></li>
        <li><a href="<?php echo base_url();?>rentals/rental_owners">Rental owners</a></li>
        <li><a href="<?php echo base_url();?>rentals/outstanding_balances">Outstanding balances</a></li>
      </ul>
    </li>
    <li><a href="#" class="MenuBarItemSubmenu">Associations</a>
      <ul>
        <li><a href="<?php echo base_url();?>associations/index">Associations</a></li>
        <li><a href="<?php echo base_url();?>associations/ownership_account">Ownership accounts</a></li>
        <li><a href="<?php echo base_url();?>associations/association_owners">Association owners</a></li>
        <li><a href="<?php echo base_url();?>associations/outstanding_balances">Outstanding balances</a></li>
      </ul>
    </li>
    <li><a class="MenuBarItemSubmenu" href="#">Accounting</a>
      <ul>
        <li><a href="<?php echo base_url();?>accounts/index">Financials</a></li>
        <li><a href="<?php echo base_url();?>accounts/general_ledger">General ledger</a></li>
        <li><a href="<?php echo base_url();?>accounts/banking">Banking</a></li>
        <li class="divider"><a href="<?php echo base_url();?>accounts/vendors">Vendors</a></li>
        <li><a href="<?php echo base_url();?>accounts/work_orders">Work orders</a></li>
        <li><a href="<?php echo base_url();?>accounts/bills">Bills</a></li>
        <li><a href="<?php echo base_url();?>accounts/recurring_transactions">Recurring transactions</a></li>
        <li class="divider"><a href="<?php echo base_url();?>accounts/budget">Budgets</a></li>
        <li><a href="<?php echo base_url();?>accounts/chart_of_accounts">Chart of accounts</a></li>
        <li class="divider"><a href="<?php echo base_url();?>accounts/filing_1099">1099-MISC tax filings</a></li>
      </ul>
    </li>
    <li><a href="#" class="MenuBarItemSubmenu">Tasks</a>
      <ul>
        <li><a href="<?php echo base_url();?>tasks/index">My tasks</a></li>
        <li><a href="<?php echo base_url();?>tasks/unassigned_tasks">Unassigned tasks</a></li>
        <li><a href="<?php echo base_url();?>tasks/all_tasks">All tasks</a></li>
        <li class="divider"><a href="<?php echo base_url();?>tasks/recurring_tasks">Recurring tasks</a></li>
      </ul>
    </li>
    <li><a href="#" class="MenuBarItemSubmenu">Communications</a>
      <ul>
        <li><a href='#'> Public site</a></li>
        <li class="divider"><a href="#">Resident site users</a></li>
        <li><a href="#">Resident site contact directory</a></li>
        <li><a href="#">Resident site announcements</a></li>
        <li><a href="#">Association discussions</a></li>
        <li class="divider"><a href="#">Mailings</a></li>
        <li><a href="#">Mailing templates</a></li>
        <li><a href="#">Email templates</a></li>
      </ul>
    </li>
  </ul>
  <input name="txt" type="text" class="searchBox" id="txt" value="Search..." />
</div>
<div id="wrapper">
  <h1>Recurring Transactions</h1>
  <div class="button" style="float:right"><a href="#" onclick="window.open('add_recurring_credit','mywindow','menubar=1,resizable=1, scrollbars=1, width=800,height=700');">Add Recurring Credit</a></div>
  <div class="button" style="float:right"><a href="#" onclick="window.open('add_recurring_bill','mywindow','menubar=1,resizable=1, scrollbars=1, width=800,height=700');">Add Recurring Bill</a></div>
  <p><br />
    <br />
    <strong>Bill:</strong> </p>
  <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td colspan="13" bgcolor="#EFEFEF"><select name="ddlPropertyType2" id="ddlPropertyType2">
        <option selected="selected" value="1701">All Properties</option>
      </select></td>
    </tr>    
    <tr>
      <td bgcolor="#EFEFEF"><strong>Frequency</strong></td>
      <td bgcolor="#EFEFEF"><strong>Next Date</strong></td>
      <td bgcolor="#EFEFEF"><strong>Duration</strong></td>
      <td bgcolor="#EFEFEF"><strong>Occurances</strong></td>
      <td bgcolor="#EFEFEF"><strong>Posting Day</strong></td>
      <td bgcolor="#EFEFEF" align="center"><strong>Actions</strong></td>
    </tr>
      <?php foreach($recurringData as $bData){ ?>
      <tr>
          <td bgcolor="#FFF"><strong><?php echo $bData["frequency"];?></strong></td>
          <td bgcolor="#FFF"><strong><?php echo $bData["next_date"];?></strong></td>
          <td bgcolor="#FFF"><strong><?php echo $bData["duration"];?></strong></td>
          <td bgcolor="#FFF"><strong><?php echo $bData["txtOccurrences"];?></strong></td>
          <td bgcolor="#FFF"><strong><?php echo $bData["posting_day"];?></strong></td>
          <td bgcolor="#FFF" align="center"><strong>Actions</strong></td>
      </tr>
  <?php }?>
<!--    --><?php /*echo $trBill; */?>
  </table>
  <p><strong>Credit:</strong></p>
  <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td colspan="13" bgcolor="#EFEFEF"><select name="ddlPropertyType" id="ddlPropertyType">
          <option selected="selected" value="1701">All Properties</option>
      </select></td>
    </tr>
    <tr>
      <td bgcolor="#EFEFEF"><strong>Frequency</strong></td>
      <td bgcolor="#EFEFEF"><strong>Next Date</strong></td>
      <td bgcolor="#EFEFEF"><strong>Duration</strong></td>
      <td bgcolor="#EFEFEF"><strong>Occurances</strong></td>
      <td bgcolor="#EFEFEF"><strong>Posting Day</strong></td>
      <td bgcolor="#EFEFEF" align="center"><strong>Actions</strong></td>
    </tr>
      <?php foreach($creditData as $cData){ ?>
      <tr>
          <td bgcolor="#FFF"><strong><?php echo $cData["frequency"];?></strong></td>
          <td bgcolor="#FFF"><strong><?php echo $cData["next_date"];?></strong></td>
          <td bgcolor="#FFF"><strong><?php echo $cData["duration"];?></strong></td>
          <td bgcolor="#FFF"><strong><?php echo $cData["occurrances"];?></strong></td>
          <td bgcolor="#FFF"><strong><?php echo $cData["posting_day"];?></strong></td>
          <td bgcolor="#FFF" align="center"><strong>Actions</strong></td>
      </tr>
      <?php }?>
<!--    --><?php /*echo $trCredit; */?>
  </table>
  <p>&nbsp;</p>
  <p>&nbsp;</p>
  <p>
    <label for="txt"></label></p>
</div>
<div id="footer" align="right">&copy; 2012 Real Estate Shark • All rights reserved.</div>
<script type="text/javascript">
var MenuBar1 = new Spry.Widget.MenuBar("MenuBar1", {imgDown:"SpryAssets/SpryMenuBarDownHover.gif", imgRight:"SpryAssets/SpryMenuBarRightHover.gif"});
</script>
</body>
</html>
