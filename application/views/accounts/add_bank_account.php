<?php
/*include ("../includes/classes/class.db.php");
include ("../includes/functions/general.php");*/

//-------------------------------------------------------------------------------------------
								/*Objects*/
//-------------------------------------------------------------------------------------------					
	/*$db = new db();*/
	//isLoggedUser();
//-------------------------------------------------------------------------------------------
								/*General Coding Area*/
	/*$isAdded = false;
	if ($_POST){
		$sqlInsert = "INSERT INTO bank_account ( 
			name,
			type,
			account_number,
			routing_number,
			comments,
			enable_check_printing,
			check_stock_paper,
			check_format,
			signature_heading,
			fractional_number,
			bank_information
		) VALUES (
			'".clearFormData("txtName")."',
			'".clearFormData("ddlType")."',
			'".clearFormData("txtAccountNumber")."',
			'".clearFormData("txtRoutingNumber")."',
			'".clearFormData("txtComment")."',
			'".clearFormData("chkLocalPrinting")."',
			'".clearFormData("rdCheckStock")."',
			'".clearFormData("ddlCheckFormat")."',
			'".clearFormData("txtSignatureHeading")."',
			'".clearFormData("txtFractionalNumber")."',
			'".clearFormData("txtBankInformation")."'
		)";

		$db->query($sqlInsert);
		$isAdded = true;
	}
	
*/?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Add Bank Account</title>
<link href="<?php echo base_url();?>css/styles_popup.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url();?>js/SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<link href="<?php echo base_url();?>css/SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
</head>

<body>
<form action="<?php echo base_url();?>accounts/save_bank_account " method="post">
<div id="wrapper_header"><a href="#" onclick="return confirm('Do you want to cancel?');"><img src="<?php echo base_url();?>images/btn_cancel.png" alt="Cancel" width="109" height="34" border="0" style="float:right" /></a>
  <input type="image" name="submit2" id="submit2" src="<?php echo base_url();?>images/btn_save.png" />
</div>
<p>&nbsp;</p>
<?php /*if ($isAdded) { echo '<div class="msg_success">Bank Account is added successfully.</div>'; } */?>
<table width="675" border="0" align="center" cellpadding="2" cellspacing="3">
  <tr>
    <td colspan="3"><h1>Add Bank Account</h1></td>
    </tr>
  <tr>
    <td colspan="3"><h2>Bank Information</h2></td>
    </tr>
  <tr>
    <td width="188">Name</td>
    <td colspan="2"><span id="sprytextfield1">
      <input name="txtName" type="text" class="textbox" id="txtName" size="35" />
      <span class="textfieldRequiredMsg">*</span></span></td>
  </tr>
  <tr>
    <td>Type</td>
    <td colspan="2"><select name="ddlType" id="ddlType">
      <option value="Checking">Checking</option>
      <option value="Savings">Savings</option>
            </select></td>
  </tr>
  <tr>
    <td>Account Number</td>
    <td width="156"><input name="txtAccountNumber" type="text" class="textbox" id="txtAccountNumber" /></td>
    <td width="307" rowspan="2"><img src="https://ukhire.managebuilding.com/Manager/Images/img_check_usa.png" /></td>
  </tr>
  <tr>
    <td>Routing Number</td>
    <td><input name="txtRoutingNumber" type="text" class="textbox" id="txtRoutingNumber" /></td>
    </tr>
  <tr>
    <td valign="top">Comments</td>
    <td colspan="2"><textarea name="txtComment" cols="71" rows="5" class="textarea" id="txtComment"></textarea></td>
  </tr>
  <tr>
    <td colspan="3"><h2>Check Printing Confirmation</h2></td>
    </tr>
  <tr>
    <td valign="top">Enable Check Printing</td>
    <td colspan="2">
      <input name="chkLocalPrinting" type="checkbox" id="chkLocalPrinting" value="1" />
      Enable Local Prinitng Check.
      <p class="textarea" style="color:#999999;">
      <br />Order compatible checks and deposit slips from Nelco Solutions.<br /><br /></p></td>
  </tr>
  <tr>
    <td valign="top">Check Stock (Paper)</td>
    <td colspan="2">
      <input name="rdCheckStock" type="radio" id="rd" value="Blank" checked="checked" />
      Blank<br />
      <label for="_ctl7_rbPrePrinted">
      <input type="radio" name="rdCheckStock" id="rd2" value="Pre-printed with account and routing number" />
      Pre-printed with account and routing number</label>      </td>
  </tr>
  <tr>
    <td>Check Format</td>
    <td colspan="2"><select name="ddlCheckFormat" id="ddlCheckFormat">
      <option value="One Voucher/One Signature">One Voucher/One Signature</option>
      <option value="Two Voucher/One Signature">Two Voucher/One Signature</option>
      <option value="Two Voucher/Two Signature">Two Voucher/Two Signature</option>
    </select></td>
  </tr>
  <tr>
    <td>Signature Heading</td>
    <td colspan="2"><input name="txtSignatureHeading" type="text" class="textbox" id="txtSignatureHeading" size="35" /></td>
  </tr>
  <tr>
    <td>Fractional Number</td>
    <td colspan="2"><input name="txtFractionalNumber" type="text" class="textbox" id="txtFractionalNumber" size="35" /></td>
  </tr>
  <tr>
    <td>Bank Information</td>
    <td colspan="2"><input name="txtBankInformation" type="text" class="textbox" id="txtBankInformation" size="35" /></td>
  </tr>
  <tr>
    <td>Company Information</td>
    <td colspan="2"><input type="checkbox" name="chk2" id="chk2" />
      Override Company Information</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2"><input name="txtPropertyName7" type="text" class="textbox" id="txtPropertyName7" size="35" style="background-color:#EEEEEE" disabled="disabled" /></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2"><input name="txtPropertyName3" type="text" class="textbox" id="txtPropertyName3" size="35" style="background-color:#EEEEEE" disabled="disabled"/></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2"><input name="txtPropertyName4" type="text" class="textbox" id="txtPropertyName4" size="35" style="background-color:#EEEEEE" disabled="disabled"/></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2"><input name="txtPropertyName5" type="text" class="textbox" id="txtPropertyName5" size="35" style="background-color:#EEEEEE" disabled="disabled"/></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2"><input name="txtPropertyName7" type="text" class="textbox" id="txtPropertyName7" size="35" style="background-color:#EEEEEE" disabled="disabled"/></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2"><input name="submit" type="submit" class="submit_button" id="submit" value="Save" /></td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
</form>
<script type="text/javascript">
<!--
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1");
//-->
</script>
</body>
</html>
