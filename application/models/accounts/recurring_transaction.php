<?php
class Recurring_Transaction extends CI_Model{

    function bill_info(){
        $query="SELECT * FROM recurring_bill";
        return $this->db->query($query)->result_array();
    }
    function recurringBill($isAdded) {
        $query = "INSERT INTO recurring_bill(vendor,memo,frequency,next_date,next_due_date,duration,txtOccurrences,posting_day)
            VALUES ('$isAdded[vendor]','$isAdded[date]','$isAdded[memo]','$isAdded[frequency]','$isAdded[nextDueDate]',
            '$isAdded[duration]','$isAdded[occurrences]','$isAdded[postingDay]')";

        if($this->db->query($query) > 0){
            return "successfully Inserted";
        }
        return "Error occurred";
    }
    function credit_info(){
        $query="SELECT * FROM recurring_credit";
        return $this->db->query($query)->result_array();
    }

    function recurringCredit($isAdded) {
        $query = "INSERT INTO recurring_credit ( vendor,payee,memo,frequency,next_date,duration,occurrances,posting_day)
            VALUES ('$isAdded[vendor]','$isAdded[payee]','$isAdded[memo]','$isAdded[frequency]','$isAdded[date]',
            '$isAdded[duration]','$isAdded[occurrences]','$isAdded[postingDay]')";

        if($this->db->query($query) > 0){
            return "successfully Inserted";
        }
        return "Error occurred";
    }
}