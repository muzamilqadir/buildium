<?php
/*include ("../includes/classes/class.db.php");
include ("../includes/functions/general.php");*/

//-------------------------------------------------------------------------------------------
								/*Objects*/
//-------------------------------------------------------------------------------------------					
/*	$db = new db();*/
//-------------------------------------------------------------------------------------------
								/*General Coding Area*/
	
	/*if (isset($_GET['id']) && isset($_GET['mode']) && isset($_GET['record']) && $_GET['id'] != "" && $_GET['mode'] == "delete" && $_GET['record'] == "bill" ) {
		$sqlDelete = "DELETE FROM record_bill WHERE bill_id = '".$_GET['id']."'";
		$db->query($sqlDelete);
		redirect("?");
	}
	
	if (isset($_GET['id']) && isset($_GET['mode']) && isset($_GET['record']) && $_GET['id'] != "" && $_GET['mode'] == "delete" && $_GET['record'] == "credit" ) {
		$sqlDelete = "DELETE FROM credit_bill WHERE bill_id = '".$_GET['id']."'";
		$db->query($sqlDelete);
		redirect("?");
	}
	
	$sql = "SELECT bill_id, dated, bill_due_date, vendor, ref_number, memo
			FROM record_bill
			ORDER BY bill_id ASC";
	
	$query= $db->ExeQuersys($sql);
	
	while ($row = mysql_fetch_array($query))
	{
		$valBillID = $row['bill_id'];
		$valDated = formatDate($row['dated']);
		$valDueDate = formatDate($row['bill_due_date']);
		$valVendor = $row['vendor'];
		$valRefNumber = $row['ref_number'];
		$valMemo = $row['memo'];
		
		$trBill .= '<tr>
				  <td>'.$valDated.'</td>
				  <td>'.$valDueDate.'</td>
				  <td>'.$valVendor.'</td>
				  <td>'.$valRefNumber.'</td>
				  <td>'.$valMemo.'</td>
				  <td align="center">
				  <a href="javascript:void();" title="Click here to edit" onclick="window.open(\'update_bill_record.php?id='.$valBillID.'\',\'mywindow\',\'menubar=1,resizable=1, scrollbars=1, width=750,height=650\');" >
				  <img src="../images/icon_edit.png" width="16" height="16" alt="Edit" border="0" /></a> &nbsp;&nbsp;|&nbsp;&nbsp;
				  <a href="?id='.$valBillID.'&mode=delete&record=bill" title="Click here to delete" onclick="return confirm(\'Do you want to delete this record?\');">
				  <img src="../images/icon_delete.png" width="16" height="16" alt="Delete" border="0" />
				  </a>
				  </td>
				</tr>';
	}
	
	
	$sql = "SELECT bill_id, dated, vendor, ref_number, memo
			FROM credit_bill
			ORDER BY bill_id DESC";
	
	$query= $db->ExeQuersys($sql);
	
	while ($row = mysql_fetch_array($query))
	{
		$valBillID = $row['bill_id'];
		$valDated = formatDate($row['dated']);
		$valVendor = $row['vendor'];
		$valRefNumber = $row['ref_number'];
		$valMemo = $row['memo'];
		
		$trCredit .= '<tr>
				  <td>'.$valDated.'</td>
				  <td>'.$valVendor.'</td>
				  <td>'.$valRefNumber.'</td>
				  <td>'.$valMemo.'</td>
				  <td align="center">
				  <a href="javascript:void();" title="Click here to edit" onclick="window.open(\'update_bill_credit.php?id='.$valBillID.'\',\'mywindow\',\'menubar=1,resizable=1, scrollbars=1, width=750,height=650\');" >
				  <img src="../images/icon_edit.png" width="16" height="16" alt="Edit" border="0" /></a> &nbsp;&nbsp;|&nbsp;&nbsp;
				  <a href="?id='.$valBillID.'&mode=delete&record=credit" title="Click here to delete" onclick="return confirm(\'Do you want to delete this record?\');">
				  <img src="../images/icon_delete.png" width="16" height="16" alt="Delete" border="0" />
				  </a>
				  </td>
				</tr>';
	}
	
*/?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Bills</title>
<link href="<?php echo base_url();?>css/styles.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url();?>js/SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<link href="<?php echo base_url();?>css/SpryAssets/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="top_menu">
  <img src="<?php echo base_url();?>images/logo.png" width="126" height="67" alt="Logo" style="float:left; padding-right:10px;" />
  <h1>Real Estate Shark</h1>
</div>
<div id="wrapper_header">
  <ul id="MenuBar1" class="MenuBarHorizontal">
    <li><a class="MenuBarItemSubmenu" href="#">Rentals</a>
      <ul>
        <li><a href="<?php echo base_url();?>rentals/properties">Properties</a></li>
        <li><a href="<?php echo base_url();?>rentals/leases">Leases</a></li>
        <li><a href="<?php echo base_url();?>rentals/tenants">Tenants</a></li>
        <li><a href="<?php echo base_url();?>rentals/listings">Listings</a></li>
        <li><a href="<?php echo base_url();?>rentals/rental_applications">Rental applications</a></li>
        <li><a href="<?php echo base_url();?>rentals/rental_owners">Rental owners</a></li>
        <li><a href="<?php echo base_url();?>rentals/outstanding_balances">Outstanding balances</a></li>
      </ul>
    </li>
    <li><a href="#" class="MenuBarItemSubmenu">Associations</a>
      <ul>
        <li><a href="<?php echo base_url();?>associations/association">Associations</a></li>
        <li><a href="<?php echo base_url();?>associations/ownership_account">Ownership accounts</a></li>
        <li><a href="<?php echo base_url();?>associations/association_owners">Association owners</a></li>
        <li><a href="<?php echo base_url();?>associations/outstanding_balances">Outstanding balances</a></li>
      </ul>
    </li>
    <li><a class="MenuBarItemSubmenu" href="#">Accounting</a>
      <ul>
        <li><a href="<?php echo base_url();?>accounts/index">Financials</a></li>
        <li><a href="<?php echo base_url();?>accounts/general_ledger">General ledger</a></li>
        <li><a href="<?php echo base_url();?>accounts/banking">Banking</a></li>
        <li class="divider"><a href="<?php echo base_url();?>accounts/vendors">Vendors</a></li>
        <li><a href="<?php echo base_url();?>accounts/work_orders">Work orders</a></li>
        <li><a href="<?php echo base_url();?>accounts/bills">Bills</a></li>
        <li><a href="<?php echo base_url();?>accounts/recurring_transactions">Recurring transactions</a></li>
        <li class="divider"><a href="<?php echo base_url();?>accounts/budget">Budgets</a></li>
        <li><a href="<?php echo base_url();?>accounts/chart_of_accounts">Chart of accounts</a></li>
        <li class="divider"><a href="<?php echo base_url();?>accounts/filing_1099">1099-MISC tax filings</a></li>
      </ul>
    </li>
    <li><a href="#" class="MenuBarItemSubmenu">Tasks</a>
      <ul>
        <li><a href="<?php echo base_url();?>tasks/index">My tasks</a></li>
        <li><a href="<?php echo base_url();?>tasks/unassigned_tasks">Unassigned tasks</a></li>
        <li><a href="<?php echo base_url();?>tasks/all_tasks">All tasks</a></li>
        <li class="divider"><a href="<?php echo base_url();?>tasks/recurring_tasks">Recurring tasks</a></li>
      </ul>
    </li>
    <li><a href="#" class="MenuBarItemSubmenu">Communications</a>
      <ul>
        <li><a href='#'> Public site</a></li>
        <li class="divider"><a href="#">Resident site users</a></li>
        <li><a href="#">Resident site contact directory</a></li>
        <li><a href="#">Resident site announcements</a></li>
        <li><a href="#">Association discussions</a></li>
        <li class="divider"><a href="#">Mailings</a></li>
        <li><a href="#">Mailing templates</a></li>
        <li><a href="#">Email templates</a></li>
      </ul>
    </li>
  </ul>
  <input name="txt" type="text" class="searchBox" id="txt" value="Search..." />
</div>
<div id="wrapper">
  <h1>Bills</h1>
  <div class="button" style="float:right"><a href="#" onclick="window.open('add_bill_credit','mywindow','menubar=1,resizable=1, scrollbars=1, width=800,height=700');">Record Credit</a></div>
  <div class="button" style="float:right"><a href="#" onclick="window.open('add_bill_record','mywindow','menubar=1,resizable=1, scrollbars=1, width=800,height=700');">Record Bill</a></div>
  <p><br />
    <br />
    <strong>Bills:</strong></p>
  <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td colspan="11" bgcolor="#EFEFEF"><select name="ddlPropertyType2" id="ddlPropertyType2">
        <option selected="selected" value="1701">All Properties</option>
      </select></td>
    </tr>    
    <tr>
      <td bgcolor="#EFEFEF"><strong>Dated</strong></td>
      <td bgcolor="#EFEFEF"><strong>Due Date</strong></td>
      <td bgcolor="#EFEFEF"><strong>Vendor</strong></td>
      <td bgcolor="#EFEFEF"><strong>Ref No.</strong></td>
      <td bgcolor="#EFEFEF"><strong>Memo</strong></td>
<!--      <td bgcolor="#EFEFEF" align="center"><strong>Actions></td>
-->    </tr>
      <?php foreach ($record_data as $bdata){?>
      <tr>
          <td bgcolor="#FFF"><strong><?php echo $bdata["dated"];?></strong></td>
<!--         <td bgcolor="#FFF"><strong><?php /*echo $bdata["due_date"];*/?></strong></td>
-->          <td bgcolor="#FFF"><strong><?php echo $bdata["bill_due_date"];?></strong></td>
-->          <td bgcolor="#FFF"><strong><?php echo $bdata["vendor"];?></strong></td>
             <td bgcolor="#FFF"><strong><?php echo $bdata["ref_number"];?></strong></td>
             <td bgcolor="#FFF"><strong><?php echo $bdata["memo"];?></strong></td>
         <!-- <td bgcolor="#FFF" align="center"><strong>Actions></td>
-->      </tr>
  <?php } ?>


  <!--  --><?php /*echo $trBill; */?>
  </table>
  <p>&nbsp;</p>
  <p><strong>Credits:</strong></p>
  <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td colspan="11" bgcolor="#EFEFEF"><select name="ddlPropertyType" id="ddlPropertyType">
          <option selected="selected" value="1701">All Properties</option>
      </select></td>
    </tr>
    <tr>
      <td bgcolor="#EFEFEF"><strong>Dated</strong></td>
      <td bgcolor="#EFEFEF"><strong>Vendor</strong></td>
      <td bgcolor="#EFEFEF"><strong>Ref No.</strong></td>
      <td bgcolor="#EFEFEF"><strong>Memo</strong></td>
<!--      <td bgcolor="#EFEFEF" align="center"><strong>Actions></td>
-->    </tr>
      <?php foreach ($credit_data as $bdata){?>
      <tr>
          <td bgcolor="#FFF"><strong><?php echo $bdata["dated"];?></strong></td>
<!--          <td bgcolor="#FFF"><strong><?php /*echo $bdata["due_date"];*/?></strong></td>
-->          <td bgcolor="#FFF"><strong><?php echo $bdata["vendor"];?></strong></td>
          <td bgcolor="#FFF"><strong><?php echo $bdata["ref_number"];?></strong></td>
          <td bgcolor="#FFF"><strong><?php echo $bdata["memo"];?></strong></td>
          <!-- <td bgcolor="#FFF" align="center"><strong>Actions></td>
 -->      </tr>
      <?php } ?>


      <!-- --><?php /*echo $trCredit; */?>
  </table>
  <p>&nbsp;</p>
  <p>&nbsp;</p>
  <p>
    <label for="txt"></label></p>
</div>
<div id="footer" align="right">&copy; 2012 Real Estate Shark • All rights reserved.</div>
<script type="text/javascript">
var MenuBar1 = new Spry.Widget.MenuBar("MenuBar1", {imgDown:"SpryAssets/SpryMenuBarDownHover.gif", imgRight:"SpryAssets/SpryMenuBarRightHover.gif"});
</script>
</body>
</html>
