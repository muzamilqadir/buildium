<?php
/*include ("../includes/classes/class.db.php");
include ("../includes/functions/general.php");*/
//-------------------------------------------------------------------------------------------
								/*Objects*/
//-------------------------------------------------------------------------------------------					
/*	$db = new db();*/
//-------------------------------------------------------------------------------------------
								/*General Coding Area*/
	/*
	if (isset($_GET['id']) && isset($_GET['mode']) && $_GET['id'] != "" && $_GET['mode'] == "delete") {
		$sqlDelete = "DELETE FROM rental_owner WHERE owner_id = '".$_GET['id']."'";
		$db->query($sqlDelete);
		redirect("?");
	}

	$sql = "SELECT owner_id, first_name, last_name, home_phone, primary_email, country
			FROM
			rental_owner
			ORDER BY
			owner_id DESC";

	$query= $db->ExeQuersys($sql);

	while ($row = mysql_fetch_array($query))
	{
		$valAppID = $row['owner_id'];
		$valFirstName = $row['first_name'];
		$valLastName = $row['last_name'];
		$valHome = $row['home_phone'];
		$valPrimaryEmail = $row['primary_email'];
		$valCountry = $row['country'];

		$tr .= '<tr>
				  <td>'.$valFirstName.'</td>
				  <td>'.$valLastName.'</td>
				  <td>'.$valHome.'</td>
				  <td>'.$valPrimaryEmail.'</td>
				  <td>'.$valCountry.'</td>
				  <td align="center">
				  <a href="javascript:void();" title="Click here to edit" onclick="window.open(\'update_rental_owner.php?id='.$valAppID.'\',\'mywindow\',\'menubar=1,resizable=1, scrollbars=1, width=850,height=700\');" >
				  <img src="../images/icon_edit.png" width="16" height="16" alt="Edit" border="0" /></a> &nbsp;&nbsp;|&nbsp;&nbsp;
				  <a href="?id='.$valAppID.'&mode=delete" title="Click here to delete" onclick="return confirm(\'Do you want to delete this record?\');">
				  <img src="../images/icon_delete.png" width="16" height="16" alt="Delete" border="0" />
				  </a>
				  </td>
				</tr>';
	}
*/?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Rental Owners</title>
<link href="<?php echo base_url();?>css/styles.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url();?>js/SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<link href="<?php echo base_url();?>css/SpryAssets/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="top_menu">
  <img src="<?php echo base_url();?>images/logo.png" width="126" height="67" alt="Logo" style="float:left; padding-right:10px;" />
  <h1>Real Estate Shark</h1>
</div>
<div id="wrapper_header">
  <ul id="MenuBar1" class="MenuBarHorizontal">
    <li><a class="MenuBarItemSubmenu" href="#">Rentals</a>
      <ul>
        <li><a href="<?php echo base_url();?>rentals/properties">Properties</a></li>
        <li><a href="<?php echo base_url();?>rentals/leases">Leases</a></li>
        <li><a href="<?php echo base_url();?>rentals/tenants">Tenants</a></li>
        <li><a href="<?php echo base_url();?>rentals/listings">Listings</a></li>
        <li><a href="<?php echo base_url();?>rentals/rental_applications">Rental applications</a></li>
        <li><a href="<?php echo base_url();?>rentals/rentals_owners">Rental owners</a></li>
        <li><a href="<?php echo base_url();?>rentals/outstanding_balances">Outstanding balances</a></li>
      </ul>
    </li>
    <li><a href="#" class="MenuBarItemSubmenu">Associations</a>
      <ul>
        <li><a href="<?php echo base_url();?>associations/association">Associations</a></li>
        <li><a href="<?php echo base_url();?>associations/ownership_account">Ownership accounts</a></li>
        <li><a href="<?php echo base_url();?>associations/association_owners">Association owners</a></li>
        <li><a href="<?php echo base_url();?>associations/outstanding_balances">Outstanding balances</a></li>
      </ul>
    </li>
    <li><a class="MenuBarItemSubmenu" href="#">Accounting</a>
      <ul>
        <li><a href="<?php echo base_url();?>accounts/index">Financials</a></li>
        <li><a href="<?php echo base_url();?>accounts/general_ledger">General ledger</a></li>
        <li><a href="<?php echo base_url();?>accounts/banking">Banking</a></li>
        <li class="divider"><a href="<?php echo base_url();?>accounts/vendors">Vendors</a></li>
        <li><a href="<?php echo base_url();?>accounts/work_orders">Work orders</a></li>
        <li><a href="<?php echo base_url();?>accounts/bills">Bills</a></li>
        <li><a href="<?php echo base_url();?>accounts/recurring_transactions">Recurring transactions</a></li>
        <li class="divider"><a href="<?php echo base_url();?>accounts/budget">Budgets</a></li>
        <li><a href="<?php echo base_url();?>accounts/chart_of_accounts">Chart of accounts</a></li>
        <li class="divider"><a href="<?php echo base_url();?>accounts/filing_1099">1099-MISC tax filings</a></li>
      </ul>
    </li>
    <li><a href="#" class="MenuBarItemSubmenu">Tasks</a>
      <ul>
        <li><a href="<?php echo base_url();?>tasks/index">My tasks</a></li>
        <li><a href="<?php echo base_url();?>tasks/unassigned_tasks">Unassigned tasks</a></li>
        <li><a href="<?php echo base_url();?>tasks/all_tasks">All tasks</a></li>
        <li class="divider"><a href="<?php echo base_url();?>tasks/recurring_tasks">Recurring tasks</a></li>
      </ul>
    </li>
    <li><a href="#" class="MenuBarItemSubmenu">Communications</a>
      <ul>
        <li><a href='#'> Public site</a></li>
        <li class="divider"><a href="#">Resident site users</a></li>
        <li><a href="#">Resident site contact directory</a></li>
        <li><a href="#">Resident site announcements</a></li>
        <li><a href="#">Association discussions</a></li>
        <li class="divider"><a href="#">Mailings</a></li>
        <li><a href="#">Mailing templates</a></li>
        <li><a href="#">Email templates</a></li>
      </ul>
    </li>
  </ul>
  <input name="txt" type="text" class="searchBox" id="txt" value="Search..." />
</div>
<div id="wrapper">
  <h1> Rental Owners</h1>
  <div class="button" style="float:right"><a href="#" onclick="window.open('add_rental_owner','mywindow','menubar=1,resizable=1, scrollbars=1, width=650,height=700');">Add Owner</a></div>
  <br />
<br />
</p>
  <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td bgcolor="#EFEFEF"><strong>First Name</strong></td>
      <td bgcolor="#EFEFEF"><strong>Last Name</strong></td>
      <td bgcolor="#EFEFEF"><strong>Home</strong></td>
      <td bgcolor="#EFEFEF"><strong>Primary Email</strong></td>
      <td bgcolor="#EFEFEF"><strong>Country</strong></td>
      <td bgcolor="#EFEFEF" align="center"><strong>Action</strong></td>
    </tr>
      <?php foreach ($owners_data as $odata){?>
      <tr>
          <td bgcolor="#FFF"><strong><?php echo $odata['first_name'];?></strong></td>
          <td bgcolor="#FFF"><strong><?php echo $odata['last_name'];?> </strong></td>
          <td bgcolor="#FFF"><strong><?php echo $odata['home_phone'];?> </strong></td>
        <td bgcolor="#FFF"><strong><?php echo $odata['primary_email'];?> </strong></td>
          <td bgcolor="#FFF" ><strong><?php echo $odata['country'];?></strong></td>
          <!--<td bgcolor="#FFF"><strong><?php /*echo $odata['properties'];*/?></strong></td>-->
     </tr>
      <?php }?>

   <!-- --><?php /*echo $tr; */?>
  </table>
  <p>&nbsp;</p>
  <p>
    <label for="txt"></label></p>
</div>
<div id="footer" align="right">&copy; 2012 Real Estate Shark • All rights reserved.</div>
<script type="text/javascript">
var MenuBar1 = new Spry.Widget.MenuBar("MenuBar1", {imgDown:"SpryAssets/SpryMenuBarDownHover.gif", imgRight:"SpryAssets/SpryMenuBarRightHover.gif"});
</script>
</body>
</html>
