<?php
class Rental_Applications  extends CI_Model{
    function applications_info(){
        $query = "SELECT * FROM rental_applications";
        return $this->db->query($query)->result_array();

    }
    function add_rental_application($createData){
        $query = "INSERT INTO rental_applications(	details_property,
			details_number_of_units,details_first_name,details_last_name,details_notes,application_property,application_number_of_units,application_status,
			general_first_name,general_last_name,general_email,general_ssn,general_phone,general_date_of_birth,res_address,res_city,
	        res_state,res_postal_code,res_landlord,res_monthly_rent,res_date_from,res_date_to,res_leaving_reason,res_landlord_phone,
			employer_name,employer_city,employer_phone,employer_date_from,employer_date_to,employer_gross_pay,employer_occupation,
			ref_name_1,ref_name_1_phone,ref_name_2,ref_name_2_phone,ref_gross_pay,ref_occupation,other_comments	)
			 VALUES('$createData[ddlPropertyType]','$createData[ddlNumberOfUnits]','$createData[txtFirstName]','$createData[txtLastName]',
			 '$createData[txtNotes]','$createData[ddlAppPropertyType]','$createData[ddlAppNumberOfUnits]',
			 '$createData[ddlAppStatus]','$createData[txtGeneralFirstName]','$createData[txtGeneralLastName]',
			 '$createData[txtGeneralEmail]','$createData[txtGeneralSSN]','$createData[txtGeneralPhone]',
			 '$createData[txtGeneralBirthDate]','$createData[txtResAddress]','$createData[txtResCity]',
			 '$createData[ddlResState]','$createData[txtResPostalCode]','$createData[txtResLandlord]',
			 '$createData[txtResMonthlyRent]','$createData[txtResDateFrom]','$createData[txtResDateTo]',
			 '$createData[txtResReason]','$createData[txtResLandlordPhone]','$createData[txtEmployerName]',
			 '$createData[txtEmployerCity]','$createData[txtEmployerPhone]','$createData[txtEmployerDateFrom]',
			 '$createData[txtEmployerDateTo]','$createData[txtEmployerGross]','$createData[txtEmployerOccupation]',
			 '$createData[txtRefName_1]','$createData[txtRefPhone_1]','$createData[txtRefName_2]',
			 '$createData[txtRefPhone_2]','$createData[txtRefGross]','$createData[txtRefOccupation]',
			 '$createData[txtOtherComments]' )";

        if ($this->db->query($query)>0){
          return "create successfully";
        }
        return "error occurred";
    }
}
