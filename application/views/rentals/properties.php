<?php
/*include ("../includes/classes/class.db.php");
include ("../includes/functions/general.php");*/

//-------------------------------------------------------------------------------------------
								/*Objects*/
//-------------------------------------------------------------------------------------------					
	/*$db = new db();*/
//-------------------------------------------------------------------------------------------
								/*General Coding Area*/
	
	/*if (isset($_GET['id']) && isset($_GET['mode']) && $_GET['id'] != "" && $_GET['mode'] == "delete") {
		$sqlDelete = "DELETE FROM properties WHERE property_id = '".$_GET['id']."'";
		$db->query($sqlDelete);
		redirect("?");
	}
	
	$sql = "SELECT property_id, property_name, property_type, property_sub_type, operating_account, number_of_units, country,
			address_1, address_2, address_3, city, state, zipcode FROM properties ORDER BY property_id DESC";
	
	$query= $db->ExeQuersys($sql);
	
	while ($row = mysql_fetch_array($query))
	{
		$valPropertyID = $row['property_id'];
		$valPropertyName = $row['property_name'];
		$valPropertyType = $row['property_type'];
		$valNumberOfUnits = $row['number_of_units'];
		$valOperatingAccount = $row['operating_account'];
		
		$tr .= '<tr>
				  <td>'.$valPropertyName.'</td>
				  <td>'.$valPropertyType.'</td>
				  <td>'.$valNumberOfUnits.'</td>
				  <td>'.$valOperatingAccount.'</td>
				  <td align="center">
				  <a href="javascript:void();" title="Click here to edit" onclick="window.open(\'update_property.php?id='.$valPropertyID.'\',\'mywindow\',\'menubar=1,resizable=1, scrollbars=1, width=650,height=700\');"    >
				  <img src="../images/icon_edit.png" width="16" height="16" alt="Edit" border="0" /></a> &nbsp;&nbsp;|&nbsp;&nbsp;
				  <a href="?id='.$valPropertyID.'&mode=delete" title="Click here to delete" onclick="return confirm(\'Do you want to delete this record?\');">
				  <img src="../images/icon_delete.png" width="16" height="16" alt="Delete" border="0" />
				  </a>
				  </td>
				</tr>';
	}
*/?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Properties</title>
<link href="<?php echo base_url();?>css/styles.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url();?>js/SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<link href="<?php echo base_url();?>css/SpryAssets/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="top_menu">
  <img src="<?php echo base_url();?>images/logo.png" width="126" height="67" alt="Logo" style="float:left; padding-right:10px;" />
  <h1>Real Estate Shark</h1>
</div>
<div id="wrapper_header">
  <ul id="MenuBar1" class="MenuBarHorizontal">
    <li><a class="MenuBarItemSubmenu" href="#">Rentals</a>
      <ul>
        <li><a href="<?php echo base_url();?>rentals/properties">Properties</a></li>
        <li><a href="<?php echo base_url();?>rentals/leases">Leases</a></li>
        <li><a href="<?php echo base_url();?>rentals/tenants">Tenants</a></li>
        <li><a href="<?php echo base_url();?>rentals/listings">Listings</a></li>
        <li><a href="<?php echo base_url();?>rentals/rental_applications">Rental applications</a></li>
        <li><a href="<?php echo base_url();?>rentals/rental_owners">Rental owners</a></li>
        <li><a href="<?php echo base_url();?>rentals/outstanding_balances">Outstanding balances</a></li>
      </ul>
    </li>
    <li><a href="#" class="MenuBarItemSubmenu">Associations</a>
      <ul>
        <li><a href="<?php echo base_url();?>associations/association">Associations</a></li>
        <li><a href="<?php echo base_url();?>associations/ownership_account">Ownership accounts</a></li>
        <li><a href="<?php echo base_url();?>associations/association_owners">Association owners</a></li>
        <li><a href="<?php echo base_url();?>associations/outstanding_balances">Outstanding balances</a></li>
      </ul>
    </li>
    <li><a class="MenuBarItemSubmenu" href="#">Accounting</a>
      <ul>
        <li><a href="<?php echo base_url();?>accounts/index">Financials</a></li>
        <li><a href="<?php echo base_url();?>accounts/general_ledger">General ledger</a></li>
        <li><a href="<?php echo base_url();?>accounts/banking">Banking</a></li>
        <li class="divider"><a href="<?php echo base_url();?>accounts/vendors">Vendors</a></li>
        <li><a href="<?php echo base_url();?>accounts/work_orders">Work orders</a></li>
        <li><a href="<?php echo base_url();?>accounts/bills">Bills</a></li>
        <li><a href="<?php echo base_url();?>accounts/recurring_transactions">Recurring transactions</a></li>
        <li class="divider"><a href="<?php echo base_url();?>accounts/budget">Budgets</a></li>
        <li><a href="<?php echo base_url();?>accounts/chart_of_accounts">Chart of accounts</a></li>
        <li class="divider"><a href="<?php echo base_url();?>accounts/filing_1099">1099-MISC tax filings</a></li>
      </ul>
    </li>
    <li><a href="#" class="MenuBarItemSubmenu">Tasks</a>
      <ul>
        <li><a href="<?php echo base_url();?>tasks/index">My tasks</a></li>
        <li><a href="<?php echo base_url();?>tasks/unassigned_tasks">Unassigned tasks</a></li>
        <li><a href="<?php echo base_url();?>tasks/all_tasks">All tasks</a></li>
        <li class="divider"><a href="<?php echo base_url();?>tasks/recurring_tasks">Recurring tasks</a></li>
      </ul>
    </li>
    <li><a href="#" class="MenuBarItemSubmenu">Communications</a>
      <ul>
        <li><a href='#'> Public site</a></li>
        <li class="divider"><a href="#">Resident site users</a></li>
        <li><a href="#">Resident site contact directory</a></li>
        <li><a href="#">Resident site announcements</a></li>
        <li><a href="#">Association discussions</a></li>
        <li class="divider"><a href="#">Mailings</a></li>
        <li><a href="#">Mailing templates</a></li>
        <li><a href="#">Email templates</a></li>
      </ul>
    </li>
  </ul>
  <input name="txt" type="text" class="searchBox" id="txt" value="Search..." />
</div>
<div id="wrapper">
  <h1>  Properties  </h1>
  <div class="button" style="float:right"><a href="#" onclick="window.open('add_property','mywindow','menubar=1,resizable=1, scrollbars=1, width=650,height=700');">Add Property</a></div>
  <br />
<br />
</p>
  <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td bgcolor="#EFEFEF"><strong>Property Name</strong></td>
      <td bgcolor="#EFEFEF"><strong>Property Type</strong></td>
      <td bgcolor="#EFEFEF"><strong>No. of Units </strong></td>
      <td bgcolor="#EFEFEF"><strong>Operating Account</strong></td>
      <td bgcolor="#EFEFEF" align="center"><strong>Actions</strong></td>
    </tr>

      <?php foreach ($property_data as $pdata){?>
      <tr>
    <td bgcolor="#FFF"><strong><?php echo $pdata['property_name'];?></strong></td>
          <td bgcolor="#FFF"><strong><?php echo $pdata['property_type'];?> </strong></td>
          <td bgcolor="#FFF"><strong><?php echo $pdata['number_of_units'];?> </strong></td>
          <td bgcolor="#FFF"><strong><?php echo $pdata['operating_account'];?> </strong></td>
         <!-- <td bgcolor="#FFF" align="center"><strong><?php /*echo $pdata['country'];*/?></strong></td>
  --></tr>
  <?php } ?>

  <!--  --><?php /*echo $tr; */?>
  </table>
  <p>&nbsp;</p>
  <p>
    <label for="txt"></label></p>
  <p>&nbsp;</p>
</div>
<div id="footer" align="right">&copy; 2012 Real Estate Shark • All rights reserved.</div>
<script type="text/javascript">
var MenuBar1 = new Spry.Widget.MenuBar("MenuBar1", {imgDown:"SpryAssets/SpryMenuBarDownHover.gif", imgRight:"SpryAssets/SpryMenuBarRightHover.gif"});
</script>
</body>
</html>

