<?php
class Rentals extends CI_Controller{

   /* public function index(){
        $this->load->model('rentals/properties');
        $this->data["property_data"] = $this->properties->property_name();
         //var_dump($this->data);
        $this->load->view('rentals/index', $this->data);

    }*/

    public function leases(){
        $this->load->model('rentals/leases');
        $this->data["leases_data"] = $this->leases->leases_info();
        $this->load->view('rentals/leases',$this->data);

    }
    public function properties(){
        $this->load->model('rentals/properties');
        $this->data["property_data"] = $this->properties->property_name();
        $this->load->view('rentals/properties',$this->data);

    }
    public function tenants(){
        $this->load->model('rentals/tenants');
        $this->data["tenants_data"] = $this->tenants->tenants_info();
        $this->load->view('rentals/tenants',$this->data);

    }
    public function listings(){
        $this->load->model('rentals/listings');
        $this->data["listing_data"] = $this->listings->listings_info();

        $this->load->view('rentals/listings',$this->data);
    }
        public function rental_applications(){
            $this->load->model('rentals/rental_applications');
            $this->data["applications_data"] = $this->rental_applications->applications_info();
            $this->load->view('rentals/rental_applications',$this->data);

    }
    public function rental_owners(){
        $this->load->model('rentals/rental_owners');
        $this->data["owners_data"] = $this->rental_owners->owners_info();
        $this->load->view('rentals/rental_owners',$this->data);

    }
    public function outstanding_balances(){
        $this->load->model('rentals/outstanding_balances');
        $this->data["balance_data"] = $this->outstanding_balances->balance_info();

        $this->load->view('rentals/outstanding_balances',$this->data);

    }
    public function add_leases(){
    $this->load->view('rentals/add_leases');
    }
       public function save_leases(){
            //var_dump($_POST);die();
           $propertyName = $this->input->post("txtPropertyName");
           $type = $this->input->post("dllType");
           $unitNumber = $this->input->post("ddlUnitNumber");
          $date_from = $this->input->post("date_from");
           $date_to = $this->input->post("date_t0");
            $rentDue = $this->input->post("ddlRentDue");
           $frequency = $this->input->post("ddlFrequency");
           $txtRent = $this->input->post("txtRent");
           $txtPrepaidRent = $this->input->post("txtPrepaidRent");
           $txtSecurityDeposit = $this->input->post("txtSecurityDeposit");
           $rdTenant = $this->input->post("rdTenant");
           $txtFirstName = $this->input->post("txtFirstName");
           $txtLastName = $this->input->post("txtLastName");
           $txtPrimaryEmail = $this->input->post("txtPrimaryEmail");
           $txtAlternateEmail = $this->input->post("txtAlternateEmail");
           $txtHome = $this->input->post("txtHome");
           $txtWork = $this->input->post("txtWork");
           $txtMobile = $this->input->post("txtMobile");
           $txtFax = $this->input->post("txtFax");
           $countries = $this->input->post("ddlCountries");
           $txtAddress_1 = $this->input->post("txtAddress_1");
           $txtAddress_2 = $this->input->post("txtAddress_2");
           $txtAddress_3 = $this->input->post("txtAddress_3");
           $txtCity = $this->input->post("txtCity");
           $state = $this->input->post("ddlState");
           $txtPostalCode = $this->input->post("txtPostalCode");

           $ddlAltCountry = $this->input->post("ddlAltCountry");
           $txtAltAddress_1 = $this->input->post("txtAltAddress_1");
           $txtAltAddress_2 = $this->input->post("txtAltAddress_2");
           $txtAltAddress_3 = $this->input->post("txtAltAddress_3");
           $txtAltCity = $this->input->post("txtAltCity");
           $txtAltZipcode = $this->input->post("txtAltZipcode");
           $rdMailing = $this->input->post("rdMailing");
           $txtComments = $this->input->post("txtComments");
           $birth_date = $this->input->post("birth_date");
           $txtEmergencyName = $this->input->post("txtEmergencyName");
           $txtEmergencyPhone = $this->input->post("txtEmergencyPhone");
            $leasesData = array("propertyName"=>$propertyName,  "type"=>$type,"unitNumber"=>$unitNumber,"date_from"=>$date_from,
           "date_to"=>$date_to,"rentDue"=>$rentDue,
            "frequency"=>$frequency,"txtRent"=>$txtRent,"txtPrepaidRent"=>$txtPrepaidRent,
            "txtSecurityDeposit"=>$txtSecurityDeposit,"rdTenant"=>$rdTenant,
            "txtFirstName"=>$txtFirstName,"txtLastName"=>$txtLastName,"txtPrimaryEmail"=>$txtPrimaryEmail,
            "txtAlternateEmail"=>$txtAlternateEmail,"txtHome"=>$txtHome,"txtWork"=>$txtWork,
            "txtMobile"=>$txtMobile,"txtFax"=>$txtFax,"countries"=>$countries,"txtAddress_1"=>$txtAddress_1,
            "txtAddress_2"=>$txtAddress_2,"txtAddress_3"=>$txtAddress_3,"txtCity"=>$txtCity,"state"=>$state,
            "txtPostalCode"=>$txtPostalCode,"ddlAltCountry"=>$ddlAltCountry,"txtAltAddress_1"=>$txtAltAddress_1,
            "txtAltAddress_2"=>$txtAltAddress_2,"txtAltAddress_3"=>$txtAltAddress_3,"txtAltCity"=>$txtAltCity,
             "txtAltZipcode"=>$txtAltZipcode,"rdMailing"=>$rdMailing,"txtComments"=>$txtComments,
            "birth_date"=>$birth_date,"txtEmergencyName"=>$txtEmergencyName,"txtEmergencyPhone"=>$txtEmergencyPhone);

            $this->load->model('rentals/leases');
            echo $this->leases->add_leases($leasesData);

        }


    public function add_property(){

        $this->load->view('rentals/add_property');

    }
    public function save_property(){
       // var_dump($_POST);die();
        $propertyName = $this->input->post("txtPropertyName");
        $propertyType = $this->input->post("ddlPropertyType");
        $subType = $this->input->post("ddlSubtype");
        $operatingAccount = $this->input->post("ddlOperatingAccount");
        $numberOfUnits = $this->input->post("txtNumberOfUnits");
        $countries = $this->input->post("ddlCountries");

        $txtAddress_1 = $this->input->post("txtAddress_1");
        $txtAddress_2 = $this->input->post("txtAddress_2");
        $txtAddress_3 = $this->input->post("txtAddress_3");
        $txtCity = $this->input->post("txtCity");
        $state = $this->input->post("ddlState");
        $txtZipCode = $this->input->post("txtZipCode");

        $propertyData= array("propertyName"=>$propertyName,"propertyType"=>$propertyType,
            "subType"=>$subType,
            "countries"=>$countries,
            "numberOfUnits"=>$numberOfUnits,"operatingAccount"=>$operatingAccount,
           "txtAddress_1"=>$txtAddress_1,"txtAddress_2"=>$txtAddress_2,
            "txtAddress_3"=>$txtAddress_3,"txtCity"=>$txtCity,"state"=>$state,
            "txtZipCode"=>$txtZipCode);

        $this->load->model('rentals/properties');
        echo $this->properties->add_property($propertyData);
       /*var_dump($user);die();
        echo $user;
        var_dump($user1);die();
        echo $user1;*/
    }
    public function add_rental_application(){
        $this->load->view('rentals/add_rental_application');

    }
    public function create_application(){
       // var_dump($_POST);//die();
        $ddlPropertyType = $this->input->post("ddlPropertyType");
        $ddlNumberOfUnits = $this->input->post("ddlNumberOfUnits");
        $txtFirstName = $this->input->post("txtFirstName");
        $txtLastName = $this->input->post("txtLastName");
        $txtNotes = $this->input->post("txtNotes");
        $ddlAppPropertyType = $this->input->post("ddlAppPropertyType");
        $ddlAppNumberOfUnits = $this->input->post("ddlAppNumberOfUnits");
        $ddlAppStatus = $this->input->post("ddlAppStatus");
        $txtGeneralFirstName = $this->input->post("txtGeneralFirstName");
        $txtGeneralLastName = $this->input->post("txtGeneralLastName");
        $txtGeneralEmail = $this->input->post("txtGeneralEmail");
        $txtGeneralSSN = $this->input->post("txtGeneralSSN");
        $txtGeneralPhone = $this->input->post("txtGeneralPhone");
        $txtGeneralBirthDate = $this->input->post("txtGeneralBirthDate");
        $txtResAddress = $this->input->post("txtResAddress");
        $txtResCity = $this->input->post("txtResCity");
        $ddlResState = $this->input->post("ddlResState");
        $txtResPostalCode = $this->input->post("txtResPostalCode");
        $txtResLandlord = $this->input->post("txtResLandlord");
        $txtResMonthlyRent = $this->input->post("txtResMonthlyRent");
        $txtResDateFrom = $this->input->post("txtResDateFrom");
        $txtResDateTo = $this->input->post("txtResDateTo");
        $txtResReason = $this->input->post("txtResReason");
        $txtResLandlordPhone = $this->input->post("txtResLandlordPhone");
        $txtEmployerName = $this->input->post("txtEmployerName");
        $txtEmployerCity = $this->input->post("txtEmployerCity");
        $txtEmployerPhone = $this->input->post("txtEmployerPhone");
        $txtEmployerDateForm = $this->input->post("txtEmployerDateFrom");
        $txtEmployerDateTo = $this->input->post("txtEmployerDateTo");
        $txtEmployerGross = $this->input->post("txtEmployerGross");
        $txtEmployerOccupation = $this->input->post("txtEmployerOccupation");
        $txtRefName_1 = $this->input->post("txtRefName_1");
        $txtRefPhone_1 = $this->input->post("txtRefPhone_1");
        $txtRefName_2 = $this->input->post("txtRefName_2");
        $txtRefPhone_2 = $this->input->post("txtRefPhone_2");
        $txtRefGross = $this->input->post("txtRefGross");
        $txtRefOccupation = $this->input->post("txtRefOccupation");
        $txtOtherComments = $this->input->post("txtOtherComments");

        $createData = array("ddlPropertyType"=>$ddlPropertyType,"ddlNumberOfUnits"=>$ddlNumberOfUnits,
        "txtFirstName"=>$txtFirstName,"txtLastName"=>$txtLastName,"txtNotes"=>$txtNotes,"ddlAppPropertyType"=>$ddlAppPropertyType,
        "ddlAppNumberOfUnits"=>$ddlAppNumberOfUnits,"ddlAppStatus"=>$ddlAppStatus,"txtGeneralFirstName"=>$txtGeneralFirstName,
        "txtGeneralLastName"=>$txtGeneralLastName,"txtGeneralEmail"=>$txtGeneralEmail,"txtGeneralSSN"=>$txtGeneralSSN,
        "txtGeneralPhone"=>$txtGeneralPhone,"txtGeneralBirthDate"=>$txtGeneralBirthDate,
        "txtResAddress"=>$txtResAddress,"txtResCity"=>$txtResCity,"ddlResState"=>$ddlResState,"txtResPostalCode"=>$txtResPostalCode,
        "txtResLandlord"=>$txtResLandlord,"txtResMonthlyRent"=>$txtResMonthlyRent,"txtResDateFrom"=>$txtResDateFrom,
        "txtResDateTo"=>$txtResDateTo,"txtResReason"=>$txtResReason,"txtResLandlordPhone"=>$txtResLandlordPhone,
        "txtEmployerName"=>$txtEmployerName,"txtEmployerCity"=>$txtEmployerCity,"txtEmployerPhone"=>$txtEmployerPhone,
            "txtEmployerDateFrom"=>$txtEmployerDateForm,"txtEmployerDateTo"=>$txtEmployerDateTo,
        "txtEmployerGross"=>$txtEmployerGross,"txtEmployerOccupation"=>$txtEmployerOccupation,
        "txtRefName_1"=>$txtRefName_1,"txtRefPhone_1"=>$txtRefPhone_1,"txtRefName_2"=>$txtRefName_2,
        "txtRefPhone_2"=>$txtRefPhone_2,"txtRefGross"=>$txtRefGross,"txtRefOccupation"=>$txtRefOccupation,
        "txtOtherComments"=>$txtOtherComments);

        $this->load->model('rentals/rental_applications');
        echo $this->rental_applications->add_rental_application($createData);

    }
    public  function add_rental_owner(){

        $this->load->view('rentals/add_rental_owner');
    }
    public function add_owner(){
        //var_dump($_POST);die();
        $txtFirstName = $this->input->post("txtFirstName");
        $txtLastName = $this->input->post("txtLastName");
        $txtCompany = $this->input->post("txtCompany");
        $txtPrimaryEmail = $this->input->post("txtPrimaryEmail");
        $txtAlternateEmail = $this->input->post("txtAlternateEmail");
        $txtTaxID = $this->input->post("txtTaxID");
        $chkEligible = $this->input->post("chkEligible");
        $txtHomePhone = $this->input->post("txtHomePhone");
        $txtMobilePhone = $this->input->post("txtMobilePhone");
        $txtWorkPhone = $this->input->post("txtWorkPhone");
        $txtFax = $this->input->post("txtFax");
        $txtBirthDate = $this->input->post("txtBirthDate");
        $ddlCountries = $this->input->post("ddlCountries");
        $txtAddress_1 = $this->input->post("txtAddress_1");
        $txtAddress_2 = $this->input->post("txtAddress_2");
        $txtAddress_3 = $this->input->post("txtAddress_3");
        $txtCity = $this->input->post("txtCity");
        $ddlState = $this->input->post("ddlState");
        $txtZipCode = $this->input->post("txtZipCode");
        $txtComments = $this->input->post("txtComments");
        $chkProperties = $this->input->post("chkProperties");



        $ownerData = array("txtFirstName"=>$txtFirstName,"txtLastName"=>$txtLastName,"txtCompany"=>$txtCompany,
            "txtPrimaryEmail"=>$txtPrimaryEmail,"txtAlternateEmail"=>$txtAlternateEmail,"txtTaxID"=>$txtTaxID,
            "chkEligible"=>$chkEligible,"txtHomePhone"=>$txtHomePhone,"txtMobilePhone"=>$txtMobilePhone,
            "txtWorkPhone"=>$txtWorkPhone, "txtFax"=>$txtFax,"txtBirthDate"=>$txtBirthDate,"ddlCountries"=>$ddlCountries,
            "txtAddress_1"=>$txtAddress_1,"txtAddress_2"=>$txtAddress_2,"txtAddress_3"=>$txtAddress_3,"txtCity"=>$txtCity,"ddlState"=>$ddlState,
        "txtZipCode"=>$txtZipCode,"txtComments"=>$txtComments,"chkProperties"=>$chkProperties);


        $this->load->model('rentals/rental_owners');
        echo $this->rental_owners->add_rental_owner($ownerData);
    }
}