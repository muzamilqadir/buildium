<?php
class All_Tasks extends CI_Model{

    function current_tasks(){
        $query = "SELECT * FROM tasks";
        return  $this->db->query($query)->result_array();
    }
    function recurring_tasks(){
        $query = "SELECT * FROM recurring_tasks";
        return  $this->db->query($query)->result_array();
    }
}