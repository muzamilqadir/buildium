<?php
/*include ("../includes/classes/class.db.php");
include ("../includes/functions/general.php");*/

//-------------------------------------------------------------------------------------------
								/*Objects*/
//-------------------------------------------------------------------------------------------					
/*	$db = new db();*/
	//isLoggedUser();
//-------------------------------------------------------------------------------------------
								/*General Coding Area*/
	/*$isAdded = false;
	if ($_POST){
		$sqlInsert = "INSERT INTO tenant_leases ( 
			property_name,
			number_of_units,
			type,
			date_from,
			date_to,
			rent_due_day,
			frequency,
			rent,
			prepaid_rent,
			security_deposite,
			tenant_entry,
			first_name,
			last_name,
			primary_email,
			alternate_email,
			home,
			work,
			mobile,
			fax,
			country,
			address_1,
			address_2,
			address_3,
			city,
			state,
			postal_code,
			alt_country,
			alt_address_1,
			alt_address_2,
			alt_address_3,
			alt_city,
			alt_state,
			alt_postal_code,
			mailing_pref,
			comments,
			date_of_birth,
			emergency_contact_name,
			emergency_contact_phone
		) VALUES (
			'".clearFormData("txtPropertyName")."',
			'".clearFormData("ddlUnitNumber")."',
			'".clearFormData("dllType")."',
			'".clearFormData("date_from")."',
			'".clearFormData("date_to")."',
			'".clearFormData("ddlRentDue")."',
			'".clearFormData("ddlFrequency")."',
			'".clearFormData("txtRent")."',
			'".clearFormData("txtPrepaidRent")."',
			'".clearFormData("txtSecurityDeposit")."',
			'".clearFormData("rdTenant")."',
			'".clearFormData("txtFirstName")."',
			'".clearFormData("txtLastName")."',
			'".clearFormData("txtPrimaryEmail")."',
			'".clearFormData("txtAlternateEmail")."',
			'".clearFormData("txtHome")."',
			'".clearFormData("txtWork")."',
			'".clearFormData("txtMobile")."',
			'".clearFormData("txtFax")."',
			'".clearFormData("ddlCountries")."',
			'".clearFormData("txtAddress_1")."',
			'".clearFormData("txtAddress_2")."',
			'".clearFormData("txtAddress_3")."',
			'".clearFormData("txtCity")."',
			'".clearFormData("ddlState")."',
			'".clearFormData("txtPostalCode")."',
			'".clearFormData("ddlAltCountry")."',
			'".clearFormData("txtAltAddress_1")."',
			'".clearFormData("txtAltAddress_2")."',
			'".clearFormData("txtAltAddress_3")."',
			'".clearFormData("txtAltCity")."',
			'".clearFormData("ddlAltState")."',
			'".clearFormData("txtAltZipcode")."',
			'".clearFormData("rdMailing")."',
			'".clearFormData("txtComments")."',
			'".clearFormData("birth_date")."',
			'".clearFormData("txtEmergencyName")."',
			'".clearFormData("txtEmergencyPhone")."'
		)";

		$db->query($sqlInsert);
		$isAdded = true;
	}
	
*/?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Move in tenant</title>
<link href="<?php echo base_url();?>css/styles_popup.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url();?>js/calendarDateInput.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>js/SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<link href="<?php echo base_url();?>css/SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
</head>

<body>
<form action="<?php echo base_url();?>rentals/save_leases" method="post">
<div id="wrapper_header"><a href="#" onclick="return confirm('Do you want to cancel?');"><img src="<?php echo base_url();?>images/btn_cancel.png" alt="Cancel" width="109" height="34" border="0" style="float:right" /></a>
  <input type="image" name="submit2" id="submit2" src="<?php echo base_url();?>images/btn_save.png" />
</div>
<p>&nbsp;</p>
<?php /*if ($isAdded) { echo '<div class="msg_success">Tenant/Lease information is added successfully.</div>'; } */?>
<table width="575" border="0" align="center" cellpadding="2" cellspacing="3">
  <tr>
    <td colspan="2"><h1>Move In Tenant</h1></td>
    </tr>
  <tr>
    <td colspan="2"> <h2>Step 1 of 4: Select a rental unit</h2></td>
    </tr>
  <tr>
    <td width="188">Property name</td>
    <td width="370"><span id="sprytextfield1">
      <input name="txtPropertyName" type="text" class="textbox" id="txtPropertyName" />
      <span class="textfieldRequiredMsg">*</span></span></td>
  </tr>
  <tr>
    <td>Unit number</td>
    <td><select name="ddlUnitNumber" id="ddlUnitNumber">
      <option value="1">1</option>
      <option value="2">2</option>
      <option value="3">3</option>
      <option value="4">4</option>
      <option value="5">5</option>
      <option value="6">6</option>
      <option value="7">7</option>
      <option value="8">8</option>
      <option value="9">9</option>
      <option value="10">10</option>
      <option value="11">11</option>
      <option value="12">12</option>
      <option value="13">13</option>
      <option value="14">14</option>
      <option value="15">15</option>
    </select>    </td>
  </tr>
  <tr>
    <td colspan="2"> <h2>Step 2 of 4: Enter lease information</h2></td>
    </tr>
  <tr>
    <td>Type</td>
    <td><select name="dllType" id="dllType">
      <option value="Fixed">Fixed</option>
      <option value="Fixed w/rollover">Fixed w/rollover</option>
      <option value="At-Will">At-Will</option>
        </select></td>
  </tr>
  <tr>
    <td valign="top">Date</td>
    <td>From: <script>DateInput('date_from', true, 'YYYY-MM-DD')</script> 
      To: <script>DateInput('date_to', true, 'YYYY-MM-DD')</script>
        <br />
      <input type="checkbox" name="chk" id="chk" />
      Move tenants out automatically</td>
  </tr>
  <tr>
    <td>Rent due on</td>
    <td><select name="ddlRentDue" id="ddlRentDue">
      <option value="1st">1st</option>
      <option value="2nd">2nd</option>
      <option value="3rd">3rd</option>
      <option value="4th">4th</option>
      <option value="5th">5th</option>
      <option value="6th">6th</option>
      <option value="7th">7th</option>
      <option value="9th">9th</option>
      <option value="10th">10th</option>
      <option value="11th">11th</option>
      <option value="12th">12th</option>
      <option value="13th">13th</option>
      <option value="14th">14th</option>
      <option value="15th">15th</option>
      <option value="16th">16th</option>
      <option value="17th">17th</option>
      <option value="18th">18th</option>
      <option value="19th">19th</option>
      <option value="20th">20th</option>
      <option value="21th">21th</option>
      <option value="22th">22th</option>
      <option value="23th">23th</option>
      <option value="24th">24th</option>
      <option value="25th">25th</option>
      <option value="26th">26th</option>
      <option value="27th">27th</option>
      <option value="28th">28th</option>
    </select>       
      of the month</td>
  </tr>
  <tr>
    <td>Frequency</td>
    <td><select name="ddlFrequency" id="ddlFrequency">
      <option value="Daily">Daily</option>
      <option value="Weekly">Weekly</option>
      <option value="Every Two Weeks">Every Two Weeks</option>
      <option value="Monthly">Monthly</option>
      <option value="Every Two Months">Every Two Months</option>
      <option value="Quarterly">Quarterly</option>
      <option value="Every Six Months">Every Six Months</option>
      <option value="Yearly">Yearly</option>
      <option value="One Time">One Time</option>
    </select></td>
  </tr>
  <tr>
    <td>Rent</td>
    <td>$
      <span id="sprytextfield2">
      <input name="txtRent" type="text" class="textbox" id="txtRent" value="0.00" />
      <span class="textfieldInvalidFormatMsg">Invalid format.</span></span></td>
  </tr>
  <tr>
    <td>Prepaid rent</td>
    <td>$
      <span id="sprytextfield3">
      <input name="txtPrepaidRent" type="text" class="textbox" id="txtPrepaidRent" />
      <span class="textfieldInvalidFormatMsg">Invalid format.</span>      </span></td>
  </tr>
  <tr>
    <td>Security deposit</td>
    <td>$
      <span id="sprytextfield4">
      <input name="txtSecurityDeposit" type="text" class="textbox" id="txtSecurityDeposit" />
      <span class="textfieldInvalidFormatMsg">Invalid format.</span>      </span></td>
  </tr>
  <tr>
    <td colspan="2"><h2>Step 3 of 4: Select a new or existing tenant or applicant</h2></td>
    </tr>
  <tr>
    <td colspan="2"><input name="rdTenant" type="radio" id="rd1" value="New Tenant" checked="checked" />
      Add a new tenant<br />
      <input type="radio" name="rdTenant" id="rd2" value="Current Tenant" />
      Select a current tenant<br />
      <input type="radio" name="rdTenant" id="rd3" value="Former Tenant" />
      Select a former tenant<br />
      <input type="radio" name="rdTenant" id="rd4" value="Approved rental application" />
      Select an approved rental applicant<br /></td>
    </tr>
  <tr>
    <td colspan="2"><h2>Step 4 of 4: Enter tenant information</h2></td>
    </tr>
  <tr>
    <td colspan="2"> <h3>Contact Information</h3></td>
    </tr>
  <tr>
    <td>First name</td>
    <td><span id="sprytextfield5">
      <input name="txtFirstName" type="text" class="textbox" id="txtFirstName" />
      <span class="textfieldRequiredMsg">*</span></span></td>
  </tr>
  <tr>
    <td>Last name</td>
    <td><span id="sprytextfield6">
      <input name="txtLastName" type="text" class="textbox" id="txtLastName" />
      <span class="textfieldRequiredMsg">*</span></span></td>
  </tr>
  <tr>
    <td>Primary email </td>
    <td><input name="txtPrimaryEmail" type="text" class="textbox" id="txtPrimaryEmail" /></td>
  </tr>
  <tr>
    <td> Alternate email </td>
    <td><input name="txtAlternateEmail" type="text" class="textbox" id="txtAlternateEmail" /></td>
  </tr>
  <tr>
    <td>Home</td>
    <td><input name="txtHome" type="text" class="textbox" id="txtHome" /></td>
  </tr>
  <tr>
    <td>Work</td>
    <td><input name="txtWork" type="text" class="textbox" id="txtWork" /></td>
  </tr>
  <tr>
    <td>Mobile</td>
    <td><input name="txtMobile" type="text" class="textbox" id="txtMobile" /></td>
  </tr>
  <tr>
    <td>Fax</td>
    <td><input name="txtFax" type="text" class="textbox" id="txtFax" /></td>
  </tr>
  <tr>
    <td colspan="2"> <h3>Main Address</h3></td>
    </tr>
  <tr>
    <td>Country</td>
    <td><select name="ddlCountries" id="ddlCountries">
      <option value="1">Afghanistan</option>
      <option value="2">Akrotiri</option>
      <option value="3">Albania</option>
      <option value="4">Algeria</option>
      <option value="5">American Samoa</option>
      <option value="6">Andorra</option>
      <option value="7">Angola</option>
      <option value="8">Anguilla</option>
      <option value="9">Antarctica</option>
      <option value="10">Antigua and Barbuda</option>
      <option value="11">Argentina</option>
      <option value="12">Armenia</option>
      <option value="13">Aruba</option>
      <option value="14">Ashmore and Cartier Islands</option>
      <option value="15">Australia</option>
      <option value="16">Austria</option>
      <option value="17">Azerbaijan</option>
      <option value="18">Bahamas, The</option>
      <option value="19">Bahrain</option>
      <option value="20">Bangladesh</option>
      <option value="21">Barbados</option>
      <option value="22">Bassas da India</option>
      <option value="23">Belarus</option>
      <option value="24">Belgium</option>
      <option value="25">Belize</option>
      <option value="26">Benin</option>
      <option value="27">Bermuda</option>
      <option value="28">Bhutan</option>
      <option value="29">Bolivia</option>
      <option value="30">Bosnia and Herzegovina</option>
      <option value="31">Botswana</option>
      <option value="32">Bouvet Island</option>
      <option value="33">Brazil</option>
      <option value="34">British Indian Ocean Territory</option>
      <option value="35">British Virgin Islands</option>
      <option value="36">Brunei</option>
      <option value="37">Bulgaria</option>
      <option value="38">Burkina Faso</option>
      <option value="39">Burma</option>
      <option value="40">Burundi</option>
      <option value="41">Cambodia</option>
      <option value="42">Cameroon</option>
      <option value="43">Canada</option>
      <option value="44">Cape Verde</option>
      <option value="45">Cayman Islands</option>
      <option value="46">Central African Republic</option>
      <option value="47">Chad</option>
      <option value="48">Chile</option>
      <option value="49">China</option>
      <option value="50">Christmas Island</option>
      <option value="51">Clipperton Island</option>
      <option value="52">Cocos (Keeling) Islands</option>
      <option value="53">Colombia</option>
      <option value="54">Comoros</option>
      <option value="55">Congo, Democratic Republic of the</option>
      <option value="56">Congo, Republic of the</option>
      <option value="57">Cook Islands</option>
      <option value="58">Coral Sea Islands</option>
      <option value="59">Costa Rica</option>
      <option value="60">Cote d&#39;Ivoire</option>
      <option value="61">Croatia</option>
      <option value="62">Cuba</option>
      <option value="63">Cyprus</option>
      <option value="64">Czech Republic</option>
      <option value="65">Denmark</option>
      <option value="66">Dhekelia</option>
      <option value="67">Djibouti</option>
      <option value="68">Dominica</option>
      <option value="69">Dominican Republic</option>
      <option value="70">Ecuador</option>
      <option value="71">Egypt</option>
      <option value="72">El Salvador</option>
      <option value="73">Equatorial Guinea</option>
      <option value="74">Eritrea</option>
      <option value="75">Estonia</option>
      <option value="76">Ethiopia</option>
      <option value="77">Europa Island</option>
      <option value="78">Falkland Islands (Islas Malvinas)</option>
      <option value="79">Faroe Islands</option>
      <option value="80">Fiji</option>
      <option value="81">Finland</option>
      <option value="82">France</option>
      <option value="83">French Guiana</option>
      <option value="84">French Polynesia</option>
      <option value="85">French Southern and Antarctic Lands</option>
      <option value="86">Gabon</option>
      <option value="87">Gambia, The</option>
      <option value="88">Gaza Strip</option>
      <option value="89">Georgia</option>
      <option value="90">Germany</option>
      <option value="91">Ghana</option>
      <option value="92">Gibraltar</option>
      <option value="93">Glorioso Islands</option>
      <option value="94">Greece</option>
      <option value="95">Greenland</option>
      <option value="96">Grenada</option>
      <option value="97">Guadeloupe</option>
      <option value="98">Guam</option>
      <option value="99">Guatemala</option>
      <option value="100">Guernsey</option>
      <option value="101">Guinea</option>
      <option value="102">Guinea-Bissau</option>
      <option value="103">Guyana</option>
      <option value="104">Haiti</option>
      <option value="105">Heard Island and McDonald Islands</option>
      <option value="106">Holy See (Vatican City)</option>
      <option value="107">Honduras</option>
      <option value="108">Hong Kong</option>
      <option value="109">Hungary</option>
      <option value="110">Iceland</option>
      <option value="111">India</option>
      <option value="112">Indonesia</option>
      <option value="113">Iran</option>
      <option value="114">Iraq</option>
      <option value="115">Ireland</option>
      <option value="116">Isle of Man</option>
      <option value="117">Israel</option>
      <option value="118">Italy</option>
      <option value="119">Jamaica</option>
      <option value="120">Jan Mayen</option>
      <option value="121">Japan</option>
      <option value="122">Jersey</option>
      <option value="123">Jordan</option>
      <option value="124">Juan de Nova Island</option>
      <option value="125">Kazakhstan</option>
      <option value="126">Kenya</option>
      <option value="127">Kiribati</option>
      <option value="128">Korea, North</option>
      <option value="129">Korea, South</option>
      <option value="130">Kuwait</option>
      <option value="131">Kyrgyzstan</option>
      <option value="132">Laos</option>
      <option value="133">Latvia</option>
      <option value="134">Lebanon</option>
      <option value="135">Lesotho</option>
      <option value="136">Liberia</option>
      <option value="137">Libya</option>
      <option value="138">Liechtenstein</option>
      <option value="139">Lithuania</option>
      <option value="140">Luxembourg</option>
      <option value="141">Macau</option>
      <option value="142">Macedonia</option>
      <option value="143">Madagascar</option>
      <option value="144">Malawi</option>
      <option value="145">Malaysia</option>
      <option value="146">Maldives</option>
      <option value="147">Mali</option>
      <option value="148">Malta</option>
      <option value="149">Marshall Islands</option>
      <option value="150">Martinique</option>
      <option value="151">Mauritania</option>
      <option value="152">Mauritius</option>
      <option value="153">Mayotte</option>
      <option value="154">Mexico</option>
      <option value="155">Micronesia, Federated States of</option>
      <option value="156">Moldova</option>
      <option value="157">Monaco</option>
      <option value="158">Mongolia</option>
      <option value="159">Montserrat</option>
      <option value="160">Morocco</option>
      <option value="161">Mozambique</option>
      <option value="162">Namibia</option>
      <option value="163">Nauru</option>
      <option value="164">Navassa Island</option>
      <option value="165">Nepal</option>
      <option value="166">Netherlands</option>
      <option value="167">Netherlands Antilles</option>
      <option value="168">New Caledonia</option>
      <option value="169">New Zealand</option>
      <option value="170">Nicaragua</option>
      <option value="171">Niger</option>
      <option value="172">Nigeria</option>
      <option value="173">Niue</option>
      <option value="174">Norfolk Island</option>
      <option value="175">Northern Mariana Islands</option>
      <option value="176">Norway</option>
      <option value="177">Oman</option>
      <option value="178">Pakistan</option>
      <option value="179">Palau</option>
      <option value="180">Panama</option>
      <option value="181">Papua New Guinea</option>
      <option value="182">Paracel Islands</option>
      <option value="183">Paraguay</option>
      <option value="184">Peru</option>
      <option value="185">Philippines</option>
      <option value="186">Pitcairn Islands</option>
      <option value="187">Poland</option>
      <option value="188">Portugal</option>
      <option value="189">Puerto Rico</option>
      <option value="190">Qatar</option>
      <option value="191">Reunion</option>
      <option value="192">Romania</option>
      <option value="193">Russia</option>
      <option value="194">Rwanda</option>
      <option value="195">Saint Helena</option>
      <option value="196">Saint Kitts and Nevis</option>
      <option value="197">Saint Lucia</option>
      <option value="198">Saint Pierre and Miquelon</option>
      <option value="199">Saint Vincent and the Grenadines</option>
      <option value="200">Samoa</option>
      <option value="201">San Marino</option>
      <option value="202">Sao Tome and Principe</option>
      <option value="203">Saudi Arabia</option>
      <option value="204">Senegal</option>
      <option value="205">Serbia and Montenegro</option>
      <option value="206">Seychelles</option>
      <option value="207">Sierra Leone</option>
      <option value="208">Singapore</option>
      <option value="209">Slovakia</option>
      <option value="210">Slovenia</option>
      <option value="211">Solomon Islands</option>
      <option value="212">Somalia</option>
      <option value="213">South Africa</option>
      <option value="214">South Georgia and the South Sandwich Islands</option>
      <option value="215">Spain</option>
      <option value="216">Spratly Islands</option>
      <option value="217">Sri Lanka</option>
      <option value="218">Sudan</option>
      <option value="219">Suriname</option>
      <option value="220">Svalbard</option>
      <option value="221">Swaziland</option>
      <option value="222">Sweden</option>
      <option value="223">Switzerland</option>
      <option value="224">Syria</option>
      <option value="225">Taiwan</option>
      <option value="226">Tajikistan</option>
      <option value="227">Tanzania</option>
      <option value="228">Thailand</option>
      <option value="229">Timor-Leste</option>
      <option value="230">Togo</option>
      <option value="231">Tokelau</option>
      <option value="232">Tonga</option>
      <option value="233">Trinidad and Tobago</option>
      <option value="234">Tromelin Island</option>
      <option value="235">Tunisia</option>
      <option value="236">Turkey</option>
      <option value="237">Turkmenistan</option>
      <option value="238">Turks and Caicos Islands</option>
      <option value="239">Tuvalu</option>
      <option value="240">Uganda</option>
      <option value="241">Ukraine</option>
      <option value="242">United Arab Emirates</option>
      <option value="243">United Kingdom</option>
      <option selected="selected" value="244">United States</option>
      <option value="245">Uruguay</option>
      <option value="246">Uzbekistan</option>
      <option value="247">Vanuatu</option>
      <option value="248">Venezuela</option>
      <option value="249">Vietnam</option>
      <option value="250">Virgin Islands</option>
      <option value="251">Wake Island</option>
      <option value="252">Wallis and Futuna</option>
      <option value="253">West Bank</option>
      <option value="254">Western Sahara</option>
      <option value="255">Yemen</option>
      <option value="256">Zambia</option>
      <option value="257">Zimbabwe</option>
    </select></td>
  </tr>
  <tr>
    <td>Address 1</td>
    <td><input name="txtAddress_1" type="text" class="textbox" id="txtAddress_1" /></td>
  </tr>
  <tr>
    <td>Address 2 </td>
    <td><input name="txtAddress_2" type="text" class="textbox" id="txtAddress_2" /></td>
  </tr>
  <tr>
    <td>Address 3</td>
    <td><input name="txtAddress_3" type="text" class="textbox" id="txtAddress_3" /></td>
  </tr>
  <tr>
    <td>City/Locality</td>
    <td><input name="txtCity" type="text" class="textbox" id="txtCity" /></td>
  </tr>
  <tr>
    <td>Prov./Terr./State</td>
    <td><select name="ddlState" id="ddlState">
      <option value="AE">AE</option>
	<option value="AK">AK</option>
	<option value="AL">AL</option>
	<option value="AP">AP</option>
	<option value="AR">AR</option>
	<option value="AZ">AZ</option>
	<option value="CA">CA</option>
	<option value="CO">CO</option>
	<option value="CT">CT</option>
	<option value="DC">DC</option>
	<option value="DE">DE</option>
	<option value="FL">FL</option>
	<option value="GA">GA</option>
	<option value="HI">HI</option>
	<option value="IA">IA</option>
	<option value="ID">ID</option>
	<option value="IL">IL</option>
	<option value="IN">IN</option>
	<option value="KS">KS</option>
	<option value="KY">KY</option>
	<option value="LA">LA</option>
	<option value="MA">MA</option>
	<option value="MD">MD</option>
	<option value="ME">ME</option>
	<option value="MI">MI</option>
	<option value="MN">MN</option>
	<option value="MO">MO</option>
	<option value="MS">MS</option>
	<option value="MT">MT</option>
	<option value="NC">NC</option>
	<option value="ND">ND</option>
	<option value="NE">NE</option>
	<option value="NH">NH</option>
	<option value="NJ">NJ</option>
	<option value="NM">NM</option>
	<option value="NV">NV</option>
	<option value="NY">NY</option>
	<option value="OH">OH</option>
	<option value="OK">OK</option>
	<option value="OR">OR</option>
	<option value="PA">PA</option>
	<option value="RI">RI</option>
	<option value="SC">SC</option>
	<option value="SD">SD</option>
	<option value="TN">TN</option>
	<option value="TX">TX</option>
	<option value="UT">UT</option>
	<option value="VA">VA</option>
	<option value="VT">VT</option>
	<option value="WA">WA</option>
	<option value="WI">WI</option>
	<option value="WV">WV</option>
	<option value="WY">WY</option>
    </select>
    </td>
  </tr>
  <tr>
    <td>Postal code</td>
    <td><input name="txtPostalCode" type="text" class="textbox" id="txtPostalCode" /></td>
  </tr>
  <tr>
    <td colspan="2"><h3>Alternate Address</h3></td>
    </tr>
  <tr>
    <td>Country</td>
    <td><select name="ddlAltCountry" id="ddlAltCountry">
      <option value="1">Afghanistan</option>
      <option value="2">Akrotiri</option>
      <option value="3">Albania</option>
      <option value="4">Algeria</option>
      <option value="5">American Samoa</option>
      <option value="6">Andorra</option>
      <option value="7">Angola</option>
      <option value="8">Anguilla</option>
      <option value="9">Antarctica</option>
      <option value="10">Antigua and Barbuda</option>
      <option value="11">Argentina</option>
      <option value="12">Armenia</option>
      <option value="13">Aruba</option>
      <option value="14">Ashmore and Cartier Islands</option>
      <option value="15">Australia</option>
      <option value="16">Austria</option>
      <option value="17">Azerbaijan</option>
      <option value="18">Bahamas, The</option>
      <option value="19">Bahrain</option>
      <option value="20">Bangladesh</option>
      <option value="21">Barbados</option>
      <option value="22">Bassas da India</option>
      <option value="23">Belarus</option>
      <option value="24">Belgium</option>
      <option value="25">Belize</option>
      <option value="26">Benin</option>
      <option value="27">Bermuda</option>
      <option value="28">Bhutan</option>
      <option value="29">Bolivia</option>
      <option value="30">Bosnia and Herzegovina</option>
      <option value="31">Botswana</option>
      <option value="32">Bouvet Island</option>
      <option value="33">Brazil</option>
      <option value="34">British Indian Ocean Territory</option>
      <option value="35">British Virgin Islands</option>
      <option value="36">Brunei</option>
      <option value="37">Bulgaria</option>
      <option value="38">Burkina Faso</option>
      <option value="39">Burma</option>
      <option value="40">Burundi</option>
      <option value="41">Cambodia</option>
      <option value="42">Cameroon</option>
      <option value="43">Canada</option>
      <option value="44">Cape Verde</option>
      <option value="45">Cayman Islands</option>
      <option value="46">Central African Republic</option>
      <option value="47">Chad</option>
      <option value="48">Chile</option>
      <option value="49">China</option>
      <option value="50">Christmas Island</option>
      <option value="51">Clipperton Island</option>
      <option value="52">Cocos (Keeling) Islands</option>
      <option value="53">Colombia</option>
      <option value="54">Comoros</option>
      <option value="55">Congo, Democratic Republic of the</option>
      <option value="56">Congo, Republic of the</option>
      <option value="57">Cook Islands</option>
      <option value="58">Coral Sea Islands</option>
      <option value="59">Costa Rica</option>
      <option value="60">Cote d&#39;Ivoire</option>
      <option value="61">Croatia</option>
      <option value="62">Cuba</option>
      <option value="63">Cyprus</option>
      <option value="64">Czech Republic</option>
      <option value="65">Denmark</option>
      <option value="66">Dhekelia</option>
      <option value="67">Djibouti</option>
      <option value="68">Dominica</option>
      <option value="69">Dominican Republic</option>
      <option value="70">Ecuador</option>
      <option value="71">Egypt</option>
      <option value="72">El Salvador</option>
      <option value="73">Equatorial Guinea</option>
      <option value="74">Eritrea</option>
      <option value="75">Estonia</option>
      <option value="76">Ethiopia</option>
      <option value="77">Europa Island</option>
      <option value="78">Falkland Islands (Islas Malvinas)</option>
      <option value="79">Faroe Islands</option>
      <option value="80">Fiji</option>
      <option value="81">Finland</option>
      <option value="82">France</option>
      <option value="83">French Guiana</option>
      <option value="84">French Polynesia</option>
      <option value="85">French Southern and Antarctic Lands</option>
      <option value="86">Gabon</option>
      <option value="87">Gambia, The</option>
      <option value="88">Gaza Strip</option>
      <option value="89">Georgia</option>
      <option value="90">Germany</option>
      <option value="91">Ghana</option>
      <option value="92">Gibraltar</option>
      <option value="93">Glorioso Islands</option>
      <option value="94">Greece</option>
      <option value="95">Greenland</option>
      <option value="96">Grenada</option>
      <option value="97">Guadeloupe</option>
      <option value="98">Guam</option>
      <option value="99">Guatemala</option>
      <option value="100">Guernsey</option>
      <option value="101">Guinea</option>
      <option value="102">Guinea-Bissau</option>
      <option value="103">Guyana</option>
      <option value="104">Haiti</option>
      <option value="105">Heard Island and McDonald Islands</option>
      <option value="106">Holy See (Vatican City)</option>
      <option value="107">Honduras</option>
      <option value="108">Hong Kong</option>
      <option value="109">Hungary</option>
      <option value="110">Iceland</option>
      <option value="111">India</option>
      <option value="112">Indonesia</option>
      <option value="113">Iran</option>
      <option value="114">Iraq</option>
      <option value="115">Ireland</option>
      <option value="116">Isle of Man</option>
      <option value="117">Israel</option>
      <option value="118">Italy</option>
      <option value="119">Jamaica</option>
      <option value="120">Jan Mayen</option>
      <option value="121">Japan</option>
      <option value="122">Jersey</option>
      <option value="123">Jordan</option>
      <option value="124">Juan de Nova Island</option>
      <option value="125">Kazakhstan</option>
      <option value="126">Kenya</option>
      <option value="127">Kiribati</option>
      <option value="128">Korea, North</option>
      <option value="129">Korea, South</option>
      <option value="130">Kuwait</option>
      <option value="131">Kyrgyzstan</option>
      <option value="132">Laos</option>
      <option value="133">Latvia</option>
      <option value="134">Lebanon</option>
      <option value="135">Lesotho</option>
      <option value="136">Liberia</option>
      <option value="137">Libya</option>
      <option value="138">Liechtenstein</option>
      <option value="139">Lithuania</option>
      <option value="140">Luxembourg</option>
      <option value="141">Macau</option>
      <option value="142">Macedonia</option>
      <option value="143">Madagascar</option>
      <option value="144">Malawi</option>
      <option value="145">Malaysia</option>
      <option value="146">Maldives</option>
      <option value="147">Mali</option>
      <option value="148">Malta</option>
      <option value="149">Marshall Islands</option>
      <option value="150">Martinique</option>
      <option value="151">Mauritania</option>
      <option value="152">Mauritius</option>
      <option value="153">Mayotte</option>
      <option value="154">Mexico</option>
      <option value="155">Micronesia, Federated States of</option>
      <option value="156">Moldova</option>
      <option value="157">Monaco</option>
      <option value="158">Mongolia</option>
      <option value="159">Montserrat</option>
      <option value="160">Morocco</option>
      <option value="161">Mozambique</option>
      <option value="162">Namibia</option>
      <option value="163">Nauru</option>
      <option value="164">Navassa Island</option>
      <option value="165">Nepal</option>
      <option value="166">Netherlands</option>
      <option value="167">Netherlands Antilles</option>
      <option value="168">New Caledonia</option>
      <option value="169">New Zealand</option>
      <option value="170">Nicaragua</option>
      <option value="171">Niger</option>
      <option value="172">Nigeria</option>
      <option value="173">Niue</option>
      <option value="174">Norfolk Island</option>
      <option value="175">Northern Mariana Islands</option>
      <option value="176">Norway</option>
      <option value="177">Oman</option>
      <option value="178">Pakistan</option>
      <option value="179">Palau</option>
      <option value="180">Panama</option>
      <option value="181">Papua New Guinea</option>
      <option value="182">Paracel Islands</option>
      <option value="183">Paraguay</option>
      <option value="184">Peru</option>
      <option value="185">Philippines</option>
      <option value="186">Pitcairn Islands</option>
      <option value="187">Poland</option>
      <option value="188">Portugal</option>
      <option value="189">Puerto Rico</option>
      <option value="190">Qatar</option>
      <option value="191">Reunion</option>
      <option value="192">Romania</option>
      <option value="193">Russia</option>
      <option value="194">Rwanda</option>
      <option value="195">Saint Helena</option>
      <option value="196">Saint Kitts and Nevis</option>
      <option value="197">Saint Lucia</option>
      <option value="198">Saint Pierre and Miquelon</option>
      <option value="199">Saint Vincent and the Grenadines</option>
      <option value="200">Samoa</option>
      <option value="201">San Marino</option>
      <option value="202">Sao Tome and Principe</option>
      <option value="203">Saudi Arabia</option>
      <option value="204">Senegal</option>
      <option value="205">Serbia and Montenegro</option>
      <option value="206">Seychelles</option>
      <option value="207">Sierra Leone</option>
      <option value="208">Singapore</option>
      <option value="209">Slovakia</option>
      <option value="210">Slovenia</option>
      <option value="211">Solomon Islands</option>
      <option value="212">Somalia</option>
      <option value="213">South Africa</option>
      <option value="214">South Georgia and the South Sandwich Islands</option>
      <option value="215">Spain</option>
      <option value="216">Spratly Islands</option>
      <option value="217">Sri Lanka</option>
      <option value="218">Sudan</option>
      <option value="219">Suriname</option>
      <option value="220">Svalbard</option>
      <option value="221">Swaziland</option>
      <option value="222">Sweden</option>
      <option value="223">Switzerland</option>
      <option value="224">Syria</option>
      <option value="225">Taiwan</option>
      <option value="226">Tajikistan</option>
      <option value="227">Tanzania</option>
      <option value="228">Thailand</option>
      <option value="229">Timor-Leste</option>
      <option value="230">Togo</option>
      <option value="231">Tokelau</option>
      <option value="232">Tonga</option>
      <option value="233">Trinidad and Tobago</option>
      <option value="234">Tromelin Island</option>
      <option value="235">Tunisia</option>
      <option value="236">Turkey</option>
      <option value="237">Turkmenistan</option>
      <option value="238">Turks and Caicos Islands</option>
      <option value="239">Tuvalu</option>
      <option value="240">Uganda</option>
      <option value="241">Ukraine</option>
      <option value="242">United Arab Emirates</option>
      <option value="243">United Kingdom</option>
      <option selected="selected" value="244">United States</option>
      <option value="245">Uruguay</option>
      <option value="246">Uzbekistan</option>
      <option value="247">Vanuatu</option>
      <option value="248">Venezuela</option>
      <option value="249">Vietnam</option>
      <option value="250">Virgin Islands</option>
      <option value="251">Wake Island</option>
      <option value="252">Wallis and Futuna</option>
      <option value="253">West Bank</option>
      <option value="254">Western Sahara</option>
      <option value="255">Yemen</option>
      <option value="256">Zambia</option>
      <option value="257">Zimbabwe</option>
    </select></td>
  </tr>
  <tr>
    <td>Address 1</td>
    <td><input name="txtAltAddress_1" type="text" class="textbox" id="txtAltAddress_1" /></td>
  </tr>
  <tr>
    <td>Address 2 </td>
    <td><input name="txtAltAddress_2" type="text" class="textbox" id="txtAltAddress_2" /></td>
  </tr>
  <tr>
    <td>Address 3</td>
    <td><input name="txtAltAddress_3" type="text" class="textbox" id="txtAltAddress_3" /></td>
  </tr>
  <tr>
    <td>City</td>
    <td><input name="txtAltCity" type="text" class="textbox" id="txtAltCity" /></td>
  </tr>
  <tr>
    <td>State</td>
    <td><select name="ddlAltState" id="ddlAltState">
      <option value="AE">AE</option>
      <option value="AK">AK</option>
      <option value="AL">AL</option>
      <option value="AP">AP</option>
      <option value="AR">AR</option>
      <option value="AZ">AZ</option>
      <option value="CA">CA</option>
      <option value="CO">CO</option>
      <option value="CT">CT</option>
      <option value="DC">DC</option>
      <option value="DE">DE</option>
      <option value="FL">FL</option>
      <option value="GA">GA</option>
      <option value="HI">HI</option>
      <option value="IA">IA</option>
      <option value="ID">ID</option>
      <option value="IL">IL</option>
      <option value="IN">IN</option>
      <option value="KS">KS</option>
      <option value="KY">KY</option>
      <option value="LA">LA</option>
      <option value="MA">MA</option>
      <option value="MD">MD</option>
      <option value="ME">ME</option>
      <option value="MI">MI</option>
      <option value="MN">MN</option>
      <option value="MO">MO</option>
      <option value="MS">MS</option>
      <option value="MT">MT</option>
      <option value="NC">NC</option>
      <option value="ND">ND</option>
      <option value="NE">NE</option>
      <option value="NH">NH</option>
      <option value="NJ">NJ</option>
      <option value="NM">NM</option>
      <option value="NV">NV</option>
      <option value="NY">NY</option>
      <option value="OH">OH</option>
      <option value="OK">OK</option>
      <option value="OR">OR</option>
      <option value="PA">PA</option>
      <option value="RI">RI</option>
      <option value="SC">SC</option>
      <option value="SD">SD</option>
      <option value="TN">TN</option>
      <option value="TX">TX</option>
      <option value="UT">UT</option>
      <option value="VA">VA</option>
      <option value="VT">VT</option>
      <option value="WA">WA</option>
      <option value="WI">WI</option>
      <option value="WV">WV</option>
      <option value="WY">WY</option>
    </select></td>
  </tr>
  <tr>
    <td>Zip code</td>
    <td><input name="txtAltZipcode" type="text" class="textbox" id="txtAltZipcode" /></td>
  </tr>
  <tr>
    <td valign="top">Mailing preference</td>
    <td><input name="rdMailing" type="radio" id="rd5" value="Send mail to main address" checked="checked" />
      Send mail to main address<br />
      <input type="radio" name="rdMailing" id="rd6" value="Send mail to main address" />
      Send mail to alternate address</td>
  </tr>
  <tr>
    <td colspan="2"><h3>Additional Information</h3></td>
    </tr>
  <tr>
    <td valign="top">Comments</td>
    <td><textarea name="txtComments" cols="45" rows="5" class="textarea" id="txtComments"></textarea></td>
  </tr>
  <tr>
    <td>Date of birth</td>
    <td><script>DateInput('birth_date', true, 'YYYY-MM-DD')</script></td>
  </tr>
  <tr>
    <td colspan="2"> <h3>Emergency Contact</h3></td>
    </tr>
  <tr>
    <td>Name</td>
    <td><input name="txtEmergencyName" type="text" class="textbox" id="txtEmergencyName" /></td>
  </tr>
  <tr>
    <td>Phone</td>
    <td><input name="txtEmergencyPhone" type="text" class="textbox" id="txtEmergencyPhone" /></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><input name="submit" type="submit" class="submit_button" id="submit" value="Save" /></td>
  </tr>
</table>
<p><br />
</p>
<p>&nbsp;</p>
</form>
<script type="text/javascript">
<!--
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1");
var sprytextfield2 = new Spry.Widget.ValidationTextField("sprytextfield2", "currency", {isRequired:false, validateOn:["blur"]});
var sprytextfield3 = new Spry.Widget.ValidationTextField("sprytextfield3", "currency", {isRequired:false, validateOn:["blur"]});
var sprytextfield4 = new Spry.Widget.ValidationTextField("sprytextfield4", "currency", {isRequired:false, validateOn:["blur"]});
var sprytextfield5 = new Spry.Widget.ValidationTextField("sprytextfield5");
var sprytextfield6 = new Spry.Widget.ValidationTextField("sprytextfield6");
//-->
</script>
</body>
</html>
