<?php
class Associations extends CI_Controller{

    public function association(){
       $this->load->model('associations/association');
        $this->data["aso_data"] = $this->association->as_info();
        //var_dump($this->data);
        $this->load->view('associations/association',$this->data);
    }
    public function ownership_account(){
        $this->load->model('associations/ownership_account');
        $this->data["owner_data"] = $this->ownership_account->owner_info();
            //var_dump($this->data);
        $this->load->view('associations/ownership_account',$this->data);

    }
    public function association_owners(){
        $this->load->model('associations/association_owner');
        $this->data["asso_data"] = $this->association_owner->association_info();
        $this->load->view('associations/association_owners',$this->data);

    }
    public function outstanding_balances(){
        $this->load->model('associations/outstanding_balances');
        $this->data["balance_data"] = $this->outstanding_balances->balance_info();
       //var_dump($this->data);
        $this->load->view('associations/outstanding_balances',$this->data);

    }
    public function add_association(){
        $this->load->view('associations/add_association');
    }
    public function save_association(){
          // var_dump($_POST);
        $txtAssociation = $this->input->post("txtAssociation");
        $ddlAccountType = $this->input->post("ddlAccountType");
        $ddlNumberOfUnits = $this->input->post("ddlNumberOfUnits");
        $ddlCountries = $this->input->post("ddlCountries");
        $txtAddress_1 = $this->input->post("txtAddress_1");
        $txtAddress_2 = $this->input->post("txtAddress_2");
        $txtAddress_3 = $this->input->post("txtAddress_3");
        $txtCity = $this->input->post("txtCity");
        $ddlState = $this->input->post("ddlState");
        $txtZipCode = $this->input->post("txtZipCode");
            $addAssociation = array("txtAssociation"=>$txtAssociation,"ddlAccountType"=>$ddlAccountType,
            "ddlNumberOfUnits"=>$ddlNumberOfUnits,"ddlCountries"=>$ddlCountries,"txtAddress_1"=>$txtAddress_1,
            "txtAddress_2"=>$txtAddress_2,"txtAddress_3"=>$txtAddress_3,"txtCity"=>$txtCity,"ddlState"=>$ddlState,
            "txtZipCode"=>$txtZipCode);
        $this->load->model('associations/association');
        echo $this->association->add_association($addAssociation);
    }
    public function add_ownership(){
        $this->load->view('associations/add_ownership');

    }
    public function add_account(){
       //var_dump($_POST);
        $txtAssociationName =   $this->input->post("txtAssociationName");
        $ddlNumberOfUnits = $this->input->post("ddlNumberOfUnits");
		$txtMoveInDate = $this->input->post("txtMoveInDate");
	    $txtAssociationFee = $this->input->post("txtAssociationFee");
	    $ddlFrequency = $this->input->post("ddlFrequency");
		$rdOwnerInfo=$this->input->post("rdOwnerInfo");
	    $txtFirstName = $this->input->post("txtFirstName");
        $txtLastName = $this->input->post("txtLastName");
        $txtLoginEmail =$this->input->post("txtLoginEmail");
        $txtAlternateEmail =$this->input->post("txtAlternateEmail");
        $txtHome =$this->input->post("txtHome");
        $txtWork =$this->input->post("txtWork");
        $txtMobile =$this->input->post("txtMobile");
        $txtFax =$this->input->post("txtFax");
        $ddlCountries =$this->input->post("ddlCountries");
        $txtAddress_1 =$this->input->post("txtAddress_1");
        $txtAddress_2 =$this->input->post("txtAddress_2");
        $txtAddress_3 =$this->input->post("txtAddress_3");
        $txtCity =$this->input->post("txtCity");
        $ddlState =$this->input->post("ddlState");
        $txtPostalCode =$this->input->post("txtPostalCode");
        $ddlAltCountries =$this->input->post("ddlAltCountries");
        $txtAltAddress_1 =$this->input->post("txtAltAddress_1");
        $txtAltAddress_2 =$this->input->post("txtAltAddress_2");
        $txtAltAddress_3 =$this->input->post("txtAltAddress_3");
        $txtAltCity =$this->input->post("txtAltCity");
        $ddlAltState =$this->input->post("ddlAltState");
        $txtAltZipcode =$this->input->post("txtAltZipcode");
        $rdMailingRef =$this->input->post("rdMailingRef");
        $txtComments=$this->input->post("txtComments");
        $txtBirthDate =$this->input->post("txtBirthDate");
        $txtEmergencyName =$this->input->post("txtEmergencyName");
        $txtEmergencyPhone =$this->input->post("txtEmergencyPhone");
        $chkBoardMember =$this->input->post("chkBoardMember");
        $chkOccupancy =$this->input->post("chkOccupancy");

        $saveAccountData = array("txtAssociationName"=>$txtAssociationName,
            "ddlNumberOfUnits"=>$ddlNumberOfUnits,"txtMoveInDate"=>$txtMoveInDate,
            "txtAssociationFee"=>$txtAssociationFee,"ddlFrequency"=>$ddlFrequency,
            "rdOwnerInfo"=>$rdOwnerInfo,"txtFirstName"=>$txtFirstName,"txtLastName"=>$txtLastName,
            "txtLoginEmail"=>$txtLoginEmail,"txtAlternateEmail"=>$txtAlternateEmail,
            "txtHome"=>$txtHome,"txtWork"=>$txtWork,"txtMobile"=>$txtMobile,"txtFax"=>$txtFax,
            "ddlCountries"=>$ddlCountries,"txtAddress_1"=>$txtAddress_1,"txtAddress_2"=>$txtAddress_2,
            "txtAddress_3"=>$txtAddress_3,"txtCity"=>$txtCity,"ddlState"=>$ddlState,"txtPostalCode"=>$txtPostalCode,
            "ddlAltCountries"=>$ddlAltCountries,"txtAltAddress_1"=>$txtAltAddress_1,"txtAltAddress_2"=>$txtAltAddress_2,
            "txtAltAddress_3"=>$txtAltAddress_3,"txtAltCity"=>$txtAltCity,"ddlAltState"=>$ddlAltState,
            "txtAltZipcode"=>$txtAltZipcode,"rdMailingRef"=>$rdMailingRef,"txtComments"=>$txtComments,
            "txtBirthDate"=>$txtBirthDate,"txtEmergencyName"=>$txtEmergencyName,
            "txtEmergencyPhone"=>$txtEmergencyPhone,"chkBoardMember"=>$chkBoardMember,"chkOccupancy"=>$chkOccupancy

        );

        $this->load->model('associations/ownership_account');
        echo $this->ownership_account->add_ownership_account($saveAccountData);
    }
    /*public function add_owner(){
        $this->load->model('associations/association_owner');
        echo $this->association_owner->add_ownership();
    }*/
}