<?php
/*include ("../includes/classes/class.db.php");
include ("../includes/functions/general.php");*/

//-------------------------------------------------------------------------------------------
								/*Objects*/
//-------------------------------------------------------------------------------------------					
/*	$db = new db();*/
//-------------------------------------------------------------------------------------------
								/*General Coding Area*/
	
	/*if (isset($_GET['id']) && isset($_GET['mode']) && $_GET['id'] != "" && $_GET['mode'] == "delete") {
		$sqlDelete = "DELETE FROM bank_vendor WHERE vendor_id = '".$_GET['id']."'";
		$db->query($sqlDelete);
		redirect("?");
	}
	
	$sql = "SELECT vendor_id, first_name, last_name, expense_account, primary_email, home
			FROM
			bank_vendor
			ORDER BY
			vendor_id DESC";
	
	$query= $db->ExeQuersys($sql);
	
	while ($row = mysql_fetch_array($query))
	{
		$valVendorID = $row['vendor_id'];
		$valFirstName = $row['first_name'];
		$valLastName = $row['last_name'];
		$valExpenseAccount = $row['expense_account'];
		$valPrimaryEmail = $row['primary_email'];
		$valHomePhone = $row['home'];
		
		$tr .= '<tr>
				  <td>'.$valFirstName.'</td>
				  <td>'.$valLastName.'</td>
				  <td>'.$valExpenseAccount.'</td>
				  <td>'.$valPrimaryEmail.'</td>
				  <td>'.$valHomePhone.'</td>
				  <td align="center">
				  <a href="javascript:void();" title="Click here to edit" onclick="window.open(\'update_vendor.php?id='.$valVendorID.'\',\'mywindow\',\'menubar=1,resizable=1, scrollbars=1, width=750,height=650\');" >
				  <img src="../images/icon_edit.png" width="16" height="16" alt="Edit" border="0" /></a> &nbsp;&nbsp;|&nbsp;&nbsp;
				  <a href="?id='.$valVendorID.'&mode=delete" title="Click here to delete" onclick="return confirm(\'Do you want to delete this record?\');">
				  <img src="../images/icon_delete.png" width="16" height="16" alt="Delete" border="0" />
				  </a>
				  </td>
				</tr>';
	}
*/?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Vendors</title>
<link href="<?php echo base_url();?>css/styles.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url();?>js/SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<link href="<?php echo base_url();?>css/SpryAssets/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="top_menu">
  <img src="<?php echo base_url();?>images/logo.png" width="126" height="67" alt="Logo" style="float:left; padding-right:10px;" />
  <h1>Real Estate Shark</h1>
</div>
<div id="wrapper_header">
  <ul id="MenuBar1" class="MenuBarHorizontal">
    <li><a class="MenuBarItemSubmenu" href="#">Rentals</a>
      <ul>
        <li><a href="<?php echo base_url();?>rentals/properties">Properties</a></li>
        <li><a href="<?php echo base_url();?>rentals/leases ">Leases</a></li>
        <li><a href="<?php echo base_url();?>rentals/tenants">Tenants</a></li>
        <li><a href="<?php echo base_url();?>rentals/tenants">Tenants</a></li>
        <li><a href="<?php echo base_url();?>rentals/listings">Listings</a></li>
        <li><a href="<?php echo base_url();?>rentals/rental_applications">Rental applications</a></li>
        <li><a href="<?php echo base_url();?>rentals/rental_owners">Rental owners</a></li>
        <li><a href="<?php echo base_url();?>rentals/outstanding_balances">Outstanding balances</a></li>
      </ul>
    </li>
    <li><a href="#" class="MenuBarItemSubmenu">Associations</a>
      <ul>
        <li><a href="<?php echo base_url();?>associations/association">Associations</a></li>
        <li><a href="<?php echo base_url();?>associations/ownership_account">Ownership accounts</a></li>
        <li><a href="<?php echo base_url();?>associations/association_owners">Association owners</a></li>
        <li><a href="<?php echo base_url();?>associations/outstanding_balances">Outstanding balances</a></li>
      </ul>
    </li>
    <li><a class="MenuBarItemSubmenu" href="#">Accounting</a>
      <ul>
        <li><a href="<?php echo base_url();?>accounts/index">Financials</a></li>
        <li><a href="<?php echo base_url();?>accounts/general_ledger">General ledger</a></li>
        <li><a href="<?php echo base_url();?>accounts/banking">Banking</a></li>
        <li class="divider"><a href="<?php echo base_url();?>accounts/vendors">Vendors</a></li>
        <li><a href="<?php echo base_url();?>accounts/work_orders">Work orders</a></li>
        <li><a href="<?php echo base_url();?>accounts/bills">Bills</a></li>
        <li><a href="<?php echo base_url();?>accounts/recurring_transactions">Recurring transactions</a></li>
        <li class="divider"><a href="<?php echo base_url();?>accounts/budget">Budgets</a></li>
        <li><a href="<?php echo base_url();?>accounts/chart_of_accounts">Chart of accounts</a></li>
        <li class="divider"><a href="<?php echo base_url();?>accounts/filing_1099">1099-MISC tax filings</a></li>
      </ul>
    </li>
    <li><a href="#" class="MenuBarItemSubmenu">Tasks</a>
      <ul>
        <li><a href="<?php echo base_url();?>tasks/index">My tasks</a></li>
        <li><a href="<?php echo base_url();?>tasks/unassigned_tasks">Unassigned tasks</a></li>
        <li><a href="<?php echo base_url();?>tasks/all_tasks">All tasks</a></li>
        <li class="divider"><a href="<?php echo base_url();?>tasks/recurring_tasks">Recurring tasks</a></li>
      </ul>
    </li>
    <li><a href="#" class="MenuBarItemSubmenu">Communications</a>
      <ul>
        <li><a href='#'> Public site</a></li>
        <li class="divider"><a href="#">Resident site users</a></li>
        <li><a href="#">Resident site contact directory</a></li>
        <li><a href="#">Resident site announcements</a></li>
        <li><a href="#">Association discussions</a></li>
        <li class="divider"><a href="#">Mailings</a></li>
        <li><a href="#">Mailing templates</a></li>
        <li><a href="#">Email templates</a></li>
      </ul>
    </li>
  </ul>
  <input name="txt" type="text" class="searchBox" id="txt" value="Search..." />
</div>
<div id="wrapper">
  <h1>Vendors</h1>
  <div class="button" style="float:right"><a href="#" onclick="window.open('add_vendor','mywindow','menubar=1,resizable=1, scrollbars=1, width=750,height=700');">Add Vendor</a></div>
  <br />
<br />
</p>
  <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td colspan="6" bgcolor="#EFEFEF"><select name="ddlPropertyType2" id="ddlPropertyType2">
        <option selected="selected" value="1701">Uncategorized</option>
        <option value="1702">Contractors - Dry Wall</option>
        <option value="1703">Contractors - Electrical</option>
        <option value="1704">Contractors - Flooring</option>
        <option value="1705">Contractors - General</option>
        <option value="1706">Contractors - HVAC</option>
        <option value="1707">Contractors - Landscaping</option>
        <option value="1708">Contractors - Masonry</option>
        <option value="1709">Contractors - Painting</option>
        <option value="1710">Contractors - Paving</option>
        <option value="1726">Contractors - Pest</option>
        <option value="1711">Contractors - Plumbing</option>
        <option value="1712">Contractors - Roofing</option>
        <option value="1721">Contractors - Trash</option>
        <option value="1713">Contractors - Windows</option>
        <option value="1727">Lenders - Bank</option>
        <option value="1714">Management Company</option>
        <option value="1730">Professionals - Accounting</option>
        <option value="1729">Professionals - Legal</option>
        <option value="1732">Professionals - Realtor</option>
        <option value="1715">Suppliers - Electrical</option>
        <option value="1716">Suppliers - General</option>
        <option value="1728">Suppliers - Office</option>
        <option value="1717">Suppliers - Plumbing</option>
        <option value="1731">Suppliers - Software &amp; Services </option>
        <option value="1724">Tax - Federal</option>
        <option value="1723">Tax - State</option>
        <option value="1722">Tax - Town</option>
        <option value="1719">Utilities - Gas &amp; Electric</option>
        <option value="1725">Utilities - Insurance</option>
        <option value="1718">Utilities - Internet &amp; Phone</option>
        <option value="1720">Utilities - Water and Sewer</option>
      </select></td>
    </tr>
    <tr>
      <td bgcolor="#EFEFEF"><strong>First Name</strong></td>
      <td bgcolor="#EFEFEF"><strong>Last Name</strong></td>
      <td bgcolor="#EFEFEF"><strong>Expense Account</strong></td>
      <td bgcolor="#EFEFEF"><strong>Email</strong></td>
      <td bgcolor="#EFEFEF"><strong>Phone (Home)</strong></td>
      <td bgcolor="#EFEFEF" align="center"><strong>Actions</strong></td>
    </tr>
      <?php foreach ($vendors_data as $vdata){?>
      <tr>
          <td bgcolor="#FFF"><strong><?php echo $vdata["first_name"];?></strong></td>
          <td bgcolor="#FFF"><strong><?php echo $vdata["last_name"];?></strong></td>
          <td bgcolor="#FFF"><strong><?php echo $vdata["expense_account"];?></strong></td>
          <td bgcolor="#FFF"><strong><?php echo $vdata["primary_email"];?></strong></td>
          <td bgcolor="#FFF"><strong><?php echo $vdata["home"];?></strong></td>
<!--          <td bgcolor="#FFF" align="center"><strong><?php /*echo $vdata["country"];*/?></strong></td>
-->      </tr>
      <?php }?>
   <!-- --><?php /*echo $tr; */?>
  </table>
  <p>&nbsp;</p>
  <p>
    <label for="txt"></label></p>
  <p>&nbsp;</p>
</div>
<div id="footer" align="right">&copy; 2012 Real Estate Shark • All rights reserved.</div>
<script type="text/javascript">
var MenuBar1 = new Spry.Widget.MenuBar("MenuBar1", {imgDown:"SpryAssets/SpryMenuBarDownHover.gif", imgRight:"SpryAssets/SpryMenuBarRightHover.gif"});
</script>
</body>
</html>
