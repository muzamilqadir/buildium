-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 27, 2013 at 10:36 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `realestate`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE IF NOT EXISTS `account` (
  `account_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `account_name` varchar(250) DEFAULT NULL,
  `account_type` varchar(250) DEFAULT NULL,
  `sub_account` varchar(250) DEFAULT NULL,
  `comments` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`account_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`account_id`, `account_name`, `account_type`, `sub_account`, `comments`) VALUES
(1, 'new one2', '21', '1', 'asdsad 2'),
(2, 'dfsdf', '30', '', 'dsfa');

-- --------------------------------------------------------

--
-- Table structure for table `associations`
--

CREATE TABLE IF NOT EXISTS `associations` (
  `association_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `association` varchar(200) DEFAULT NULL,
  `operating_account` varchar(200) DEFAULT NULL,
  `number_of_units` int(11) DEFAULT NULL,
  `country` varchar(200) DEFAULT NULL,
  `address_1` text,
  `address_2` text,
  `address_3` text,
  `city` varchar(200) DEFAULT NULL,
  `state` varchar(80) DEFAULT NULL,
  `zipcode` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`association_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `associations`
--

INSERT INTO `associations` (`association_id`, `association`, `operating_account`, `number_of_units`, `country`, `address_1`, `address_2`, `address_3`, `city`, `state`, `zipcode`) VALUES
(1, 'acco', 'Bank Account', 4, '178', 'add 1', 'add 2', 'add 3', 'city', 'AL', '87300'),
(2, 'acco', 'Bank Account', 4, '178', 'add 1', 'add 2', 'add 3', 'city', 'AL', '87300'),
(3, 'acco2', 'Bank Account', 8, '178', 'add 12', 'add 22', 'add 32', 'city2', 'PA', '873002'),
(4, 'dsafasd', 'Select Account', 3, '230', 'dasfaf', 'adsfa', 'fasfa', 'fdsfsdaf', 'AE', 'dfsdaf'),
(5, 'rewqrweq', 'Select Account', 1, '232', 'rewr', 'wq', 'erer', '', 'IN', 'er'),
(6, 'rewqrweq', 'Select Account', 1, '232', 'rewr', 'wq', 'erer', '', 'IN', 'er');

-- --------------------------------------------------------

--
-- Table structure for table `assoc_ownership`
--

CREATE TABLE IF NOT EXISTS `assoc_ownership` (
  `owner_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `association_name` varchar(200) DEFAULT NULL,
  `number_of_units` int(11) DEFAULT NULL,
  `move_in_date` date DEFAULT NULL,
  `association_fee` varchar(40) DEFAULT NULL,
  `frequency` varchar(50) DEFAULT NULL,
  `owner_info` varchar(255) DEFAULT NULL,
  `first_name` varchar(200) DEFAULT NULL,
  `last_name` varchar(200) DEFAULT NULL,
  `login_email` varchar(255) DEFAULT NULL,
  `alternate_email` varchar(255) DEFAULT NULL,
  `home` varchar(200) DEFAULT NULL,
  `work` varchar(200) DEFAULT NULL,
  `mobile` varchar(200) DEFAULT NULL,
  `fax` varchar(200) DEFAULT NULL,
  `country` varchar(200) DEFAULT NULL,
  `address_1` text,
  `address_2` text,
  `address_3` text,
  `city` varchar(200) DEFAULT NULL,
  `state` varchar(80) DEFAULT NULL,
  `postal_code` varchar(80) DEFAULT NULL,
  `alt_country` varchar(200) DEFAULT NULL,
  `alt_address_1` text,
  `alt_address_2` text,
  `alt_address_3` text,
  `alt_city` varchar(200) DEFAULT NULL,
  `alt_state` varchar(80) DEFAULT NULL,
  `alt_postal_code` varchar(80) DEFAULT NULL,
  `mailing_pref` varchar(255) DEFAULT NULL,
  `additional_comments` text,
  `additional_birth_date` date DEFAULT NULL,
  `emergency_name` varchar(200) DEFAULT NULL,
  `emergency_phone` varchar(200) DEFAULT NULL,
  `is_board_member` int(11) DEFAULT NULL,
  `is_occupancy` int(11) DEFAULT NULL,
  PRIMARY KEY (`owner_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `assoc_ownership`
--

INSERT INTO `assoc_ownership` (`owner_id`, `association_name`, `number_of_units`, `move_in_date`, `association_fee`, `frequency`, `owner_info`, `first_name`, `last_name`, `login_email`, `alternate_email`, `home`, `work`, `mobile`, `fax`, `country`, `address_1`, `address_2`, `address_3`, `city`, `state`, `postal_code`, `alt_country`, `alt_address_1`, `alt_address_2`, `alt_address_3`, `alt_city`, `alt_state`, `alt_postal_code`, `mailing_pref`, `additional_comments`, `additional_birth_date`, `emergency_name`, `emergency_phone`, `is_board_member`, `is_occupancy`) VALUES
(1, 'testing2', 4, '2012-09-13', '100.02', 'Every Two Weeks', 'Add a new association owner', 'f name', 'l name', 'email@domain.com', 'email2@domain2.com', 'home', 'work', 'mobile', 'fax', '178', 'add 1', 'add 2', 'add 3', 'city', 'AL', '87300', '1', 'add 1', 'add 2', 'add 3', 'city', 'AE', '87300', 'Send mail to alternate address', 'hjkhjkasd', '2012-09-12', 'name', 'phone', 1, 1),
(2, 'fffffdsaf', 1, '0000-00-00', '', 'Daily', 'Add a new association owner', '', '', '', '', 'afas', '', '', '', '244', '', '', '', '', 'AE', '', '244', 'sdf', '', '', '', 'AE', '', 'Send mail to main address', '', '2013-03-01', 'afa', '', 0, 0),
(3, 'fffffdsaf', 1, '0000-00-00', '', 'Daily', 'Add a new association owner', '', '', '', '', 'afas', '', '', '', '244', '', '', '', '', 'AE', '', '244', 'sdf', '', '', '', 'AE', '', 'Send mail to main address', '', '2013-03-01', 'afa', '', 0, 0),
(4, 'assssladd', 1, '0000-00-00', '', 'Daily', 'Add a new association owner', 'fff', '', 'fd', 'df', '', '', '', '', '244', '', '', '', '', 'AE', '', '244', '', '', '', '', 'AE', '', 'Send mail to main address', '', '2013-03-01', '', '', 0, 0),
(5, 'dfa', 1, '0000-00-00', 'asdf', 'Daily', 'Add a new association owner', '', '', '', '', 'dasf', '', '', '', '244', '', '', '', '', 'AE', '', '244', '', '', '', 'asdf', 'AE', '', 'Send mail to main address', '', '2013-03-02', '', '', 0, 0),
(6, 'sdf', 11, '0000-00-00', '', 'Daily', 'Add a new association owner', 'fafdaf', 'safasdf', 'mhnjkgui', '', 'dfasd', '', '', '', '244', '', '', '', '', 'AE', '', '244', '', '', '', '', 'AE', '', 'Send mail to main address', '', '2013-03-02', '', '6341963', 0, 0),
(7, 'wtew', 5, '0000-00-00', 'ewf', 'Daily', 'Add a new association owner', '', '', '', '', 'fdsfdfffsdaff', '', 'dasfas', '', '244', '', '', '', '', 'AE', 'fasdf', '244', 'dsf', '', '', 'dsffd', 'AE', '', 'Send mail to main address', '', '2013-03-02', '', '', 0, 0),
(8, 'erewr', 8, '0000-00-00', '25000', 'Monthly', 'Add a new association owner', '', '', '', '', '', '', '', '', '244', '', '', '', '', 'AE', '', '244', '', '', '', '', 'AE', '', 'Send mail to main address', '', '2013-03-02', '', '', 0, 0),
(9, 'dffdf', 9, '0000-00-00', 'dfsdaf', 'Every Six Months', 'Add a new association owner', '', '', 'a@gmail.com', '', '', '', '', '', '244', '', '', '', '', 'AE', '', '244', '', '', '', '', 'AE', '', 'Send mail to main address', '', '2013-03-14', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `bank_account`
--

CREATE TABLE IF NOT EXISTS `bank_account` (
  `bank_account_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `type` varchar(150) DEFAULT NULL,
  `account_number` varchar(200) DEFAULT NULL,
  `routing_number` varchar(200) DEFAULT NULL,
  `comments` text,
  `enable_check_printing` int(11) DEFAULT NULL,
  `check_stock_paper` varchar(255) DEFAULT NULL,
  `check_format` varchar(200) DEFAULT NULL,
  `signature_heading` varchar(255) DEFAULT NULL,
  `fractional_number` varchar(255) DEFAULT NULL,
  `bank_information` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`bank_account_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `bank_account`
--

INSERT INTO `bank_account` (`bank_account_id`, `name`, `type`, `account_number`, `routing_number`, `comments`, `enable_check_printing`, `check_stock_paper`, `check_format`, `signature_heading`, `fractional_number`, `bank_information`) VALUES
(1, 'kjhjk2', 'Savings', '213122', '32132', 'kljlkj2', 0, 'Pre-printed with account and routing number', 'Two Voucher/One Signature', 'lkjlk', 'jlk', 'jlk'),
(2, 'dfsdaf', 'Checking', 'sdaf', '', '', 0, 'Blank', 'One Voucher/One Signature', '', '', ''),
(3, 'vxczvxc', 'Savings', '', '', '', 0, 'Blank', 'One Voucher/One Signature', '', '', ''),
(4, 'sdf', 'Checking', 'sdfs', 'fsdf', 'dffd', 0, 'Pre-printed with account and routing number', 'One Voucher/One Signature', 'dfsd', 'dfa', 'dsfsd'),
(5, 'dff', 'Checking', '', '', '', 0, 'Blank', 'One Voucher/One Signature', 'wer', 'd', 'f'),
(6, 'szxd', 'Checking', 'zxc', 'xzc', 'cx', 0, 'Blank', 'One Voucher/One Signature', 'zxc', 'xzc', 'xzzxcxzc'),
(7, 'fgd', 'Checking', 'ghdf', 'fgh', 'hgdf', 0, 'Blank', 'One Voucher/One Signature', 'gjh', 'gfh', 'gh'),
(8, '', 'Checking', 'dd', 'df', 'df', 0, 'Blank', 'One Voucher/One Signature', 'dfdf', 'df', 'df'),
(9, 'xcxzc', 'Savings', 'xzc', 'xzc', 'xc', 0, 'Blank', 'One Voucher/One Signature', 'xc', '', 'xc'),
(10, 'ff', 'Savings', 'df', 'df', 'df', 0, 'Blank', 'One Voucher/One Signature', 'dfdff', 'df', 'fdsf');

-- --------------------------------------------------------

--
-- Table structure for table `bank_vendor`
--

CREATE TABLE IF NOT EXISTS `bank_vendor` (
  `vendor_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(200) DEFAULT NULL,
  `last_name` varchar(200) DEFAULT NULL,
  `company` varchar(200) DEFAULT NULL,
  `is_company` int(11) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `expense_account` varchar(255) DEFAULT NULL,
  `primary_email` varchar(255) DEFAULT NULL,
  `alternate_email` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `home` varchar(200) DEFAULT NULL,
  `work` varchar(200) DEFAULT NULL,
  `mobile` varchar(200) DEFAULT NULL,
  `fax` varchar(200) DEFAULT NULL,
  `account_number` varchar(200) DEFAULT NULL,
  `taxpayer_id` varchar(200) DEFAULT NULL,
  `1099_eligible` int(11) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `address_1` varchar(255) DEFAULT NULL,
  `address_2` varchar(255) DEFAULT NULL,
  `address_3` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `zipcode` varchar(200) DEFAULT NULL,
  `comments` text,
  PRIMARY KEY (`vendor_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `bank_vendor`
--

INSERT INTO `bank_vendor` (`vendor_id`, `first_name`, `last_name`, `company`, `is_company`, `category`, `expense_account`, `primary_email`, `alternate_email`, `website`, `home`, `work`, `mobile`, `fax`, `account_number`, `taxpayer_id`, `1099_eligible`, `country`, `address_1`, `address_2`, `address_3`, `city`, `state`, `zipcode`, `comments`) VALUES
(1, 'f name2', 'l name2', 'company2', 0, '1730', '114721', 'p email2', 'a email2', 'http://www.website.com2', 'home2', 'work2', 'mobile2', 'fax2', 'acc no2', 'taxpayer2', 0, '178', 'add 12', 'add 22', 'add 32', 'city2', 'PA', 'zipcode2', 'comments2'),
(2, '', '', '', 0, '1710', '0', 'a@gmail.com', 'a', 'a', '', '', '', '', '', '', 0, '244', '', '', '', '', 'AE', '', ''),
(3, 'data ', 'entry ', 'fdf', 0, '1703', '114722', 'sdaf', 'df', 'dfd', 'asf', 'dasf', 'asf', 'adsf', 'adsf', 'a', 0, '244', '', '', '', '', 'AE', '', 'daf'),
(4, 'zxczc', 'c', 'cxc', 1, '1705', '114723', 'xcxzc', 'xc', 'cc', 'cxc', 'cxc', 'cxzc', 'xcxc', 'xc', 'xcc', 0, '244', 'xcc', 'xc', 'cc', 'xc', 'CA', 'cxc', 'xc'),
(5, 'fdajkfl', 'df', 'dfsdf', 0, '1706', '0', 'dfdf', '', 'dffdfdff', '', '', '', '', '', '', 0, '244', '', 'fdsfd', '', '', 'AE', 'fdffdsf', ''),
(6, 'fdajkfl', 'df', 'dfsdf', 0, '1706', '0', 'dfdf', '', 'dffdfdff', '', '', '', '', '', '', 0, '244', '', 'fdsfd', '', '', 'AE', 'fdffdsf', '');

-- --------------------------------------------------------

--
-- Table structure for table `budget`
--

CREATE TABLE IF NOT EXISTS `budget` (
  `budget_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `property` varchar(250) DEFAULT NULL,
  `budget_name` varchar(250) DEFAULT NULL,
  `fiscal_year_start` varchar(50) DEFAULT NULL,
  `fiscal_year` varchar(200) DEFAULT NULL,
  `copy_amount_from` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`budget_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `budget`
--

INSERT INTO `budget` (`budget_id`, `property`, `budget_name`, `fiscal_year_start`, `fiscal_year`, `copy_amount_from`) VALUES
(2, 'Residential Area', 'asd 2', 'August', 'FY2014 (Jan 2014 - Dec 2014)', 'Another budget'),
(3, 'Residential Area', 'dsf', '', 'FY2012 (Jan 2012 - Dec 2012)', ''),
(4, 'Residential Area', 'dsf', '', 'FY2012 (Jan 2012 - Dec 2012)', ''),
(5, 'Residential Area', 'dsf', 'April', 'FY2012 (Jan 2012 - Dec 2012)', ''),
(6, 'Residential Area', 'cv', 'January', 'FY2012 (Jan 2012 - Dec 2012)', 'None; default budget amounts to zero'),
(7, 'Residential Area', 'vf', 'May', 'FY2012 (Jan 2012 - Dec 2012)', 'None; default budget amounts to zero'),
(8, 'Residential Area', 'dg', 'June', 'FY2012 (Jan 2012 - Dec 2012)', 'None; default budget amounts to zero'),
(9, 'Commercial Area', 'dfsdf', 'June', 'FY2013 (Jan 2013 - Dec 2013)', 'None; default budget amounts to zero');

-- --------------------------------------------------------

--
-- Table structure for table `credit_bill`
--

CREATE TABLE IF NOT EXISTS `credit_bill` (
  `bill_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dated` date DEFAULT NULL,
  `vendor` varchar(200) DEFAULT NULL,
  `ref_number` varchar(200) DEFAULT NULL,
  `memo` varchar(255) DEFAULT NULL,
  `due_date` int(11) NOT NULL,
  PRIMARY KEY (`bill_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `credit_bill`
--

INSERT INTO `credit_bill` (`bill_id`, `dated`, `vendor`, `ref_number`, `memo`, `due_date`) VALUES
(2, '2012-10-14', 'Vendor', 'asd2', 'asd22', 0),
(3, '2012-01-13', 'etqewt', 'eretd', 'sdafasf', 12),
(4, '2012-01-13', 'etqewt', 'eretd', 'sdafasf', 12),
(5, '2013-03-23', 'Select Vendor', 'vcnvc xbv', 'vcxbxcxcv', 0),
(6, '2013-03-24', 'Select Vendor', 'sdfdsf', 'dsfsd', 0);

-- --------------------------------------------------------

--
-- Table structure for table `listings`
--

CREATE TABLE IF NOT EXISTS `listings` (
  `listings` varchar(22) NOT NULL,
  `available` varchar(22) NOT NULL,
  `unit` int(11) NOT NULL,
  `bedrooms` varchar(22) NOT NULL,
  `bathrooms` int(11) NOT NULL,
  `size` int(11) NOT NULL,
  `listing_rent` varchar(22) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `listings`
--

INSERT INTO `listings` (`listings`, `available`, `unit`, `bedrooms`, `bathrooms`, `size`, `listing_rent`) VALUES
('Listing One', 'yes', 2, '2', 2, 2, '10000'),
('Listing two', 'yes', 22, '11', 47, 77, '20000'),
('Listing One', 'yes', 2, '2', 2, 2, '10000'),
('Listing two', 'yes', 22, '11', 47, 77, '20000'),
('Listing One', 'yes', 2, '2', 2, 2, '10000'),
('Listing two', 'yes', 22, '11', 47, 77, '20000');

-- --------------------------------------------------------

--
-- Table structure for table `outstanding_balances`
--

CREATE TABLE IF NOT EXISTS `outstanding_balances` (
  `lease` varchar(11) NOT NULL,
  `past_email` varchar(11) NOT NULL,
  `start_days` varchar(11) NOT NULL,
  `one_month` varchar(11) NOT NULL,
  `two_month` varchar(11) NOT NULL,
  `three_month` varchar(11) NOT NULL,
  `balance` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `outstanding_balances`
--

INSERT INTO `outstanding_balances` (`lease`, `past_email`, `start_days`, `one_month`, `two_month`, `three_month`, `balance`) VALUES
('lease', 'past_emai', '0-30days', '31-60 days', '61-90 days', '90+', 'balaances'),
('lease', 'past_emai', '0-30days', '31-60 days', '61-90 days', '90+', 'balaances'),
('lease', 'past_emai', '0-30days', '31-60 days', '61-90 days', '90+', 'balaances'),
('lease', 'past_emai', '0-30days', '31-60 days', '61-90 days', '90+', 'balaances'),
('lease', 'past_emai', '0-30days', '31-60 days', '61-90 days', '90+', 'balaances'),
('lease', 'past_emai', '0-30days', '31-60 days', '61-90 days', '90+', 'balaances');

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE IF NOT EXISTS `payments` (
  `payment_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tenant_id` bigint(20) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `payment_date` date DEFAULT NULL,
  `added_date` date DEFAULT NULL,
  PRIMARY KEY (`payment_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`payment_id`, `tenant_id`, `amount`, `payment_date`, `added_date`) VALUES
(1, 2, 20.00, '2012-11-16', '2012-11-16'),
(2, 0, 0.00, '2012-11-16', '2012-11-16'),
(3, 3, 20.00, '2012-11-16', '2012-11-16'),
(6, 3, 10.00, '2012-11-16', '2012-11-16');

-- --------------------------------------------------------

--
-- Table structure for table `properties`
--

CREATE TABLE IF NOT EXISTS `properties` (
  `property_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `property_name` varchar(255) DEFAULT NULL,
  `property_type` varchar(150) DEFAULT NULL,
  `property_sub_type` varchar(150) DEFAULT NULL,
  `operating_account` varchar(200) DEFAULT NULL,
  `number_of_units` int(11) DEFAULT NULL,
  `country` varchar(200) DEFAULT NULL,
  `address_1` text,
  `address_2` text,
  `address_3` text,
  `city` varchar(200) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `zipcode` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`property_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `properties`
--

INSERT INTO `properties` (`property_id`, `property_name`, `property_type`, `property_sub_type`, `operating_account`, `number_of_units`, `country`, `address_1`, `address_2`, `address_3`, `city`, `state`, `zipcode`) VALUES
(1, 'pro name', '2', '2', '2', 3, '178', 'add 1', 'add 2', 'add 3', 'city', 'AK', '87300'),
(2, '', '1', '1', '1', 0, '244', '', '', '', '', 'AE', ''),
(3, 'asda2', 'Residential Rental', 'Multi-Family', 'Secure Deposit Bank Account', 2, '178', '2', '3', '4', '5', 'PA', '6'),
(4, 'kjhjk', 'Residential Rental', 'Condo / Town House', 'Operating Bank Account', 1, '244', 'kjh', 'kjh', 'jkh', 'jkh', 'AE', 'h'),
(6, 'prop', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'property newton', 'Residential Rental', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 'kashi', 'Residential Rental', NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, 'asfdasf', 'Residential Rental', NULL, 'Secure Deposit Bank Account', 56, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 'property of pakistan', 'Residential Rental', NULL, 'propertyData[countries]', 236, 'Secure Deposit Bank Account', NULL, NULL, NULL, NULL, NULL, NULL),
(11, 'zadasfsa', 'Residential Rental', NULL, 'Operating Bank Account', 333, '238', NULL, NULL, NULL, NULL, NULL, NULL),
(12, 'qwe', 'Residential Rental', 'Multi-Family', 'Secure Deposit Bank Account', 4, '', '23', '234', '3435', 'fgqer', 'KS', ''),
(13, 'aaaaaaaaaaaaaaaaa', 'Residential Rental', 'Multi-Family', 'Secure Deposit Bank Account', 111, '', '', '', '', '', 'AE', ''),
(14, 'ffff', 'Residential Rental', 'Condo / Town House', 'Operating Bank Account', 4, '244', '', '', '', '', 'AE', ''),
(15, 'ffff', 'Residential Rental', 'Condo / Town House', 'Operating Bank Account', 4, '244', '', '', '', '', 'AE', ''),
(16, 'ffff', 'Residential Rental', 'Condo / Town House', 'Operating Bank Account', 4, '244', '', '', '', '', 'AE', ''),
(17, 'dsfkjhskfh', 'Residential Rental', 'Condo / Town House', 'Secure Deposit Bank Account', 5, '244', 'ff', 'ff', '', '', 'AE', '');

-- --------------------------------------------------------

--
-- Table structure for table `record_bill`
--

CREATE TABLE IF NOT EXISTS `record_bill` (
  `bill_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dated` date DEFAULT NULL,
  `bill_due_date` date DEFAULT NULL,
  `vendor` varchar(200) DEFAULT NULL,
  `ref_number` varchar(200) DEFAULT NULL,
  `memo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`bill_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `record_bill`
--

INSERT INTO `record_bill` (`bill_id`, `dated`, `bill_due_date`, `vendor`, `ref_number`, `memo`) VALUES
(1, '2012-10-10', '2012-11-09', 'Vendor', 'ref num2', 'memo2'),
(2, '2013-03-10', '2013-03-10', 'Vendor', 'ffdf', 'fsd'),
(3, '2013-03-10', '2013-03-10', 'Vendor', 'fffff', 'fffff'),
(4, '2013-03-10', '2013-03-10', 'Vendor', '', 'xcx'),
(5, '2013-03-14', '2013-03-14', 'Vendor', 'zx', 'xzxZx'),
(6, '2013-03-14', '2013-03-14', 'Vendor', 'xzxc', 'xczxczxc'),
(7, '2013-03-23', '2013-03-23', 'Vendor', 'aaaaaase', 'sdfasd'),
(8, '2013-03-24', '2013-03-24', 'Vendor', 'dsfsdaf', 'dfsdaf'),
(9, '2013-03-24', '2013-03-24', 'Vendor', 'sdsd', 'sdsad');

-- --------------------------------------------------------

--
-- Table structure for table `recurring_bill`
--

CREATE TABLE IF NOT EXISTS `recurring_bill` (
  `recurring_bill_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `vendor` varchar(200) DEFAULT NULL,
  `memo` varchar(200) DEFAULT NULL,
  `frequency` varchar(200) DEFAULT NULL,
  `next_date` date DEFAULT NULL,
  `next_due_date` int(11) DEFAULT NULL,
  `duration` varchar(100) DEFAULT NULL,
  `txtOccurrences` varchar(20) DEFAULT NULL,
  `posting_day` int(11) DEFAULT NULL,
  PRIMARY KEY (`recurring_bill_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `recurring_bill`
--

INSERT INTO `recurring_bill` (`recurring_bill_id`, `vendor`, `memo`, `frequency`, `next_date`, `next_due_date`, `duration`, `txtOccurrences`, `posting_day`) VALUES
(1, 'select', 'Memo2', 'Every six months', '2012-05-04', 22, 'Until cancelled', '62', 13),
(2, 'select', '2013-03-24', 'dvsdfg', '0000-00-00', 0, 'Until cancelled', 'fgdfg', 5);

-- --------------------------------------------------------

--
-- Table structure for table `recurring_credit`
--

CREATE TABLE IF NOT EXISTS `recurring_credit` (
  `recurring_credit_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `vendor` varchar(200) DEFAULT NULL,
  `payee` varchar(200) DEFAULT NULL,
  `memo` varchar(200) DEFAULT NULL,
  `frequency` varchar(200) DEFAULT NULL,
  `next_date` date DEFAULT NULL,
  `duration` varchar(200) DEFAULT NULL,
  `occurrances` varchar(20) DEFAULT NULL,
  `posting_day` int(11) DEFAULT NULL,
  PRIMARY KEY (`recurring_credit_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `recurring_credit`
--

INSERT INTO `recurring_credit` (`recurring_credit_id`, `vendor`, `payee`, `memo`, `frequency`, `next_date`, `duration`, `occurrances`, `posting_day`) VALUES
(1, 'Select', 'Vendor', 'memo2', 'Every six months', '2012-09-02', 'End after', '2', 15),
(2, '', '', '', '', '0000-00-00', '', '', 0),
(3, 'Select', 'Vendor', 'dfdsf', '', '2013-03-24', 'Until cancelled', 'dsfsf', 5),
(4, '', '', '', '', '0000-00-00', '', '', 0),
(5, '', '', '', '', '0000-00-00', '', '', 0),
(6, 'Select', 'Vendor', 'fsdf', '', '2013-03-24', 'Until cancelled', 'fsf', 5),
(7, '', '', '', '', '0000-00-00', '', '', 0),
(8, 'Select', 'Vendor', 'vdsgg', '', '2013-03-24', 'Until cancelled', 'gfdg', 5),
(9, 'Select', 'Vendor', 'sdsdsdf', '', '2013-03-24', 'Until cancelled', 'dfsd', 5),
(10, 'Select', 'Vendor', 'fgdg', '', '2013-03-24', 'Until cancelled', 'fgds', 5),
(11, 'Select', 'Vendor', '', 'Quarterly', '2013-03-24', 'Until cancelled', 'as', 5);

-- --------------------------------------------------------

--
-- Table structure for table `recurring_tasks`
--

CREATE TABLE IF NOT EXISTS `recurring_tasks` (
  `task_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) DEFAULT NULL,
  `description` text,
  `property` varchar(250) DEFAULT NULL,
  `unit` varchar(50) DEFAULT NULL,
  `assigned_to` varchar(250) DEFAULT NULL,
  `priority` varchar(50) DEFAULT NULL,
  `category` varchar(150) DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `resident` varchar(200) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `home_phone` varchar(200) DEFAULT NULL,
  `work_phone` varchar(250) DEFAULT NULL,
  `mobile_phone` varchar(250) DEFAULT NULL,
  `frequency` varchar(250) DEFAULT NULL,
  `next_date` date DEFAULT NULL,
  `duration` varchar(250) DEFAULT NULL,
  `occurances` int(11) DEFAULT NULL,
  PRIMARY KEY (`task_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `recurring_tasks`
--

INSERT INTO `recurring_tasks` (`task_id`, `subject`, `description`, `property`, `unit`, `assigned_to`, `priority`, `category`, `due_date`, `resident`, `email`, `home_phone`, `work_phone`, `mobile_phone`, `frequency`, `next_date`, `duration`, `occurances`) VALUES
(1, 'recurring task2', 'test description2', 'Commercial Rental', '9', 'Assigned To', 'Normal', 'Maintence Request', '2012-05-24', 'Resident', 'asd2', 'asd2', 'asd2', 'asd2', 'Monthly', '2012-04-24', 'No end date', NULL),
(2, 'cfxzcvxz', 'vxcvb', 'Residential Rental', '1', 'Assigned To', 'Low', 'Uncategorized', '2013-03-10', '-- Select --', 'cvxzvv', 'cxv', 'cxv', 'cxv', '', '2013-03-10', 'No end date', NULL),
(3, 'dfdsf', 'fdsf', 'Residential Rental', '1', 'Assigned To', 'Low', 'Uncategorized', '2013-03-10', '-- Select --', 'dfdf', '', 'dffdsfd', '', '', '2013-03-10', 'No end date', NULL),
(4, 'dsfsad', 'sdafsdf', 'Residential Rental', '1', 'Assigned To', 'Low', 'Uncategorized', '2013-03-10', '-- Select --', 'dfsa', 'fdsaf', 'sdaf', 'sdaf', '', '2013-03-10', 'No end date', NULL),
(5, 'hgh', 'ghhgd', 'Residential Rental', '1', 'Assigned To', 'Low', 'Uncategorized', '2013-03-10', '-- Select --', 'gfh', 'ghd', 'gh', 'dh', 'Yearly', '2013-03-10', 'No end date', NULL),
(6, 'bbbb', 'bbbb', 'Residential Rental', '1', 'Assigned To', 'Low', 'Uncategorized', '2013-03-10', '-- Select --', 'bb', 'bb', 'bb', 'bb', 'Quarterly', '2013-03-10', 'No end date', NULL),
(7, 'tr', 'rt', 'Residential Rental', '1', 'Assigned To', 'Low', 'Feedback/Suggestion', '2013-03-10', '-- Select --', 'rtret', 'ret', 'retert', 'ert', 'Monthly', '2013-03-10', 'No end date', NULL),
(8, 'assddff', 'adfdfa', 'Residential Rental', '1', 'Assigned To', 'Low', 'Uncategorized', '2013-03-11', '-- Select --', 'afaf', 'afaf', '', 'a', '', '2013-03-11', 'No end date', NULL),
(9, 'dfasf', 'af', 'Residential Rental', '1', 'Assigned To', 'Low', 'Uncategorized', '2013-03-21', '-- Select --', 'sf', 'fasf', 'afdsfaf', 'af', '', '2013-03-21', 'No end date', NULL),
(10, 'fa', 'asdf', 'Residential Rental', '1', 'Assigned To', 'Low', 'Uncategorized', '2013-03-21', '-- Select --', '', 'af', '', '', '', '2013-03-21', 'No end date', NULL),
(11, 'xz', 'xzx', 'Residential Rental', '1', 'Assigned To', 'Low', 'Uncategorized', '2013-03-21', '-- Select --', 'xzx', 'xx', 'xzx', '', '', '2013-03-21', 'No end date', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `rental_applications`
--

CREATE TABLE IF NOT EXISTS `rental_applications` (
  `app_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `details_property` varchar(200) DEFAULT NULL,
  `details_number_of_units` int(11) DEFAULT NULL,
  `details_first_name` varchar(200) DEFAULT NULL,
  `details_last_name` varchar(200) DEFAULT NULL,
  `details_notes` text,
  `application_property` varchar(200) DEFAULT NULL,
  `application_number_of_units` int(11) DEFAULT NULL,
  `application_status` varchar(100) DEFAULT NULL,
  `general_first_name` varchar(200) DEFAULT NULL,
  `general_last_name` varchar(200) DEFAULT NULL,
  `general_email` varchar(255) DEFAULT NULL,
  `general_ssn` varchar(200) DEFAULT NULL,
  `general_phone` varchar(200) DEFAULT NULL,
  `general_date_of_birth` date DEFAULT NULL,
  `res_address` text,
  `res_city` varchar(200) DEFAULT NULL,
  `res_state` varchar(200) DEFAULT NULL,
  `res_postal_code` varchar(200) DEFAULT NULL,
  `res_landlord` varchar(200) DEFAULT NULL,
  `res_monthly_rent` varchar(200) DEFAULT NULL,
  `res_date_from` date DEFAULT NULL,
  `res_date_to` date DEFAULT NULL,
  `res_leaving_reason` varchar(255) DEFAULT NULL,
  `res_landlord_phone` varchar(200) DEFAULT NULL,
  `employer_name` varchar(200) DEFAULT NULL,
  `employer_city` varchar(200) DEFAULT NULL,
  `employer_phone` varchar(200) DEFAULT NULL,
  `employer_date_from` date DEFAULT NULL,
  `employer_date_to` date DEFAULT NULL,
  `employer_gross_pay` varchar(20) DEFAULT NULL,
  `employer_occupation` varchar(200) DEFAULT NULL,
  `ref_name_1` varchar(200) DEFAULT NULL,
  `ref_name_1_phone` varchar(200) DEFAULT NULL,
  `ref_name_2` varchar(200) DEFAULT NULL,
  `ref_name_2_phone` varchar(200) DEFAULT NULL,
  `ref_gross_pay` varchar(20) DEFAULT NULL,
  `ref_occupation` varchar(200) DEFAULT NULL,
  `other_comments` text,
  PRIMARY KEY (`app_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `rental_applications`
--

INSERT INTO `rental_applications` (`app_id`, `details_property`, `details_number_of_units`, `details_first_name`, `details_last_name`, `details_notes`, `application_property`, `application_number_of_units`, `application_status`, `general_first_name`, `general_last_name`, `general_email`, `general_ssn`, `general_phone`, `general_date_of_birth`, `res_address`, `res_city`, `res_state`, `res_postal_code`, `res_landlord`, `res_monthly_rent`, `res_date_from`, `res_date_to`, `res_leaving_reason`, `res_landlord_phone`, `employer_name`, `employer_city`, `employer_phone`, `employer_date_from`, `employer_date_to`, `employer_gross_pay`, `employer_occupation`, `ref_name_1`, `ref_name_1_phone`, `ref_name_2`, `ref_name_2_phone`, `ref_gross_pay`, `ref_occupation`, `other_comments`) VALUES
(1, '2', 1, 'asdsa', 'asdas', 'dasasd', '1', 1, '1', '', '', '', '', '', '2012-09-25', '', '', 'AE', '', '', '', '2012-09-25', '2012-09-25', '', '', '', '', '', '2012-09-25', '2012-09-25', '', '', '', '', '', '', '', '', ''),
(2, 'Commercial Rental', 3, 'f name', 'l name', 'notes', 'Residential Rental', 8, 'Approved', 'f name', 'l name', 'email@domain.com', 'ssn', '123-123-1234', '2012-09-27', 'address', 'city', 'AE', '87300', 'landlord', '10000', '2012-09-27', '2012-11-25', 'reason', '123-123-1234', 'emp name', 'city', 'phone', '2014-09-27', '2014-12-27', '100000000', 'IT Industry', 'name 1', 'Phone 1', 'name 2', 'phone 2', '1000000000', 'occupation', 'other comments'),
(3, 'Residential Rental', 5, 'f name2', 'l name2', 'notes2', 'Commercial Rental', 12, 'Defered', 'f name', 'l name', 'email@domain.com', 'ssn', '123-123-1234', '2012-09-27', 'address', 'city', 'AE', '87300', 'landlord', '10000', '2012-09-27', '2012-11-25', 'reason', '123-123-1234', 'emp name', 'city', 'phone', '2014-09-27', '2014-12-27', '100000000', 'IT Industry', 'name 1', 'Phone 1', 'name 2', 'phone 2', '1000000000', 'occupation', 'other comments'),
(4, 'Residential Rental', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 'Residential Rental', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 'Commercial Rental', 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'Commercial Rental', 6, '', '', '', '', 0, '', '', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '0000-00-00', '0000-00-00', '', '', '', '', '', '0000-00-00', '0000-00-00', '', '', '', '', '', '', '', '', ''),
(8, 'Commercial Rental', 14, '', 'fsaf', '', 'Residential Rental', 1, 'Undecided', '', '', '', '', '', '2013-02-28', '', '', 'AE', '', '', '', '0000-00-00', '2013-02-28', '', '', '', '', '', '2013-02-28', '2013-02-28', '', '', '', '', '', '', '', '', ''),
(9, 'Commercial Rental', 14, '', 'fsaf', '', 'Residential Rental', 1, 'Undecided', '', '', '', '', '', '2013-02-28', '', '', 'AE', '', '', '', '0000-00-00', '2013-02-28', '', '', '', '', '', '2013-02-28', '2013-02-28', '', '', '', '', '', '', '', '', ''),
(10, 'Commercial Rental', 14, 'sds', 'fsaf', '', 'Residential Rental', 1, 'Undecided', '', '', '', '', '', '2013-02-28', '', '', 'AE', '', '', '', '2013-02-28', '2013-02-28', '', '', '', '', '', '2013-02-28', '2013-02-28', '', '', '', '', '', '', '', '', ''),
(11, 'Commercial Rental', 14, 'sds', 'fsaf', '', 'Residential Rental', 1, 'Undecided', '', '', '', '', '', '2013-02-28', '', '', 'AE', '', '', '', '2013-02-28', '2013-02-28', '', '', '', '', '', '2013-02-28', '2013-02-28', '', '', '', '', '', '', '', '', ''),
(12, 'Commercial Rental', 14, 'sds', 'fsaf', '', 'Residential Rental', 1, 'Undecided', '', '', '', '', '', '2013-02-28', '', '', 'AE', '', '', '', '2013-02-28', '2013-02-28', '', '', '', '', '', '2013-02-28', '2013-02-28', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `rental_owner`
--

CREATE TABLE IF NOT EXISTS `rental_owner` (
  `owner_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(200) DEFAULT NULL,
  `last_name` varchar(200) DEFAULT NULL,
  `company` varchar(200) DEFAULT NULL,
  `primary_email` varchar(200) DEFAULT NULL,
  `alternate_email` varchar(200) DEFAULT NULL,
  `taxpayer_id` varchar(200) DEFAULT NULL,
  `1099_eligible` varchar(10) DEFAULT NULL,
  `home_phone` varchar(200) DEFAULT NULL,
  `work_phone` varchar(200) DEFAULT NULL,
  `mobile_phone` varchar(200) DEFAULT NULL,
  `fax` varchar(200) DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `country` varchar(200) DEFAULT NULL,
  `address_1` text,
  `address_2` text,
  `address_3` text,
  `city` varchar(200) DEFAULT NULL,
  `state` varchar(150) DEFAULT NULL,
  `address_zipcode` varchar(200) DEFAULT NULL,
  `comments` text,
  `properties` text,
  PRIMARY KEY (`owner_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `rental_owner`
--

INSERT INTO `rental_owner` (`owner_id`, `first_name`, `last_name`, `company`, `primary_email`, `alternate_email`, `taxpayer_id`, `1099_eligible`, `home_phone`, `work_phone`, `mobile_phone`, `fax`, `date_of_birth`, `country`, `address_1`, `address_2`, `address_3`, `city`, `state`, `address_zipcode`, `comments`, `properties`) VALUES
(1, 'f name', 'asdas', 'DEVMET', 'cto@devmet.com', 'info@devmet.com', '992018', 'Yes', 'home', 'work', 'mobile', 'fax', '2012-09-10', '238', 'add 1', 'add 2', 'add 3', 'city', 'AE', '87300', 'comments', 'on'),
(2, 'f name', 'asdas', 'DEVMET', 'cto@devmet.com', 'info@devmet.com', '992018', 'Yes', 'home', 'work', 'mobile', 'fax', '2012-09-10', '238', 'add 1', 'add 2', 'add 3', 'city', 'AE', '87300', 'comments', 'on'),
(3, 'f name2', 'asdas2', 'DEVMET', 'INFO@devmet.com', 'info@devmet.com', '992018', 'Yes', 'home2', 'work2', 'mobile2', 'fax2', '2012-09-09', '178', 'add 1', 'add 2', 'add 3', 'city', 'AE', '87300', 'comments', 'Yes'),
(5, 'fafsdaf', 'gdgggg', 'gggg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 'assads', 'nadnfasdf', 'fff', 'ffd', 'ffaf', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'dffas', 'faf', 'fa', 'afsd', 'faf', 'fa', '', '21274521', '2457496', '1527496', '145749', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 'sdf', 'dsaf', 'dfsaf', 'asf', 'dsf', 'daf', '', 'afaf', 'af', 'afafa', 'afaf', '2013-03-01', '244', 'afa', 'faf', 'afaf', NULL, NULL, NULL, NULL, NULL),
(9, 'fiax faskfj', 'afasdf', 'afasf', 'adfaf', 'afafd', 'asf', '', 'afasf', 'afdasf', 'afasfaf', 'afsa', '2013-03-01', '244', 'adfad', 'sdfa', 'sdfaf', 'adfadf', 'AE', 'adfadsf', 'adfdafdafdf', ''),
(10, '', '', '', '', '', '', '', '', '', '', '', '2013-03-01', '235', '', '', '', '', 'AE', '', '', ''),
(11, 'fdgdfshsdfgsfsdfgsdf', 'sdghdfhhhhhhhhs', 'dfsgdfgsdfg', 'dfgsdfgsdfgs', 'gsdfgsdfgs', 'dfgsdfgs', '', 'sdfggggggggggggg', 'fgsdg', 'sdfgfgsdfg', 'fdsgdgdfgsdfg', '2013-09-01', '244', 'sdfdfdfdfdfdfdfdfdfdfdfdfdf', 'sdfdfdfdfdfdfdfdfdfdfdfdfdfdf', 'sdffffffffffg', 'sgdd', 'AE', 'sdfggggggg', '', ''),
(12, 'ali', 'raza', 'dskal', 'a@gmail.com', 'faf', 'dfa', '', 'asdfasdfaf', 'sdfasdf', 'afasfa', 'adfasd', '2013-03-01', '229', 'dasfsadf', 'sdafasdfa', 'asdfasfads', 'fsdfasdf', 'KY', '', 'fasf', ''),
(13, 'sf', 'dsf', 'faf', 'faf', 'af', 'fa', 'dfa', 'sadad', 'dfasf', 'asdf', 'df', '0000-00-00', 'pakistan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE IF NOT EXISTS `tasks` (
  `task_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) DEFAULT NULL,
  `description` text,
  `property` varchar(250) DEFAULT NULL,
  `unit` varchar(50) DEFAULT NULL,
  `assigned_to` varchar(250) DEFAULT NULL,
  `priority` varchar(50) DEFAULT NULL,
  `category` varchar(150) DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `request_type` varchar(200) DEFAULT NULL,
  `contact_name` varchar(250) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `home_phone` varchar(200) DEFAULT NULL,
  `work_phone` varchar(250) DEFAULT NULL,
  `mobile_phone` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`task_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`task_id`, `subject`, `description`, `property`, `unit`, `assigned_to`, `priority`, `category`, `due_date`, `request_type`, `contact_name`, `email`, `home_phone`, `work_phone`, `mobile_phone`) VALUES
(2, 'asjkdh2', 'asdasdhk2', 'Commercial Rental', '11', 'Assigned To', 'High', 'General Inquiry', '2012-06-23', 'Resident request', 'lklkjlk2', 'jlk2', 'khjkh2', 'jkh2', 'jk2'),
(3, 'fdf', 'ffsdaf', 'Residential Rental', '1', '', 'Low', 'Uncategorized', '2013-03-08', 'To do', 'dsf', 'fdsa', '', '', ''),
(4, 'vdf', 'dfsd', 'Residential Rental', '1', '', 'Low', 'Uncategorized', '2013-03-10', 'To do', 'fsdaf', 'df', '', '', ''),
(5, 'fff', 'fff', 'Residential Rental', '1', '', 'Low', 'Uncategorized', '2013-03-10', 'To do', 'ff', 'ff', '', '', ''),
(6, 'fff', 'fff', 'Residential Rental', '1', '', 'Low', 'Uncategorized', '2013-03-10', 'To do', 'ff', 'ff', 'ff', 'ff', 'ff'),
(7, 'fff', 'fff', 'Residential Rental', '1', 'Assigned To', 'Low', 'Uncategorized', '2013-03-10', 'To do', 'ff', 'ff', 'ff', 'ff', 'ff'),
(8, 'dfdf', 'ffff', 'Residential Rental', '1', 'Assigned To', 'Low', 'Uncategorized', '2013-03-10', 'To do', 'fsdff', 'ff', '', 'fdf', 'fa'),
(9, 'ghj', 'h', 'Residential Rental', '1', 'Assigned To', 'Low', 'Uncategorized', '2013-03-10', 'To do', 'fj', 'fj', 'fj', 'fjj', 'fj'),
(10, 'gfsd', '', 'Residential Rental', '1', 'Assigned To', 'Low', 'Uncategorized', '2013-03-10', 'To do', 'gfg', 'gfsdg', 'dfg', '', ''),
(11, 'czxc', 'xzcz', 'Residential Rental', '1', 'Assigned To', 'Low', 'Uncategorized', '2013-03-10', 'To do', 'xczc', 'zxc', 'cxc', 'xczc', 'xc'),
(12, ' xzc', 'c', 'Residential Rental', '1', 'Assigned To', 'Low', 'Uncategorized', '2013-03-10', 'To do', 'xcx', '', 'xc', '', 'xc'),
(13, 'xczv', 'df', 'Residential Rental', '1', 'Assigned To', 'Low', 'Uncategorized', '2013-03-10', 'To do', '', 'fdd', '', 'df', ''),
(14, 'aaaa', 'aaaa', 'Commercial Rental', '9', 'Assigned To', 'Low', 'Uncategorized', '2013-03-10', 'To do', 'aa', 'aa', 'aa', 'aa', 'aa'),
(15, 'asd', 'dsadsa', 'Residential Rental', '1', 'Assigned To', 'Low', 'General Inquiry', '2013-03-10', 'To do', 'dsadsad', 'sad', 'dsad', '', 'sadsa'),
(16, 'ffff', 'ffff', 'Residential Rental', '1', 'Assigned To', 'Low', 'Uncategorized', '2013-03-11', 'To do', 'fff', 'fff', 'fff', 'fff', 'ff'),
(17, 'fdsaf', 'sdafsda', 'Residential Rental', '1', 'Assigned To', 'Low', 'Uncategorized', '2013-03-11', 'To do', 'fdsfa', 'sdafsd', 'ffsda', '', ''),
(18, 'dfs', 'fsaf', 'Residential Rental', '1', 'Assigned To', 'Low', 'Uncategorized', '2013-03-21', 'To do', 'sdfa', 'df', 'fasf', 'adf', 'af'),
(19, 'asf', 'af', 'Residential Rental', '1', 'Assigned To', 'Low', 'Uncategorized', '2013-03-21', 'To do', 'af', 'asd', 'afasfa', '', 'af'),
(20, 'cxcc', 'xc', 'Residential Rental', '1', 'Assigned To', 'Low', 'Uncategorized', '2013-03-21', 'To do', '', 'xc', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tenant_leases`
--

CREATE TABLE IF NOT EXISTS `tenant_leases` (
  `tenant_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `property_name` varchar(200) DEFAULT NULL,
  `number_of_units` int(11) DEFAULT NULL,
  `type` varchar(150) DEFAULT NULL,
  `date_from` date DEFAULT NULL,
  `date_to` date DEFAULT NULL,
  `rent_due_day` varchar(20) DEFAULT NULL,
  `frequency` varchar(80) DEFAULT NULL,
  `rent` varchar(20) DEFAULT NULL,
  `prepaid_rent` varchar(20) DEFAULT NULL,
  `sec_dep` varchar(20) DEFAULT NULL,
  `tenant_entry` varchar(200) DEFAULT NULL,
  `first_name` varchar(200) DEFAULT NULL,
  `last_name` varchar(200) DEFAULT NULL,
  `primary_email` varchar(200) DEFAULT NULL,
  `alternate_email` varchar(200) DEFAULT NULL,
  `home` varchar(200) DEFAULT NULL,
  `work` varchar(200) DEFAULT NULL,
  `mobile` varchar(200) DEFAULT NULL,
  `fax` varchar(200) DEFAULT NULL,
  `country` varchar(200) DEFAULT NULL,
  `address_1` text,
  `address_2` text,
  `address_3` text,
  `city` varchar(200) DEFAULT NULL,
  `state` varchar(200) DEFAULT NULL,
  `postal_code` varchar(150) DEFAULT NULL,
  `alt_country` varchar(200) DEFAULT NULL,
  `alt_address_1` varchar(200) DEFAULT NULL,
  `alt_address_2` varchar(200) DEFAULT NULL,
  `alt_address_3` varchar(200) DEFAULT NULL,
  `alt_city` varchar(200) DEFAULT NULL,
  `alt_state` varchar(200) DEFAULT NULL,
  `alt_postal_code` varchar(150) DEFAULT NULL,
  `mailing_pref` varchar(200) DEFAULT NULL,
  `comments` text,
  `date_of_birth` date DEFAULT NULL,
  `emergency_contact_name` varchar(200) DEFAULT NULL,
  `emergency_contact_phone` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`tenant_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=38 ;

--
-- Dumping data for table `tenant_leases`
--

INSERT INTO `tenant_leases` (`tenant_id`, `property_name`, `number_of_units`, `type`, `date_from`, `date_to`, `rent_due_day`, `frequency`, `rent`, `prepaid_rent`, `sec_dep`, `tenant_entry`, `first_name`, `last_name`, `primary_email`, `alternate_email`, `home`, `work`, `mobile`, `fax`, `country`, `address_1`, `address_2`, `address_3`, `city`, `state`, `postal_code`, `alt_country`, `alt_address_1`, `alt_address_2`, `alt_address_3`, `alt_city`, `alt_state`, `alt_postal_code`, `mailing_pref`, `comments`, `date_of_birth`, `emergency_contact_name`, `emergency_contact_phone`) VALUES
(2, 'pro name', 4, 'At-Will', '2012-10-25', '2013-11-26', '1st', 'Weekly', '20', '11', '12', 'Current Tenant', 'f name', 'l name', 'p email', 'a email', 'home', 'work', 'mobile', 'fix', '1', 'add 1', 'add 2', 'add 3', 'city', 'AE', '87300', '1', '1', '2', '3', '4', 'AE', '87300', 'Send mail to main address', 'commentyyyyyy', '2012-08-24', 'emer name', 'emer phone'),
(3, 'pro name', 4, 'At-Will', '2012-10-25', '2013-11-26', '2nd', 'Weekly', '10', '11', '12', 'Current Tenant', 'f name', 'l name', 'p email', 'a email', 'home', 'work', 'mobile', 'fix', '244', 'add 1', 'add 2', 'add 3', 'city', 'AE', '87300', '244', '1', '2', '3', '4', 'AE', '87300', 'Send mail to main address', 'commentyyyyyy', '2012-08-24', 'emer name', 'emer phone'),
(4, 'rrrrwert', 1, 'Fixed w/rollover', '2013-02-23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 'erqwer', 1, 'Fixed', '2013-02-23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 'rgadsg', 1, 'Fixed', '2013-02-23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'rgadsg', 1, 'Fixed', '2013-02-23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(21, 'kkjkjhgjkl', 9, 'Fixed w/rollover', '0000-00-00', '2013-04-24', '1st', 'Daily', '45000', '', '', 'New Tenant', 'aaaa', 'dddd', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(22, 'ggg', 7, 'At-Will', '0000-00-00', '0000-00-00', 'Daily', '0.00', '2013-02-24', '', '', 'New Tenant', 'fgfd', 'fgfdgd', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(23, 'sasd', 1, 'At-Will', '0000-00-00', '2013-02-24', '20th', 'Weekly', '0.00', '', '', 'New Tenant', 'dfasdf', 'afsadfsaf', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(24, 'dsssds', 8, 'Fixed w/rollover', '0000-00-00', '2013-02-09', '1st', 'Daily', '0.00', '', '', 'New Tenant', 'dfff', 'sdfds', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(25, 'a', 1, 'Fixed', '0000-00-00', '2013-02-24', '1st', 'Daily', '0.00', '', '', 'New Tenant', 'a', 'a', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(26, 'adfgh', 9, 'Fixed w/rollover', '0000-00-00', '2006-11-19', '1st', 'Daily', '0.00', '', '', 'New Tenant', 'fdsf', 'dfsafsdaf', '', '', '', '', '', '', '244', '', '', '', '', 'AE', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(27, 'err', 1, 'Fixed', '0000-00-00', '2013-02-04', '19th', 'Daily', '0.00', '111', '', 'New Tenant', 'fg', 'dg', '', '', '', '', '', '', '244', '', '', '', '', 'AE', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(28, 'tyt', 22, 'ggg', '2013-02-24', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'fggg', 'gggg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(29, 'tyt', 22, 'ggg', '2013-02-24', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'fggg', 'gggg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(30, 'tgerg', 1, 'Fixed', '0000-00-00', '2013-02-24', '1st', 'Daily', '0.00', '', '', 'New Tenant', 'fgfg', 'ggg', '', '', '', '', '', '', '244', '', '', '', '', 'AE', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(31, 'fgfdg', 1, 'Fixed', '0000-00-00', '0000-00-00', '1st', 'Daily', '0.00', '', '', 'New Tenant', 'gg', 'ggg', '', '', '', '', '', '', '244', '', '', '', '', 'AE', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(32, 'dff', 1, 'Fixed', '0000-00-00', '0000-00-00', '10th', 'Daily', '0.00', '', '', 'New Tenant', 'fff', 'fff', '', '', '', '', '', '', '244', '', '', '', '', 'AE', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(33, 'dff', 1, 'Fixed', '0000-00-00', '2013-02-24', '10th', 'Daily', '0.00', '', '', 'New Tenant', 'fff', 'fff', '', '', '', '', '', '', '244', '', '', '', '', 'AE', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(34, 'fff', 3, 'Fixed w/rollover', '0000-00-00', '2013-02-24', '1st', 'Daily', '0.00', '', '', 'New Tenant', 'fadsfsadf', 'fadasfsdaff', '', '', '', '', '', '', '244', '', '', '', '', 'AE', '', '244', '', '', '', '', NULL, '', 'Send mail to main address', '', NULL, NULL, NULL),
(35, 'dddd', 1, 'Fixed', '0000-00-00', '2013-02-25', '1st', 'Daily', '0.00', '', '', 'New Tenant', 'ff', 'sdf', '', '', '', '', '', '', '244', '', '', '', '', 'AE', '', '244', '', '', '', '', NULL, '', 'Send mail to main address', '', '2013-02-25', '', ''),
(36, 'sssssssssssssss', 1, 'Fixed', '0000-00-00', '2013-02-25', '1st', 'Daily', '0.00', '', '', 'New Tenant', 'ss', 'ss', '', '', '', '', '', '', '244', '', '', '', '', 'AE', '', '244', '', '', '', '', NULL, '', 'Send mail to main address', '', '2013-02-25', '', ''),
(37, 'eeeeeeeeeeeeee', 1, 'Fixed', '0000-00-00', '2013-02-25', '1st', 'Daily', '0.00', '', '', 'New Tenant', 'yhy', 'hfh', 'a@gmail.com', '', '123654', '', '', '', '244', '', '', '', '', 'AE', '', '244', '', '', '', '', NULL, '', 'Send mail to main address', '', '2013-02-25', '', '52634');

-- --------------------------------------------------------

--
-- Table structure for table `work_orders`
--

CREATE TABLE IF NOT EXISTS `work_orders` (
  `work_order_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) DEFAULT NULL,
  `description` text,
  `property` varchar(200) DEFAULT NULL,
  `units` varchar(100) DEFAULT NULL,
  `attachment` varchar(255) DEFAULT NULL,
  `assigned_to` varchar(255) DEFAULT NULL,
  `priority` varchar(50) DEFAULT NULL,
  `category` varchar(200) DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `request_type` varchar(200) DEFAULT NULL,
  `contact_name` varchar(200) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `home` varchar(200) DEFAULT NULL,
  `work` varchar(200) DEFAULT NULL,
  `mobile` varchar(200) DEFAULT NULL,
  `vendor` varchar(200) DEFAULT NULL,
  `invoice_number` varchar(200) DEFAULT NULL,
  `rechargable_to` varchar(200) DEFAULT NULL,
  `entry_allowed` varchar(200) DEFAULT NULL,
  `entry_notes` text,
  `work_perform_vendor` text,
  `vendor_notes` text,
  PRIMARY KEY (`work_order_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `work_orders`
--

INSERT INTO `work_orders` (`work_order_id`, `subject`, `description`, `property`, `units`, `attachment`, `assigned_to`, `priority`, `category`, `due_date`, `request_type`, `contact_name`, `email`, `home`, `work`, `mobile`, `vendor`, `invoice_number`, `rechargable_to`, `entry_allowed`, `entry_notes`, `work_perform_vendor`, `vendor_notes`) VALUES
(1, 'subject2', 'description2', 'Residential Rental', '12', NULL, '2', 'High', 'General Inquiry', '2012-10-16', 'Resident request', 'name2', 'email2', 'home2', 'work2', 'mobile2', '1', '1', 'rechargable2', 'Unknown', 'notes2', 'vendor work2', 'vendor notes2'),
(2, 'fldjfo', 'fa', 'Residential Rental', '', NULL, '1', 'Low', 'Uncategorized', '2013-03-06', 'To do', '', 'dasf', 'af', '', '[mobile]', '1', '1', '', 'Unknown', 'daf', '', 'adsf'),
(3, 'fldjfo', 'fa', 'Residential Rental', '2', NULL, '1', 'Low', 'Uncategorized', '2013-03-06', 'To do', 'dafsda', 'dasf', 'af', '', '[mobile]', '1', '1', '', 'Unknown', 'daf', 'df', 'adsf'),
(4, 'sdf', '', 'Residential Rental', '1', NULL, '1', 'Low', 'Uncategorized', '2013-03-06', 'To do', 'dsf', 'dsf', '', '', '[mobile]', '1', '1', '', 'Unknown', 'dsf', '', 'df'),
(5, 'fgs', 'rt', 'Residential Rental', '1', NULL, '1', 'Low', 'Uncategorized', '2013-03-06', 'To do', 'ret', 'rt', 'rtwert', 'wert', '[mobile]', '1', '1', '', 'Unknown', '', '', ''),
(6, 'dff', 'fsdf', 'Residential Rental', '1', NULL, '1', 'Low', 'Uncategorized', '2013-03-08', 'To do', 'df', 'df', 'dfdf', 'dfasfdf', '[mobile]', '1', '1', 'fdsf', 'Unknown', '', 'df', 'dff'),
(7, 'dsfasf', 'sdf', 'Residential Rental', '1', NULL, '1', 'Low', 'Uncategorized', '2013-03-21', 'To do', 'dsaf', 'fasdf', 'dsafadsf', 'dsfa', '[mobile]', '1', '1', 'dfas', 'Unknown', 'dsfsda', 'dsfasdf', 'sadfas');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
