<?php
include ("../includes/classes/class.db.php");
include ("../includes/functions/general.php");

//-------------------------------------------------------------------------------------------
								/*Objects*/
//-------------------------------------------------------------------------------------------					
	$db = new db();
	//isLoggedUser();
//-------------------------------------------------------------------------------------------
								/*General Coding Area*/
	$aid = "0";
	if (isset($_GET['id']) && $_GET['id'] != "" ) {
		$aid = $_GET['id'];
	}							
	//---------------------------------------------------------------------------------------
	
	$isUpdated = false;
	
	if ($_POST){
		$sqlUpdate = "UPDATE bank_account SET
		name = '".clearFormData("txtName")."',
		type = '".clearFormData("ddlType")."',
		account_number = '".clearFormData("txtAccountNumber")."',
		routing_number = '".clearFormData("txtRoutingNumber")."',
		comments = '".clearFormData("txtComment")."',
		enable_check_printing = '".clearFormData("chkLocalPrinting")."',
		check_stock_paper = '".clearFormData("rdCheckStock")."',
		check_format = '".clearFormData("ddlCheckFormat")."',
		signature_heading = '".clearFormData("txtSignatureHeading")."',
		fractional_number = '".clearFormData("txtFractionalNumber")."',
		bank_information = '".clearFormData("txtBankInformation")."'
		WHERE 
		bank_account_id = '".$aid."'";

		$db->query($sqlUpdate);
		$isUpdated = true;
	}
	
	$sqlSelect = "SELECT * FROM bank_account WHERE bank_account_id = '".$aid."'";
	
	$query= $db->ExeQuersys($sqlSelect);
		
	while ($row = mysql_fetch_array($query)) {
		$name = $row['name'];
		$type = $row['type'];
		$account_number = $row['account_number'];
		$routing_number = $row['routing_number'];
		$comments = $row['comments'];
		$enable_check_printing = $row['enable_check_printing'];
		$check_stock_paper = $row['check_stock_paper'];
		$check_format = $row['check_format'];
		$signature_heading = $row['signature_heading'];
		$fractional_number = $row['fractional_number'];
		$bank_information = $row['bank_information'];
	}
	
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Update Bank Account</title>
<link href="../styles_popup.css" rel="stylesheet" type="text/css" />
<script src="../SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<link href="../SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
</head>

<body>
<form action="" method="post">
<div id="wrapper_header"><a href="#" onclick="return confirm('Do you want to cancel?');"><img src="../images/btn_cancel.png" alt="Cancel" width="109" height="34" border="0" style="float:right" /></a>
  <input type="image" name="submit2" id="submit2" src="../images/btn_save.png" />
</div>
<p>&nbsp;</p>
<?php if ($isUpdated) { echo '<div class="msg_success">Bank Account is updated successfully.</div>'; } ?>
<table width="675" border="0" align="center" cellpadding="2" cellspacing="3">
  <tr>
    <td colspan="3"><h1>Update Bank Account</h1></td>
    </tr>
  <tr>
    <td colspan="3"><h2>Bank Information</h2></td>
    </tr>
  <tr>
    <td width="188">Name</td>
    <td colspan="2"><span id="sprytextfield1">
      <input name="txtName" type="text" class="textbox" id="txtName" size="35" value="<?php echo $name; ?>" />
      <span class="textfieldRequiredMsg">*</span></span></td>
  </tr>
  <tr>
    <td>Type</td>
    <td colspan="2"><select name="ddlType" id="ddlType">
      <option value="Checking" <?php if($type == "Checking") {echo 'selected="selected"';} ?> >Checking</option>
      <option value="Savings" <?php if($type == "Savings") {echo 'selected="selected"';} ?> >Savings</option>
            </select></td>
  </tr>
  <tr>
    <td>Account Number</td>
    <td width="156"><input name="txtAccountNumber" type="text" class="textbox" id="txtAccountNumber" value="<?php echo $account_number; ?>" /></td>
    <td width="307" rowspan="2"><img src="https://ukhire.managebuilding.com/Manager/Images/img_check_usa.png" /></td>
  </tr>
  <tr>
    <td>Routing Number</td>
    <td><input name="txtRoutingNumber" type="text" class="textbox" id="txtRoutingNumber" value="<?php echo $routing_number; ?>" /></td>
    </tr>
  <tr>
    <td valign="top">Comments</td>
    <td colspan="2"><textarea name="txtComment" cols="71" rows="5" class="textarea" id="txtComment"><?php echo $comments; ?></textarea></td>
  </tr>
  <tr>
    <td colspan="3"><h2>Check Printing Confirmation</h2></td>
    </tr>
  <tr>
    <td valign="top">Enable Check Printing</td>
    <td colspan="2">
      <input name="chkLocalPrinting" type="checkbox" id="chkLocalPrinting" value="1" <?php if($enable_check_printing == "1") {echo 'checked="checked"';} ?> />
      Enable Local Prinitng Check.
      <p class="textarea" style="color:#999999;">
      <br />Order compatible checks and deposit slips from Nelco Solutions.<br /><br /></p></td>
  </tr>
  <tr>
    <td valign="top">Check Stock (Paper)</td>
    <td colspan="2">
      <input name="rdCheckStock" type="radio" id="rd" value="Blank" <?php if($check_stock_paper == "Blank") {echo 'checked="checked"';} ?> />
      Blank<br />
      <label for="_ctl7_rbPrePrinted">
      <input type="radio" name="rdCheckStock" id="rd2" value="Pre-printed with account and routing number" <?php if($check_stock_paper == "Pre-printed with account and routing number") {echo 'checked="checked"';} ?> />
      Pre-printed with account and routing number</label>      </td>
  </tr>
  <tr>
    <td>Check Format</td>
    <td colspan="2"><select name="ddlCheckFormat" id="ddlCheckFormat">
      <option value="One Voucher/One Signature" <?php if($check_format == "One Voucher/One Signature") {echo 'selected="selected"';} ?> >One Voucher/One Signature</option>
      <option value="Two Voucher/One Signature" <?php if($check_format == "Two Voucher/One Signature") {echo 'selected="selected"';} ?> >Two Voucher/One Signature</option>
      <option value="Two Voucher/Two Signature" <?php if($check_format == "Two Voucher/Two Signature") {echo 'selected="selected"';} ?> >Two Voucher/Two Signature</option>
    </select></td>
  </tr>
  <tr>
    <td>Signature Heading</td>
    <td colspan="2"><input name="txtSignatureHeading" type="text" class="textbox" id="txtSignatureHeading" size="35" value="<?php echo $signature_heading; ?>" /></td>
  </tr>
  <tr>
    <td>Fractional Number</td>
    <td colspan="2"><input name="txtFractionalNumber" type="text" class="textbox" id="txtFractionalNumber" size="35" value="<?php echo $fractional_number; ?>" /></td>
  </tr>
  <tr>
    <td>Bank Information</td>
    <td colspan="2"><input name="txtBankInformation" type="text" class="textbox" id="txtBankInformation" size="35" value="<?php echo $bank_information; ?>" /></td>
  </tr>
  <tr>
    <td>Company Information</td>
    <td colspan="2"><input type="checkbox" name="chk2" id="chk2" />
      Override Company Information</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2"><input name="txtPropertyName7" type="text" class="textbox" id="txtPropertyName7" size="35" style="background-color:#EEEEEE" disabled="disabled" /></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2"><input name="txtPropertyName3" type="text" class="textbox" id="txtPropertyName3" size="35" style="background-color:#EEEEEE" disabled="disabled"/></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2"><input name="txtPropertyName4" type="text" class="textbox" id="txtPropertyName4" size="35" style="background-color:#EEEEEE" disabled="disabled"/></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2"><input name="txtPropertyName5" type="text" class="textbox" id="txtPropertyName5" size="35" style="background-color:#EEEEEE" disabled="disabled"/></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2"><input name="txtPropertyName7" type="text" class="textbox" id="txtPropertyName7" size="35" style="background-color:#EEEEEE" disabled="disabled"/></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2"><input name="submit" type="submit" class="submit_button" id="submit" value="Update" /></td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
</form>
<script type="text/javascript">
<!--
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1");
//-->
</script>
</body>
</html>
