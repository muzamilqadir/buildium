<?php
include ("../includes/classes/class.db.php");
include ("../includes/functions/general.php");

//-------------------------------------------------------------------------------------------
								/*Objects*/
//-------------------------------------------------------------------------------------------					
	$db = new db();
	//isLoggedUser();
//-------------------------------------------------------------------------------------------
								/*General Coding Area*/
	$isUpdated = false;
	if ($_POST){
		$sqlUpdate = "UPDATE payments SET 
			amount = '".clearFormData("txtAmount")."',
			payment_date = '".clearFormData("txtPaymentDate")."'
			WHERE tenant_id = '".$_GET['tid']."'";
		$db->query($sqlUpdate);
		$isUpdated = true;		
	}
	
	// -------------- Populating Tenant Info ----------------
	$sqlTenant = "SELECT tenant_id, first_name, last_name FROM tenant_leases ORDER BY first_name ASC";
	$queryTenant= $db->ExeQuersys($sqlTenant);	
	while ($rowTenant = mysql_fetch_array($queryTenant)) {
		if (isset($_GET['tid']) && $_GET['tid'] != "" ) {
			if ($rowTenant['tenant_id'] == $_GET['tid']) {
				$tenantNames .= '<option value="'.$rowTenant['tenant_id'].'" selected="selected">'.$rowTenant['first_name'].' '.$rowTenant['last_name'].'</option>';
			} else {
				$tenantNames .= '<option value="'.$rowTenant['tenant_id'].'">'.$rowTenant['first_name'].' '.$rowTenant['last_name'].'</option>';
			}
		}
	}
	
	$tenantInfo = "<em>Please select tenant first.</em>";
	
	if (isset($_GET['tid']) && $_GET['tid'] != "" ) {
		$sqlTenant = "SELECT property_name, tenant_entry, number_of_units, frequency, rent, prepaid_rent, security_deposite, rent_due_day
		FROM tenant_leases
		WHERE tenant_id = '".$_GET['tid']."'";
		$queryTenant= $db->ExeQuersys($sqlTenant);	
		while ($rowTenant = mysql_fetch_array($queryTenant)) {
			$property_name = $rowTenant['property_name'];
			$tenant_entry = $rowTenant['tenant_entry'];
			$number_of_units = $rowTenant['number_of_units'];
			$frequency = $rowTenant['frequency'];
			$rent = $rowTenant['rent'];
			$prepaid_rent = $rowTenant['prepaid_rent'];
			$security_deposite = $rowTenant['security_deposite'];
			$rent_due_day = $rowTenant['rent_due_day'];
			
			$tenantInfo = '<strong>Property Name:</strong> '.$rowTenant['property_name'].'<br />
			<strong>Tenant Entry:</strong> '.$rowTenant['tenant_entry'].'<br />
			<strong>Number of Units:</strong> '.$rowTenant['number_of_units'].'<br />
			<strong>Frequency:</strong> '.$rowTenant['frequency'].'<br />
			<strong>Rent:</strong> '.$rowTenant['rent'].'<br />
			<strong>Prepaid Rent:</strong> '.$rowTenant['prepaid_rent'].'<br />
			<strong>Security Deposit:</strong> '.$rowTenant['security_deposite'].'<br />
			<strong>Rent Due Day:</strong> '.$rowTenant['rent_due_day'];
			
		}
	}
	
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Add A Payment</title>
<link href="../styles_popup.css" rel="stylesheet" type="text/css" />
<script src="../calendarDateInput.js" type="text/javascript"></script>
<script src="../SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<script src="../SpryAssets/SpryValidationSelect.js" type="text/javascript"></script>
<link href="../SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
<link href="../SpryAssets/SpryValidationSelect.css" rel="stylesheet" type="text/css" />
</head>

<body>
<form action="" method="post">
<div id="wrapper_header"><a href="#" onclick="return confirm('Do you want to cancel?');"><img src="../images/btn_cancel.png" alt="Cancel" width="109" height="34" border="0" style="float:right" /></a>
  <input type="image" name="submit2" id="submit2" src="../images/btn_save.png" />
</div>
<p>&nbsp;</p>
<?php if ($isUpdated) { echo '<div class="msg_success">Payment is updated successfully.</div>'; } ?>
<table width="471" border="0" align="center" cellpadding="2" cellspacing="3">
  <tr>
    <td colspan="3"><h1>Add A Payment</h1></td>
    </tr>
  <tr>
    <td colspan="3"><h2>Payment Information</h2></td>
    </tr>
  <tr>
    <td width="157" valign="top">Tenant</td>
    <td width="297" colspan="2"><span id="spryselect1">
      <select name="ddlTenants" id="ddlTenants" disabled="disabled">
        <option value="0">-- Select Tenant --</option>
        <?php echo $tenantNames; ?>
      </select>
      <span class="selectInvalidMsg">Please select a tenant</span>      <span class="selectRequiredMsg">Please select a tenant.</span></span><br />
    <em style="color:#999999">(Please select tenant first to load details)</em></td>
  </tr>
  <tr>
    <td valign="top">&nbsp;</td>
    <td colspan="2">&nbsp;</td>
  </tr>
  <tr>
    <td valign="top">Amount</td>
    <td colspan="2"><span id="sprytextfield1">
    <input name="txtAmount" type="text" class="textbox" id="txtAmount" value="<?php echo $rent; ?>" />
    <span class="textfieldRequiredMsg">Required.</span><span class="textfieldInvalidFormatMsg">Invalid amount.</span></span></td>
  </tr>
  <tr>
    <td valign="top">&nbsp;</td>
    <td colspan="2">&nbsp;</td>
  </tr>
  <tr>
    <td valign="top">Tenant Info</td>
    <td colspan="2"><?php echo $tenantInfo; ?></td>
  </tr>
  <tr>
    <td valign="top">&nbsp;</td>
    <td colspan="2">&nbsp;</td>
  </tr>
  <tr>
    <td valign="top">Payment Date</td>
    <td colspan="2"><script>DateInput('txtPaymentDate', true, 'YYYY-MM-DD')</script></td>
  </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td colspan="2">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2"><input name="submit" type="submit" class="submit_button" id="submit" value="Save" /></td>
  </tr>
</table>
</form>

<script type="text/javascript">
<!--
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1", "currency");
var spryselect1 = new Spry.Widget.ValidationSelect("spryselect1", {invalidValue:"0"});
//-->
</script>
</body>
</html>
