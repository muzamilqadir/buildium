<?php
include ("../includes/classes/class.db.php");
include ("../includes/functions/general.php");
//-------------------------------------------------------------------------------------------
								/*Objects*/
//-------------------------------------------------------------------------------------------					
	$db = new db();
	//isLoggedUser();
//-------------------------------------------------------------------------------------------
								/*General Coding Area*/
	$tid = "0";
	if (isset($_GET['id']) && $_GET['id'] != "" ) {
		$tid = $_GET['id'];
	}							
	//---------------------------------------------------------------------------------------
	
	$isUpdated = false;
	
	if ($_POST){
		$sqlUpdate = "UPDATE tasks SET
		subject = '".clearFormData("txtSubject")."', 
		description = '".clearFormData("txtDescription")."', 
		property = '".clearFormData("ddlProperty")."', 
		unit = '".clearFormData("ddlNumberOfUnits")."', 
		assigned_to = '".clearFormData("ddlAssignedTo")."', 
		priority = '".clearFormData("ddlPriority")."', 
		category = '".clearFormData("ddlCategory")."', 
		due_date = '".clearFormData("txtDueDate")."', 
		request_type = '".clearFormData("rdRequestType")."', 
		contact_name = '".clearFormData("txtContactName")."',
		email = '".clearFormData("txtEmail")."', 
		home_phone = '".clearFormData("txtHome")."', 
		work_phone = '".clearFormData("txtWork")."', 
		mobile_phone = '".clearFormData("txtMobile")."'
		WHERE task_id = '".$tid."'";
		
		$db->query($sqlUpdate);
		$isUpdated = true;
	}
	
	
	$sqlSelect = "SELECT * FROM tasks WHERE task_id = '".$tid."'";
	
	$query= $db->ExeQuersys($sqlSelect);
		
	while ($row = mysql_fetch_array($query)) {
		$subject = $row['subject'];
		$description = $row['description'];
		$property = $row['property'];
		$unit = $row['unit'];
		$assigned_to = $row['assigned_to'];
		$priority = $row['priority'];
		$category = $row['category'];
		$due_date = $row['due_date'];
		$request_type = $row['request_type'];
		$contact_name = $row['contact_name'];
		$email = $row['email'];
		$home_phone = $row['home_phone'];
		$work_phone = $row['work_phone'];
		$mobile_phone = $row['mobile_phone'];		
	}
	
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Update Task</title>
<link href="../styles_popup.css" rel="stylesheet" type="text/css" />
<script src="../calendarDateInput.js" type="text/javascript"></script>
</head>

<body>
<form action="" method="post" enctype="multipart/form-data">
<div id="wrapper_header"><a href="#" onclick="return confirm('Do you want to cancel?');"><img src="../images/btn_cancel.png" alt="Cancel" width="109" height="34" border="0" style="float:right" /></a>
  <input type="image" name="submit2" id="submit2" src="../images/btn_save.png" />
</div>
<p>&nbsp;</p>
<?php if ($isUpdated) { echo '<div class="msg_success">Task is updated successfully.</div>'; } ?>
<table width="664" border="0" align="center" cellpadding="2" cellspacing="3">
  <tr>
    <td colspan="2"><h1>Update Task</h1></td>
    </tr>
  <tr>
    <td colspan="2"><h2>Details:</h2></td>
    </tr>
  <tr>
    <td width="98">Subject</td>
    <td width="549"><input name="txtSubject" type="text" class="textbox" id="txtSubject" size="80" value="<?php echo $subject; ?>" /></td>
  </tr>
  <tr>
    <td valign="top">Description</td>
    <td><textarea name="txtDescription" cols="77" rows="5" class="textarea" id="txtDescription"><?php echo $description; ?></textarea></td>
  </tr>
  <tr>
    <td colspan="2"><table width="100%" border="0" cellspacing="2" cellpadding="0">
      <tr>
        <td width="16%" valign="top">Property</td>
        <td width="40%" valign="top"><select name="ddlProperty" id="ddlProperty">
          <option value="Residential Rental" <?php if($property == "Residential Rental") { echo 'selected="selected"'; } ?> >Residential Rental</option>
          <option value="Commercial Rental" <?php if($property == "Commercial Rental") { echo 'selected="selected"'; } ?> >Commercial Rental</option>
        </select></td>
        <td width="14%" valign="top">Request Type</td>
        <td width="30%" valign="top"><input name="rdRequestType" type="radio" id="rd" value="To do" <?php if($request_type == "To do") { echo 'checked="checked"'; } ?> />
          To do</td>
      </tr>
      <tr>
        <td valign="top">Unit</td>
        <td valign="top"><select name="ddlNumberOfUnits" id="ddlNumberOfUnits">
          <option value="1" <?php if($unit == "1") { echo 'selected="selected"'; } ?> >1</option>
          <option value="2" <?php if($unit == "2") { echo 'selected="selected"'; } ?> >2</option>
          <option value="3" <?php if($unit == "3") { echo 'selected="selected"'; } ?> >3</option>
          <option value="4" <?php if($unit == "4") { echo 'selected="selected"'; } ?> >4</option>
          <option value="5" <?php if($unit == "5") { echo 'selected="selected"'; } ?> >5</option>
          <option value="6" <?php if($unit == "6") { echo 'selected="selected"'; } ?> >6</option>
          <option value="7" <?php if($unit == "7") { echo 'selected="selected"'; } ?> >7</option>
          <option value="8" <?php if($unit == "8") { echo 'selected="selected"'; } ?> >8</option>
          <option value="9" <?php if($unit == "9") { echo 'selected="selected"'; } ?> >9</option>
          <option value="10" <?php if($unit == "10") { echo 'selected="selected"'; } ?> >10</option>
          <option value="11" <?php if($unit == "11") { echo 'selected="selected"'; } ?> >11</option>
          <option value="12" <?php if($unit == "12") { echo 'selected="selected"'; } ?> >12</option>
          <option value="13" <?php if($unit == "13") { echo 'selected="selected"'; } ?> >13</option>
          <option value="14" <?php if($unit == "14") { echo 'selected="selected"'; } ?> >14</option>
          <option value="15" <?php if($unit == "15") { echo 'selected="selected"'; } ?> >15</option>
        </select></td>
        <td valign="top">&nbsp;</td>
        <td valign="top"><input type="radio" name="rdRequestType" id="rd2" value="Prospect request" <?php if($request_type == "Prospect request") { echo 'checked="checked"'; } ?> />
Prospect request</td>
      </tr>
      <tr>
        <td valign="top">Attachment</td>
        <td valign="top"><input name="fileField" type="file" class="textbox" id="fileField" /></td>
        <td valign="top">&nbsp;</td>
        <td valign="top"><input type="radio" name="rdRequestType" id="rd3" value="Resident request" <?php if($request_type == "Resident request") { echo 'checked="checked"'; } ?> />
Resident request</td>
      </tr>
      <tr>
        <td valign="top">Assigned To</td>
        <td valign="top"><select name="ddlAssignedTo" id="ddlAssignedTo">
          <option value="Assigned To" <?php if($assigned_to == "Low") { echo 'selected="selected"'; } ?> >Assigned To</option>
        </select></td>
        <td valign="top">&nbsp;</td>
        <td valign="top"><input type="radio" name="rdRequestType" id="rd4" value="Rental owner request" <?php if($request_type == "Rental owner request") { echo 'checked="checked"'; } ?> />
Rental owner request</td>
      </tr>
      <tr>
        <td valign="top">Priority</td>
        <td valign="top"><select name="ddlPriority" id="ddlPriority">
          <option value="Low" <?php if($priority == "Low") { echo 'selected="selected"'; } ?> >Low</option>
          <option value="Normal" <?php if($priority == "Normal") { echo 'selected="selected"'; } ?> >Normal</option>
          <option value="High" <?php if($priority == "High") { echo 'selected="selected"'; } ?> >High</option>
                </select></td>
        <td valign="top">Contact Name</td>
        <td valign="top"><input name="txtContactName" type="text" class="textbox" id="txtPropertyName3" value="<?php echo $contact_name; ?>" /></td>
      </tr>
      <tr>
        <td valign="top">Category</td>
        <td valign="top"><select name="ddlCategory" id="ddlCategory">
          <option value="Uncategorized" <?php if($category == "Uncategorized") { echo 'selected="selected"'; } ?> >Uncategorized</option>
          <option value="Complaint" <?php if($category == "Complaint") { echo 'selected="selected"'; } ?> >Complaint</option>
          <option value="Feedback/Suggestion" <?php if($category == "Feedback/Suggestion") { echo 'selected="selected"'; } ?> >Feedback/Suggestion</option>
          <option value="General Inquiry" <?php if($category == "General Inquiry") { echo 'selected="selected"'; } ?> >General Inquiry</option>
          <option value="Maintence Request" <?php if($category == "Maintence Request") { echo 'selected="selected"'; } ?> >Maintence Request</option>
          <option value="Other" <?php if($category == "Other") { echo 'selected="selected"'; } ?> >Other</option>
                </select></td>
        <td valign="top">Email</td>
        <td valign="top"><input name="txtEmail" type="text" class="textbox" id="txtPropertyName4" value="<?php echo $email; ?>" /></td>
      </tr>
      <tr>
        <td valign="top">Due Date</td>
        <td valign="top"><script>DateInput('txtDueDate', true, 'YYYY-MM-DD', '<?php echo $due_date; ?>')</script>&nbsp;</td>
        <td valign="top">Home</td>
        <td valign="top"><input name="txtHome" type="text" class="textbox" id="txtHome" value="<?php echo $home_phone; ?>" /></td>
      </tr>
      <tr>
        <td valign="top">&nbsp;</td>
        <td valign="top">&nbsp;</td>
        <td valign="top">Work</td>
        <td valign="top"><input name="txtWork" type="text" class="textbox" id="txtPropertyName8" value="<?php echo $work_phone; ?>" /></td>
      </tr>
      <tr>
        <td valign="top">&nbsp;</td>
        <td valign="top">&nbsp;</td>
        <td valign="top">Mobile</td>
        <td valign="top"><input name="txtMobile" type="text" class="textbox" id="txtPropertyName17" value="<?php echo $mobile_phone; ?>" /></td>
      </tr>
    </table></td>
    </tr>
  <tr>
    <td colspan="2" align="center"><input name="submit" type="submit" class="submit_button" id="submit" value="Update" /></td>
    </tr>
</table>
</form>
</body>
</html>