<?php
include ("../includes/classes/class.db.php");
include ("../includes/functions/general.php");
//-------------------------------------------------------------------------------------------
								/*Objects*/
//-------------------------------------------------------------------------------------------					
	$db = new db();
	//isLoggedUser();
//-------------------------------------------------------------------------------------------
								/*General Coding Area*/
	$bid = "0";
	if (isset($_GET['id']) && $_GET['id'] != "" ) {
		$bid = $_GET['id'];
	}							
	//---------------------------------------------------------------------------------------
	
	$isUpdated = false;
	
	if ($_POST){
		$sqlUpdate = "UPDATE recurring_credit SET 
			vendor = '".clearFormData("ddlVendor")."',
			payee = '".clearFormData("rdPayee")."',
			memo = '".clearFormData("txtMemo")."',
			frequency = '".clearFormData("ddlFrequency")."',
			next_date = '".clearFormData("txtDate")."',
			duration = '".clearFormData("rdDuration")."',
			occurrances = '".clearFormData("txtOccurrences")."',
			posting_day = '".clearFormData("ddlPostingDay")."'
			WHERE recurring_credit_id = '".$bid."'";
		$db->query($sqlUpdate);
		$isUpdated = true;
	}
	
	$sqlSelect = "SELECT * FROM recurring_credit WHERE recurring_credit_id = '".$bid."'";
	
	$query= $db->ExeQuersys($sqlSelect);
		
	while ($row = mysql_fetch_array($query)) {
		$vendor = $row['vendor'];
		$payee = $row['payee'];
		$memo = $row['memo'];
		$frequency = $row['frequency'];
		$next_date = $row['next_date'];
		$duration = $row['duration'];
		$txtOccurrences = $row['txtOccurrences'];
		$posting_day = $row['posting_day'];
	}
	
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Recurring Credit</title>
<link href="../styles_popup.css" rel="stylesheet" type="text/css" />
<script src="../calendarDateInput.js" type="text/javascript"></script>
</head>

<body>
<form action="" method="post" enctype="multipart/form-data">
<div id="wrapper_header"><a href="#" onclick="return confirm('Do you want to cancel?');"><img src="../images/btn_cancel.png" alt="Cancel" width="109" height="34" border="0" style="float:right" /></a>
  <input type="image" name="submit2" id="submit2" src="../images/btn_save.png" />
</div>
<p>&nbsp;</p>
<?php if ($isUpdated) { echo '<div class="msg_success">Recurring Credit is updated successfully.</div>'; } ?>
<table width="675" border="0" align="center" cellpadding="2" cellspacing="3">
  <tr>
    <td colspan="3"><h1>Recurring Credit</h1></td>
    </tr>
  <tr>
    <td colspan="3"><h2>Bill Information</h2></td>
    </tr>
  <tr>
    <td valign="top">Vendor</td>
    <td colspan="2"><select name="ddlVendor" id="ddlVendor">
      <option value="Select" <?php if ($vendor == "Select") { echo 'selected="selected"'; } ?> >Select Vendor</option>
    </select>
[<a href="add_vendor.html"> Add Vendor</a> ]</td>
  </tr>
  <tr>
    <td valign="top">Payee</td>
    <td colspan="2"><input name="rdPayee" type="radio" id="rd3" value="Vendor" <?php if ($payee == "Vendor") { echo 'checked="checked""'; } ?> />
      Vendor<br />
      <input type="radio" name="rdPayee" id="rd4" value="Rental Owner" <?php if ($payee == "Rental Owner") { echo 'checked="checked""'; } ?> />
      Rental Owner</td>
  </tr>
  <tr>
    <td valign="top">Memo</td>
    <td colspan="2"><input name="txtMemo" type="text" class="textbox" id="txtMemo" value="<?php echo $memo; ?>" /></td>
  </tr>
  <tr>
    <td colspan="3" valign="top" bordercolor="#ECC3A1"><h2>Recurrence Information</h2></td>
    </tr>
  <tr>
    <td valign="top">Frequency</td>
    <td colspan="2">
    <select name="ddlFrequency" id="ddlFrequency">
    <option selected="selected" value="">Select a frequency</option>
		<option value="Daily" <?php if($frequency == "Daily") { echo 'selected="selected"'; } ?> >Daily</option>
		<option value="Weekly" <?php if($frequency == "Weekly") { echo 'selected="selected"'; } ?> >Weekly</option>
		<option value="Every two weeks" <?php if($frequency == "Every two weeks") { echo 'selected="selected"'; } ?> >Every two weeks</option>
		<option value="Monthly" <?php if($frequency == "Monthly") { echo 'selected="selected"'; } ?> >Monthly</option>
		<option value="Every two months" <?php if($frequency == "Every two months") { echo 'selected="selected"'; } ?> >Every two months</option>
		<option value="Quarterly" <?php if($frequency == "Quarterly") { echo 'selected="selected"'; } ?> >Quarterly</option>
		<option value="Every six months" <?php if($frequency == "Every six months") { echo 'selected="selected"'; } ?> >Every six months</option>
		<option value="Yearly" <?php if($frequency == "Yearly") { echo 'selected="selected"'; } ?> >Yearly</option>
		<option value="One time" <?php if($frequency == "One time") { echo 'selected="selected"'; } ?> >One time</option>
    </select>    </td>
  </tr>
  <tr>
    <td valign="top">Next date</td>
    <td colspan="2"><script>DateInput('txtDate', true, 'YYYY-MM-DD', '<?php echo $next_date; ?>')</script>&nbsp;</td>
  </tr>
  <tr>
    <td valign="top"> Duration </td>
    <td colspan="2"><input type="radio" name="rdDuration" id="rd" value="End After" <?php if ($duration == "End After") { echo 'checked="checked"'; } ?> />
      End after 
      <input name="txtOccurrences" type="text" class="textbox" id="txtOccurrences" size="10" value="<?php echo $txtOccurrences; ?>" />
      occurrences<br />
      <input name="rdDuration" type="radio" id="rd2" value="Until cancelled" <?php if ($duration == "Until cancelled") { echo 'checked="checked"'; } ?> />
      Until cancelled</td>
  </tr>
  <tr>
    <td valign="top">Posting day</td>
    <td colspan="2"> Post 
      <select name="ddlPostingDay" id="ddlPostingDay">
      	<option value="0" <?php if($posting_day == "0") { echo 'selected="selected"'; } ?> >0</option>
		<option value="1" <?php if($posting_day == "1") { echo 'selected="selected"'; } ?> >1</option>
		<option value="2" <?php if($posting_day == "2") { echo 'selected="selected"'; } ?> >2</option>
		<option value="3" <?php if($posting_day == "3") { echo 'selected="selected"'; } ?> >3</option>
		<option value="4" <?php if($posting_day == "4") { echo 'selected="selected"'; } ?> >4</option>
		<option value="5" <?php if($posting_day == "5") { echo 'selected="selected"'; } ?> >5</option>
		<option value="6" <?php if($posting_day == "6") { echo 'selected="selected"'; } ?> >6</option>
		<option value="7" <?php if($posting_day == "7") { echo 'selected="selected"'; } ?> >7</option>
		<option value="8" <?php if($posting_day == "8") { echo 'selected="selected"'; } ?> >8</option>
		<option value="9" <?php if($posting_day == "9") { echo 'selected="selected"'; } ?> >9</option>
		<option value="10" <?php if($posting_day == "10") { echo 'selected="selected"'; } ?> >10</option>
		<option value="11" <?php if($posting_day == "11") { echo 'selected="selected"'; } ?> >11</option>
		<option value="12" <?php if($posting_day == "12") { echo 'selected="selected"'; } ?> >12</option>
		<option value="13" <?php if($posting_day == "13") { echo 'selected="selected"'; } ?> >13</option>
		<option value="14" <?php if($posting_day == "14") { echo 'selected="selected"'; } ?> >14</option>
		<option value="15" <?php if($posting_day == "15") { echo 'selected="selected"'; } ?> >15</option>
		<option value="16" <?php if($posting_day == "16") { echo 'selected="selected"'; } ?> >16</option>
		<option value="17" <?php if($posting_day == "17") { echo 'selected="selected"'; } ?> >17</option>
		<option value="18" <?php if($posting_day == "18") { echo 'selected="selected"'; } ?> >18</option>
		<option value="19" <?php if($posting_day == "19") { echo 'selected="selected"'; } ?> >19</option>
		<option value="20" <?php if($posting_day == "20") { echo 'selected="selected"'; } ?> >20</option>
		<option value="21" <?php if($posting_day == "21") { echo 'selected="selected"'; } ?> >21</option>
		<option value="22" <?php if($posting_day == "22") { echo 'selected="selected"'; } ?> >22</option>
		<option value="23" <?php if($posting_day == "23") { echo 'selected="selected"'; } ?> >23</option>
		<option value="24" <?php if($posting_day == "24") { echo 'selected="selected"'; } ?> >24</option>
		<option value="25" <?php if($posting_day == "25") { echo 'selected="selected"'; } ?> >25</option>
		<option value="26" <?php if($posting_day == "26") { echo 'selected="selected"'; } ?> >26</option>
		<option value="27" <?php if($posting_day == "27") { echo 'selected="selected"'; } ?> >27</option>
		<option value="28" <?php if($posting_day == "28") { echo 'selected="selected"'; } ?> >28</option>
		<option value="29" <?php if($posting_day == "29") { echo 'selected="selected"'; } ?> >29</option>
		<option value="30" <?php if($posting_day == "30") { echo 'selected="selected"'; } ?> >30</option>
		<option value="31" <?php if($posting_day == "31") { echo 'selected="selected"'; } ?> >31</option>
		<option value="32" <?php if($posting_day == "32") { echo 'selected="selected"'; } ?> >32</option>
		<option value="33" <?php if($posting_day == "33") { echo 'selected="selected"'; } ?> >33</option>
		<option value="34" <?php if($posting_day == "34") { echo 'selected="selected"'; } ?> >34</option>
		<option value="35" <?php if($posting_day == "35") { echo 'selected="selected"'; } ?> >35</option>
		<option value="36" <?php if($posting_day == "36") { echo 'selected="selected"'; } ?> >36</option>
		<option value="37" <?php if($posting_day == "37") { echo 'selected="selected"'; } ?> >37</option>
		<option value="38" <?php if($posting_day == "38") { echo 'selected="selected"'; } ?> >38</option>
		<option value="39" <?php if($posting_day == "39") { echo 'selected="selected"'; } ?> >39</option>
		<option value="40" <?php if($posting_day == "40") { echo 'selected="selected"'; } ?> >40</option>
		<option value="41" <?php if($posting_day == "41") { echo 'selected="selected"'; } ?> >41</option>
		<option value="42" <?php if($posting_day == "42") { echo 'selected="selected"'; } ?> >42</option>
		<option value="43" <?php if($posting_day == "43") { echo 'selected="selected"'; } ?> >43</option>
		<option value="44" <?php if($posting_day == "44") { echo 'selected="selected"'; } ?> >44</option>
		<option value="45" <?php if($posting_day == "45") { echo 'selected="selected"'; } ?> >45</option>
		<option value="60" <?php if($posting_day == "60") { echo 'selected="selected"'; } ?> >60</option>
		<option value="75" <?php if($posting_day == "75") { echo 'selected="selected"'; } ?> >75</option>
		<option value="90" <?php if($posting_day == "90") { echo 'selected="selected"'; } ?> >90</option>
      </select>
      days in advance</td>
  </tr>
  <tr>
    <td colspan="3"><h2>Apply amount to acconts</h2></td>
    </tr>
  <tr>
    <td colspan="3" valign="top"><table width="100%" border="0" bgcolor="#FFFFFF">
      <tr>
        <td width="24%" height="26" bgcolor="#FFDFEF">Property </td>
        <td width="37%" bgcolor="#FFDFEF">Account</td>
        <td width="29%" bgcolor="#FFDFEF">Description</td>
        <td width="10%" bgcolor="#FFDFEF">Amount</td>
      </tr>
      <tr>
        <td height="39"><select name="ddlCountries3" id="ddlCountries3">
          <option>Select Property</option>
                </select></td>
        <td><select name="ddlCountries4" id="ddlCountries4">
          <option>Select Account</option>
                </select></td>
        <td><input name="txtPropertyName22" type="text" class="textbox" id="txtPropertyName37" /></td>
        <td><input name="txtPropertyName23" type="text" class="textbox" id="txtPropertyName38" size="7" /></td>
      </tr>
      <tr>
        <td colspan="5"><strong>Total:</strong></td>
      </tr>
    </table></td>
    </tr>
  <tr>
    <td colspan="3" align="center"><br />
      <input name="submit" type="submit" class="submit_button" id="submit" value="Save" /></td>
    </tr>
</table>
<p>&nbsp;</p>
</form>
</body>
</html>
