<?php
include ("../includes/classes/class.db.php");
include ("../includes/functions/general.php");

//-------------------------------------------------------------------------------------------
								/*Objects*/
//-------------------------------------------------------------------------------------------					
	$db = new db();
	//isLoggedUser();
//-------------------------------------------------------------------------------------------
								/*General Coding Area*/
								
	$pid = "0";
	if (isset($_GET['id']) && $_GET['id'] != "" ) {
		$pid = $_GET['id'];
	}							
	//---------------------------------------------------------------------------------------
								
	$isUpdated = false;
	
	if ($_POST){
		$sqlUpdate = "UPDATE properties SET
		property_name = '".clearFormData("txtPropertyName")."',
		property_type = '".clearFormData("ddlPropertyType")."',
		property_sub_type = '".clearFormData("ddlSubtype")."',
		operating_account = '".clearFormData("ddlOperatingAccount")."',
		number_of_units = '".clearFormData("txtNumberOfUnits")."',
		country = '".clearFormData("ddlCountries")."',
		address_1 = '".clearFormData("txtAddress_1")."',
		address_2 = '".clearFormData("txtAddress_2")."',
		address_3 = '".clearFormData("txtAddress_3")."',
		city = '".clearFormData("txtCity")."',
		state = '".clearFormData("ddlState")."',
		zipcode = '".clearFormData("txtZipCode")."'
		WHERE
		property_id = '".$pid."'";
		
		$db->query($sqlUpdate);
		$isUpdated = true;
	}
	
	$sqlSelect = "SELECT property_id, property_name, property_type, property_sub_type, operating_account, number_of_units,
	country, address_1, address_2, address_3, city, state, zipcode
	FROM properties
	WHERE property_id = '".$pid."'";
	
	$query= $db->ExeQuersys($sqlSelect);
		
	while ($row = mysql_fetch_array($query)) {
		$valPropertyID = $row['property_id'];
		$valPropertyName = $row['property_name'];
		$valPropertyType = $row['property_type'];
		$valProperySubType = $row['property_sub_type'];
		$valOperatingAccount = $row['operating_account'];
		$valNumberOfUnits = $row['number_of_units'];
		$valCountry = $row['country'];
		$valAddress_1 = $row['address_1'];
		$valAddress_2 = $row['address_2'];
		$valAddress_3 = $row['address_3'];
		$valCity = $row['city'];
		$valState = $row['state'];
		$valZipCode = $row['zipcode'];
	}
	
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Update Property</title>
<link href="../styles_popup.css" rel="stylesheet" type="text/css" />
<script src="../SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<link href="../SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
</head>

<body>
<form action="" method="post">
<div id="wrapper_header"><a href="#" onclick="return confirm('Do you want to cancel?');"><img src="../images/btn_cancel.png" alt="Cancel" width="109" height="34" border="0" style="float:right" /></a>
  <input type="image" name="submit2" id="submit2" src="../images/btn_save.png" />
</div>
<p>&nbsp;</p>
<?php if ($isUpdated) { echo '<div class="msg_success">Property is updated successfully.</div>'; } ?>
<table width="575" border="0" align="center" cellpadding="2" cellspacing="3">
  <tr>
    <td colspan="2"><h1>Update Property</h1></td>
    </tr>
  <tr>
    <td width="188">Property name</td>
    <td width="370"><span id="sprytextfield1">
      <input name="txtPropertyName" type="text" class="textbox" id="txtPropertyName" value="<?php echo $valPropertyName; ?>" />
      <span class="textfieldRequiredMsg">*</span></span></td>
  </tr>
  <tr>
    <td>Property type</td>
    <td><select name="ddlPropertyType" id="ddlPropertyType">
      <option value="Residential Rental" <?php if ($valPropertyType == "Residential Rental") { echo 'selected="selected"';} ?> >Residential Rental</option>
      <option value="Commercial Rental" <?php if ($valPropertyType == "Commercial Rental") { echo 'selected="selected"';} ?> ></option>
    </select>    </td>
  </tr>
  <tr>
    <td>Property subtype</td>
    <td><select name="ddlSubtype" id="ddlSubtype">
      <option value="Condo / Town House" <?php if ($valProperySubType == "Condo / Town House") { echo 'selected="selected"';} ?> >Condo / Town House</option>
      <option value="Multi-Family" <?php if ($valProperySubType == "Multi-Family") { echo 'selected="selected"';} ?> >Multi-Family</option>
      <option value="Single-Family" <?php if ($valProperySubType == "Single-Family") { echo 'selected="selected"';} ?> >Single-Family</option>
        </select></td>
  </tr>
  <tr>
    <td>Operating account </td>
    <td><select name="ddlOperatingAccount" id="ddlOperatingAccount">
      <option value="Operating Bank Account" <?php if ($valOperatingAccount == "Operating Bank Account") { echo 'selected="selected"';} ?> >Operating Bank Account</option>
      <option value="Secure Deposit Bank Account" <?php if ($valOperatingAccount == "Secure Deposit Bank Account") { echo 'selected="selected"';} ?> >Secure Deposit Bank Account</option>
        </select></td>
  </tr>
  <tr>
    <td>Number of units</td>
    <td><span id="sprytextfield2">
    <input name="txtNumberOfUnits" type="text" class="textbox" id="txtNumberOfUnits" value="<?php echo $valNumberOfUnits; ?>" />
    <span class="textfieldRequiredMsg">*</span><span class="textfieldInvalidFormatMsg">This can be only numeric value.</span></span></td>
  </tr>
  <tr>
    <td>Country</td>
    <td><select name="ddlCountries" id="ddlCountries">
    <option value="1" <?php if ($valCountry == "1") { echo 'selected="selected"';} ?> >Afghanistan</option>
	<option value="2" <?php if ($valCountry == "2") { echo 'selected="selected"';} ?> >Akrotiri</option>
	<option value="3" <?php if ($valCountry == "3") { echo 'selected="selected"';} ?> >Albania</option>
	<option value="4" <?php if ($valCountry == "4") { echo 'selected="selected"';} ?> >Algeria</option>
	<option value="5" <?php if ($valCountry == "5") { echo 'selected="selected"';} ?> >American Samoa</option>
	<option value="6" <?php if ($valCountry == "6") { echo 'selected="selected"';} ?> >Andorra</option>
	<option value="7" <?php if ($valCountry == "7") { echo 'selected="selected"';} ?> >Angola</option>
	<option value="8" <?php if ($valCountry == "8") { echo 'selected="selected"';} ?> >Anguilla</option>
	<option value="9" <?php if ($valCountry == "9") { echo 'selected="selected"';} ?> >Antarctica</option>
	<option value="10" <?php if ($valCountry == "10") { echo 'selected="selected"';} ?> >Antigua and Barbuda</option>
	<option value="11" <?php if ($valCountry == "11") { echo 'selected="selected"';} ?> >Argentina</option>
	<option value="12" <?php if ($valCountry == "12") { echo 'selected="selected"';} ?> >Armenia</option>
	<option value="13" <?php if ($valCountry == "13") { echo 'selected="selected"';} ?> >Aruba</option>
	<option value="14" <?php if ($valCountry == "14") { echo 'selected="selected"';} ?> >Ashmore and Cartier Islands</option>
	<option value="15" <?php if ($valCountry == "15") { echo 'selected="selected"';} ?> >Australia</option>
	<option value="16" <?php if ($valCountry == "16") { echo 'selected="selected"';} ?> >Austria</option>
	<option value="17" <?php if ($valCountry == "17") { echo 'selected="selected"';} ?> >Azerbaijan</option>
	<option value="18" <?php if ($valCountry == "18") { echo 'selected="selected"';} ?> >Bahamas, The</option>
	<option value="19" <?php if ($valCountry == "19") { echo 'selected="selected"';} ?> >Bahrain</option>
	<option value="20" <?php if ($valCountry == "20") { echo 'selected="selected"';} ?> >Bangladesh</option>
	<option value="21" <?php if ($valCountry == "21") { echo 'selected="selected"';} ?> >Barbados</option>
	<option value="22" <?php if ($valCountry == "22") { echo 'selected="selected"';} ?> >Bassas da India</option>
	<option value="23" <?php if ($valCountry == "23") { echo 'selected="selected"';} ?> >Belarus</option>
	<option value="24" <?php if ($valCountry == "24") { echo 'selected="selected"';} ?> >Belgium</option>
	<option value="25" <?php if ($valCountry == "25") { echo 'selected="selected"';} ?> >Belize</option>
	<option value="26" <?php if ($valCountry == "26") { echo 'selected="selected"';} ?> >Benin</option>
	<option value="27" <?php if ($valCountry == "27") { echo 'selected="selected"';} ?> >Bermuda</option>
	<option value="28" <?php if ($valCountry == "28") { echo 'selected="selected"';} ?> >Bhutan</option>
	<option value="29" <?php if ($valCountry == "29") { echo 'selected="selected"';} ?> >Bolivia</option>
	<option value="30" <?php if ($valCountry == "30") { echo 'selected="selected"';} ?> >Bosnia and Herzegovina</option>
	<option value="31" <?php if ($valCountry == "31") { echo 'selected="selected"';} ?> >Botswana</option>
	<option value="32" <?php if ($valCountry == "32") { echo 'selected="selected"';} ?> >Bouvet Island</option>
	<option value="33" <?php if ($valCountry == "33") { echo 'selected="selected"';} ?> >Brazil</option>
	<option value="34" <?php if ($valCountry == "34") { echo 'selected="selected"';} ?> >British Indian Ocean Territory</option>
	<option value="35" <?php if ($valCountry == "35") { echo 'selected="selected"';} ?> >British Virgin Islands</option>
	<option value="36" <?php if ($valCountry == "36") { echo 'selected="selected"';} ?> >Brunei</option>
	<option value="37" <?php if ($valCountry == "37") { echo 'selected="selected"';} ?> >Bulgaria</option>
	<option value="38" <?php if ($valCountry == "38") { echo 'selected="selected"';} ?> >Burkina Faso</option>
	<option value="39" <?php if ($valCountry == "39") { echo 'selected="selected"';} ?> >Burma</option>
	<option value="40" <?php if ($valCountry == "40") { echo 'selected="selected"';} ?> >Burundi</option>
	<option value="41" <?php if ($valCountry == "41") { echo 'selected="selected"';} ?> >Cambodia</option>
	<option value="42" <?php if ($valCountry == "42") { echo 'selected="selected"';} ?> >Cameroon</option>
	<option value="43" <?php if ($valCountry == "43") { echo 'selected="selected"';} ?> >Canada</option>
	<option value="44" <?php if ($valCountry == "44") { echo 'selected="selected"';} ?> >Cape Verde</option>
	<option value="45" <?php if ($valCountry == "45") { echo 'selected="selected"';} ?> >Cayman Islands</option>
	<option value="46" <?php if ($valCountry == "46") { echo 'selected="selected"';} ?> >Central African Republic</option>
	<option value="47" <?php if ($valCountry == "47") { echo 'selected="selected"';} ?> >Chad</option>
	<option value="48" <?php if ($valCountry == "48") { echo 'selected="selected"';} ?> >Chile</option>
	<option value="49" <?php if ($valCountry == "49") { echo 'selected="selected"';} ?> >China</option>
	<option value="50" <?php if ($valCountry == "50") { echo 'selected="selected"';} ?> >Christmas Island</option>
	<option value="51" <?php if ($valCountry == "51") { echo 'selected="selected"';} ?> >Clipperton Island</option>
	<option value="52" <?php if ($valCountry == "52") { echo 'selected="selected"';} ?> >Cocos (Keeling) Islands</option>
	<option value="53" <?php if ($valCountry == "53") { echo 'selected="selected"';} ?> >Colombia</option>
	<option value="54" <?php if ($valCountry == "54") { echo 'selected="selected"';} ?> >Comoros</option>
	<option value="55" <?php if ($valCountry == "55") { echo 'selected="selected"';} ?> >Congo, Democratic Republic of the</option>
	<option value="56" <?php if ($valCountry == "56") { echo 'selected="selected"';} ?> >Congo, Republic of the</option>
	<option value="57" <?php if ($valCountry == "57") { echo 'selected="selected"';} ?> >Cook Islands</option>
	<option value="58" <?php if ($valCountry == "58") { echo 'selected="selected"';} ?> >Coral Sea Islands</option>
	<option value="59" <?php if ($valCountry == "59") { echo 'selected="selected"';} ?> >Costa Rica</option>
	<option value="60" <?php if ($valCountry == "60") { echo 'selected="selected"';} ?> >Cote d&#39;Ivoire</option>
	<option value="61" <?php if ($valCountry == "61") { echo 'selected="selected"';} ?> >Croatia</option>
	<option value="62" <?php if ($valCountry == "62") { echo 'selected="selected"';} ?> >Cuba</option>
	<option value="63" <?php if ($valCountry == "63") { echo 'selected="selected"';} ?> >Cyprus</option>
	<option value="64" <?php if ($valCountry == "64") { echo 'selected="selected"';} ?> >Czech Republic</option>
	<option value="66" <?php if ($valCountry == "66") { echo 'selected="selected"';} ?> >Dhekelia</option>
	<option value="67" <?php if ($valCountry == "67") { echo 'selected="selected"';} ?> >Djibouti</option>
	<option value="68" <?php if ($valCountry == "68") { echo 'selected="selected"';} ?> >Dominica</option>
	<option value="69" <?php if ($valCountry == "69") { echo 'selected="selected"';} ?> >Dominican Republic</option>
	<option value="70" <?php if ($valCountry == "70") { echo 'selected="selected"';} ?> >Ecuador</option>
	<option value="71" <?php if ($valCountry == "71") { echo 'selected="selected"';} ?> >Egypt</option>
	<option value="72" <?php if ($valCountry == "72") { echo 'selected="selected"';} ?> >El Salvador</option>
	<option value="73" <?php if ($valCountry == "73") { echo 'selected="selected"';} ?> >Equatorial Guinea</option>
	<option value="74" <?php if ($valCountry == "74") { echo 'selected="selected"';} ?> >Eritrea</option>
	<option value="75" <?php if ($valCountry == "75") { echo 'selected="selected"';} ?> >Estonia</option>
	<option value="76" <?php if ($valCountry == "76") { echo 'selected="selected"';} ?> >Ethiopia</option>
	<option value="77" <?php if ($valCountry == "77") { echo 'selected="selected"';} ?> >Europa Island</option>
	<option value="78" <?php if ($valCountry == "78") { echo 'selected="selected"';} ?> >Falkland Islands (Islas Malvinas)</option>
	<option value="79" <?php if ($valCountry == "79") { echo 'selected="selected"';} ?> >Faroe Islands</option>
	<option value="80" <?php if ($valCountry == "80") { echo 'selected="selected"';} ?> >Fiji</option>
	<option value="81" <?php if ($valCountry == "81") { echo 'selected="selected"';} ?> >Finland</option>
	<option value="82" <?php if ($valCountry == "82") { echo 'selected="selected"';} ?> >France</option>
	<option value="83" <?php if ($valCountry == "83") { echo 'selected="selected"';} ?> >French Guiana</option>
	<option value="84" <?php if ($valCountry == "84") { echo 'selected="selected"';} ?> >French Polynesia</option>
	<option value="85" <?php if ($valCountry == "85") { echo 'selected="selected"';} ?> >French Southern and Antarctic Lands</option>
	<option value="86" <?php if ($valCountry == "86") { echo 'selected="selected"';} ?> >Gabon</option>
	<option value="87" <?php if ($valCountry == "87") { echo 'selected="selected"';} ?> >Gambia, The</option>
	<option value="88" <?php if ($valCountry == "88") { echo 'selected="selected"';} ?> >Gaza Strip</option>
	<option value="89" <?php if ($valCountry == "89") { echo 'selected="selected"';} ?> >Georgia</option>
	<option value="90" <?php if ($valCountry == "90") { echo 'selected="selected"';} ?> >Germany</option>
	<option value="91" <?php if ($valCountry == "91") { echo 'selected="selected"';} ?> >Ghana</option>
	<option value="92" <?php if ($valCountry == "92") { echo 'selected="selected"';} ?> >Gibraltar</option>
	<option value="93" <?php if ($valCountry == "93") { echo 'selected="selected"';} ?> >Glorioso Islands</option>
	<option value="94" <?php if ($valCountry == "94") { echo 'selected="selected"';} ?> >Greece</option>
	<option value="95" <?php if ($valCountry == "95") { echo 'selected="selected"';} ?> >Greenland</option>
	<option value="96" <?php if ($valCountry == "96") { echo 'selected="selected"';} ?> >Grenada</option>
	<option value="97" <?php if ($valCountry == "97") { echo 'selected="selected"';} ?> >Guadeloupe</option>
	<option value="98" <?php if ($valCountry == "98") { echo 'selected="selected"';} ?> >Guam</option>
	<option value="99" <?php if ($valCountry == "99") { echo 'selected="selected"';} ?> >Guatemala</option>
	<option value="100" <?php if ($valCountry == "100") { echo 'selected="selected"';} ?> >Guernsey</option>
	<option value="101" <?php if ($valCountry == "101") { echo 'selected="selected"';} ?> >Guinea</option>
	<option value="102" <?php if ($valCountry == "102") { echo 'selected="selected"';} ?> >Guinea-Bissau</option>
	<option value="103" <?php if ($valCountry == "103") { echo 'selected="selected"';} ?> >Guyana</option>
	<option value="104" <?php if ($valCountry == "104") { echo 'selected="selected"';} ?> >Haiti</option>
	<option value="105" <?php if ($valCountry == "105") { echo 'selected="selected"';} ?> >Heard Island and McDonald Islands</option>
	<option value="106" <?php if ($valCountry == "106") { echo 'selected="selected"';} ?> >Holy See (Vatican City)</option>
	<option value="107" <?php if ($valCountry == "107") { echo 'selected="selected"';} ?> >Honduras</option>
	<option value="108" <?php if ($valCountry == "108") { echo 'selected="selected"';} ?> >Hong Kong</option>
	<option value="109" <?php if ($valCountry == "109") { echo 'selected="selected"';} ?> >Hungary</option>
	<option value="110" <?php if ($valCountry == "110") { echo 'selected="selected"';} ?> >Iceland</option>
	<option value="111" <?php if ($valCountry == "111") { echo 'selected="selected"';} ?> >India</option>
	<option value="112" <?php if ($valCountry == "112") { echo 'selected="selected"';} ?> >Indonesia</option>
	<option value="113" <?php if ($valCountry == "113") { echo 'selected="selected"';} ?> >Iran</option>
	<option value="114" <?php if ($valCountry == "114") { echo 'selected="selected"';} ?> >Iraq</option>
	<option value="115" <?php if ($valCountry == "115") { echo 'selected="selected"';} ?> >Ireland</option>
	<option value="116" <?php if ($valCountry == "116") { echo 'selected="selected"';} ?> >Isle of Man</option>
	<option value="117" <?php if ($valCountry == "117") { echo 'selected="selected"';} ?> >Israel</option>
	<option value="118" <?php if ($valCountry == "118") { echo 'selected="selected"';} ?> >Italy</option>
	<option value="119" <?php if ($valCountry == "119") { echo 'selected="selected"';} ?> >Jamaica</option>
	<option value="120" <?php if ($valCountry == "120") { echo 'selected="selected"';} ?> >Jan Mayen</option>
	<option value="121" <?php if ($valCountry == "121") { echo 'selected="selected"';} ?> >Japan</option>
	<option value="122" <?php if ($valCountry == "122") { echo 'selected="selected"';} ?> >Jersey</option>
	<option value="123" <?php if ($valCountry == "123") { echo 'selected="selected"';} ?> >Jordan</option>
	<option value="124" <?php if ($valCountry == "124") { echo 'selected="selected"';} ?> >Juan de Nova Island</option>
	<option value="125" <?php if ($valCountry == "125") { echo 'selected="selected"';} ?> >Kazakhstan</option>
	<option value="126" <?php if ($valCountry == "126") { echo 'selected="selected"';} ?> >Kenya</option>
	<option value="127" <?php if ($valCountry == "127") { echo 'selected="selected"';} ?> >Kiribati</option>
	<option value="128" <?php if ($valCountry == "128") { echo 'selected="selected"';} ?> >Korea, North</option>
	<option value="129" <?php if ($valCountry == "129") { echo 'selected="selected"';} ?> >Korea, South</option>
	<option value="130" <?php if ($valCountry == "130") { echo 'selected="selected"';} ?> >Kuwait</option>
	<option value="131" <?php if ($valCountry == "131") { echo 'selected="selected"';} ?> >Kyrgyzstan</option>
	<option value="132" <?php if ($valCountry == "132") { echo 'selected="selected"';} ?> >Laos</option>
	<option value="133" <?php if ($valCountry == "133") { echo 'selected="selected"';} ?> >Latvia</option>
	<option value="134" <?php if ($valCountry == "134") { echo 'selected="selected"';} ?> >Lebanon</option>
	<option value="135" <?php if ($valCountry == "135") { echo 'selected="selected"';} ?> >Lesotho</option>
	<option value="136" <?php if ($valCountry == "136") { echo 'selected="selected"';} ?> >Liberia</option>
	<option value="137" <?php if ($valCountry == "137") { echo 'selected="selected"';} ?> >Libya</option>
	<option value="138" <?php if ($valCountry == "138") { echo 'selected="selected"';} ?> >Liechtenstein</option>
	<option value="139" <?php if ($valCountry == "139") { echo 'selected="selected"';} ?> >Lithuania</option>
	<option value="140" <?php if ($valCountry == "140") { echo 'selected="selected"';} ?> >Luxembourg</option>
	<option value="141" <?php if ($valCountry == "141") { echo 'selected="selected"';} ?> >Macau</option>
	<option value="142" <?php if ($valCountry == "142") { echo 'selected="selected"';} ?> >Macedonia</option>
	<option value="143" <?php if ($valCountry == "143") { echo 'selected="selected"';} ?> >Madagascar</option>
	<option value="144" <?php if ($valCountry == "144") { echo 'selected="selected"';} ?> >Malawi</option>
	<option value="145" <?php if ($valCountry == "145") { echo 'selected="selected"';} ?> >Malaysia</option>
	<option value="146" <?php if ($valCountry == "146") { echo 'selected="selected"';} ?> >Maldives</option>
	<option value="147" <?php if ($valCountry == "147") { echo 'selected="selected"';} ?> >Mali</option>
	<option value="148" <?php if ($valCountry == "148") { echo 'selected="selected"';} ?> >Malta</option>
	<option value="149" <?php if ($valCountry == "149") { echo 'selected="selected"';} ?> >Marshall Islands</option>
	<option value="150" <?php if ($valCountry == "150") { echo 'selected="selected"';} ?> >Martinique</option>
	<option value="151" <?php if ($valCountry == "151") { echo 'selected="selected"';} ?> >Mauritania</option>
	<option value="152" <?php if ($valCountry == "152") { echo 'selected="selected"';} ?> >Mauritius</option>
	<option value="153" <?php if ($valCountry == "153") { echo 'selected="selected"';} ?> >Mayotte</option>
	<option value="154" <?php if ($valCountry == "154") { echo 'selected="selected"';} ?> >Mexico</option>
	<option value="155" <?php if ($valCountry == "155") { echo 'selected="selected"';} ?> >Micronesia, Federated States of</option>
	<option value="156" <?php if ($valCountry == "156") { echo 'selected="selected"';} ?> >Moldova</option>
	<option value="157" <?php if ($valCountry == "157") { echo 'selected="selected"';} ?> >Monaco</option>
	<option value="158" <?php if ($valCountry == "158") { echo 'selected="selected"';} ?> >Mongolia</option>
	<option value="159" <?php if ($valCountry == "159") { echo 'selected="selected"';} ?> >Montserrat</option>
	<option value="160" <?php if ($valCountry == "160") { echo 'selected="selected"';} ?> >Morocco</option>
	<option value="161" <?php if ($valCountry == "161") { echo 'selected="selected"';} ?> >Mozambique</option>
	<option value="162" <?php if ($valCountry == "162") { echo 'selected="selected"';} ?> >Namibia</option>
	<option value="163" <?php if ($valCountry == "163") { echo 'selected="selected"';} ?> >Nauru</option>
	<option value="164" <?php if ($valCountry == "164") { echo 'selected="selected"';} ?> >Navassa Island</option>
	<option value="165" <?php if ($valCountry == "165") { echo 'selected="selected"';} ?> >Nepal</option>
	<option value="166" <?php if ($valCountry == "166") { echo 'selected="selected"';} ?> >Netherlands</option>
	<option value="167" <?php if ($valCountry == "167") { echo 'selected="selected"';} ?> >Netherlands Antilles</option>
	<option value="168" <?php if ($valCountry == "168") { echo 'selected="selected"';} ?> >New Caledonia</option>
	<option value="169" <?php if ($valCountry == "169") { echo 'selected="selected"';} ?> >New Zealand</option>
	<option value="170" <?php if ($valCountry == "170") { echo 'selected="selected"';} ?> >Nicaragua</option>
	<option value="171" <?php if ($valCountry == "171") { echo 'selected="selected"';} ?> >Niger</option>
	<option value="172" <?php if ($valCountry == "172") { echo 'selected="selected"';} ?> >Nigeria</option>
	<option value="173" <?php if ($valCountry == "173") { echo 'selected="selected"';} ?> >Niue</option>
	<option value="174" <?php if ($valCountry == "174") { echo 'selected="selected"';} ?> >Norfolk Island</option>
	<option value="175" <?php if ($valCountry == "175") { echo 'selected="selected"';} ?> >Northern Mariana Islands</option>
	<option value="176" <?php if ($valCountry == "176") { echo 'selected="selected"';} ?> >Norway</option>
	<option value="177" <?php if ($valCountry == "177") { echo 'selected="selected"';} ?> >Oman</option>
	<option value="178" <?php if ($valCountry == "178") { echo 'selected="selected"';} ?> >Pakistan</option>
	<option value="179" <?php if ($valCountry == "179") { echo 'selected="selected"';} ?> >Palau</option>
	<option value="180" <?php if ($valCountry == "180") { echo 'selected="selected"';} ?> >Panama</option>
	<option value="181" <?php if ($valCountry == "181") { echo 'selected="selected"';} ?> >Papua New Guinea</option>
	<option value="182" <?php if ($valCountry == "182") { echo 'selected="selected"';} ?> >Paracel Islands</option>
	<option value="183" <?php if ($valCountry == "183") { echo 'selected="selected"';} ?> >Paraguay</option>
	<option value="184" <?php if ($valCountry == "184") { echo 'selected="selected"';} ?> >Peru</option>
	<option value="185" <?php if ($valCountry == "185") { echo 'selected="selected"';} ?> >Philippines</option>
	<option value="186" <?php if ($valCountry == "186") { echo 'selected="selected"';} ?> >Pitcairn Islands</option>
	<option value="187" <?php if ($valCountry == "187") { echo 'selected="selected"';} ?> >Poland</option>
	<option value="188" <?php if ($valCountry == "188") { echo 'selected="selected"';} ?> >Portugal</option>
	<option value="189" <?php if ($valCountry == "189") { echo 'selected="selected"';} ?> >Puerto Rico</option>
	<option value="190" <?php if ($valCountry == "190") { echo 'selected="selected"';} ?> >Qatar</option>
	<option value="191" <?php if ($valCountry == "191") { echo 'selected="selected"';} ?> >Reunion</option>
	<option value="192" <?php if ($valCountry == "192") { echo 'selected="selected"';} ?> >Romania</option>
	<option value="193" <?php if ($valCountry == "193") { echo 'selected="selected"';} ?> >Russia</option>
	<option value="194" <?php if ($valCountry == "194") { echo 'selected="selected"';} ?> >Rwanda</option>
	<option value="195" <?php if ($valCountry == "195") { echo 'selected="selected"';} ?> >Saint Helena</option>
	<option value="196" <?php if ($valCountry == "196") { echo 'selected="selected"';} ?> >Saint Kitts and Nevis</option>
	<option value="197" <?php if ($valCountry == "197") { echo 'selected="selected"';} ?> >Saint Lucia</option>
	<option value="198" <?php if ($valCountry == "198") { echo 'selected="selected"';} ?> >Saint Pierre and Miquelon</option>
	<option value="199" <?php if ($valCountry == "199") { echo 'selected="selected"';} ?> >Saint Vincent and the Grenadines</option>
	<option value="200" <?php if ($valCountry == "200") { echo 'selected="selected"';} ?> >Samoa</option>
	<option value="201" <?php if ($valCountry == "201") { echo 'selected="selected"';} ?> >San Marino</option>
	<option value="202" <?php if ($valCountry == "202") { echo 'selected="selected"';} ?> >Sao Tome and Principe</option>
	<option value="203" <?php if ($valCountry == "203") { echo 'selected="selected"';} ?> >Saudi Arabia</option>
	<option value="204" <?php if ($valCountry == "204") { echo 'selected="selected"';} ?> >Senegal</option>
	<option value="205" <?php if ($valCountry == "205") { echo 'selected="selected"';} ?> >Serbia and Montenegro</option>
	<option value="206" <?php if ($valCountry == "206") { echo 'selected="selected"';} ?> >Seychelles</option>
	<option value="207" <?php if ($valCountry == "207") { echo 'selected="selected"';} ?> >Sierra Leone</option>
	<option value="208" <?php if ($valCountry == "208") { echo 'selected="selected"';} ?> >Singapore</option>
	<option value="209" <?php if ($valCountry == "209") { echo 'selected="selected"';} ?> >Slovakia</option>
	<option value="210" <?php if ($valCountry == "210") { echo 'selected="selected"';} ?> >Slovenia</option>
	<option value="211" <?php if ($valCountry == "211") { echo 'selected="selected"';} ?> >Solomon Islands</option>
	<option value="212" <?php if ($valCountry == "212") { echo 'selected="selected"';} ?> >Somalia</option>
	<option value="213" <?php if ($valCountry == "213") { echo 'selected="selected"';} ?> >South Africa</option>
	<option value="214" <?php if ($valCountry == "214") { echo 'selected="selected"';} ?> >South Georgia and the South Sandwich Islands</option>
	<option value="215" <?php if ($valCountry == "215") { echo 'selected="selected"';} ?> >Spain</option>
	<option value="216" <?php if ($valCountry == "216") { echo 'selected="selected"';} ?> >Spratly Islands</option>
	<option value="217" <?php if ($valCountry == "217") { echo 'selected="selected"';} ?> >Sri Lanka</option>
	<option value="218" <?php if ($valCountry == "218") { echo 'selected="selected"';} ?> >Sudan</option>
	<option value="219" <?php if ($valCountry == "219") { echo 'selected="selected"';} ?> >Suriname</option>
	<option value="220" <?php if ($valCountry == "220") { echo 'selected="selected"';} ?> >Svalbard</option>
	<option value="221" <?php if ($valCountry == "221") { echo 'selected="selected"';} ?> >Swaziland</option>
	<option value="222" <?php if ($valCountry == "222") { echo 'selected="selected"';} ?> >Sweden</option>
	<option value="223" <?php if ($valCountry == "223") { echo 'selected="selected"';} ?> >Switzerland</option>
	<option value="224" <?php if ($valCountry == "224") { echo 'selected="selected"';} ?> >Syria</option>
	<option value="225" <?php if ($valCountry == "225") { echo 'selected="selected"';} ?> >Taiwan</option>
	<option value="226" <?php if ($valCountry == "226") { echo 'selected="selected"';} ?> >Tajikistan</option>
	<option value="227" <?php if ($valCountry == "227") { echo 'selected="selected"';} ?> >Tanzania</option>
	<option value="228" <?php if ($valCountry == "228") { echo 'selected="selected"';} ?> >Thailand</option>
	<option value="229" <?php if ($valCountry == "229") { echo 'selected="selected"';} ?> >Timor-Leste</option>
	<option value="230" <?php if ($valCountry == "230") { echo 'selected="selected"';} ?> >Togo</option>
	<option value="231" <?php if ($valCountry == "231") { echo 'selected="selected"';} ?> >Tokelau</option>
	<option value="232" <?php if ($valCountry == "232") { echo 'selected="selected"';} ?> >Tonga</option>
	<option value="233" <?php if ($valCountry == "233") { echo 'selected="selected"';} ?> >Trinidad and Tobago</option>
	<option value="234" <?php if ($valCountry == "234") { echo 'selected="selected"';} ?> >Tromelin Island</option>
	<option value="235" <?php if ($valCountry == "235") { echo 'selected="selected"';} ?> >Tunisia</option>
	<option value="236" <?php if ($valCountry == "236") { echo 'selected="selected"';} ?> >Turkey</option>
	<option value="237" <?php if ($valCountry == "237") { echo 'selected="selected"';} ?> >Turkmenistan</option>
	<option value="238" <?php if ($valCountry == "238") { echo 'selected="selected"';} ?> >Turks and Caicos Islands</option>
	<option value="239" <?php if ($valCountry == "238") { echo 'selected="selected"';} ?> >Tuvalu</option>
	<option value="240" <?php if ($valCountry == "240") { echo 'selected="selected"';} ?> >Uganda</option>
	<option value="241" <?php if ($valCountry == "241") { echo 'selected="selected"';} ?> >Ukraine</option>
	<option value="242" <?php if ($valCountry == "242") { echo 'selected="selected"';} ?> >United Arab Emirates</option>
	<option value="243" <?php if ($valCountry == "243") { echo 'selected="selected"';} ?> >United Kingdom</option>
	<option value="245" <?php if ($valCountry == "245") { echo 'selected="selected"';} ?> >Uruguay</option>
	<option value="246" <?php if ($valCountry == "246") { echo 'selected="selected"';} ?> >Uzbekistan</option>
	<option value="247" <?php if ($valCountry == "247") { echo 'selected="selected"';} ?> >Vanuatu</option>
	<option value="248" <?php if ($valCountry == "248") { echo 'selected="selected"';} ?> >Venezuela</option>
	<option value="249" <?php if ($valCountry == "249") { echo 'selected="selected"';} ?> >Vietnam</option>
	<option value="250" <?php if ($valCountry == "250") { echo 'selected="selected"';} ?> >Virgin Islands</option>
	<option value="251" <?php if ($valCountry == "251") { echo 'selected="selected"';} ?> >Wake Island</option>
	<option value="252" <?php if ($valCountry == "252") { echo 'selected="selected"';} ?> >Wallis and Futuna</option>
	<option value="253" <?php if ($valCountry == "253") { echo 'selected="selected"';} ?> >West Bank</option>
	<option value="254" <?php if ($valCountry == "254") { echo 'selected="selected"';} ?> >Western Sahara</option>
	<option value="255" <?php if ($valCountry == "255") { echo 'selected="selected"';} ?> >Yemen</option>
	<option value="256" <?php if ($valCountry == "256") { echo 'selected="selected"';} ?> >Zambia</option>
	<option value="257" <?php if ($valCountry == "257") { echo 'selected="selected"';} ?> >Zimbabwe</option>
    </select>    </td>
  </tr>
  <tr>
    <td>Address 1</td>
    <td><input name="txtAddress_1" type="text" class="textbox" id="txtAddress_1" value="<?php echo $valAddress_1; ?>" /></td>
  </tr>
  <tr>
    <td>Address 2 </td>
    <td><input name="txtAddress_2" type="text" class="textbox" id="txtAddress_2" value="<?php echo $valAddress_2; ?>" /></td>
  </tr>
  <tr>
    <td>Address 3</td>
    <td><input name="txtAddress_3" type="text" class="textbox" id="txtAddress_3" value="<?php echo $valAddress_3; ?>" /></td>
  </tr>
  <tr>
    <td>City</td>
    <td><input name="txtCity" type="text" class="textbox" id="txtCity" value="<?php echo $valCity; ?>" /></td>
  </tr>
  <tr>
    <td>State</td>
    <td><select name="ddlState" id="ddlState">
        <option value="AE" <?php if ($valCity == "AE") { echo 'selected="selected"';} ?> >AE</option>
        <option value="AK" <?php if ($valCity == "AK") { echo 'selected="selected"';} ?> >AK</option>
        <option value="AL" <?php if ($valCity == "AL") { echo 'selected="selected"';} ?> >AL</option>
        <option value="AP" <?php if ($valCity == "AP") { echo 'selected="selected"';} ?> >AP</option>
        <option value="AR" <?php if ($valCity == "AR") { echo 'selected="selected"';} ?> >AR</option>
        <option value="AZ" <?php if ($valCity == "AZ") { echo 'selected="selected"';} ?> >AZ</option>
        <option value="CA" <?php if ($valCity == "CA") { echo 'selected="selected"';} ?> >CA</option>
        <option value="CO" <?php if ($valCity == "CO") { echo 'selected="selected"';} ?> >CO</option>
        <option value="CT" <?php if ($valCity == "CT") { echo 'selected="selected"';} ?> >CT</option>
        <option value="DC" <?php if ($valCity == "DC") { echo 'selected="selected"';} ?> >DC</option>
        <option value="DE" <?php if ($valCity == "DE") { echo 'selected="selected"';} ?> >DE</option>
        <option value="FL" <?php if ($valCity == "FL") { echo 'selected="selected"';} ?> >FL</option>
        <option value="GA" <?php if ($valCity == "GA") { echo 'selected="selected"';} ?> >GA</option>
        <option value="HI" <?php if ($valCity == "HI") { echo 'selected="selected"';} ?> >HI</option>
        <option value="IA" <?php if ($valCity == "IA") { echo 'selected="selected"';} ?> >IA</option>
        <option value="ID" <?php if ($valCity == "ID") { echo 'selected="selected"';} ?> >ID</option>
        <option value="IL" <?php if ($valCity == "IL") { echo 'selected="selected"';} ?> >IL</option>
        <option value="IN" <?php if ($valCity == "IN") { echo 'selected="selected"';} ?> >IN</option>
        <option value="KS" <?php if ($valCity == "KS") { echo 'selected="selected"';} ?> >KS</option>
        <option value="KY" <?php if ($valCity == "KY") { echo 'selected="selected"';} ?> >KY</option>
        <option value="LA" <?php if ($valCity == "LA") { echo 'selected="selected"';} ?> >LA</option>
        <option value="MA" <?php if ($valCity == "MA") { echo 'selected="selected"';} ?> >MA</option>
        <option value="MD" <?php if ($valCity == "MD") { echo 'selected="selected"';} ?> >MD</option>
        <option value="ME" <?php if ($valCity == "ME") { echo 'selected="selected"';} ?> >ME</option>
        <option value="MI" <?php if ($valCity == "MI") { echo 'selected="selected"';} ?> >MI</option>
        <option value="MN" <?php if ($valCity == "MN") { echo 'selected="selected"';} ?> >MN</option>
        <option value="MO" <?php if ($valCity == "MO") { echo 'selected="selected"';} ?> >MO</option>
        <option value="MS" <?php if ($valCity == "MS") { echo 'selected="selected"';} ?> >MS</option>
        <option value="MT" <?php if ($valCity == "MT") { echo 'selected="selected"';} ?> >MT</option>
        <option value="NC" <?php if ($valCity == "NC") { echo 'selected="selected"';} ?> >NC</option>
        <option value="ND" <?php if ($valCity == "ND") { echo 'selected="selected"';} ?> >ND</option>
        <option value="NE" <?php if ($valCity == "NE") { echo 'selected="selected"';} ?> >NE</option>
        <option value="NH" <?php if ($valCity == "NH") { echo 'selected="selected"';} ?> >NH</option>
        <option value="NJ" <?php if ($valCity == "NJ") { echo 'selected="selected"';} ?> >NJ</option>
        <option value="NM" <?php if ($valCity == "NM") { echo 'selected="selected"';} ?> >NM</option>
        <option value="NV" <?php if ($valCity == "NV") { echo 'selected="selected"';} ?> >NV</option>
        <option value="NY" <?php if ($valCity == "NY") { echo 'selected="selected"';} ?> >NY</option>
        <option value="OH" <?php if ($valCity == "OH") { echo 'selected="selected"';} ?> >OH</option>
        <option value="OK" <?php if ($valCity == "OK") { echo 'selected="selected"';} ?> >OK</option>
        <option value="OR" <?php if ($valCity == "OR") { echo 'selected="selected"';} ?> >OR</option>
        <option value="PA" <?php if ($valCity == "PA") { echo 'selected="selected"';} ?> >PA</option>
        <option value="RI" <?php if ($valCity == "RI") { echo 'selected="selected"';} ?> >RI</option>
        <option value="SC" <?php if ($valCity == "SC") { echo 'selected="selected"';} ?> >SC</option>
        <option value="SD" <?php if ($valCity == "SD") { echo 'selected="selected"';} ?> >SD</option>
        <option value="TN" <?php if ($valCity == "TN") { echo 'selected="selected"';} ?> >TN</option>
        <option value="TX" <?php if ($valCity == "TX") { echo 'selected="selected"';} ?> >TX</option>
        <option value="UT" <?php if ($valCity == "UT") { echo 'selected="selected"';} ?> >UT</option>
        <option value="VA" <?php if ($valCity == "VA") { echo 'selected="selected"';} ?> >VA</option>
        <option value="VT" <?php if ($valCity == "VT") { echo 'selected="selected"';} ?> >VT</option>
        <option value="WA" <?php if ($valCity == "WA") { echo 'selected="selected"';} ?> >WA</option>
        <option value="WI" <?php if ($valCity == "WI") { echo 'selected="selected"';} ?> >WI</option>
        <option value="WV" <?php if ($valCity == "WV") { echo 'selected="selected"';} ?> >WV</option>
        <option value="WY" <?php if ($valCity == "WY") { echo 'selected="selected"';} ?> >WY</option>
    </select></td>
  </tr>
  <tr>
    <td>Zip code</td>
    <td><input name="txtZipCode" type="text" class="textbox" id="txtZipCode" value="<?php echo $valZipCode; ?>" /></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><input name="submit" type="submit" class="submit_button" id="submit" value="Update" /></td>
  </tr>
</table>
</form>
<script type="text/javascript">
<!--
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1");
var sprytextfield2 = new Spry.Widget.ValidationTextField("sprytextfield2", "integer");
//-->
</script>
</body>
</html>
