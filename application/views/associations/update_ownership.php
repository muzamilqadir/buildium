<?php
include ("../includes/classes/class.db.php");
include ("../includes/functions/general.php");

//-------------------------------------------------------------------------------------------
								/*Objects*/
//-------------------------------------------------------------------------------------------					
	$db = new db();
	//isLoggedUser();
//-------------------------------------------------------------------------------------------
								/*General Coding Area*/
	$oid = "0";
	if (isset($_GET['id']) && $_GET['id'] != "" ) {
		$oid = $_GET['id'];
	}							
	//---------------------------------------------------------------------------------------
	
	$isUpdated = false;
	
	if ($_POST){
		$sqlUpdate = "UPDATE assoc_ownership SET		
		association_name = '".clearFormData("txtAssociationName")."',
		number_of_units = '".clearFormData("ddlNumberOfUnits")."',
		move_in_date = '".clearFormData("txtMoveinDate")."',
		association_fee = '".clearFormData("txtAssociationFee")."',
		frequency = '".clearFormData("ddlFrequency")."',
		owner_info = '".clearFormData("rdOwnerInfo")."',
		first_name = '".clearFormData("txtFirstName")."',
		last_name = '".clearFormData("txtLastName")."',
		login_email = '".clearFormData("txtLoginEmail")."',
		alternate_email = '".clearFormData("txtAlternateEmail")."',
		home = '".clearFormData("txtHome")."',
		work = '".clearFormData("txtWork")."',
		mobile = '".clearFormData("txtMobile")."',
		fax = '".clearFormData("txtFax")."',
		country = '".clearFormData("ddlCountries")."',
		address_1 = '".clearFormData("txtAddress_1")."',
		address_2 = '".clearFormData("txtAddress_2")."',
		address_3 = '".clearFormData("txtAddress_3")."',
		city = '".clearFormData("txtCity")."',
		state = '".clearFormData("ddlState")."',
		postal_code = '".clearFormData("txtPostalCode")."',
		alt_country = '".clearFormData("ddlAltCountries")."',
		alt_address_1 = '".clearFormData("txtAltAddress_1")."',
		alt_address_2 = '".clearFormData("txtAltAddress_2")."',
		alt_address_3 = '".clearFormData("txtAltAddress_3")."',
		alt_city = '".clearFormData("txtAltCity")."',
		alt_state = '".clearFormData("ddlAltState")."',
		alt_postal_code = '".clearFormData("txtAltZipcode")."',
		mailing_pref = '".clearFormData("rdMailingRef")."',
		additional_comments = '".clearFormData("txtComments")."',
		additional_birth_date = '".clearFormData("txtBirthDate")."',
		emergency_name = '".clearFormData("txtEmergencyName")."',
		emergency_phone = '".clearFormData("txtEmergencyPhone")."',
		is_board_member = '".clearFormData("chkBoardMember")."',
		is_occupancy = '".clearFormData("chkOccupancy")."'
		WHERE 
		owner_id = '".$oid."'";

		$db->query($sqlUpdate);
		
		$isUpdated = true;
	}
	
	$sqlSelect = "SELECT * FROM assoc_ownership WHERE owner_id = '".$oid."'";
	
	$query= $db->ExeQuersys($sqlSelect);
		
	while ($row = mysql_fetch_array($query)) {
		$association_name = $row['association_name'];
		$number_of_units = $row['number_of_units'];
		$move_in_date = $row['move_in_date'];
		$association_fee = $row['association_fee'];
		$frequency = $row['frequency'];
		$owner_info = $row['owner_info'];
		$first_name = $row['first_name'];
		$last_name = $row['last_name'];
		$login_email = $row['login_email'];
		$alternate_email = $row['alternate_email'];
		$home = $row['home'];
		$work = $row['work'];
		$mobile = $row['mobile'];
		$fax = $row['fax'];
		$country = $row['country'];
		$address_1 = $row['address_1'];
		$address_2 = $row['address_2'];
		$address_3 = $row['address_3'];
		$city = $row['city'];
		$state = $row['state'];
		$postal_code = $row['postal_code'];
		$alt_country = $row['alt_country'];
		$alt_address_1 = $row['alt_address_1'];
		$alt_address_2 = $row['alt_address_2'];
		$alt_address_3 = $row['alt_address_3'];
		$alt_city = $row['alt_city'];
		$alt_state = $row['alt_state'];
		$alt_postal_code = $row['alt_postal_code'];
		$mailing_pref = $row['mailing_pref'];
		$additional_comments = $row['additional_comments'];
		$additional_birth_date = $row['additional_birth_date'];
		$emergency_name = $row['emergency_name'];
		$emergency_phone = $row['emergency_phone'];
		$is_board_member = $row['is_board_member'];
		$is_occupancy = $row['is_occupancy'];
	}
	
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Association Owner</title>
<link href="../styles_popup.css" rel="stylesheet" type="text/css" />
<script src="../calendarDateInput.js" type="text/javascript"></script>
</head>

<body>
<form action="" method="post">
<div id="wrapper_header"><a href="#" onclick="return confirm('Do you want to cancel?');"><img src="../images/btn_cancel.png" alt="Cancel" width="109" height="34" border="0" style="float:right" /></a>
  <input type="image" name="submit2" id="submit2" src="../images/btn_save.png" />
</div>
<p>&nbsp;</p>
<?php if ($isUpdated) { echo '<div class="msg_success">Association Owner is updated successfully.</div>'; } ?>
<table width="575" border="0" align="center" cellpadding="2" cellspacing="3">
  <tr>
    <td colspan="2"><h1>Move In Association Owner</h1></td>
    </tr>
  <tr>
    <td colspan="2"> <h2>Select Unit</h2></td>
    </tr>
  <tr>
    <td width="188">Association name</td>
    <td width="370"><input name="txtAssociationName" type="text" class="textbox" id="txtAssociationName" value="<?php echo $association_name; ?>" /></td>
  </tr>
  <tr>
    <td>Unit</td>
    <td><select name="ddlNumberOfUnits" id="ddlNumberOfUnits">
      <option value="1" <?php if($number_of_units == "1") { echo 'selected="selected"'; } ?> >1</option>
      <option value="2" <?php if($number_of_units == "2") { echo 'selected="selected"'; } ?> >2</option>
      <option value="3" <?php if($number_of_units == "3") { echo 'selected="selected"'; } ?> >3</option>
      <option value="4" <?php if($number_of_units == "4") { echo 'selected="selected"'; } ?> >4</option>
      <option value="5" <?php if($number_of_units == "5") { echo 'selected="selected"'; } ?> >5</option>
      <option value="6" <?php if($number_of_units == "6") { echo 'selected="selected"'; } ?> >6</option>
      <option value="7" <?php if($number_of_units == "7") { echo 'selected="selected"'; } ?> >7</option>
      <option value="8" <?php if($number_of_units == "8") { echo 'selected="selected"'; } ?> >8</option>
      <option value="9" <?php if($number_of_units == "9") { echo 'selected="selected"'; } ?> >9</option>
      <option value="10" <?php if($number_of_units == "10") { echo 'selected="selected"'; } ?> >10</option>
      <option value="11" <?php if($number_of_units == "11") { echo 'selected="selected"'; } ?> >11</option>
      <option value="12" <?php if($number_of_units == "12") { echo 'selected="selected"'; } ?> >12</option>
      <option value="13" <?php if($number_of_units == "13") { echo 'selected="selected"'; } ?> >13</option>
      <option value="14" <?php if($number_of_units == "14") { echo 'selected="selected"'; } ?> >14</option>
      <option value="15" <?php if($number_of_units == "15") { echo 'selected="selected"'; } ?> >15</option>
    </select>    </td>
  </tr>
  <tr>
    <td colspan="2"> <h2>Enter move-in information</h2></td>
    </tr>
  <tr>
    <td>Move-in date</td>
    <td><script>DateInput('txtMoveinDate', true, 'YYYY-MM-DD', '<?php echo $move_in_date; ?>')</script></td>
  </tr>
  <tr>
    <td colspan="2"><h2>Set up recurring association fee charge</h2></td>
    </tr>
  <tr>
    <td>Association fee</td>
    <td>$
      <input name="txtAssociationFee" type="text" class="textbox" id="txtAssociationFee" value="<?php echo $association_fee; ?>" /></td>
  </tr>
  <tr>
    <td>Frequency</td>
    <td><select name="ddlFrequency" id="ddlFrequency">
        <option value="Daily" <?php if($frequency == "Daily") { echo 'selected="selected"'; } ?> >Daily</option>
        <option value="Weekly" <?php if($frequency == "Weekly") { echo 'selected="selected"'; } ?> >Weekly</option>
        <option value="Every Two Weeks" <?php if($frequency == "Every Two Weeks") { echo 'selected="selected"'; } ?> >Every Two Weeks</option>
        <option value="Monthly" <?php if($frequency == "Monthly") { echo 'selected="selected"'; } ?> >Monthly</option>
        <option value="Every Two Months" <?php if($frequency == "Every Two Months") { echo 'selected="selected"'; } ?> >Every Two Months</option>
        <option value="Quarterly" <?php if($frequency == "Quarterly") { echo 'selected="selected"'; } ?> >Quarterly</option>
        <option value="Every Six Months" <?php if($number_of_units == "Every Six Months") { echo 'selected="selected"'; } ?> >Every Six Months</option>
        <option value="Yearly" <?php if($frequency == "Yearly") { echo 'selected="selected"'; } ?> >Yearly</option>
        <option value="One Time" <?php if($frequency == "One Time") { echo 'selected="selected"'; } ?> >One Time</option>
      </select></td>
  </tr>
  <tr>
    <td colspan="2"><h2>Enter association owner information</h2></td>
    </tr>
  <tr>
    <td colspan="2"><p>
      <input name="rdOwnerInfo" type="radio" id="radio" value="Add a new association owner" <?php if($owner_info == "Add a new association owner") { echo 'checked="checked"'; } ?> />
      Add a new association owner<br />
      <input type="radio" name="rdOwnerInfo" id="radio2" value="Select a current association owner" <?php if($owner_info == "Select a current association owner") { echo 'checked="checked"'; } ?> />
      Select a current association owner<br />
      <input type="radio" name="rdOwnerInfo" id="radio3" value="Select a former association owner" <?php if($owner_info == "Select a former association owner") { echo 'checked="checked"'; } ?> />
      Select a former association owner</p>      </td>
    </tr>
  <tr>
    <td colspan="2"><h2>Contact Information</h2></td>
    </tr>
  <tr>
    <td>First name</td>
    <td><input name="txtFirstName" type="text" class="textbox" id="txtFirstName" value="<?php echo $first_name; ?>" /></td>
  </tr>
  <tr>
    <td>Last name</td>
    <td><input name="txtLastName" type="text" class="textbox" id="txtLastName" value="<?php echo $last_name; ?>" /></td>
  </tr>
  <tr>
    <td>Login email </td>
    <td><input name="txtLoginEmail" type="text" class="textbox" id="txtLoginEmail" value="<?php echo $login_email; ?>" /></td>
  </tr>
  <tr>
    <td>Alternate email </td>
    <td><input name="txtAlternateEmail" type="text" class="textbox" id="txtAlternateEmail" value="<?php echo $alternate_email; ?>" /></td>
  </tr>
  <tr>
    <td>Home</td>
    <td><input name="txtHome" type="text" class="textbox" id="txtHome" value="<?php echo $home; ?>" /></td>
  </tr>
  <tr>
    <td>Work</td>
    <td><input name="txtWork" type="text" class="textbox" id="txtWork" value="<?php echo $work; ?>" /></td>
  </tr>
  <tr>
    <td>Mobile</td>
    <td><input name="txtMobile" type="text" class="textbox" id="txtMobile" value="<?php echo $mobile; ?>" /></td>
  </tr>
  <tr>
    <td>Fax</td>
    <td><input name="txtFax" type="text" class="textbox" id="txtFax" value="<?php echo $fax; ?>" /></td>
  </tr>
  <tr>
    <td colspan="2"> <h2>Main Address</h2></td>
    </tr>
  <tr>
    <td>Country</td>
    <td><select name="ddlCountries" id="ddlCountries">
      	<option value="1" <?php if ($country == "1") { echo 'selected="selected"';} ?> >Afghanistan</option>
        <option value="2" <?php if ($country == "2") { echo 'selected="selected"';} ?> >Akrotiri</option>
        <option value="3" <?php if ($country == "3") { echo 'selected="selected"';} ?> >Albania</option>
        <option value="4" <?php if ($country == "4") { echo 'selected="selected"';} ?> >Algeria</option>
        <option value="5" <?php if ($country == "5") { echo 'selected="selected"';} ?> >American Samoa</option>
        <option value="6" <?php if ($country == "6") { echo 'selected="selected"';} ?> >Andorra</option>
        <option value="7" <?php if ($country == "7") { echo 'selected="selected"';} ?> >Angola</option>
        <option value="8" <?php if ($country == "8") { echo 'selected="selected"';} ?> >Anguilla</option>
        <option value="9" <?php if ($country == "9") { echo 'selected="selected"';} ?> >Antarctica</option>
        <option value="10" <?php if ($country == "10") { echo 'selected="selected"';} ?> >Antigua and Barbuda</option>
        <option value="11" <?php if ($country == "11") { echo 'selected="selected"';} ?> >Argentina</option>
        <option value="12" <?php if ($country == "12") { echo 'selected="selected"';} ?> >Armenia</option>
        <option value="13" <?php if ($country == "13") { echo 'selected="selected"';} ?> >Aruba</option>
        <option value="14" <?php if ($country == "14") { echo 'selected="selected"';} ?> >Ashmore and Cartier Islands</option>
        <option value="15" <?php if ($country == "15") { echo 'selected="selected"';} ?> >Australia</option>
        <option value="16" <?php if ($country == "16") { echo 'selected="selected"';} ?> >Austria</option>
        <option value="17" <?php if ($country == "17") { echo 'selected="selected"';} ?> >Azerbaijan</option>
        <option value="18" <?php if ($country == "18") { echo 'selected="selected"';} ?> >Bahamas, The</option>
        <option value="19" <?php if ($country == "19") { echo 'selected="selected"';} ?> >Bahrain</option>
        <option value="20" <?php if ($country == "20") { echo 'selected="selected"';} ?> >Bangladesh</option>
        <option value="21" <?php if ($country == "21") { echo 'selected="selected"';} ?> >Barbados</option>
        <option value="22" <?php if ($country == "22") { echo 'selected="selected"';} ?> >Bassas da India</option>
        <option value="23" <?php if ($country == "23") { echo 'selected="selected"';} ?> >Belarus</option>
        <option value="24" <?php if ($country == "24") { echo 'selected="selected"';} ?> >Belgium</option>
        <option value="25" <?php if ($country == "25") { echo 'selected="selected"';} ?> >Belize</option>
        <option value="26" <?php if ($country == "26") { echo 'selected="selected"';} ?> >Benin</option>
        <option value="27" <?php if ($country == "27") { echo 'selected="selected"';} ?> >Bermuda</option>
        <option value="28" <?php if ($country == "28") { echo 'selected="selected"';} ?> >Bhutan</option>
        <option value="29" <?php if ($country == "29") { echo 'selected="selected"';} ?> >Bolivia</option>
        <option value="30" <?php if ($country == "30") { echo 'selected="selected"';} ?> >Bosnia and Herzegovina</option>
        <option value="31" <?php if ($country == "31") { echo 'selected="selected"';} ?> >Botswana</option>
        <option value="32" <?php if ($country == "32") { echo 'selected="selected"';} ?> >Bouvet Island</option>
        <option value="33" <?php if ($country == "33") { echo 'selected="selected"';} ?> >Brazil</option>
        <option value="34" <?php if ($country == "34") { echo 'selected="selected"';} ?> >British Indian Ocean Territory</option>
        <option value="35" <?php if ($country == "35") { echo 'selected="selected"';} ?> >British Virgin Islands</option>
        <option value="36" <?php if ($country == "36") { echo 'selected="selected"';} ?> >Brunei</option>
        <option value="37" <?php if ($country == "37") { echo 'selected="selected"';} ?> >Bulgaria</option>
        <option value="38" <?php if ($country == "38") { echo 'selected="selected"';} ?> >Burkina Faso</option>
        <option value="39" <?php if ($country == "39") { echo 'selected="selected"';} ?> >Burma</option>
        <option value="40" <?php if ($country == "40") { echo 'selected="selected"';} ?> >Burundi</option>
        <option value="41" <?php if ($country == "41") { echo 'selected="selected"';} ?> >Cambodia</option>
        <option value="42" <?php if ($country == "42") { echo 'selected="selected"';} ?> >Cameroon</option>
        <option value="43" <?php if ($country == "43") { echo 'selected="selected"';} ?> >Canada</option>
        <option value="44" <?php if ($country == "44") { echo 'selected="selected"';} ?> >Cape Verde</option>
        <option value="45" <?php if ($country == "45") { echo 'selected="selected"';} ?> >Cayman Islands</option>
        <option value="46" <?php if ($country == "46") { echo 'selected="selected"';} ?> >Central African Republic</option>
        <option value="47" <?php if ($country == "47") { echo 'selected="selected"';} ?> >Chad</option>
        <option value="48" <?php if ($country == "48") { echo 'selected="selected"';} ?> >Chile</option>
        <option value="49" <?php if ($country == "49") { echo 'selected="selected"';} ?> >China</option>
        <option value="50" <?php if ($country == "50") { echo 'selected="selected"';} ?> >Christmas Island</option>
        <option value="51" <?php if ($country == "51") { echo 'selected="selected"';} ?> >Clipperton Island</option>
        <option value="52" <?php if ($country == "52") { echo 'selected="selected"';} ?> >Cocos (Keeling) Islands</option>
        <option value="53" <?php if ($country == "53") { echo 'selected="selected"';} ?> >Colombia</option>
        <option value="54" <?php if ($country == "54") { echo 'selected="selected"';} ?> >Comoros</option>
        <option value="55" <?php if ($country == "55") { echo 'selected="selected"';} ?> >Congo, Democratic Republic of the</option>
        <option value="56" <?php if ($country == "56") { echo 'selected="selected"';} ?> >Congo, Republic of the</option>
        <option value="57" <?php if ($country == "57") { echo 'selected="selected"';} ?> >Cook Islands</option>
        <option value="58" <?php if ($country == "58") { echo 'selected="selected"';} ?> >Coral Sea Islands</option>
        <option value="59" <?php if ($country == "59") { echo 'selected="selected"';} ?> >Costa Rica</option>
        <option value="60" <?php if ($country == "60") { echo 'selected="selected"';} ?> >Cote d&#39;Ivoire</option>
        <option value="61" <?php if ($country == "61") { echo 'selected="selected"';} ?> >Croatia</option>
        <option value="62" <?php if ($country == "62") { echo 'selected="selected"';} ?> >Cuba</option>
        <option value="63" <?php if ($country == "63") { echo 'selected="selected"';} ?> >Cyprus</option>
        <option value="64" <?php if ($country == "64") { echo 'selected="selected"';} ?> >Czech Republic</option>
        <option value="66" <?php if ($country == "66") { echo 'selected="selected"';} ?> >Dhekelia</option>
        <option value="67" <?php if ($country == "67") { echo 'selected="selected"';} ?> >Djibouti</option>
        <option value="68" <?php if ($country == "68") { echo 'selected="selected"';} ?> >Dominica</option>
        <option value="69" <?php if ($country == "69") { echo 'selected="selected"';} ?> >Dominican Republic</option>
        <option value="70" <?php if ($country == "70") { echo 'selected="selected"';} ?> >Ecuador</option>
        <option value="71" <?php if ($country == "71") { echo 'selected="selected"';} ?> >Egypt</option>
        <option value="72" <?php if ($country == "72") { echo 'selected="selected"';} ?> >El Salvador</option>
        <option value="73" <?php if ($country == "73") { echo 'selected="selected"';} ?> >Equatorial Guinea</option>
        <option value="74" <?php if ($country == "74") { echo 'selected="selected"';} ?> >Eritrea</option>
        <option value="75" <?php if ($country == "75") { echo 'selected="selected"';} ?> >Estonia</option>
        <option value="76" <?php if ($country == "76") { echo 'selected="selected"';} ?> >Ethiopia</option>
        <option value="77" <?php if ($country == "77") { echo 'selected="selected"';} ?> >Europa Island</option>
        <option value="78" <?php if ($country == "78") { echo 'selected="selected"';} ?> >Falkland Islands (Islas Malvinas)</option>
        <option value="79" <?php if ($country == "79") { echo 'selected="selected"';} ?> >Faroe Islands</option>
        <option value="80" <?php if ($country == "80") { echo 'selected="selected"';} ?> >Fiji</option>
        <option value="81" <?php if ($country == "81") { echo 'selected="selected"';} ?> >Finland</option>
        <option value="82" <?php if ($country == "82") { echo 'selected="selected"';} ?> >France</option>
        <option value="83" <?php if ($country == "83") { echo 'selected="selected"';} ?> >French Guiana</option>
        <option value="84" <?php if ($country == "84") { echo 'selected="selected"';} ?> >French Polynesia</option>
        <option value="85" <?php if ($country == "85") { echo 'selected="selected"';} ?> >French Southern and Antarctic Lands</option>
        <option value="86" <?php if ($country == "86") { echo 'selected="selected"';} ?> >Gabon</option>
        <option value="87" <?php if ($country == "87") { echo 'selected="selected"';} ?> >Gambia, The</option>
        <option value="88" <?php if ($country == "88") { echo 'selected="selected"';} ?> >Gaza Strip</option>
        <option value="89" <?php if ($country == "89") { echo 'selected="selected"';} ?> >Georgia</option>
        <option value="90" <?php if ($country == "90") { echo 'selected="selected"';} ?> >Germany</option>
        <option value="91" <?php if ($country == "91") { echo 'selected="selected"';} ?> >Ghana</option>
        <option value="92" <?php if ($country == "92") { echo 'selected="selected"';} ?> >Gibraltar</option>
        <option value="93" <?php if ($country == "93") { echo 'selected="selected"';} ?> >Glorioso Islands</option>
        <option value="94" <?php if ($country == "94") { echo 'selected="selected"';} ?> >Greece</option>
        <option value="95" <?php if ($country == "95") { echo 'selected="selected"';} ?> >Greenland</option>
        <option value="96" <?php if ($country == "96") { echo 'selected="selected"';} ?> >Grenada</option>
        <option value="97" <?php if ($country == "97") { echo 'selected="selected"';} ?> >Guadeloupe</option>
        <option value="98" <?php if ($country == "98") { echo 'selected="selected"';} ?> >Guam</option>
        <option value="99" <?php if ($country == "99") { echo 'selected="selected"';} ?> >Guatemala</option>
        <option value="100" <?php if ($country == "100") { echo 'selected="selected"';} ?> >Guernsey</option>
        <option value="101" <?php if ($country == "101") { echo 'selected="selected"';} ?> >Guinea</option>
        <option value="102" <?php if ($country == "102") { echo 'selected="selected"';} ?> >Guinea-Bissau</option>
        <option value="103" <?php if ($country == "103") { echo 'selected="selected"';} ?> >Guyana</option>
        <option value="104" <?php if ($country == "104") { echo 'selected="selected"';} ?> >Haiti</option>
        <option value="105" <?php if ($country == "105") { echo 'selected="selected"';} ?> >Heard Island and McDonald Islands</option>
        <option value="106" <?php if ($country == "106") { echo 'selected="selected"';} ?> >Holy See (Vatican City)</option>
        <option value="107" <?php if ($country == "107") { echo 'selected="selected"';} ?> >Honduras</option>
        <option value="108" <?php if ($country == "108") { echo 'selected="selected"';} ?> >Hong Kong</option>
        <option value="109" <?php if ($country == "109") { echo 'selected="selected"';} ?> >Hungary</option>
        <option value="110" <?php if ($country == "110") { echo 'selected="selected"';} ?> >Iceland</option>
        <option value="111" <?php if ($country == "111") { echo 'selected="selected"';} ?> >India</option>
        <option value="112" <?php if ($country == "112") { echo 'selected="selected"';} ?> >Indonesia</option>
        <option value="113" <?php if ($country == "113") { echo 'selected="selected"';} ?> >Iran</option>
        <option value="114" <?php if ($country == "114") { echo 'selected="selected"';} ?> >Iraq</option>
        <option value="115" <?php if ($country == "115") { echo 'selected="selected"';} ?> >Ireland</option>
        <option value="116" <?php if ($country == "116") { echo 'selected="selected"';} ?> >Isle of Man</option>
        <option value="117" <?php if ($country == "117") { echo 'selected="selected"';} ?> >Israel</option>
        <option value="118" <?php if ($country == "118") { echo 'selected="selected"';} ?> >Italy</option>
        <option value="119" <?php if ($country == "119") { echo 'selected="selected"';} ?> >Jamaica</option>
        <option value="120" <?php if ($country == "120") { echo 'selected="selected"';} ?> >Jan Mayen</option>
        <option value="121" <?php if ($country == "121") { echo 'selected="selected"';} ?> >Japan</option>
        <option value="122" <?php if ($country == "122") { echo 'selected="selected"';} ?> >Jersey</option>
        <option value="123" <?php if ($country == "123") { echo 'selected="selected"';} ?> >Jordan</option>
        <option value="124" <?php if ($country == "124") { echo 'selected="selected"';} ?> >Juan de Nova Island</option>
        <option value="125" <?php if ($country == "125") { echo 'selected="selected"';} ?> >Kazakhstan</option>
        <option value="126" <?php if ($country == "126") { echo 'selected="selected"';} ?> >Kenya</option>
        <option value="127" <?php if ($country == "127") { echo 'selected="selected"';} ?> >Kiribati</option>
        <option value="128" <?php if ($country == "128") { echo 'selected="selected"';} ?> >Korea, North</option>
        <option value="129" <?php if ($country == "129") { echo 'selected="selected"';} ?> >Korea, South</option>
        <option value="130" <?php if ($country == "130") { echo 'selected="selected"';} ?> >Kuwait</option>
        <option value="131" <?php if ($country == "131") { echo 'selected="selected"';} ?> >Kyrgyzstan</option>
        <option value="132" <?php if ($country == "132") { echo 'selected="selected"';} ?> >Laos</option>
        <option value="133" <?php if ($country == "133") { echo 'selected="selected"';} ?> >Latvia</option>
        <option value="134" <?php if ($country == "134") { echo 'selected="selected"';} ?> >Lebanon</option>
        <option value="135" <?php if ($country == "135") { echo 'selected="selected"';} ?> >Lesotho</option>
        <option value="136" <?php if ($country == "136") { echo 'selected="selected"';} ?> >Liberia</option>
        <option value="137" <?php if ($country == "137") { echo 'selected="selected"';} ?> >Libya</option>
        <option value="138" <?php if ($country == "138") { echo 'selected="selected"';} ?> >Liechtenstein</option>
        <option value="139" <?php if ($country == "139") { echo 'selected="selected"';} ?> >Lithuania</option>
        <option value="140" <?php if ($country == "140") { echo 'selected="selected"';} ?> >Luxembourg</option>
        <option value="141" <?php if ($country == "141") { echo 'selected="selected"';} ?> >Macau</option>
        <option value="142" <?php if ($country == "142") { echo 'selected="selected"';} ?> >Macedonia</option>
        <option value="143" <?php if ($country == "143") { echo 'selected="selected"';} ?> >Madagascar</option>
        <option value="144" <?php if ($country == "144") { echo 'selected="selected"';} ?> >Malawi</option>
        <option value="145" <?php if ($country == "145") { echo 'selected="selected"';} ?> >Malaysia</option>
        <option value="146" <?php if ($country == "146") { echo 'selected="selected"';} ?> >Maldives</option>
        <option value="147" <?php if ($country == "147") { echo 'selected="selected"';} ?> >Mali</option>
        <option value="148" <?php if ($country == "148") { echo 'selected="selected"';} ?> >Malta</option>
        <option value="149" <?php if ($country == "149") { echo 'selected="selected"';} ?> >Marshall Islands</option>
        <option value="150" <?php if ($country == "150") { echo 'selected="selected"';} ?> >Martinique</option>
        <option value="151" <?php if ($country == "151") { echo 'selected="selected"';} ?> >Mauritania</option>
        <option value="152" <?php if ($country == "152") { echo 'selected="selected"';} ?> >Mauritius</option>
        <option value="153" <?php if ($country == "153") { echo 'selected="selected"';} ?> >Mayotte</option>
        <option value="154" <?php if ($country == "154") { echo 'selected="selected"';} ?> >Mexico</option>
        <option value="155" <?php if ($country == "155") { echo 'selected="selected"';} ?> >Micronesia, Federated States of</option>
        <option value="156" <?php if ($country == "156") { echo 'selected="selected"';} ?> >Moldova</option>
        <option value="157" <?php if ($country == "157") { echo 'selected="selected"';} ?> >Monaco</option>
        <option value="158" <?php if ($country == "158") { echo 'selected="selected"';} ?> >Mongolia</option>
        <option value="159" <?php if ($country == "159") { echo 'selected="selected"';} ?> >Montserrat</option>
        <option value="160" <?php if ($country == "160") { echo 'selected="selected"';} ?> >Morocco</option>
        <option value="161" <?php if ($country == "161") { echo 'selected="selected"';} ?> >Mozambique</option>
        <option value="162" <?php if ($country == "162") { echo 'selected="selected"';} ?> >Namibia</option>
        <option value="163" <?php if ($country == "163") { echo 'selected="selected"';} ?> >Nauru</option>
        <option value="164" <?php if ($country == "164") { echo 'selected="selected"';} ?> >Navassa Island</option>
        <option value="165" <?php if ($country == "165") { echo 'selected="selected"';} ?> >Nepal</option>
        <option value="166" <?php if ($country == "166") { echo 'selected="selected"';} ?> >Netherlands</option>
        <option value="167" <?php if ($country == "167") { echo 'selected="selected"';} ?> >Netherlands Antilles</option>
        <option value="168" <?php if ($country == "168") { echo 'selected="selected"';} ?> >New Caledonia</option>
        <option value="169" <?php if ($country == "169") { echo 'selected="selected"';} ?> >New Zealand</option>
        <option value="170" <?php if ($country == "170") { echo 'selected="selected"';} ?> >Nicaragua</option>
        <option value="171" <?php if ($country == "171") { echo 'selected="selected"';} ?> >Niger</option>
        <option value="172" <?php if ($country == "172") { echo 'selected="selected"';} ?> >Nigeria</option>
        <option value="173" <?php if ($country == "173") { echo 'selected="selected"';} ?> >Niue</option>
        <option value="174" <?php if ($country == "174") { echo 'selected="selected"';} ?> >Norfolk Island</option>
        <option value="175" <?php if ($country == "175") { echo 'selected="selected"';} ?> >Northern Mariana Islands</option>
        <option value="176" <?php if ($country == "176") { echo 'selected="selected"';} ?> >Norway</option>
        <option value="177" <?php if ($country == "177") { echo 'selected="selected"';} ?> >Oman</option>
        <option value="178" <?php if ($country == "178") { echo 'selected="selected"';} ?> >Pakistan</option>
        <option value="179" <?php if ($country == "179") { echo 'selected="selected"';} ?> >Palau</option>
        <option value="180" <?php if ($country == "180") { echo 'selected="selected"';} ?> >Panama</option>
        <option value="181" <?php if ($country == "181") { echo 'selected="selected"';} ?> >Papua New Guinea</option>
        <option value="182" <?php if ($country == "182") { echo 'selected="selected"';} ?> >Paracel Islands</option>
        <option value="183" <?php if ($country == "183") { echo 'selected="selected"';} ?> >Paraguay</option>
        <option value="184" <?php if ($country == "184") { echo 'selected="selected"';} ?> >Peru</option>
        <option value="185" <?php if ($country == "185") { echo 'selected="selected"';} ?> >Philippines</option>
        <option value="186" <?php if ($country == "186") { echo 'selected="selected"';} ?> >Pitcairn Islands</option>
        <option value="187" <?php if ($country == "187") { echo 'selected="selected"';} ?> >Poland</option>
        <option value="188" <?php if ($country == "188") { echo 'selected="selected"';} ?> >Portugal</option>
        <option value="189" <?php if ($country == "189") { echo 'selected="selected"';} ?> >Puerto Rico</option>
        <option value="190" <?php if ($country == "190") { echo 'selected="selected"';} ?> >Qatar</option>
        <option value="191" <?php if ($country == "191") { echo 'selected="selected"';} ?> >Reunion</option>
        <option value="192" <?php if ($country == "192") { echo 'selected="selected"';} ?> >Romania</option>
        <option value="193" <?php if ($country == "193") { echo 'selected="selected"';} ?> >Russia</option>
        <option value="194" <?php if ($country == "194") { echo 'selected="selected"';} ?> >Rwanda</option>
        <option value="195" <?php if ($country == "195") { echo 'selected="selected"';} ?> >Saint Helena</option>
        <option value="196" <?php if ($country == "196") { echo 'selected="selected"';} ?> >Saint Kitts and Nevis</option>
        <option value="197" <?php if ($country == "197") { echo 'selected="selected"';} ?> >Saint Lucia</option>
        <option value="198" <?php if ($country == "198") { echo 'selected="selected"';} ?> >Saint Pierre and Miquelon</option>
        <option value="199" <?php if ($country == "199") { echo 'selected="selected"';} ?> >Saint Vincent and the Grenadines</option>
        <option value="200" <?php if ($country == "200") { echo 'selected="selected"';} ?> >Samoa</option>
        <option value="201" <?php if ($country == "201") { echo 'selected="selected"';} ?> >San Marino</option>
        <option value="202" <?php if ($country == "202") { echo 'selected="selected"';} ?> >Sao Tome and Principe</option>
        <option value="203" <?php if ($country == "203") { echo 'selected="selected"';} ?> >Saudi Arabia</option>
        <option value="204" <?php if ($country == "204") { echo 'selected="selected"';} ?> >Senegal</option>
        <option value="205" <?php if ($country == "205") { echo 'selected="selected"';} ?> >Serbia and Montenegro</option>
        <option value="206" <?php if ($country == "206") { echo 'selected="selected"';} ?> >Seychelles</option>
        <option value="207" <?php if ($country == "207") { echo 'selected="selected"';} ?> >Sierra Leone</option>
        <option value="208" <?php if ($country == "208") { echo 'selected="selected"';} ?> >Singapore</option>
        <option value="209" <?php if ($country == "209") { echo 'selected="selected"';} ?> >Slovakia</option>
        <option value="210" <?php if ($country == "210") { echo 'selected="selected"';} ?> >Slovenia</option>
        <option value="211" <?php if ($country == "211") { echo 'selected="selected"';} ?> >Solomon Islands</option>
        <option value="212" <?php if ($country == "212") { echo 'selected="selected"';} ?> >Somalia</option>
        <option value="213" <?php if ($country == "213") { echo 'selected="selected"';} ?> >South Africa</option>
        <option value="214" <?php if ($country == "214") { echo 'selected="selected"';} ?> >South Georgia and the South Sandwich Islands</option>
        <option value="215" <?php if ($country == "215") { echo 'selected="selected"';} ?> >Spain</option>
        <option value="216" <?php if ($country == "216") { echo 'selected="selected"';} ?> >Spratly Islands</option>
        <option value="217" <?php if ($country == "217") { echo 'selected="selected"';} ?> >Sri Lanka</option>
        <option value="218" <?php if ($country == "218") { echo 'selected="selected"';} ?> >Sudan</option>
        <option value="219" <?php if ($country == "219") { echo 'selected="selected"';} ?> >Suriname</option>
        <option value="220" <?php if ($country == "220") { echo 'selected="selected"';} ?> >Svalbard</option>
        <option value="221" <?php if ($country == "221") { echo 'selected="selected"';} ?> >Swaziland</option>
        <option value="222" <?php if ($country == "222") { echo 'selected="selected"';} ?> >Sweden</option>
        <option value="223" <?php if ($country == "223") { echo 'selected="selected"';} ?> >Switzerland</option>
        <option value="224" <?php if ($country == "224") { echo 'selected="selected"';} ?> >Syria</option>
        <option value="225" <?php if ($country == "225") { echo 'selected="selected"';} ?> >Taiwan</option>
        <option value="226" <?php if ($country == "226") { echo 'selected="selected"';} ?> >Tajikistan</option>
        <option value="227" <?php if ($country == "227") { echo 'selected="selected"';} ?> >Tanzania</option>
        <option value="228" <?php if ($country == "228") { echo 'selected="selected"';} ?> >Thailand</option>
        <option value="229" <?php if ($country == "229") { echo 'selected="selected"';} ?> >Timor-Leste</option>
        <option value="230" <?php if ($country == "230") { echo 'selected="selected"';} ?> >Togo</option>
        <option value="231" <?php if ($country == "231") { echo 'selected="selected"';} ?> >Tokelau</option>
        <option value="232" <?php if ($country == "232") { echo 'selected="selected"';} ?> >Tonga</option>
        <option value="233" <?php if ($country == "233") { echo 'selected="selected"';} ?> >Trinidad and Tobago</option>
        <option value="234" <?php if ($country == "234") { echo 'selected="selected"';} ?> >Tromelin Island</option>
        <option value="235" <?php if ($country == "235") { echo 'selected="selected"';} ?> >Tunisia</option>
        <option value="236" <?php if ($country == "236") { echo 'selected="selected"';} ?> >Turkey</option>
        <option value="237" <?php if ($country == "237") { echo 'selected="selected"';} ?> >Turkmenistan</option>
        <option value="238" <?php if ($country == "238") { echo 'selected="selected"';} ?> >Turks and Caicos Islands</option>
        <option value="239" <?php if ($country == "238") { echo 'selected="selected"';} ?> >Tuvalu</option>
        <option value="240" <?php if ($country == "240") { echo 'selected="selected"';} ?> >Uganda</option>
        <option value="241" <?php if ($country == "241") { echo 'selected="selected"';} ?> >Ukraine</option>
        <option value="242" <?php if ($country == "242") { echo 'selected="selected"';} ?> >United Arab Emirates</option>
        <option value="243" <?php if ($country == "243") { echo 'selected="selected"';} ?> >United Kingdom</option>
        <option value="245" <?php if ($country == "245") { echo 'selected="selected"';} ?> >Uruguay</option>
        <option value="246" <?php if ($country == "246") { echo 'selected="selected"';} ?> >Uzbekistan</option>
        <option value="247" <?php if ($country == "247") { echo 'selected="selected"';} ?> >Vanuatu</option>
        <option value="248" <?php if ($country == "248") { echo 'selected="selected"';} ?> >Venezuela</option>
        <option value="249" <?php if ($country == "249") { echo 'selected="selected"';} ?> >Vietnam</option>
        <option value="250" <?php if ($country == "250") { echo 'selected="selected"';} ?> >Virgin Islands</option>
        <option value="251" <?php if ($country == "251") { echo 'selected="selected"';} ?> >Wake Island</option>
        <option value="252" <?php if ($country == "252") { echo 'selected="selected"';} ?> >Wallis and Futuna</option>
        <option value="253" <?php if ($country == "253") { echo 'selected="selected"';} ?> >West Bank</option>
        <option value="254" <?php if ($country == "254") { echo 'selected="selected"';} ?> >Western Sahara</option>
        <option value="255" <?php if ($country == "255") { echo 'selected="selected"';} ?> >Yemen</option>
        <option value="256" <?php if ($country == "256") { echo 'selected="selected"';} ?> >Zambia</option>
        <option value="257" <?php if ($country == "257") { echo 'selected="selected"';} ?> >Zimbabwe</option>
    </select></td>
  </tr>
  <tr>
    <td>Address 1</td>
    <td><input name="txtAddress_1" type="text" class="textbox" id="txtAddress_1" value="<?php echo $address_1; ?>" /></td>
  </tr>
  <tr>
    <td>Address 2 </td>
    <td><input name="txtAddress_2" type="text" class="textbox" id="txtAddress_2" value="<?php echo $address_2; ?>" /></td>
  </tr>
  <tr>
    <td>Address 3</td>
    <td><input name="txtAddress_3" type="text" class="textbox" id="txtAddress_3" value="<?php echo $address_3; ?>" /></td>
  </tr>
  <tr>
    <td>City/Locality</td>
    <td><input name="txtCity" type="text" class="textbox" id="txtCity" value="<?php echo $city; ?>" /></td>
  </tr>
  <tr>
    <td>Prov./Terr./State</td>
    <td><select name="ddlState" id="ddlState">
    	<option value="AE" <?php if ($state == "AE") { echo 'selected="selected"';} ?> >AE</option>
        <option value="AK" <?php if ($state == "AK") { echo 'selected="selected"';} ?> >AK</option>
        <option value="AL" <?php if ($state == "AL") { echo 'selected="selected"';} ?> >AL</option>
        <option value="AP" <?php if ($state == "AP") { echo 'selected="selected"';} ?> >AP</option>
        <option value="AR" <?php if ($state == "AR") { echo 'selected="selected"';} ?> >AR</option>
        <option value="AZ" <?php if ($state == "AZ") { echo 'selected="selected"';} ?> >AZ</option>
        <option value="CA" <?php if ($state == "CA") { echo 'selected="selected"';} ?> >CA</option>
        <option value="CO" <?php if ($state == "CO") { echo 'selected="selected"';} ?> >CO</option>
        <option value="CT" <?php if ($state == "CT") { echo 'selected="selected"';} ?> >CT</option>
        <option value="DC" <?php if ($state == "DC") { echo 'selected="selected"';} ?> >DC</option>
        <option value="DE" <?php if ($state == "DE") { echo 'selected="selected"';} ?> >DE</option>
        <option value="FL" <?php if ($state == "FL") { echo 'selected="selected"';} ?> >FL</option>
        <option value="GA" <?php if ($state == "GA") { echo 'selected="selected"';} ?> >GA</option>
        <option value="HI" <?php if ($state == "HI") { echo 'selected="selected"';} ?> >HI</option>
        <option value="IA" <?php if ($state == "IA") { echo 'selected="selected"';} ?> >IA</option>
        <option value="ID" <?php if ($state == "ID") { echo 'selected="selected"';} ?> >ID</option>
        <option value="IL" <?php if ($state == "IL") { echo 'selected="selected"';} ?> >IL</option>
        <option value="IN" <?php if ($state == "IN") { echo 'selected="selected"';} ?> >IN</option>
        <option value="KS" <?php if ($state == "KS") { echo 'selected="selected"';} ?> >KS</option>
        <option value="KY" <?php if ($state == "KY") { echo 'selected="selected"';} ?> >KY</option>
        <option value="LA" <?php if ($state == "LA") { echo 'selected="selected"';} ?> >LA</option>
        <option value="MA" <?php if ($state == "MA") { echo 'selected="selected"';} ?> >MA</option>
        <option value="MD" <?php if ($state == "MD") { echo 'selected="selected"';} ?> >MD</option>
        <option value="ME" <?php if ($state == "ME") { echo 'selected="selected"';} ?> >ME</option>
        <option value="MI" <?php if ($state == "MI") { echo 'selected="selected"';} ?> >MI</option>
        <option value="MN" <?php if ($state == "MN") { echo 'selected="selected"';} ?> >MN</option>
        <option value="MO" <?php if ($state == "MO") { echo 'selected="selected"';} ?> >MO</option>
        <option value="MS" <?php if ($state == "MS") { echo 'selected="selected"';} ?> >MS</option>
        <option value="MT" <?php if ($state == "MT") { echo 'selected="selected"';} ?> >MT</option>
        <option value="NC" <?php if ($state == "NC") { echo 'selected="selected"';} ?> >NC</option>
        <option value="ND" <?php if ($state == "ND") { echo 'selected="selected"';} ?> >ND</option>
        <option value="NE" <?php if ($state == "NE") { echo 'selected="selected"';} ?> >NE</option>
        <option value="NH" <?php if ($state == "NH") { echo 'selected="selected"';} ?> >NH</option>
        <option value="NJ" <?php if ($state == "NJ") { echo 'selected="selected"';} ?> >NJ</option>
        <option value="NM" <?php if ($state == "NM") { echo 'selected="selected"';} ?> >NM</option>
        <option value="NV" <?php if ($state == "NV") { echo 'selected="selected"';} ?> >NV</option>
        <option value="NY" <?php if ($state == "NY") { echo 'selected="selected"';} ?> >NY</option>
        <option value="OH" <?php if ($state == "OH") { echo 'selected="selected"';} ?> >OH</option>
        <option value="OK" <?php if ($state == "OK") { echo 'selected="selected"';} ?> >OK</option>
        <option value="OR" <?php if ($state == "OR") { echo 'selected="selected"';} ?> >OR</option>
        <option value="PA" <?php if ($state == "PA") { echo 'selected="selected"';} ?> >PA</option>
        <option value="RI" <?php if ($state == "RI") { echo 'selected="selected"';} ?> >RI</option>
        <option value="SC" <?php if ($state == "SC") { echo 'selected="selected"';} ?> >SC</option>
        <option value="SD" <?php if ($state == "SD") { echo 'selected="selected"';} ?> >SD</option>
        <option value="TN" <?php if ($state == "TN") { echo 'selected="selected"';} ?> >TN</option>
        <option value="TX" <?php if ($state == "TX") { echo 'selected="selected"';} ?> >TX</option>
        <option value="UT" <?php if ($state == "UT") { echo 'selected="selected"';} ?> >UT</option>
        <option value="VA" <?php if ($state == "VA") { echo 'selected="selected"';} ?> >VA</option>
        <option value="VT" <?php if ($state == "VT") { echo 'selected="selected"';} ?> >VT</option>
        <option value="WA" <?php if ($state == "WA") { echo 'selected="selected"';} ?> >WA</option>
        <option value="WI" <?php if ($state == "WI") { echo 'selected="selected"';} ?> >WI</option>
        <option value="WV" <?php if ($state == "WV") { echo 'selected="selected"';} ?> >WV</option>
        <option value="WY" <?php if ($state == "WY") { echo 'selected="selected"';} ?> >WY</option>
    </select>    </td>
  </tr>
  <tr>
    <td>Postal code</td>
    <td><input name="txtPostalCode" type="text" class="textbox" id="txtPostalCode" value="<?php echo $postal_code; ?>" /></td>
  </tr>
  <tr>
    <td colspan="2"><h2>Alternate Address</h2></td>
    </tr>
  <tr>
    <td>Country</td>
    <td><select name="ddlAltCountries" id="ddlAltCountries">
        <option value="1" <?php if ($alt_country == "1") { echo 'selected="selected"';} ?> >Afghanistan</option>
        <option value="2" <?php if ($alt_country == "2") { echo 'selected="selected"';} ?> >Akrotiri</option>
        <option value="3" <?php if ($alt_country == "3") { echo 'selected="selected"';} ?> >Albania</option>
        <option value="4" <?php if ($alt_country == "4") { echo 'selected="selected"';} ?> >Algeria</option>
        <option value="5" <?php if ($alt_country == "5") { echo 'selected="selected"';} ?> >American Samoa</option>
        <option value="6" <?php if ($alt_country == "6") { echo 'selected="selected"';} ?> >Andorra</option>
        <option value="7" <?php if ($alt_country == "7") { echo 'selected="selected"';} ?> >Angola</option>
        <option value="8" <?php if ($alt_country == "8") { echo 'selected="selected"';} ?> >Anguilla</option>
        <option value="9" <?php if ($alt_country == "9") { echo 'selected="selected"';} ?> >Antarctica</option>
        <option value="10" <?php if ($alt_country == "10") { echo 'selected="selected"';} ?> >Antigua and Barbuda</option>
        <option value="11" <?php if ($alt_country == "11") { echo 'selected="selected"';} ?> >Argentina</option>
        <option value="12" <?php if ($alt_country == "12") { echo 'selected="selected"';} ?> >Armenia</option>
        <option value="13" <?php if ($alt_country == "13") { echo 'selected="selected"';} ?> >Aruba</option>
        <option value="14" <?php if ($alt_country == "14") { echo 'selected="selected"';} ?> >Ashmore and Cartier Islands</option>
        <option value="15" <?php if ($alt_country == "15") { echo 'selected="selected"';} ?> >Australia</option>
        <option value="16" <?php if ($alt_country == "16") { echo 'selected="selected"';} ?> >Austria</option>
        <option value="17" <?php if ($alt_country == "17") { echo 'selected="selected"';} ?> >Azerbaijan</option>
        <option value="18" <?php if ($alt_country == "18") { echo 'selected="selected"';} ?> >Bahamas, The</option>
        <option value="19" <?php if ($alt_country == "19") { echo 'selected="selected"';} ?> >Bahrain</option>
        <option value="20" <?php if ($alt_country == "20") { echo 'selected="selected"';} ?> >Bangladesh</option>
        <option value="21" <?php if ($alt_country == "21") { echo 'selected="selected"';} ?> >Barbados</option>
        <option value="22" <?php if ($alt_country == "22") { echo 'selected="selected"';} ?> >Bassas da India</option>
        <option value="23" <?php if ($alt_country == "23") { echo 'selected="selected"';} ?> >Belarus</option>
        <option value="24" <?php if ($alt_country == "24") { echo 'selected="selected"';} ?> >Belgium</option>
        <option value="25" <?php if ($alt_country == "25") { echo 'selected="selected"';} ?> >Belize</option>
        <option value="26" <?php if ($alt_country == "26") { echo 'selected="selected"';} ?> >Benin</option>
        <option value="27" <?php if ($alt_country == "27") { echo 'selected="selected"';} ?> >Bermuda</option>
        <option value="28" <?php if ($alt_country == "28") { echo 'selected="selected"';} ?> >Bhutan</option>
        <option value="29" <?php if ($alt_country == "29") { echo 'selected="selected"';} ?> >Bolivia</option>
        <option value="30" <?php if ($alt_country == "30") { echo 'selected="selected"';} ?> >Bosnia and Herzegovina</option>
        <option value="31" <?php if ($alt_country == "31") { echo 'selected="selected"';} ?> >Botswana</option>
        <option value="32" <?php if ($alt_country == "32") { echo 'selected="selected"';} ?> >Bouvet Island</option>
        <option value="33" <?php if ($alt_country == "33") { echo 'selected="selected"';} ?> >Brazil</option>
        <option value="34" <?php if ($alt_country == "34") { echo 'selected="selected"';} ?> >British Indian Ocean Territory</option>
        <option value="35" <?php if ($alt_country == "35") { echo 'selected="selected"';} ?> >British Virgin Islands</option>
        <option value="36" <?php if ($alt_country == "36") { echo 'selected="selected"';} ?> >Brunei</option>
        <option value="37" <?php if ($alt_country == "37") { echo 'selected="selected"';} ?> >Bulgaria</option>
        <option value="38" <?php if ($alt_country == "38") { echo 'selected="selected"';} ?> >Burkina Faso</option>
        <option value="39" <?php if ($alt_country == "39") { echo 'selected="selected"';} ?> >Burma</option>
        <option value="40" <?php if ($alt_country == "40") { echo 'selected="selected"';} ?> >Burundi</option>
        <option value="41" <?php if ($alt_country == "41") { echo 'selected="selected"';} ?> >Cambodia</option>
        <option value="42" <?php if ($alt_country == "42") { echo 'selected="selected"';} ?> >Cameroon</option>
        <option value="43" <?php if ($alt_country == "43") { echo 'selected="selected"';} ?> >Canada</option>
        <option value="44" <?php if ($alt_country == "44") { echo 'selected="selected"';} ?> >Cape Verde</option>
        <option value="45" <?php if ($alt_country == "45") { echo 'selected="selected"';} ?> >Cayman Islands</option>
        <option value="46" <?php if ($alt_country == "46") { echo 'selected="selected"';} ?> >Central African Republic</option>
        <option value="47" <?php if ($alt_country == "47") { echo 'selected="selected"';} ?> >Chad</option>
        <option value="48" <?php if ($alt_country == "48") { echo 'selected="selected"';} ?> >Chile</option>
        <option value="49" <?php if ($alt_country == "49") { echo 'selected="selected"';} ?> >China</option>
        <option value="50" <?php if ($alt_country == "50") { echo 'selected="selected"';} ?> >Christmas Island</option>
        <option value="51" <?php if ($alt_country == "51") { echo 'selected="selected"';} ?> >Clipperton Island</option>
        <option value="52" <?php if ($alt_country == "52") { echo 'selected="selected"';} ?> >Cocos (Keeling) Islands</option>
        <option value="53" <?php if ($alt_country == "53") { echo 'selected="selected"';} ?> >Colombia</option>
        <option value="54" <?php if ($alt_country == "54") { echo 'selected="selected"';} ?> >Comoros</option>
        <option value="55" <?php if ($alt_country == "55") { echo 'selected="selected"';} ?> >Congo, Democratic Republic of the</option>
        <option value="56" <?php if ($alt_country == "56") { echo 'selected="selected"';} ?> >Congo, Republic of the</option>
        <option value="57" <?php if ($alt_country == "57") { echo 'selected="selected"';} ?> >Cook Islands</option>
        <option value="58" <?php if ($alt_country == "58") { echo 'selected="selected"';} ?> >Coral Sea Islands</option>
        <option value="59" <?php if ($alt_country == "59") { echo 'selected="selected"';} ?> >Costa Rica</option>
        <option value="60" <?php if ($alt_country == "60") { echo 'selected="selected"';} ?> >Cote d&#39;Ivoire</option>
        <option value="61" <?php if ($alt_country == "61") { echo 'selected="selected"';} ?> >Croatia</option>
        <option value="62" <?php if ($alt_country == "62") { echo 'selected="selected"';} ?> >Cuba</option>
        <option value="63" <?php if ($alt_country == "63") { echo 'selected="selected"';} ?> >Cyprus</option>
        <option value="64" <?php if ($alt_country == "64") { echo 'selected="selected"';} ?> >Czech Republic</option>
        <option value="66" <?php if ($alt_country == "66") { echo 'selected="selected"';} ?> >Dhekelia</option>
        <option value="67" <?php if ($alt_country == "67") { echo 'selected="selected"';} ?> >Djibouti</option>
        <option value="68" <?php if ($alt_country == "68") { echo 'selected="selected"';} ?> >Dominica</option>
        <option value="69" <?php if ($alt_country == "69") { echo 'selected="selected"';} ?> >Dominican Republic</option>
        <option value="70" <?php if ($alt_country == "70") { echo 'selected="selected"';} ?> >Ecuador</option>
        <option value="71" <?php if ($alt_country == "71") { echo 'selected="selected"';} ?> >Egypt</option>
        <option value="72" <?php if ($alt_country == "72") { echo 'selected="selected"';} ?> >El Salvador</option>
        <option value="73" <?php if ($alt_country == "73") { echo 'selected="selected"';} ?> >Equatorial Guinea</option>
        <option value="74" <?php if ($alt_country == "74") { echo 'selected="selected"';} ?> >Eritrea</option>
        <option value="75" <?php if ($alt_country == "75") { echo 'selected="selected"';} ?> >Estonia</option>
        <option value="76" <?php if ($alt_country == "76") { echo 'selected="selected"';} ?> >Ethiopia</option>
        <option value="77" <?php if ($alt_country == "77") { echo 'selected="selected"';} ?> >Europa Island</option>
        <option value="78" <?php if ($alt_country == "78") { echo 'selected="selected"';} ?> >Falkland Islands (Islas Malvinas)</option>
        <option value="79" <?php if ($alt_country == "79") { echo 'selected="selected"';} ?> >Faroe Islands</option>
        <option value="80" <?php if ($alt_country == "80") { echo 'selected="selected"';} ?> >Fiji</option>
        <option value="81" <?php if ($alt_country == "81") { echo 'selected="selected"';} ?> >Finland</option>
        <option value="82" <?php if ($alt_country == "82") { echo 'selected="selected"';} ?> >France</option>
        <option value="83" <?php if ($alt_country == "83") { echo 'selected="selected"';} ?> >French Guiana</option>
        <option value="84" <?php if ($alt_country == "84") { echo 'selected="selected"';} ?> >French Polynesia</option>
        <option value="85" <?php if ($alt_country == "85") { echo 'selected="selected"';} ?> >French Southern and Antarctic Lands</option>
        <option value="86" <?php if ($alt_country == "86") { echo 'selected="selected"';} ?> >Gabon</option>
        <option value="87" <?php if ($alt_country == "87") { echo 'selected="selected"';} ?> >Gambia, The</option>
        <option value="88" <?php if ($alt_country == "88") { echo 'selected="selected"';} ?> >Gaza Strip</option>
        <option value="89" <?php if ($alt_country == "89") { echo 'selected="selected"';} ?> >Georgia</option>
        <option value="90" <?php if ($alt_country == "90") { echo 'selected="selected"';} ?> >Germany</option>
        <option value="91" <?php if ($alt_country == "91") { echo 'selected="selected"';} ?> >Ghana</option>
        <option value="92" <?php if ($alt_country == "92") { echo 'selected="selected"';} ?> >Gibraltar</option>
        <option value="93" <?php if ($alt_country == "93") { echo 'selected="selected"';} ?> >Glorioso Islands</option>
        <option value="94" <?php if ($alt_country == "94") { echo 'selected="selected"';} ?> >Greece</option>
        <option value="95" <?php if ($alt_country == "95") { echo 'selected="selected"';} ?> >Greenland</option>
        <option value="96" <?php if ($alt_country == "96") { echo 'selected="selected"';} ?> >Grenada</option>
        <option value="97" <?php if ($alt_country == "97") { echo 'selected="selected"';} ?> >Guadeloupe</option>
        <option value="98" <?php if ($alt_country == "98") { echo 'selected="selected"';} ?> >Guam</option>
        <option value="99" <?php if ($alt_country == "99") { echo 'selected="selected"';} ?> >Guatemala</option>
        <option value="100" <?php if ($alt_country == "100") { echo 'selected="selected"';} ?> >Guernsey</option>
        <option value="101" <?php if ($alt_country == "101") { echo 'selected="selected"';} ?> >Guinea</option>
        <option value="102" <?php if ($alt_country == "102") { echo 'selected="selected"';} ?> >Guinea-Bissau</option>
        <option value="103" <?php if ($alt_country == "103") { echo 'selected="selected"';} ?> >Guyana</option>
        <option value="104" <?php if ($alt_country == "104") { echo 'selected="selected"';} ?> >Haiti</option>
        <option value="105" <?php if ($alt_country == "105") { echo 'selected="selected"';} ?> >Heard Island and McDonald Islands</option>
        <option value="106" <?php if ($alt_country == "106") { echo 'selected="selected"';} ?> >Holy See (Vatican City)</option>
        <option value="107" <?php if ($alt_country == "107") { echo 'selected="selected"';} ?> >Honduras</option>
        <option value="108" <?php if ($alt_country == "108") { echo 'selected="selected"';} ?> >Hong Kong</option>
        <option value="109" <?php if ($alt_country == "109") { echo 'selected="selected"';} ?> >Hungary</option>
        <option value="110" <?php if ($alt_country == "110") { echo 'selected="selected"';} ?> >Iceland</option>
        <option value="111" <?php if ($alt_country == "111") { echo 'selected="selected"';} ?> >India</option>
        <option value="112" <?php if ($alt_country == "112") { echo 'selected="selected"';} ?> >Indonesia</option>
        <option value="113" <?php if ($alt_country == "113") { echo 'selected="selected"';} ?> >Iran</option>
        <option value="114" <?php if ($alt_country == "114") { echo 'selected="selected"';} ?> >Iraq</option>
        <option value="115" <?php if ($alt_country == "115") { echo 'selected="selected"';} ?> >Ireland</option>
        
        <option value="116" <?php if ($alt_country == "116") { echo 'selected="selected"';} ?> >Isle of Man</option>
        <option value="117" <?php if ($alt_country == "117") { echo 'selected="selected"';} ?> >Israel</option>
        <option value="118" <?php if ($alt_country == "118") { echo 'selected="selected"';} ?> >Italy</option>
        <option value="119" <?php if ($alt_country == "119") { echo 'selected="selected"';} ?> >Jamaica</option>
        <option value="120" <?php if ($alt_country == "120") { echo 'selected="selected"';} ?> >Jan Mayen</option>
        <option value="121" <?php if ($alt_country == "121") { echo 'selected="selected"';} ?> >Japan</option>
        <option value="122" <?php if ($alt_country == "122") { echo 'selected="selected"';} ?> >Jersey</option>
        <option value="123" <?php if ($alt_country == "123") { echo 'selected="selected"';} ?> >Jordan</option>
        <option value="124" <?php if ($alt_country == "124") { echo 'selected="selected"';} ?> >Juan de Nova Island</option>
        <option value="125" <?php if ($alt_country == "125") { echo 'selected="selected"';} ?> >Kazakhstan</option>
        <option value="126" <?php if ($alt_country == "126") { echo 'selected="selected"';} ?> >Kenya</option>
        <option value="127" <?php if ($alt_country == "127") { echo 'selected="selected"';} ?> >Kiribati</option>
        <option value="128" <?php if ($alt_country == "128") { echo 'selected="selected"';} ?> >Korea, North</option>
        <option value="129" <?php if ($alt_country == "129") { echo 'selected="selected"';} ?> >Korea, South</option>
        <option value="130" <?php if ($alt_country == "130") { echo 'selected="selected"';} ?> >Kuwait</option>
        <option value="131" <?php if ($alt_country == "131") { echo 'selected="selected"';} ?> >Kyrgyzstan</option>
        <option value="132" <?php if ($alt_country == "132") { echo 'selected="selected"';} ?> >Laos</option>
        <option value="133" <?php if ($alt_country == "133") { echo 'selected="selected"';} ?> >Latvia</option>
        <option value="134" <?php if ($alt_country == "134") { echo 'selected="selected"';} ?> >Lebanon</option>
        <option value="135" <?php if ($alt_country == "135") { echo 'selected="selected"';} ?> >Lesotho</option>
        <option value="136" <?php if ($alt_country == "136") { echo 'selected="selected"';} ?> >Liberia</option>
        <option value="137" <?php if ($alt_country == "137") { echo 'selected="selected"';} ?> >Libya</option>
        <option value="138" <?php if ($alt_country == "138") { echo 'selected="selected"';} ?> >Liechtenstein</option>
        <option value="139" <?php if ($alt_country == "139") { echo 'selected="selected"';} ?> >Lithuania</option>
        <option value="140" <?php if ($alt_country == "140") { echo 'selected="selected"';} ?> >Luxembourg</option>
        <option value="141" <?php if ($alt_country == "141") { echo 'selected="selected"';} ?> >Macau</option>
        <option value="142" <?php if ($alt_country == "142") { echo 'selected="selected"';} ?> >Macedonia</option>
        <option value="143" <?php if ($alt_country == "143") { echo 'selected="selected"';} ?> >Madagascar</option>
        <option value="144" <?php if ($alt_country == "144") { echo 'selected="selected"';} ?> >Malawi</option>
        <option value="145" <?php if ($alt_country == "145") { echo 'selected="selected"';} ?> >Malaysia</option>
        <option value="146" <?php if ($alt_country == "146") { echo 'selected="selected"';} ?> >Maldives</option>
        <option value="147" <?php if ($alt_country == "147") { echo 'selected="selected"';} ?> >Mali</option>
        <option value="148" <?php if ($alt_country == "148") { echo 'selected="selected"';} ?> >Malta</option>
        <option value="149" <?php if ($alt_country == "149") { echo 'selected="selected"';} ?> >Marshall Islands</option>
        <option value="150" <?php if ($alt_country == "150") { echo 'selected="selected"';} ?> >Martinique</option>
        <option value="151" <?php if ($alt_country == "151") { echo 'selected="selected"';} ?> >Mauritania</option>
        <option value="152" <?php if ($alt_country == "152") { echo 'selected="selected"';} ?> >Mauritius</option>
        <option value="153" <?php if ($alt_country == "153") { echo 'selected="selected"';} ?> >Mayotte</option>
        <option value="154" <?php if ($alt_country == "154") { echo 'selected="selected"';} ?> >Mexico</option>
        <option value="155" <?php if ($alt_country == "155") { echo 'selected="selected"';} ?> >Micronesia, Federated States of</option>
        <option value="156" <?php if ($alt_country == "156") { echo 'selected="selected"';} ?> >Moldova</option>
        <option value="157" <?php if ($alt_country == "157") { echo 'selected="selected"';} ?> >Monaco</option>
        <option value="158" <?php if ($alt_country == "158") { echo 'selected="selected"';} ?> >Mongolia</option>
        <option value="159" <?php if ($alt_country == "159") { echo 'selected="selected"';} ?> >Montserrat</option>
        <option value="160" <?php if ($alt_country == "160") { echo 'selected="selected"';} ?> >Morocco</option>
        <option value="161" <?php if ($alt_country == "161") { echo 'selected="selected"';} ?> >Mozambique</option>
        <option value="162" <?php if ($alt_country == "162") { echo 'selected="selected"';} ?> >Namibia</option>
        <option value="163" <?php if ($alt_country == "163") { echo 'selected="selected"';} ?> >Nauru</option>
        <option value="164" <?php if ($alt_country == "164") { echo 'selected="selected"';} ?> >Navassa Island</option>
        <option value="165" <?php if ($alt_country == "165") { echo 'selected="selected"';} ?> >Nepal</option>
        <option value="166" <?php if ($alt_country == "166") { echo 'selected="selected"';} ?> >Netherlands</option>
        <option value="167" <?php if ($alt_country == "167") { echo 'selected="selected"';} ?> >Netherlands Antilles</option>
        <option value="168" <?php if ($alt_country == "168") { echo 'selected="selected"';} ?> >New Caledonia</option>
        <option value="169" <?php if ($alt_country == "169") { echo 'selected="selected"';} ?> >New Zealand</option>
        <option value="170" <?php if ($alt_country == "170") { echo 'selected="selected"';} ?> >Nicaragua</option>
        <option value="171" <?php if ($alt_country == "171") { echo 'selected="selected"';} ?> >Niger</option>
        <option value="172" <?php if ($alt_country == "172") { echo 'selected="selected"';} ?> >Nigeria</option>
        <option value="173" <?php if ($alt_country == "173") { echo 'selected="selected"';} ?> >Niue</option>
        <option value="174" <?php if ($alt_country == "174") { echo 'selected="selected"';} ?> >Norfolk Island</option>
        <option value="175" <?php if ($alt_country == "175") { echo 'selected="selected"';} ?> >Northern Mariana Islands</option>
        <option value="176" <?php if ($alt_country == "176") { echo 'selected="selected"';} ?> >Norway</option>
        <option value="177" <?php if ($alt_country == "177") { echo 'selected="selected"';} ?> >Oman</option>
        <option value="178" <?php if ($alt_country == "178") { echo 'selected="selected"';} ?> >Pakistan</option>
        <option value="179" <?php if ($alt_country == "179") { echo 'selected="selected"';} ?> >Palau</option>
        <option value="180" <?php if ($alt_country == "180") { echo 'selected="selected"';} ?> >Panama</option>
        <option value="181" <?php if ($alt_country == "181") { echo 'selected="selected"';} ?> >Papua New Guinea</option>
        <option value="182" <?php if ($alt_country == "182") { echo 'selected="selected"';} ?> >Paracel Islands</option>
        <option value="183" <?php if ($alt_country == "183") { echo 'selected="selected"';} ?> >Paraguay</option>
        <option value="184" <?php if ($alt_country == "184") { echo 'selected="selected"';} ?> >Peru</option>
        <option value="185" <?php if ($alt_country == "185") { echo 'selected="selected"';} ?> >Philippines</option>
        <option value="186" <?php if ($alt_country == "186") { echo 'selected="selected"';} ?> >Pitcairn Islands</option>
        <option value="187" <?php if ($alt_country == "187") { echo 'selected="selected"';} ?> >Poland</option>
        <option value="188" <?php if ($alt_country == "188") { echo 'selected="selected"';} ?> >Portugal</option>
        <option value="189" <?php if ($alt_country == "189") { echo 'selected="selected"';} ?> >Puerto Rico</option>
        <option value="190" <?php if ($alt_country == "190") { echo 'selected="selected"';} ?> >Qatar</option>
        <option value="191" <?php if ($alt_country == "191") { echo 'selected="selected"';} ?> >Reunion</option>
        <option value="192" <?php if ($alt_country == "192") { echo 'selected="selected"';} ?> >Romania</option>
        <option value="193" <?php if ($alt_country == "193") { echo 'selected="selected"';} ?> >Russia</option>
        <option value="194" <?php if ($alt_country == "194") { echo 'selected="selected"';} ?> >Rwanda</option>
        <option value="195" <?php if ($alt_country == "195") { echo 'selected="selected"';} ?> >Saint Helena</option>
        <option value="196" <?php if ($alt_country == "196") { echo 'selected="selected"';} ?> >Saint Kitts and Nevis</option>
        <option value="197" <?php if ($alt_country == "197") { echo 'selected="selected"';} ?> >Saint Lucia</option>
        <option value="198" <?php if ($alt_country == "198") { echo 'selected="selected"';} ?> >Saint Pierre and Miquelon</option>
        <option value="199" <?php if ($alt_country == "199") { echo 'selected="selected"';} ?> >Saint Vincent and the Grenadines</option>
        <option value="200" <?php if ($alt_country == "200") { echo 'selected="selected"';} ?> >Samoa</option>
        <option value="201" <?php if ($alt_country == "201") { echo 'selected="selected"';} ?> >San Marino</option>
        <option value="202" <?php if ($alt_country == "202") { echo 'selected="selected"';} ?> >Sao Tome and Principe</option>
        <option value="203" <?php if ($alt_country == "203") { echo 'selected="selected"';} ?> >Saudi Arabia</option>
        <option value="204" <?php if ($alt_country == "204") { echo 'selected="selected"';} ?> >Senegal</option>
        <option value="205" <?php if ($alt_country == "205") { echo 'selected="selected"';} ?> >Serbia and Montenegro</option>
        <option value="206" <?php if ($alt_country == "206") { echo 'selected="selected"';} ?> >Seychelles</option>
        <option value="207" <?php if ($alt_country == "207") { echo 'selected="selected"';} ?> >Sierra Leone</option>
        <option value="208" <?php if ($alt_country == "208") { echo 'selected="selected"';} ?> >Singapore</option>
        <option value="209" <?php if ($alt_country == "209") { echo 'selected="selected"';} ?> >Slovakia</option>
        <option value="210" <?php if ($alt_country == "210") { echo 'selected="selected"';} ?> >Slovenia</option>
        <option value="211" <?php if ($alt_country == "211") { echo 'selected="selected"';} ?> >Solomon Islands</option>
        <option value="212" <?php if ($alt_country == "212") { echo 'selected="selected"';} ?> >Somalia</option>
        <option value="213" <?php if ($alt_country == "213") { echo 'selected="selected"';} ?> >South Africa</option>
        <option value="214" <?php if ($alt_country == "214") { echo 'selected="selected"';} ?> >South Georgia and the South Sandwich Islands</option>
        <option value="215" <?php if ($alt_country == "215") { echo 'selected="selected"';} ?> >Spain</option>
        <option value="216" <?php if ($alt_country == "216") { echo 'selected="selected"';} ?> >Spratly Islands</option>
        <option value="217" <?php if ($alt_country == "217") { echo 'selected="selected"';} ?> >Sri Lanka</option>
        <option value="218" <?php if ($alt_country == "218") { echo 'selected="selected"';} ?> >Sudan</option>
        <option value="219" <?php if ($alt_country == "219") { echo 'selected="selected"';} ?> >Suriname</option>
        <option value="220" <?php if ($alt_country == "220") { echo 'selected="selected"';} ?> >Svalbard</option>
        <option value="221" <?php if ($alt_country == "221") { echo 'selected="selected"';} ?> >Swaziland</option>
        <option value="222" <?php if ($alt_country == "222") { echo 'selected="selected"';} ?> >Sweden</option>
        <option value="223" <?php if ($alt_country == "223") { echo 'selected="selected"';} ?> >Switzerland</option>
        <option value="224" <?php if ($alt_country == "224") { echo 'selected="selected"';} ?> >Syria</option>
        <option value="225" <?php if ($alt_country == "225") { echo 'selected="selected"';} ?> >Taiwan</option>
        <option value="226" <?php if ($alt_country == "226") { echo 'selected="selected"';} ?> >Tajikistan</option>
        <option value="227" <?php if ($alt_country == "227") { echo 'selected="selected"';} ?> >Tanzania</option>
        <option value="228" <?php if ($alt_country == "228") { echo 'selected="selected"';} ?> >Thailand</option>
        <option value="229" <?php if ($alt_country == "229") { echo 'selected="selected"';} ?> >Timor-Leste</option>
        <option value="230" <?php if ($alt_country == "230") { echo 'selected="selected"';} ?> >Togo</option>
        <option value="231" <?php if ($alt_country == "231") { echo 'selected="selected"';} ?> >Tokelau</option>
        <option value="232" <?php if ($alt_country == "232") { echo 'selected="selected"';} ?> >Tonga</option>
        <option value="233" <?php if ($alt_country == "233") { echo 'selected="selected"';} ?> >Trinidad and Tobago</option>
        <option value="234" <?php if ($alt_country == "234") { echo 'selected="selected"';} ?> >Tromelin Island</option>
        <option value="235" <?php if ($alt_country == "235") { echo 'selected="selected"';} ?> >Tunisia</option>
        <option value="236" <?php if ($alt_country == "236") { echo 'selected="selected"';} ?> >Turkey</option>
        <option value="237" <?php if ($alt_country == "237") { echo 'selected="selected"';} ?> >Turkmenistan</option>
        <option value="238" <?php if ($alt_country == "238") { echo 'selected="selected"';} ?> >Turks and Caicos Islands</option>
        <option value="239" <?php if ($alt_country == "238") { echo 'selected="selected"';} ?> >Tuvalu</option>
        <option value="240" <?php if ($alt_country == "240") { echo 'selected="selected"';} ?> >Uganda</option>
        <option value="241" <?php if ($alt_country == "241") { echo 'selected="selected"';} ?> >Ukraine</option>
        <option value="242" <?php if ($alt_country == "242") { echo 'selected="selected"';} ?> >United Arab Emirates</option>
        <option value="243" <?php if ($alt_country == "243") { echo 'selected="selected"';} ?> >United Kingdom</option>
        <option value="245" <?php if ($alt_country == "245") { echo 'selected="selected"';} ?> >Uruguay</option>
        <option value="246" <?php if ($alt_country == "246") { echo 'selected="selected"';} ?> >Uzbekistan</option>
        <option value="247" <?php if ($alt_country == "247") { echo 'selected="selected"';} ?> >Vanuatu</option>
        <option value="248" <?php if ($alt_country == "248") { echo 'selected="selected"';} ?> >Venezuela</option>
        <option value="249" <?php if ($alt_country == "249") { echo 'selected="selected"';} ?> >Vietnam</option>
        <option value="250" <?php if ($alt_country == "250") { echo 'selected="selected"';} ?> >Virgin Islands</option>
        <option value="251" <?php if ($alt_country == "251") { echo 'selected="selected"';} ?> >Wake Island</option>
        <option value="252" <?php if ($alt_country == "252") { echo 'selected="selected"';} ?> >Wallis and Futuna</option>
        <option value="253" <?php if ($alt_country == "253") { echo 'selected="selected"';} ?> >West Bank</option>
        <option value="254" <?php if ($alt_country == "254") { echo 'selected="selected"';} ?> >Western Sahara</option>
        <option value="255" <?php if ($alt_country == "255") { echo 'selected="selected"';} ?> >Yemen</option>
        <option value="256" <?php if ($alt_country == "256") { echo 'selected="selected"';} ?> >Zambia</option>
        <option value="257" <?php if ($alt_country == "257") { echo 'selected="selected"';} ?> >Zimbabwe</option>
    </select></td>
  </tr>
  <tr>
    <td>Address 1</td>
    <td><input name="txtAltAddress_1" type="text" class="textbox" id="txtAltAddress_1" value="<?php echo $alt_address_1; ?>" /></td>
  </tr>
  <tr>
    <td>Address 2 </td>
    <td><input name="txtAltAddress_2" type="text" class="textbox" id="txtAltAddress_2" value="<?php echo $alt_address_2; ?>" /></td>
  </tr>
  <tr>
    <td>Address 3</td>
    <td><input name="txtAltAddress_3" type="text" class="textbox" id="txtAltAddress_3" value="<?php echo $alt_address_3; ?>" /></td>
  </tr>
  <tr>
    <td>City</td>
    <td><input name="txtAltCity" type="text" class="textbox" id="txtAltCity" value="<?php echo $alt_city; ?>" /></td>
  </tr>
  <tr>
    <td>State</td>
    <td><select name="ddlAltState" id="ddlAltState">
        <option value="AE" <?php if ($alt_state == "AE") { echo 'selected="selected"';} ?> >AE</option>
        <option value="AK" <?php if ($alt_state == "AK") { echo 'selected="selected"';} ?> >AK</option>
        <option value="AL" <?php if ($alt_state == "AL") { echo 'selected="selected"';} ?> >AL</option>
        <option value="AP" <?php if ($alt_state == "AP") { echo 'selected="selected"';} ?> >AP</option>
        <option value="AR" <?php if ($alt_state == "AR") { echo 'selected="selected"';} ?> >AR</option>
        <option value="AZ" <?php if ($alt_state == "AZ") { echo 'selected="selected"';} ?> >AZ</option>
        <option value="CA" <?php if ($alt_state == "CA") { echo 'selected="selected"';} ?> >CA</option>
        <option value="CO" <?php if ($alt_state == "CO") { echo 'selected="selected"';} ?> >CO</option>
        <option value="CT" <?php if ($alt_state == "CT") { echo 'selected="selected"';} ?> >CT</option>
        <option value="DC" <?php if ($alt_state == "DC") { echo 'selected="selected"';} ?> >DC</option>
        <option value="DE" <?php if ($alt_state == "DE") { echo 'selected="selected"';} ?> >DE</option>
        <option value="FL" <?php if ($alt_state == "FL") { echo 'selected="selected"';} ?> >FL</option>
        <option value="GA" <?php if ($alt_state == "GA") { echo 'selected="selected"';} ?> >GA</option>
        <option value="HI" <?php if ($alt_state == "HI") { echo 'selected="selected"';} ?> >HI</option>
        <option value="IA" <?php if ($alt_state == "IA") { echo 'selected="selected"';} ?> >IA</option>
        <option value="ID" <?php if ($alt_state == "ID") { echo 'selected="selected"';} ?> >ID</option>
        <option value="IL" <?php if ($alt_state == "IL") { echo 'selected="selected"';} ?> >IL</option>
        <option value="IN" <?php if ($alt_state == "IN") { echo 'selected="selected"';} ?> >IN</option>
        <option value="KS" <?php if ($alt_state == "KS") { echo 'selected="selected"';} ?> >KS</option>
        <option value="KY" <?php if ($alt_state == "KY") { echo 'selected="selected"';} ?> >KY</option>
        <option value="LA" <?php if ($alt_state == "LA") { echo 'selected="selected"';} ?> >LA</option>
        <option value="MA" <?php if ($alt_state == "MA") { echo 'selected="selected"';} ?> >MA</option>
        <option value="MD" <?php if ($alt_state == "MD") { echo 'selected="selected"';} ?> >MD</option>
        <option value="ME" <?php if ($alt_state == "ME") { echo 'selected="selected"';} ?> >ME</option>
        <option value="MI" <?php if ($alt_state == "MI") { echo 'selected="selected"';} ?> >MI</option>
        <option value="MN" <?php if ($alt_state == "MN") { echo 'selected="selected"';} ?> >MN</option>
        <option value="MO" <?php if ($alt_state == "MO") { echo 'selected="selected"';} ?> >MO</option>
        <option value="MS" <?php if ($alt_state == "MS") { echo 'selected="selected"';} ?> >MS</option>
        <option value="MT" <?php if ($alt_state == "MT") { echo 'selected="selected"';} ?> >MT</option>
        <option value="NC" <?php if ($alt_state == "NC") { echo 'selected="selected"';} ?> >NC</option>
        <option value="ND" <?php if ($alt_state == "ND") { echo 'selected="selected"';} ?> >ND</option>
        <option value="NE" <?php if ($alt_state == "NE") { echo 'selected="selected"';} ?> >NE</option>
        <option value="NH" <?php if ($alt_state == "NH") { echo 'selected="selected"';} ?> >NH</option>
        <option value="NJ" <?php if ($alt_state == "NJ") { echo 'selected="selected"';} ?> >NJ</option>
        <option value="NM" <?php if ($alt_state == "NM") { echo 'selected="selected"';} ?> >NM</option>
        <option value="NV" <?php if ($alt_state == "NV") { echo 'selected="selected"';} ?> >NV</option>
        <option value="NY" <?php if ($alt_state == "NY") { echo 'selected="selected"';} ?> >NY</option>
        <option value="OH" <?php if ($alt_state == "OH") { echo 'selected="selected"';} ?> >OH</option>
        <option value="OK" <?php if ($alt_state == "OK") { echo 'selected="selected"';} ?> >OK</option>
        <option value="OR" <?php if ($alt_state == "OR") { echo 'selected="selected"';} ?> >OR</option>
        <option value="PA" <?php if ($alt_state == "PA") { echo 'selected="selected"';} ?> >PA</option>
        <option value="RI" <?php if ($alt_state == "RI") { echo 'selected="selected"';} ?> >RI</option>
        <option value="SC" <?php if ($alt_state == "SC") { echo 'selected="selected"';} ?> >SC</option>
        <option value="SD" <?php if ($alt_state == "SD") { echo 'selected="selected"';} ?> >SD</option>
        <option value="TN" <?php if ($alt_state == "TN") { echo 'selected="selected"';} ?> >TN</option>
        <option value="TX" <?php if ($alt_state == "TX") { echo 'selected="selected"';} ?> >TX</option>
        <option value="UT" <?php if ($alt_state == "UT") { echo 'selected="selected"';} ?> >UT</option>
        <option value="VA" <?php if ($alt_state == "VA") { echo 'selected="selected"';} ?> >VA</option>
        <option value="VT" <?php if ($alt_state == "VT") { echo 'selected="selected"';} ?> >VT</option>
        <option value="WA" <?php if ($alt_state == "WA") { echo 'selected="selected"';} ?> >WA</option>
        <option value="WI" <?php if ($alt_state == "WI") { echo 'selected="selected"';} ?> >WI</option>
        <option value="WV" <?php if ($alt_state == "WV") { echo 'selected="selected"';} ?> >WV</option>
        <option value="WY" <?php if ($alt_state == "WY") { echo 'selected="selected"';} ?> >WY</option>
    </select></td>
  </tr>
  <tr>
    <td>Zip code</td>
    <td><input name="txtAltZipcode" type="text" class="textbox" id="txtAltZipcode" value="<?php echo $alt_postal_code; ?>" /></td>
  </tr>
  <tr>
    <td valign="top">Mailing preference</td>
    <td><input name="rdMailingRef" type="radio" id="rd5" value="Send mail to main address" <?php if ($mailing_pref == "Send mail to main address") { echo 'checked="checked"';} ?> />
      Send mail to main address<br />
      <input type="radio" name="rdMailingRef" id="rd6" value="Send mail to alternate address" <?php if ($mailing_pref == "Send mail to alternate address") { echo 'checked="checked"';} ?> />
      Send mail to alternate address</td>
  </tr>
  <tr>
    <td colspan="2"><h2>Additional Information</h2></td>
    </tr>
  <tr>
    <td valign="top">Comments</td>
    <td><textarea name="txtComments" cols="45" rows="5" class="textarea" id="txtComments"><?php echo $additional_comments; ?></textarea></td>
  </tr>
  <tr>
    <td>Date of birth</td>
    <td><script>DateInput('txtBirthDate', true, 'YYYY-MM-DD', '<?php echo $additional_birth_date ?>')</script></td>
  </tr>
  <tr>
    <td colspan="2"> <h2>Emergency Contact</h2></td>
    </tr>
  <tr>
    <td>Name</td>
    <td><input name="txtEmergencyName" type="text" class="textbox" id="txtEmergencyName" value="<?php echo $emergency_name; ?>" /></td>
  </tr>
  <tr>
    <td>Phone</td>
    <td><input name="txtEmergencyPhone" type="text" class="textbox" id="txtEmergencyPhone" value="<?php echo $emergency_phone; ?>" /></td>
  </tr>
  <tr>
    <td colspan="2"><h2>Association Information</h2></td>
    </tr>
  <tr>
    <td>Board member</td>
    <td><input name="chkBoardMember" type="checkbox" id="chkBoardMember" value="1" <?php if ($is_board_member == "1") {echo 'checked="checked"';} ?> />
      Owner is a board member</td>
  </tr>
  <tr>
    <td>Occupancy</td>
    <td><input name="chkOccupancy" type="checkbox" id="chkOccupancy" value="1" <?php if ($is_occupancy == "1") {echo 'checked="checked"';} ?> />
      Owner occupies unit</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><input name="submit" type="submit" class="submit_button" id="submit" value="Save" /></td>
  </tr>
</table>
<p><br />
  <br />
</p>
</form>
</body>
</html>